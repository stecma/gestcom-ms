package com.globalia;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.producer.MessageProducer;
import com.globalia.application.service.credential.Credential;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.dto.DateRange;
import com.globalia.dto.I18n;
import com.globalia.dto.KafkaItem;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.Assignment;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.MatchItem;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.dto.credential.SupplierFilter;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.PercentageType;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.monitoring.Monitor;
import org.mockito.Mock;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
public abstract class HelperTest {

	@Mock
	protected MessageProducer producer;

	public static ApiRQ matchRQ() {
		ApiRQ request = new ApiRQ();
		request.setMatch(match());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ masterRQ() {
		ApiRQ request = new ApiRQ();
		request.setMaster(master());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ tradeRQ() {
		ApiRQ request = new ApiRQ();
		request.setTrade(trade());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ supplierByZoneRQ() {
		ApiRQ request = new ApiRQ();
		request.setSupplierbyzone(supplierByZone());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ releaseRQ() {
		ApiRQ request = new ApiRQ();
		request.setRelease(release());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ customerRQ() {
		ApiRQ request = new ApiRQ();
		request.setCustom(customer());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ supplierRQ() {
		ApiRQ request = new ApiRQ();
		request.setSupplier(supplier());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ credentialRQ() {
		ApiRQ request = new ApiRQ();
		request.setCredential(credential());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ cancelCostRQ() {
		ApiRQ request = new ApiRQ();
		request.setCancelCost(cancelCost());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ currencyRQ() {
		ApiRQ request = new ApiRQ();
		request.setCurrency(currency());
		request.setMonitor(monitor());
		return request;
	}

	public static Map<String, Object> map() {
		Map<String, Object> map = new HashMap<>();
		map.put("1¬1", "json");
		map.put("1¬2", "json");
		return map;
	}

	public static MatchItem match() {
		MatchItem match = new MatchItem();
		match.setSupplierId(supplier().getId());
		match.setGroupId("1");
		match.setCredentialId("1");
		match.setCodes(new HashSet<>());
		match.setSupplier(new SupplierItem());
		match.getSupplier().setFilter(new SupplierFilter());
		match.setAssignment(assignment());
		match.getAssignment().setCheckIn(new DateRange());
		match.getAssignment().getCheckIn().setTo("31/12/2050");

		return match;
	}

	public static Assignment assignment() {
		Assignment assign = new Assignment();
		assign.setBook(new DateRange());
		assign.getBook().setFrom("08/02/2021");
		assign.getBook().setTimeFrom("00:00:00");
		assign.getBook().setTo("08/02/2021");
		assign.getBook().setTimeTo("00:00:00");
		return assign;
	}

	public static GroupCredentialItem group() {
		MasterItem master = new MasterItem();
		// Credenciales externas
		ExternalCredentialItem newExt = new ExternalCredentialItem();
		newExt.setCreate(true);
		newExt.setCorporation(master);
		newExt.setInvoicingType(master);
		newExt.setCodes(new HashSet<>());
		newExt.getCodes().add(pairValue(MasterType.CORPORATION, "1"));
		newExt.getCodes().add(pairValue(MasterType.BILLING_TYPE, "2"));

		ExternalCredentialItem updExt = new ExternalCredentialItem();
		updExt.setCorporation(master);
		updExt.setInvoicingType(master);

		ExternalCredentialItem delExt = new ExternalCredentialItem();
		delExt.setDeleted(true);
		delExt.setCorporation(master);
		delExt.setInvoicingType(master);

		GroupCredentialItem newGroup = new GroupCredentialItem();
		newGroup.setCreate(true);
		newGroup.setRates(Set.of(master));
		newGroup.setCurrencies(Set.of(master));
		newGroup.setDistriModels(Set.of(master));
		newGroup.setCountries(Set.of(master));
		newGroup.setExcludedCountries(Set.of(master));
		newGroup.setClientType(master);
		newGroup.setBrand(master);
		newGroup.setCodes(new HashSet<>());
		newGroup.getCodes().add(pairValue(MasterType.COUNTRY, "1"));
		newGroup.getCodes().add(pairValue(MasterType.RATE_TYPE, "1"));
		newGroup.getCodes().add(pairValue(MasterType.DISTRIBUTION_MODEL, "1"));
		newGroup.getCodes().add(pairValue(MasterType.CURRENCY, "1"));
		newGroup.getCodes().add(pairValue(MasterType.BRAND, "1"));
		newGroup.getCodes().add(pairValue(MasterType.CLIENT_TYPE, "1"));
		newGroup.setExternalConfig(Set.of(newExt, updExt, delExt));
		return newGroup;
	}

	public static SupplierItem supplier() {
		MasterItem master = new MasterItem();
		// Grupos de credenciales
		GroupCredentialItem updGroup = new GroupCredentialItem();
		updGroup.setRates(Set.of(master));
		updGroup.setCurrencies(Set.of(master));
		updGroup.setDistriModels(Set.of(master));
		updGroup.setCountries(Set.of(master));

		GroupCredentialItem delGroup = new GroupCredentialItem();
		delGroup.setDeleted(true);
		delGroup.setRates(Set.of(master));
		delGroup.setCurrencies(Set.of(master));
		delGroup.setDistriModels(Set.of(master));
		delGroup.setCountries(Set.of(master));

		MasterItem filter = new MasterItem();
		filter.setId("1");
		filter.setOwnProduct(true);
		filter.setSapCode("test");

		SupplierItem supplier = new SupplierItem();
		supplier.setId("1");
		supplier.setStatus(SupplierStatus.DISABLED);
		supplier.setCodes(new HashSet<>());
		supplier.getCodes().add(pairValue(MasterType.SERVICE_TYPE, "1"));
		supplier.getCodes().add(pairValue(MasterType.EXTERNAL_SYSTEM, "1"));
		supplier.getCodes().add(pairValue(MasterType.BUY_MODEL, "1"));
		supplier.getCodes().add(pairValue(MasterType.USERS, "1"));
		supplier.getCodes().add(pairValue(MasterType.CLIENT_PREF, "1"));
		supplier.getCodes().add(pairValue(MasterType.AGENCY, "1"));
		supplier.setBuyModel(filter);
		supplier.setClientPref(filter);
		supplier.setUser(filter);
		supplier.setServiceType(filter);
		supplier.setSystem(filter);
		supplier.setClients(Set.of(filter));
		supplier.setTopDestinations(Set.of(filter));
		supplier.setExternalSys(Set.of(group(), updGroup, delGroup));
		return supplier;
	}

	public static CredentialItem credential() {
		PairValue brd = pairValue(MasterType.BRAND, "1");
		PairValue srvc = pairValue(MasterType.SERVICE_TYPE, "1");
		PairValue channelc = pairValue(MasterType.SALES_CHANNEL, "1");
		PairValue modelc = pairValue(MasterType.DISTRIBUTION_MODEL, "1");
		PairValue rqc = pairValue(MasterType.REQUEST, "1");
		PairValue envc = pairValue(MasterType.ENVIRONMENTS, "1");
		PairValue rates = pairValue(MasterType.RATE_TYPE, "1");
		PairValue cous = pairValue(MasterType.COUNTRY, "1");
		PairValue depth = pairValue(MasterType.DEPTH, "1");
		PairValue sales = pairValue(MasterType.SALES_MODEL, "1");
		PairValue lang = pairValue(MasterType.LANGUAGES, "1");
		PairValue platform = pairValue(MasterType.PLATFORM, "1");
		PairValue prd = pairValue(MasterType.PRODUCT_ORIGIN, "1");
		PairValue client = pairValue(MasterType.AGENCY, "1");
		PairValue userc = pairValue(MasterType.USERS, "2");
		PairValue currency = pairValue(MasterType.CURRENCY, "2");

		MasterItem master = master(MasterType.AGENCY, "2");
		master.setCodes(Set.of(depth, modelc));

		CredentialItem credential = new CredentialItem();
		credential.setId("test");
		credential.setAlias("test");
		credential.setDescription("test");
		credential.setEnvironment(master);
		credential.setBrand(master);
		credential.setServiceType(master);
		credential.setSaleChannel(master);
		credential.setDistributionModel(master);
		credential.setMarket(master);
		credential.setDepth(master);
		credential.setSalesModel(master);
		credential.setLanguage(master);
		credential.setRequest(master);
		credential.setUser(master);
		credential.setCurrency(master);
		credential.setPlatforms(new HashSet<>(Set.of(master(MasterType.PLATFORM, "1"))));
		credential.setRates(new HashSet<>(Set.of(master(MasterType.RATE_TYPE, "1"))));
		credential.setProducts(new HashSet<>(Set.of(master(MasterType.PRODUCT_ORIGIN, "1"))));
		credential.setCodes(new HashSet<>(Set.of(currency, brd, srvc, channelc, modelc, rqc, envc, rates, cous, depth, sales, lang, platform, prd, client, userc)));

		return credential;
	}

	private static PairValue pairValue(final MasterType type, final String value) {
		PairValue pair = new PairValue();
		pair.setKey(type.name());
		pair.setValue(new LinkedHashSet<>(Set.of(value)));
		return pair;
	}

	public static CurrencyItem currency() {
		CurrencyItem currency = new CurrencyItem();
		currency.setId("1");
		currency.setCredential("1");
		currency.setMasters(Set.of(master(MasterType.COUNTRY, "1")));
		currency.setCodes(new HashSet<>(Set.of(pairValue(MasterType.COUNTRY, "1"))));
		return currency;
	}

	public static CustomerItem customer() {
		CustomerItem customer = new CustomerItem();
		customer.setId("1");
		customer.setCredential("1");
		customer.setMasters(Set.of(master(MasterType.AGENCY, "1")));
		customer.setCodes(new HashSet<>(Set.of(pairValue(MasterType.AGENCY, "1"))));
		return customer;
	}

	public static ExternalCredentialItem connector() {
		MasterItem master = master(MasterType.BRAND, "1");

		ExternalCredentialItem newExt = new ExternalCredentialItem();
		newExt.setCreate(true);
		newExt.setCorporation(master);
		newExt.setInvoicingType(master);
		newExt.setSupplierId("1");
		newExt.setGroupId("1");
		newExt.setId("1");

		return newExt;
	}

	public static ReleaseItem release() {
		ReleaseItem release = new ReleaseItem();
		release.setId("1");
		release.setCodes(new HashSet<>(Set.of(pairValue(MasterType.BRAND, "1"))));
		release.setMasters(new HashSet<>(Set.of(master(MasterType.BRAND, "1"))));
		release.setCredential("1");
		release.setDays(1);
		release.setOperationDays("X");

		return release;
	}

	public static SupplierByZoneItem supplierByZone() {
		SupplierByZoneItem supplier = new SupplierByZoneItem();
		supplier.setId("1¬1¬1¬1¬1¬1¬1¬1¬1¬");
		supplier.setCodsis("1");
		supplier.setCodpro("1");
		supplier.setRefzge("1");
		supplier.setCodpai("1");
		supplier.setCodepr("1");
		supplier.setCodare("1");
		supplier.setSubprv("1");
		supplier.setCodprvext("1");
		supplier.setCoduser("test");

		return supplier;
	}

	public static CancelCostItem cancelCost() {
		CancelCostItem cancel = new CancelCostItem();
		cancel.setId("1¬1¬1¬1¬1¬1¬1¬1¬1¬");
		cancel.setCredential("1");
		cancel.setAssignment(assignment());

		return cancel;
	}

	public static TradePolicyItem trade() {
		TradePolicyItem trade = new TradePolicyItem();
		trade.setId("1¬1¬1¬1¬1¬1¬1¬1¬1¬");
		trade.setCredential("1");
		trade.setPercentage(1D);
		trade.setPercentageType(PercentageType.MARKUP);
		trade.setCodes(new HashSet<>(Set.of(pairValue(MasterType.SERVICE_TYPE, "1"))));
		trade.setMasters(new HashSet<>(Set.of(master(MasterType.SERVICE_TYPE, "1"))));

		return trade;
	}

	public static MasterItem master() {
		I18n i18n = new I18n();
		i18n.setKey("ES");
		i18n.setValue("test");

		MasterItem master = new MasterItem();
		master.setId("1");
		master.setNames(new LinkedHashSet<>(Set.of(i18n)));
		master.setEntity(MasterType.BRAND.name());
		master.setMasters(Set.of(master(MasterType.EXTERNAL_SYSTEM, "1")));
		master.setCodes(Set.of(pairValue(MasterType.BRAND, "1")));

		return master;
	}

	public static MasterItem master(final MasterType type, final String value) {
		MasterItem child = new MasterItem();
		child.setId(value);
		child.setNames(new LinkedHashSet<>());
		child.setEntity(type.name());
		return child;
	}

	public static Monitor monitor() {
		Monitor monitor = new Monitor();
		monitor.setLogs(new HashSet<>());
		monitor.setStats(new HashSet<>());
		return monitor;
	}

	public static KafkaItem message() {
		KafkaItem kafka = new KafkaItem();
		kafka.setKey("key");
		kafka.setEntity("entity");
		kafka.setAction("test");
		kafka.setJson("json");

		return kafka;
	}

	public static void masterCredential(final Object obj, final String value) {
		addValue(Credential.class, obj, "masterCredential", value, false);
	}

	public static void masterSalesChannel(final Object obj, final String value) {
		addValue(Credential.class, obj, "masterSalesChannel", value, false);
	}

	public static void masterServiceType(final Object obj, final String value) {
		addValue(Credential.class, obj, "masterServiceType", value, false);
	}

	public static void masterBrand(final Object obj, final String value) {
		addValue(Credential.class, obj, "masterBrand", value, false);
	}

	public static void masterSupplier(final Object obj, final String value) {
		addValue(Supplier.class, obj, "masterSupplier", value, false);
	}

	public static void masterGroup(final Object obj, final String value) {
		addValue(Supplier.class, obj, "masterGroup", value, false);
	}

	public static void masterConnector(final Object obj, final String value) {
		addValue(Supplier.class, obj, "masterConnector", value, false);
	}

	public static void entityScaned(final Class<?> classes, final Object obj, final String value) {
		addValue(classes, obj, "entityScaned", value, false);
	}

	public static void timeTo(final Class<?> classes, final Object obj, final boolean isSuper) {
		addValue(classes, obj, "timeTo", "23:59:59", isSuper);
	}

	public static void timeFrom(final Class<?> classes, final Object obj, final boolean isSuper) {
		addValue(classes, obj, "timeFrom", "00:00:00", isSuper);
	}

	public static void dateTo(final Class<?> classes, final Object obj, final boolean isSuper) {
		addValue(classes, obj, "dateTo", "31/12/2050", isSuper);
	}

	public static void rates(final Class<?> classes, final Object obj, final String value) {
		addValue(classes, obj, "matchRates", value, false);
	}

	private static void addValue(final Class<?> classes, final Object obj, final String fieldName, final String fieldValue, final boolean isSuper) {
		try {
			Field field;
			if (isSuper) {
				field = classes.getSuperclass().getDeclaredField(fieldName);
			} else {
				field = classes.getDeclaredField(fieldName);
			}
			field.setAccessible(true);
			field.set(obj, fieldValue);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static void init(final Class<?> classes, final Object obj, final String fieldName, final String fieldValue, final boolean isInit) {
		if (StringUtils.hasText(fieldName)) {
			try {
				Field field = classes.getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(obj, fieldValue);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		if (isInit) {
			try {
				Method postConstruct = classes.getDeclaredMethod("init");
				postConstruct.setAccessible(true);
				postConstruct.invoke(obj);
			} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
}