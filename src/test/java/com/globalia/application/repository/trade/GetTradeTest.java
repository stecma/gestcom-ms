package com.globalia.application.repository.trade;

import com.globalia.HelperTest;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetTradeTest extends HelperTest {

	@InjectMocks
	private GetTrade getTrade;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemNotImplemented() {
		assertNull(this.getTrade.getItem(trade(), monitor()));
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		assertNull(this.getTrade.getItem(trade().getCredential()));
	}

	@Test
	public void testGetItemNoRedis() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(null);
		assertNull(this.getTrade.getItem(trade().getCredential()));
	}

	@Test
	public void testGetItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", TradePolicyItem.class)).thenThrow(IOException.class);
		assertNull(this.getTrade.getItem(trade().getCredential()));
	}

	@Test
	public void testGetItem() throws IOException {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", TradePolicyItem.class)).thenReturn(new TradePolicyItem());
		assertNotNull(this.getTrade.getItem(trade().getCredential()));
	}
}
