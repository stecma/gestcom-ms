package com.globalia.application.repository.master;

import com.globalia.HelperTest;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetChidCodesTest extends HelperTest {

	@InjectMocks
	private GetChildCodes getChildCodes;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetChildCodesNoResponse() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.patternKeys(anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.getChildCodes.getChildCodes("key", "id"));
	}

	@Test
	public void testGetChildCodesEmptyMemebers() {
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("dev:gescom:masters:BRAND:1", "dev:gescom:masters:BRAND:2"));
		when(this.client.members(anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.getChildCodes.getChildCodes("test", master().getId()));
	}

	@Test
	public void testGetChildCodes() {
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("dev:gescom:masters:BRAND:1", "dev:gescom:masters:BRAND:2"));
		when(this.client.members(anyString())).thenReturn(Set.of("1", "2"));
		assertNotNull(this.getChildCodes.getChildCodes("brand", master().getId()));
	}
}
