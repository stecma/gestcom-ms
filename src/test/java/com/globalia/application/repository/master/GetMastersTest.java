package com.globalia.application.repository.master;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetMastersTest extends HelperTest {

	@InjectMocks
	private GetMasters getMasters;
	@Mock
	private GetMaster master;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private Errors error;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private RedisClient client;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllMasterRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getMasters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testGetAllMasterIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenThrow(IOException.class);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getMasters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testGetAllMasterIsDeleted() throws IOException {
		MasterItem master = new MasterItem();
		master.setDeleted(true);

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(master);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getMasters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testGetAllMaster() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(master());
		assertNull(this.getMasters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testGetAllMasterEXTERNAL() throws IOException {
		MasterItem master = new MasterItem();
		master.setEntity(MasterType.EXTERNAL_SYSTEM.name());

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(master);
		assertNull(this.getMasters.getAllItems(master, monitor()).getError());
	}

	@Test
	public void testFindCodesRuntimeException() {
		when(this.client.keys(anyString())).thenThrow(RuntimeException.class);
		assertNull(this.getMasters.findCodes(master().getEntity()));
	}

	@Test
	public void testFindCodes() {
		when(this.client.keys(anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.getMasters.findCodes(master().getEntity()));
	}

	@Test
	public void testGetAllScanIOException() throws IOException {
		when(this.client.scanKeys(anyString(), anyString())).thenThrow(IOException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getMasters.getAllScan("brand", "test", "test", monitor()).getError());
	}

	@Test
	public void testGetAllScanNull() throws IOException {
		when(this.client.scanKeys(anyString(), anyString())).thenReturn(null);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getMasters.getAllScan("brand", "test", "test", monitor()).getError());
	}

	@Test
	public void testGetAllScanMasterError() throws IOException {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.master.getMaster(anyString(), anyString(), any())).thenReturn(item);
		when(this.client.scanKeys(anyString(), anyString())).thenReturn(Set.of("test", "1"));
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getMasters.getAllScan("brand", "test", "test", monitor()).getError());
	}

	@Test
	public void testGetAllScan() throws IOException {
		when(this.client.get(anyString(), any())).thenReturn("json");
		when(this.master.getMaster(anyString(), anyString(), any())).thenReturn(new ItemResponse());
		when(this.client.scanKeys(anyString(), anyString())).thenReturn(Set.of("test"));
		assertNull(this.getMasters.getAllScan("brand", "test", "test", monitor()).getError());
	}
}
