package com.globalia.application.repository.master;

import com.globalia.HelperTest;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetTypesTest extends HelperTest {

	@InjectMocks
	private GetTypes getTypes;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetTypes() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("test");
		assertNotNull(this.getTypes.getType(MasterType.ACTIVITY));
	}
}
