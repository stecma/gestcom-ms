package com.globalia.application.repository.master;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.MasterItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetMasterTest extends HelperTest {

	@InjectMocks
	private GetMaster getMaster;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private Errors error;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private RedisClient client;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getMaster.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenThrow(IOException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getMaster.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetItemNullResponse() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(null);
		assertNull(this.getMaster.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetItemDeleted() throws IOException {
		MasterItem master = new MasterItem();
		master().setDeleted(true);
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(master);
		assertNull(this.getMaster.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetItem() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(new MasterItem());
		assertNull(this.getMaster.getItem(master(), monitor()).getError());
	}
}
