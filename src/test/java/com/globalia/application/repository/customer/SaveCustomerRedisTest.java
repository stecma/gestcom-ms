package com.globalia.application.repository.customer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveCustomerRedisTest extends HelperTest {

	@InjectMocks
	private SaveCustomerRedis save;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors errorComponent;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testAddItemJsonProcessingException() throws JsonProcessingException {
		CustomerItem customer = customer();
		customer.setId(null);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.addItem(customer, monitor()).getError());
	}

	@Test
	public void testAddItemRuntimeException() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		doThrow(RuntimeException.class).when(this.client).add(anyString(), anyString(), anyString());
		assertNotNull(this.save.addItem(customer(), monitor()).getError());
	}

	@Test
	public void testAddItem() throws JsonProcessingException {
		CustomerItem customer = customer();
		customer.setCodes(null);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.redisAccess.getFormatter()).thenReturn(new SimpleDateFormat());
		assertNull(this.save.addItem(customer, monitor()).getError());
	}
}
