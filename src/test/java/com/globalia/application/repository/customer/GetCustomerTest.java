package com.globalia.application.repository.customer;

import com.globalia.HelperTest;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetCustomerTest extends HelperTest {

	@InjectMocks
	private GetCustomer getCustomer;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemNotImplemented() {
		assertNull(this.getCustomer.getItem(customer(), monitor()));
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		assertNull(this.getCustomer.getItem(customer().getCredential()));
	}

	@Test
	public void testGetItemNoResponse() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(new HashMap<>());
		assertNull(this.getCustomer.getItem(customer().getCredential()));
	}

	@Test
	public void testGetItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", CustomerItem.class)).thenThrow(IOException.class);
		assertNull(this.getCustomer.getItem(customer().getCredential()));
	}

	@Test
	public void testGetItemDeleted() throws IOException {
		CustomerItem item = new CustomerItem();
		item.setDeleted(true);
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", CustomerItem.class)).thenReturn(item);
		assertNotNull(this.getCustomer.getItem(customer().getCredential()));
	}

	@Test
	public void testGetItem() throws IOException {
		CustomerItem item = new CustomerItem();
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", CustomerItem.class)).thenReturn(item);
		assertNotNull(this.getCustomer.getItem(customer().getCredential()));
	}
}
