package com.globalia.application.repository.group;

import com.globalia.HelperTest;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetAssignGroupsTest extends HelperTest {

	@InjectMocks
	private GetAssignGroups getAssignGroups;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private GetAssignGroup group;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemNotImplemented() {
		assertNull(this.getAssignGroups.getAllItems(group(), monitor()));
	}

	@Test
	public void testGetAllItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.patternKeys(anyString())).thenThrow(RuntimeException.class);
		assertNotNull(this.getAssignGroups.getAllItems());
	}

	@Test
	public void testGetAllItemNoResponse() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.patternKeys(anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.getAssignGroups.getAllItems());
	}

	@Test
	public void testGetAllItemNoGroup() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("1:1", "2:2"));
		when(this.group.getGroup(anyString())).thenReturn(null);
		assertNotNull(this.getAssignGroups.getAllItems());
	}

	@Test
	public void testGetAllItem() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("1:1", "2:2"));
		when(this.group.getGroup(anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.getAssignGroups.getAllItems());
	}
}
