package com.globalia.application.repository.group;

import com.globalia.HelperTest;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetAssignGroupTest extends HelperTest {

	@InjectMocks
	private GetAssignGroup getAssignGroup;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemNotImplemented() {
		assertNull(this.getAssignGroup.getItem(group(), monitor()));
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		assertNotNull(this.getAssignGroup.getItem("credential"));
	}

	@Test
	public void testGetAllItemNoResponse() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(new HashMap<>());
		assertNotNull(this.getAssignGroup.getItem("credential"));
	}

	@Test
	public void testGetAllItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", GroupCredentialItem.class)).thenThrow(IOException.class);
		assertNotNull(this.getAssignGroup.getItem("credential"));
	}

	@Test
	public void testGetAllItemNoJson() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", GroupCredentialItem.class)).thenReturn(null);
		assertNotNull(this.getAssignGroup.getItem("credential"));
	}

	@Test
	public void testGetAllItem() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", GroupCredentialItem.class)).thenReturn(new GroupCredentialItem());
		assertNotNull(this.getAssignGroup.getItem("credential"));
	}
}
