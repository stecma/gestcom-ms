package com.globalia.application.repository.credential;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetCredentialTest extends HelperTest {

	@InjectMocks
	private GetCredential getCredential;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getCredential.getItem(credential(), monitor()).getError());
	}

	@Test
	public void testGetItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", CredentialItem.class)).thenThrow(IOException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getCredential.getItem(credential(), monitor()).getError());
	}

	@Test
	public void testGetItemNoRedis() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", CredentialItem.class)).thenReturn(null);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getCredential.getItem(credential(), monitor()));
	}

	@Test
	public void testGetItemDeleted() throws IOException {
		ExternalCredentialItem item = new ExternalCredentialItem();
		item.setDeleted(true);
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", CredentialItem.class)).thenReturn(item);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getCredential.getItem(credential(), monitor()));
	}

	@Test
	public void testGetItem() throws IOException {
		ExternalCredentialItem item = new ExternalCredentialItem();
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", CredentialItem.class)).thenReturn(item);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getCredential.getItem(credential(), monitor()));
	}
}
