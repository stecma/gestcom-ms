package com.globalia.application.repository.credential;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetCredentialsTest extends HelperTest {

	@InjectMocks
	private GetCredentials getCredentials;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllIteNotImplemented() {
		assertNull(this.getCredentials.getAllItems(credential(), monitor()));
	}

	@Test
	public void testGetAllItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getCredentials.getAllItems(monitor()).getError());
	}

	@Test
	public void testGetAllItemEmptyRedis() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(new HashMap<>());
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getCredentials.getAllItems(monitor()).getError());
	}

	@Test
	public void testGetAllItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", CredentialItem.class)).thenThrow(IOException.class);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getCredentials.getAllItems(monitor()).getError());
	}

	@Test
	public void testGetAllItemDeleted() throws IOException {
		CredentialItem credential = credential();
		credential.setDeleted(true);

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", CredentialItem.class)).thenReturn(credential);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getCredentials.getAllItems(monitor()).getError());
	}

	@Test
	public void testGetAllItem() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.jsonHandler.fromJson("json", CredentialItem.class)).thenReturn(credential());
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNull(this.getCredentials.getAllItems(monitor()).getError());
	}
}
