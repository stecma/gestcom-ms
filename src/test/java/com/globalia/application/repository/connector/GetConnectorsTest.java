package com.globalia.application.repository.connector;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetConnectorsTest extends HelperTest {

	@InjectMocks
	private GetConnectors getConnectors;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllIteNotImplemented() {
		assertNull(this.getConnectors.getAllItems(connector(), monitor()));
	}

	@Test
	public void testGetAllItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getConnectors.getAllItems(connector(), true, monitor()).getError());
	}

	@Test
	public void testGetAllItemEmptyRedis() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(new HashMap<>());
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getConnectors.getAllItems(connector(), true, monitor()).getError());
	}

	@Test
	public void testGetAllItemIOException() throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("1¬1", "json");

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", ExternalCredentialItem.class)).thenThrow(IOException.class);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getConnectors.getAllItems(connector(), true, monitor()).getError());
	}

	@Test
	public void testGetAllItemDeleted() throws IOException {
		ExternalCredentialItem connector = connector();
		connector.setDeleted(true);
		Map<String, Object> map = new HashMap<>();
		map.put("1¬1", "json");

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", ExternalCredentialItem.class)).thenReturn(connector);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNull(this.getConnectors.getAllItems(connector(), true, monitor()).getError());
	}

	@Test
	public void testGetAllItem() throws IOException {
		ExternalCredentialItem connector = connector();
		Map<String, Object> map = new HashMap<>();
		map.put("1¬1", "json");
		map.put("1¬2", "json");

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", ExternalCredentialItem.class)).thenReturn(connector);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNull(this.getConnectors.getAllItems(connector, true, monitor()).getError());
	}
}
