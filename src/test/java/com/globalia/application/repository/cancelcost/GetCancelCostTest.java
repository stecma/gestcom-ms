package com.globalia.application.repository.cancelcost;

import com.globalia.HelperTest;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetCancelCostTest extends HelperTest {

	@InjectMocks
	private GetCancelCost getCancelCost;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemNotImplemented() {
		assertNull(this.getCancelCost.getItem(cancelCost(), monitor()));
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		assertNotNull(this.getCancelCost.getItem(cancelCost().getCredential()));
	}

	@Test
	public void testGetItemNoRedis() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(null);
		assertNotNull(this.getCancelCost.getItem(cancelCost().getCredential()));
	}

	@Test
	public void testGetItemIOException() throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", CancelCostItem.class)).thenThrow(IOException.class);
		assertNotNull(this.getCancelCost.getItem(cancelCost().getCredential()));
	}

	@Test
	public void testGetItem() throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", CancelCostItem.class)).thenReturn(new CancelCostItem());
		assertNotNull(this.getCancelCost.getItem(cancelCost().getCredential()));
	}
}
