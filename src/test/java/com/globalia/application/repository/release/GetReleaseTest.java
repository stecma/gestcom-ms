package com.globalia.application.repository.release;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetReleaseTest extends HelperTest {

	@InjectMocks
	private GetRelease getRelease;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getRelease.getItem(release(), monitor()).getError());
	}

	@Test
	public void testGetItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", ReleaseItem.class)).thenThrow(IOException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getRelease.getItem(release(), monitor()).getError());
	}

	@Test
	public void testGetItemNoRedis() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", ReleaseItem.class)).thenReturn(null);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getRelease.getItem(release(), monitor()));
	}

	@Test
	public void testGetItemDeleted() throws IOException {
		ReleaseItem item = new ReleaseItem();
		item.setDeleted(true);
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", ReleaseItem.class)).thenReturn(item);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getRelease.getItem(release(), monitor()));
	}

	@Test
	public void testGetItem() throws IOException {
		ReleaseItem item = new ReleaseItem();
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", ReleaseItem.class)).thenReturn(item);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getRelease.getItem(release(), monitor()));
	}
}
