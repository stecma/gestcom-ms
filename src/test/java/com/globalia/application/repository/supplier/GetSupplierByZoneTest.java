package com.globalia.application.repository.supplier;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetSupplierByZoneTest extends HelperTest {

	@InjectMocks
	private GetSupplierByZone getSupplierByZone;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplierByZone.getItem(supplierByZone(), monitor()).getError());
	}

	@Test
	public void testGetItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", SupplierByZoneItem.class)).thenThrow(IOException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplierByZone.getItem(supplierByZone(), monitor()).getError());
	}

	@Test
	public void testGetItemNull() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", SupplierByZoneItem.class)).thenReturn(null);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.getSupplierByZone.getItem(supplierByZone(), monitor()).getError());
	}

	@Test
	public void testGetItem() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", SupplierByZoneItem.class)).thenReturn(new SupplierByZoneItem());
		assertNull(this.getSupplierByZone.getItem(supplierByZone(), monitor()).getError());
	}
}
