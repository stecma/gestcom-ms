package com.globalia.application.repository.supplier;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveSupplierRedisTest extends HelperTest {

	@InjectMocks
	private SaveSupplierRedis save;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors errorComponent;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testAddItemJsonProcessingException() throws JsonProcessingException {
		SupplierItem supplier = supplier();
		supplier.setId(null);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.addItem(supplier, monitor()).getError());
	}

	@Test
	public void testAddItemRuntimeException() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		doThrow(RuntimeException.class).when(this.client).add(nullable(String.class), anyString(), anyString());
		assertNotNull(this.save.addItem(supplier(), monitor()).getError());
	}

	@Test
	public void testAddItem() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.redisAccess.getFormatter()).thenReturn(new SimpleDateFormat());
		assertNull(this.save.addItem(supplier(), monitor()).getError());
	}

	@Test
	public void testAddItemEmptyMaster() throws JsonProcessingException {
		SupplierItem supplier = supplier();
		supplier.setMasters(null);
		supplier.setServiceType(null);
		supplier.setSystem(null);
		supplier.setBuyModel(null);
		supplier.setUser(null);
		supplier.setClientPref(null);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.redisAccess.getFormatter()).thenReturn(new SimpleDateFormat());
		assertNull(this.save.addItem(supplier, monitor()).getError());
	}

	@Test
	public void testUpdateService() {
		SupplierItem supplier = supplier();
		assertNull(this.save.addServiceItem(supplier.getSystem().getId(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther(), monitor()).getError());
	}

	@Test
	public void testUpdateSap() {
		SupplierItem supplier = supplier();
		assertNull(this.save.addSapItem(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), true, monitor()).getError());
	}
}
