package com.globalia.application.repository.supplier;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.dto.credential.KeyMap;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GetSupplierTest extends HelperTest {

	@InjectMocks
	private GetSupplier getSupplier;
	@Mock
	private RedisAccess redisAccess;
	@Mock
	private RedisClient client;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getItem(supplier(), monitor()).getError());
	}

	@Test
	public void testGetItemIOException() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", SupplierItem.class)).thenThrow(IOException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getItem(supplier(), monitor()).getError());
	}

	@Test
	public void testGetItemNoRedis() throws IOException {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", SupplierItem.class)).thenReturn(null);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getItem(supplier(), monitor()));
	}

	@Test
	public void testGetItemDeleted() throws IOException {
		SupplierItem item = new SupplierItem();
		item.setDeleted(true);
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", SupplierItem.class)).thenReturn(item);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getItem(supplier(), monitor()));
	}

	@Test
	public void testGetItem() throws IOException {
		SupplierItem item = new SupplierItem();
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", SupplierItem.class)).thenReturn(item);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getItem(supplier(), monitor()));
	}

	@Test
	public void testgetMapKeyRuntimeException() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getMapKey("supplierCredential", monitor()).getError());
	}

	@Test
	public void testgetMapKeyDiffKeyMap() {
		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map());
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getMapKey("supplierCredential", monitor()).getError());
	}

	@Test
	public void testgetMapKeyIOException() throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("supplierCredential¬1", "json");

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.jsonHandler.fromJson("json", KeyMap.class)).thenThrow(IOException.class);
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.error.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.getSupplier.getMapKey("supplierCredential", monitor()).getError());
	}

	@Test
	public void testgetMapKey() throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("supplierCredential¬1", "json");

		when(this.redisAccess.getRediskey(nullable(String.class))).thenReturn("dev");
		when(this.jsonHandler.fromJson("json", KeyMap.class)).thenReturn(new KeyMap());
		when(this.client.getAll(anyString())).thenReturn(map);
		assertNull(this.getSupplier.getMapKey("supplierCredential", monitor()).getError());
	}
}
