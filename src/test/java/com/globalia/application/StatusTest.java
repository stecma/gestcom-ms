package com.globalia.application;

import com.globalia.Dates;
import com.globalia.HelperTest;
import com.globalia.dto.credential.Assignment;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class StatusTest extends HelperTest {

	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	@InjectMocks
	private Status status;
	@Mock
	private Dates dates;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetStatusNoAssgiment() {
		assertEquals(MatchLevel.RED, this.status.getStatus(null, new Date(), monitor()));
	}

	@Test
	public void testGetStatusNoBook() {
		Assignment assign = assignment();
		assign.setBook(null);
		assertEquals(MatchLevel.RED, this.status.getStatus(assign, new Date(), monitor()));
	}

	@Test
	public void testGetStatusGetConsulValues() {
		Assignment assign = assignment();
		assign.getBook().setTo(null);

		when(this.dates.getDateTime(anyString(), anyString(), any())).thenThrow(ValidateException.class);
		assertEquals(MatchLevel.RED, this.status.getStatus(assign, new Date(), monitor()));
	}

	@Test
	public void testGetStatusDatesError() {
		when(this.dates.getDateTime(anyString(), anyString(), any())).thenThrow(ValidateException.class);
		assertEquals(MatchLevel.RED, this.status.getStatus(assignment(), new Date(), monitor()));
	}

	@Test
	public void testGetStatusRed() throws ParseException {
		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(this.formatter.parse("08/02/2021 00:00:00"));
		assertEquals(MatchLevel.RED, this.status.getStatus(assignment(), new Date(), monitor()));
	}

	@Test
	public void testGetStatusYellow() throws ParseException {
		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(this.formatter.parse("08/02/2050 00:00:00"));
		assertEquals(MatchLevel.YELLOW, this.status.getStatus(assignment(), new Date(), monitor()));
	}

	@Test
	public void testGetStatusGreen() {
		Date currentDate = new Date();
		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(currentDate);
		assertEquals(MatchLevel.GREEN, this.status.getStatus(assignment(), currentDate, monitor()));
	}
}
