package com.globalia.application.service.match;

import com.globalia.Dates;
import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.application.service.credential.Credentials;
import com.globalia.dto.DateRange;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.Assignment;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MatchItem;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SupplierMatchTest extends HelperTest {

	@InjectMocks
	private SupplierMatch supplierMatch;
	@Mock
	private Supplier supplier;
	@Mock
	private Errors error;
	@Mock
	private Credentials credentials;
	@Mock
	private Match matchProcess;
	@Mock
	private Dates dates;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		dateTo(SupplierMatch.class, this.supplierMatch, true);
		timeTo(SupplierMatch.class, this.supplierMatch, true);
	}

	@Test
	public void testMatchSupplierNoSupplier() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.supplier.getItem(any(), anyBoolean(), any())).thenReturn(item);
		assertNotNull(this.supplierMatch.matchSupplier(match(), monitor()).getError());
	}

	@Test
	public void testMatchSupplierNotEnabled() {
		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());
		item.getSupplier().setStatus(SupplierStatus.DISABLED);

		MatchItem match = match();
		match.setSupplierId("2");

		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.supplier.getItem(any(), anyBoolean(), any())).thenReturn(item);
		assertNotNull(this.supplierMatch.matchSupplier(match, monitor()).getError());
	}

	@Test
	public void testMatchSupplierNoGroup() {
		GroupCredentialItem group2 = new GroupCredentialItem();
		group2.setSupplierId("1");
		group2.setId("2");
		group2.setActive(true);

		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());
		item.getSupplier().setStatus(SupplierStatus.ENABLED);
		item.getSupplier().setExternalSys(Set.of(group2));

		MatchItem match = match();
		match.setSupplierId("2");

		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.supplier.getItem(any(), anyBoolean(), any())).thenReturn(item);
		assertNotNull(this.supplierMatch.matchSupplier(match, monitor()).getError());
	}

	@Test
	public void testMatchSupplierNoCredentials() {
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setError(new Error());

		when(this.error.addError(anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.supplier.getItem(any(), anyBoolean(), any())).thenReturn(this.getSupplier1());
		when(this.credentials.getAllItems(any(), any())).thenReturn(credentials);
		assertNotNull(this.supplierMatch.matchSupplier(match(), monitor()).getError());
	}

	@Test
	public void testMatchSupplierCredentialNoGroups() {
		CredentialItem credential = this.getCredential("1");
		credential.setGroups(null);
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(Set.of(credential));

		when(this.supplier.getItem(any(), anyBoolean(), any())).thenReturn(this.getSupplier1());
		when(this.credentials.getAllItems(any(), any())).thenReturn(credentials);
		ItemResponse item = this.supplierMatch.matchSupplier(match(), monitor());
		assertNull(item.getError());
		assertNull(item.getMatch().getAssigned());
		assertNotNull(item.getMatch().getUnAssigned());
		assertNull(item.getMatch().getOtherGroupAssigned());
	}

	@Test
	public void testMatchSupplierCredential() {
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(Set.of(this.getCredential("1"), this.getCredential("2"), this.getCredential("3")));

		Calendar calendar = Calendar.getInstance();
		calendar.set(2050, Calendar.DECEMBER, 31);

		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(new Date(calendar.getTimeInMillis()));
		when(this.supplier.getItem(any(), anyBoolean(), any())).thenReturn(this.getSupplier1());
		when(this.credentials.getAllItems(any(), any())).thenReturn(credentials);
		ItemResponse item = this.supplierMatch.matchSupplier(match(), monitor());
		assertNull(item.getError());
		assertNotNull(item.getMatch().getAssigned());
		assertNotNull(item.getMatch().getUnAssigned());
		assertNotNull(item.getMatch().getOtherGroupAssigned());
	}

	@Test
	public void testMatchSupplierCredential2() {
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(Set.of(this.getCredential("3")));

		Calendar calendar = Calendar.getInstance();
		calendar.set(2050, Calendar.DECEMBER, 31);

		MatchItem match = match();
		match.setSupplierId("2");

		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(new Date(calendar.getTimeInMillis()));
		when(this.supplier.getItem(any(), anyBoolean(), any())).thenReturn(this.getSupplier1());
		when(this.credentials.getAllItems(any(), any())).thenReturn(credentials);
		ItemResponse item = this.supplierMatch.matchSupplier(match, monitor());
		assertNull(item.getError());
		assertNull(item.getMatch().getAssigned());
		assertNull(item.getMatch().getUnAssigned());
		assertNotNull(item.getMatch().getOtherGroupAssigned());
	}

	private ItemResponse getSupplier1() {
		GroupCredentialItem group1 = new GroupCredentialItem();
		group1.setSupplierId("1");
		group1.setId("1");
		group1.setAssignment(new Assignment());
		group1.getAssignment().setCheckIn(new DateRange());
		group1.getAssignment().getCheckIn().setTo("31/12/2050");
		group1.getAssignment().setBook(new DateRange());
		group1.getAssignment().getBook().setTo("31/12/2050");
		group1.getAssignment().getBook().setTimeTo("00:00:00");
		group1.setActive(true);

		GroupCredentialItem group3 = new GroupCredentialItem();
		group3.setSupplierId("2");
		group3.setId("3");
		group3.setActive(true);

		GroupCredentialItem group4 = new GroupCredentialItem();
		group4.setSupplierId("4");
		group4.setId("4");

		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());
		item.getSupplier().setStatus(SupplierStatus.ENABLED);
		item.getSupplier().setExternalSys(Set.of(group1, group3, group4));

		return item;
	}

	private CredentialItem getCredential(final String id) {
		GroupCredentialItem group1 = new GroupCredentialItem();
		group1.setSupplierId("1");
		group1.setId("1");
		group1.setAssignment(new Assignment());
		group1.getAssignment().setCheckIn(new DateRange());
		group1.getAssignment().getCheckIn().setTo("31/12/2050");
		group1.getAssignment().setBook(new DateRange());
		group1.getAssignment().getBook().setTo("31/12/2050");
		group1.getAssignment().getBook().setTimeTo("00:00:00");
		group1.setActive(true);

		GroupCredentialItem group2 = new GroupCredentialItem();
		group2.setSupplierId("1");
		group2.setId("2");
		group2.setActive(true);
		group2.setAssignment(new Assignment());
		group2.getAssignment().setCheckIn(new DateRange());
		group2.getAssignment().getCheckIn().setTo("31/12/2050");
		group2.getAssignment().setBook(new DateRange());
		group2.getAssignment().getBook().setTo("31/12/2050");
		group2.getAssignment().getBook().setTimeTo("00:00:00");

		GroupCredentialItem group3 = new GroupCredentialItem();
		group3.setSupplierId("2");
		group3.setId("3");
		group3.setActive(true);

		GroupCredentialItem group4 = new GroupCredentialItem();
		group4.setSupplierId("4");
		group4.setId("4");

		CredentialItem credential = credential();
		credential.setId(id);
		if ("1".equals(id)) {
			credential.setGroups(Set.of(group1, group2, group3, group4));
		} else if ("2".equals(id)) {
			credential.setGroups(Set.of(group2));
		} else {
			credential.setGroups(Set.of(group3));
		}
		return credential;
	}
}
