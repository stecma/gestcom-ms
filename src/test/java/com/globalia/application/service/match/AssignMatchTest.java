package com.globalia.application.service.match;

import com.globalia.HelperTest;
import com.globalia.application.service.group.Group;
import com.globalia.application.service.group.SaveGroup;
import com.globalia.application.service.match.AssignMatch;
import com.globalia.dto.Parameter;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MatchItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AssignMatchTest extends HelperTest {

	@InjectMocks
	private AssignMatch assignMatch;
	@Mock
	private Group group;
	@Mock
	private SaveGroup saveGroup;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testAssignSupplierError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());

		MatchItem match = match();
		match.setGroupId("1¬1");
		match.setCredentialId(null);
		when(this.group.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.assignMatch.assign(match, monitor()).getError());
	}

	@Test
	public void testAssignSupplier() {
		ItemResponse item = new ItemResponse();
		item.setGroup(group());

		Parameter param = new Parameter();
		param.setKey("test");
		param.setValue("test#date#user¬test#date#user");

		MatchItem match = match();
		match.setGroupId("1¬1");
		match.setCredentialId(null);
		match.setCodes(Set.of(param));

		when(this.group.getItem(any(), any())).thenReturn(item);
		when(this.saveGroup.addAssignItem(anyString(), any(), any())).thenReturn(item);
		assertNull(this.assignMatch.assign(match, monitor()).getError());
	}

	@Test
	public void testAssignCredentialError() {
		Parameter param = new Parameter();
		param.setKey("1¬1");
		param.setValue("test#date#user¬test#date#user");

		MatchItem match = match();
		match.setCodes(Set.of(param));

		ItemResponse item = new ItemResponse();
		item.setError(new Error());

		when(this.group.getAssignItem(anyString())).thenReturn(new HashSet<>());
		when(this.group.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.assignMatch.assign(match, monitor()).getError());
	}

	@Test
	public void testAssignCredentialSaveError() {
		Parameter param = new Parameter();
		param.setKey("1¬1");
		param.setValue("test#date#user¬test#date#user");

		MatchItem match = match();
		match.setCodes(Set.of(param));

		GroupCredentialItem group = group();
		group.setId("1");
		group.setSupplierId("1");

		ItemResponse item = new ItemResponse();
		item.setError(new Error());

		when(this.group.getAssignItem(anyString())).thenReturn(Set.of(group));
		when(this.saveGroup.addAssignItem(anyString(), any(), any())).thenReturn(item);
		assertNotNull(this.assignMatch.assign(match, monitor()).getError());
	}

	@Test
	public void testAssignCredential() {
		Parameter param = new Parameter();
		param.setKey("1¬1");
		param.setValue("test#date#user¬test#date#user");

		MatchItem match = match();
		match.setCodes(Set.of(param));

		GroupCredentialItem group = group();
		group.setId("1");
		group.setSupplierId("1");

		when(this.group.getAssignItem(anyString())).thenReturn(Set.of(group));
		when(this.saveGroup.addAssignItem(anyString(), any(), any())).thenReturn(new ItemResponse());
		assertNull(this.assignMatch.assign(match, monitor()).getError());
	}
}
