package com.globalia.application.service.match;

import com.globalia.Dates;
import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.application.service.credential.Credential;
import com.globalia.dto.DateRange;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.Assignment;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MatchItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerMatchTest extends HelperTest {

	@InjectMocks
	private CustomerMatch customerMatch;
	@Mock
	private Credential credential;
	@Mock
	private Suppliers suppliers;
	@Mock
	private Dates dates;
	@Mock
	private Match matchProcess;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		dateTo(CustomerMatch.class, this.customerMatch, true);
		timeTo(CustomerMatch.class, this.customerMatch, true);
	}

	@Test
	public void testMatchCustomerNoCredential() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.credential.getItem(any(), anyBoolean(), any())).thenReturn(item);
		assertNotNull(this.customerMatch.matchCustomer(match(), monitor()).getError());
	}

	@Test
	public void testMatchCustomerSuppliersError() {
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());
		when(this.credential.getItem(any(), anyBoolean(), any())).thenReturn(item);

		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setError(new Error());

		when(this.suppliers.getAllItems(any(), anyBoolean(), any())).thenReturn(suppliers);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.customerMatch.matchCustomer(match(), monitor()).getError());
	}

	@Test
	public void testMatchCustomerSuppliersNoGroup() {
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());
		when(this.credential.getItem(any(), anyBoolean(), any())).thenReturn(item);

		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setSuppliers(Set.of(supplier()));

		when(this.suppliers.getAllItems(any(), anyBoolean(), any())).thenReturn(suppliers);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.customerMatch.matchCustomer(match(), monitor()).getError());
	}

	@Test
	public void testMatchCustomerWithStatusFilterError() {
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());
		when(this.credential.getItem(any(), anyBoolean(), any())).thenReturn(item);

		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setError(new Error());
		when(this.suppliers.getAllItems(any(), anyBoolean(), any())).thenReturn(suppliers);

		MatchItem match = match();
		match.getSupplier().getFilter().setStatus(SupplierStatus.ENABLED.name());
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.customerMatch.matchCustomer(match, monitor()).getError());
	}

	@Test
	public void testMatchCustomerWithStatusFilterNoGroup() {
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());
		when(this.credential.getItem(any(), anyBoolean(), any())).thenReturn(item);

		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setSuppliers(Set.of(supplier()));
		when(this.suppliers.getAllItems(any(), anyBoolean(), any())).thenReturn(suppliers);

		MatchItem match = match();
		match.getSupplier().getFilter().setStatus(SupplierStatus.ENABLED.name());
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.customerMatch.matchCustomer(match, monitor()).getError());
	}

	@Test
	public void testMatchCustomerSuppliers() {
		ItemResponse item = new ItemResponse();
		item.setCredential(this.getCredential());
		when(this.credential.getItem(any(), anyBoolean(), any())).thenReturn(item);

		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setSuppliers(new LinkedHashSet<>(Set.of(this.getSupplier("1"), this.getSupplier("2"), this.getSupplier("3"))));

		Calendar calendar = Calendar.getInstance();
		calendar.set(2050, Calendar.DECEMBER, 31);

		when(this.dates.getDateTime(anyString(), anyString(), any())).thenReturn(new Date(calendar.getTimeInMillis()));
		when(this.suppliers.getAllItems(any(), anyBoolean(), any())).thenReturn(suppliers);
		assertNull(this.customerMatch.matchCustomer(match(), monitor()).getError());
	}

	private SupplierItem getSupplier(final String id) {
		GroupCredentialItem group1 = new GroupCredentialItem();
		group1.setSupplierId("1");
		group1.setId("1");
		group1.setAssignment(new Assignment());
		group1.getAssignment().setCheckIn(new DateRange());
		group1.getAssignment().getCheckIn().setTo("31/12/2050");
		group1.getAssignment().setBook(new DateRange());
		group1.getAssignment().getBook().setTo("31/12/2050");
		group1.getAssignment().getBook().setTimeTo("00:00:00");
		group1.setActive(true);

		GroupCredentialItem group2 = new GroupCredentialItem();
		group2.setSupplierId("1");
		group2.setId("2");
		group2.setActive(true);
		group2.setAssignment(new Assignment());
		group2.getAssignment().setCheckIn(new DateRange());
		group2.getAssignment().getCheckIn().setTo("31/12/2050");
		group2.getAssignment().setBook(new DateRange());
		group2.getAssignment().getBook().setTo("31/12/2050");
		group2.getAssignment().getBook().setTimeTo("00:00:00");

		GroupCredentialItem group3 = new GroupCredentialItem();
		group3.setSupplierId("2");
		group3.setId("3");
		group3.setActive(true);

		GroupCredentialItem group4 = new GroupCredentialItem();
		group4.setSupplierId("4");
		group4.setId("4");

		SupplierItem supplier = supplier();
		supplier.setId(id);
		if ("1".equals(id)) {
			supplier.setStatus(SupplierStatus.ENABLED);
			supplier.setExternalSys(Set.of(group1, group2, group3, group4));
		} else if ("2".equals(id)) {
			supplier.setExternalSys(Set.of(group2));
		} else {
			supplier.setExternalSys(Set.of(group3));
		}
		return supplier;
	}

	private CredentialItem getCredential() {
		GroupCredentialItem group1 = new GroupCredentialItem();
		group1.setSupplierId("1");
		group1.setId("1");
		group1.setAssignment(new Assignment());
		group1.getAssignment().setCheckIn(new DateRange());
		group1.getAssignment().getCheckIn().setTo("31/12/2050");
		group1.getAssignment().setBook(new DateRange());
		group1.getAssignment().getBook().setTo("31/12/2050");
		group1.getAssignment().getBook().setTimeTo("00:00:00");
		group1.setActive(true);


		CredentialItem credential = credential();
		credential.setId("1");
		credential.setGroups(Set.of(group1));
		return credential;
	}
}
