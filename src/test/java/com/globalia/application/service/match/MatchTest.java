package com.globalia.application.service.match;

import com.globalia.Dates;
import com.globalia.HelperTest;
import com.globalia.dto.DateRange;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.HotelX;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MatchTest extends HelperTest {

	private static final String MATCH_FIELD = "{ \"fields\": [ { \"field\": \"brand\", \"active\": true, \"mandatory\": true, \"order\" : \"0\" }, { \"field\": \"serviceType\", \"active\": true, \"mandatory\": true, \"order\" : \"1\" }, { \"field\": \"status\", \"active\": true, \"mandatory\": true, \"order\" : \"2\" }, { \"field\": \"ownProduct\", \"active\": true, \"mandatory\": true, \"order\" : \"3\" }, { \"field\": \"multiHotel\", \"active\": true, \"mandatory\": false, \"order\" : \"4\" }, { \"field\": \"timeOut\", \"active\": true, \"mandatory\": false, \"order\" : \"5\" }, { \"field\": \"blocks\", \"active\": true, \"mandatory\": false, \"order\" : \"6\" }, { \"field\": \"availCost\", \"active\": true, \"mandatory\": false, \"order\" : \"7\" }, { \"field\": \"active\", \"active\": true, \"mandatory\": true, \"order\" : \"8\" }, { \"field\": \"test\", \"active\": true, \"mandatory\": true, \"order\" : \"9\" }, { \"field\": \"hotelX\", \"active\": true, \"mandatory\": true, \"order\" : \"10\" }, { \"field\": \"market\", \"active\": true, \"mandatory\": false, \"order\" : \"12\" }, { \"field\": \"modelDistribution\", \"active\": true, \"mandatory\": true, \"order\" : \"14\" }, { \"field\": \"rates\", \"active\": true, \"mandatory\": false, \"order\" : \"16\" } ] }";
	private static final String MATCH_RATES = "{ \"rates\": [ { \"id\": \"B2B\" }, { \"id\": \"VINC\" }, { \"id\": \"NRF\" }, { \"id\": \"EMP\" } ] }";

	@InjectMocks
	private Match match;
	@Mock
	private Dates dates;

	private GroupCredentialItem group;
	private Set<CredentialItem> credentials;
	private Set<SupplierItem> suppliers;
	private CredentialItem credential1;
	private SupplierItem supplier1;
	private Map<String, String> credentialMap;
	private Map<String, String> supplierMap;
	private Monitor monitor;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		this.credentials = this.getCredentials();
		this.credentialMap = new HashMap<>();
		for (CredentialItem credential : this.credentials) {
			this.credentialMap.put(credential.getId(), MatchLevel.RED.name());
		}

		this.suppliers = this.getSuppliers();
		this.supplierMap = new HashMap<>();
		this.supplierMap.put(this.group.getId(), MatchLevel.RED.name());

		dateTo(Match.class, this.match, false);
		timeFrom(Match.class, this.match, false);
		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());
	}

	@Test
	public void testInit() {
		rates(Match.class, this.match, MatchTest.MATCH_RATES);
		init(Match.class, this.match, "matchFields", MatchTest.MATCH_FIELD, true);
		assertTrue(true);
	}

	@Test
	public void testExecuteSupplier() {
		rates(Match.class, this.match, MatchTest.MATCH_RATES);
		init(Match.class, this.match, "matchFields", MatchTest.MATCH_FIELD, true);

		this.group.setCountries(null);
		when(this.dates.getDateTime(anyString(), anyString(), any(Monitor.class))).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(true);
		this.match.execute(this.credentials, this.credentialMap, this.supplier1, this.group, this.monitor);
		assertTrue(true);
	}

	@Test
	public void testExecuteSupplierMarket() {
		rates(Match.class, this.match, MatchTest.MATCH_RATES);
		init(Match.class, this.match, "matchFields", MatchTest.MATCH_FIELD, true);

		this.supplier1.setBlock(false);
		when(this.dates.getDateTime(anyString(), anyString(), any(Monitor.class))).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(true);
		this.match.execute(this.credentials, this.credentialMap, this.supplier1, this.group, this.monitor);
		assertTrue(true);
	}

	@Test
	public void testExecuteSupplierDate() {
		rates(Match.class, this.match, MatchTest.MATCH_RATES);
		init(Match.class, this.match, "matchFields", MatchTest.MATCH_FIELD, true);

		this.supplier1.setBlock(false);
		when(this.dates.getDateTime(anyString(), anyString(), any(Monitor.class))).thenThrow(ValidateException.class);
		this.match.execute(this.credentials, this.credentialMap, this.supplier1, this.group, this.monitor);
		assertTrue(true);
	}

	@Test
	public void testExecuteCustomer() {
		rates(Match.class, this.match, MatchTest.MATCH_RATES);
		init(Match.class, this.match, "matchFields", MatchTest.MATCH_FIELD, true);

		this.group.setCountries(null);
		when(this.dates.getDateTime(anyString(), anyString(), any(Monitor.class))).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(true);
		this.match.execute(this.suppliers, this.supplierMap, this.credential1, this.monitor);
		assertTrue(true);
	}

	@Test
	public void testExecuteCustomerMarket() {
		rates(Match.class, this.match, MatchTest.MATCH_RATES);
		init(Match.class, this.match, "matchFields", MatchTest.MATCH_FIELD, true);

		this.supplier1.setBlock(false);
		when(this.dates.getDateTime(anyString(), anyString(), any(Monitor.class))).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(true);
		this.match.execute(this.suppliers, this.supplierMap, this.credential1, this.monitor);
		assertTrue(true);
	}

	@Test
	public void testExecuteCustomerDate() {
		rates(Match.class, this.match, MatchTest.MATCH_RATES);
		init(Match.class, this.match, "matchFields", MatchTest.MATCH_FIELD, true);

		this.supplier1.setBlock(false);
		when(this.dates.getDateTime(anyString(), anyString(), any(Monitor.class))).thenThrow(ValidateException.class);
		this.match.execute(this.suppliers, this.supplierMap, this.credential1, this.monitor);
		assertTrue(true);
	}

	private Set<CredentialItem> getCredentials() {
		PairValue brd = new PairValue();
		brd.setKey(MasterType.BRAND.name());
		brd.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue srvc = new PairValue();
		srvc.setKey(MasterType.SERVICE_TYPE.name());
		srvc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue prd = new PairValue();
		prd.setKey(MasterType.PRODUCT_ORIGIN.name());
		prd.setValue(new LinkedHashSet<>(Set.of("PULL3", "PPPULL")));
		PairValue rqc = new PairValue();
		rqc.setKey(MasterType.REQUEST.name());
		rqc.setValue(new LinkedHashSet<>(Set.of("MULTI", "DEST")));
		PairValue cous = new PairValue();
		cous.setKey(MasterType.COUNTRY.name());
		cous.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue modelc = new PairValue();
		modelc.setKey(MasterType.DISTRIBUTION_MODEL.name());
		modelc.setValue(new LinkedHashSet<>(Set.of("2", "1")));
		PairValue rates = new PairValue();
		rates.setKey(MasterType.RATE_TYPE.name());
		rates.setValue(new LinkedHashSet<>(Set.of("2", "VINC")));

		this.credential1 = new CredentialItem();
		this.credential1.setId("1");
		this.credential1.setPriceChange(true);
		this.credential1.setAvailCost(true);
		this.credential1.setTest(true);
		this.credential1.setHotelX(true);
		this.credential1.setTagNationality(true);
		this.credential1.setCodes(new HashSet<>(Set.of(brd, srvc, prd, rqc, cous, rates)));

		CredentialItem credential2 = new CredentialItem();
		credential2.setId("2");
		credential2.setPriceChange(true);
		credential2.setAvailCost(true);
		credential2.setTest(true);
		credential2.setHotelX(true);
		credential2.setTagNationality(false);
		credential2.setCodes(new HashSet<>(Set.of(brd, srvc, prd, rqc, modelc, rates)));

		PairValue rates2 = new PairValue();
		rates2.setKey(MasterType.RATE_TYPE.name());
		rates2.setValue(new LinkedHashSet<>(Set.of("2", "3")));
		CredentialItem credential3 = new CredentialItem();
		credential3.setId("3");
		credential3.setPriceChange(true);
		credential3.setAvailCost(true);
		credential3.setTest(true);
		credential3.setHotelX(true);
		credential3.setTagNationality(false);
		credential3.setCodes(new HashSet<>(Set.of(brd, srvc, prd, rqc, modelc, cous, rates2)));

		CredentialItem credential4 = new CredentialItem();
		credential4.setId("4");
		credential4.setPriceChange(true);
		credential4.setAvailCost(true);
		credential4.setTest(true);
		credential4.setHotelX(true);
		credential4.setTagNationality(false);
		credential4.setCodes(new HashSet<>(Set.of(brd, srvc, prd, rqc, modelc, cous)));

		return Set.of(this.credential1, credential2, credential3, credential4);
	}

	private Set<SupplierItem> getSuppliers() {
		PairValue brd = new PairValue();
		brd.setKey(MasterType.BRAND.name());
		brd.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue cous = new PairValue();
		cous.setKey(MasterType.COUNTRY.name());
		cous.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue modelc = new PairValue();
		modelc.setKey(MasterType.DISTRIBUTION_MODEL.name());
		modelc.setValue(new LinkedHashSet<>(Set.of("2", "1")));
		PairValue rates = new PairValue();
		rates.setKey(MasterType.RATE_TYPE.name());
		rates.setValue(new LinkedHashSet<>(Set.of("2", "VINC")));
		PairValue srvc = new PairValue();
		srvc.setKey(MasterType.SERVICE_TYPE.name());
		srvc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue bmodel = new PairValue();
		bmodel.setKey(MasterType.BUY_MODEL.name());
		bmodel.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue clpref = new PairValue();
		clpref.setKey(MasterType.CLIENT_PREF.name());
		clpref.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue user = new PairValue();
		user.setKey(MasterType.USERS.name());
		user.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue client = new PairValue();
		client.setKey(MasterType.AGENCY.name());
		client.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue top = new PairValue();
		top.setKey(MasterType.TOP_DESTINATION.name());
		top.setValue(new LinkedHashSet<>(Set.of("1")));

		MasterItem filter = new MasterItem();
		filter.setId("1");
		filter.setOwnProduct(true);

		ExternalCredentialItem newExt1 = new ExternalCredentialItem();
		newExt1.setUser("test");
		newExt1.setConnector("test");
		newExt1.setCheckIn(new DateRange());
		newExt1.setBook(new DateRange());
		newExt1.getCheckIn().setFrom("20/09/2020");
		newExt1.getCheckIn().setTo("27/09/2020");
		newExt1.getBook().setFrom("20/09/2020");
		newExt1.getBook().setTo("27/09/2020");

		this.group = new GroupCredentialItem();
		this.group.setId("1");
		this.group.setActive(true);
		this.group.setTest(true);
		this.group.setHotelX(HotelX.AMBOS);
		this.group.setTagNationality(false);
		this.group.setCodes(Set.of(brd, cous, modelc, rates));
		this.group.setAllMasters(Set.of("test"));
		this.group.setExternalConfig(new HashSet<>(Set.of(newExt1)));

		GroupCredentialItem group2 = new GroupCredentialItem();
		group2.setId("1");
		group2.setActive(true);
		group2.setTest(true);
		group2.setHotelX(HotelX.AMBOS);
		group2.setTagNationality(false);
		group2.setCodes(Set.of(brd, modelc, rates));
		group2.setAllMasters(Set.of("test"));
		group2.setExternalConfig(new HashSet<>(Set.of(newExt1)));

		this.supplier1 = new SupplierItem();
		this.supplier1.setId("1");
		this.supplier1.setStatus(SupplierStatus.ENABLED);
		this.supplier1.setCodes(Set.of(srvc, bmodel, clpref, user, client, top));
		this.supplier1.setSystem(filter);
		this.supplier1.setMultiHotel(true);
		this.supplier1.setTimeout(true);
		this.supplier1.setBlock(true);
		this.supplier1.setAvailCost(true);
		this.supplier1.setGiata("false");
		this.supplier1.setExternalSys(Set.of(this.group, group2));

		SupplierItem supplier2 = new SupplierItem();
		supplier2.setId("2");
		supplier2.setStatus(SupplierStatus.ENABLED);
		supplier2.setCodes(Set.of(srvc, bmodel, clpref, user, client, top));
		supplier2.setSystem(filter);
		supplier2.setMultiHotel(true);
		supplier2.setTimeout(true);
		supplier2.setBlock(true);
		supplier2.setAvailCost(true);
		supplier2.setGiata("false");
		supplier2.setExternalSys(Set.of(this.group));

		return Set.of(this.supplier1, supplier2);
	}
}
