package com.globalia.application.service.customer;

import com.globalia.HelperTest;
import com.globalia.application.repository.customer.SaveCustomerRedis;
import com.globalia.application.service.credential.Credentials;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MassiveItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveCustomerTest extends HelperTest {

	@InjectMocks
	private SaveCustomer saveCustomer;
	@Mock
	private Customer customer;
	@Mock
	private Credentials credentials;
	@Mock
	private SaveCustomerRedis save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.createItem(customer(), monitor()).getError());
	}

	@Test
	public void testCreateItemPriorityEmptyCredentials() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		item.getCustomer().setCodes(null);
		item.getCustomer().setPriority(true);

		when(this.credentials.getAllItems(any(), any())).thenReturn(new AllItemsResponse());
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.createItem(customer(), monitor()));
	}

	@Test
	public void testCreateItemPriorityDiffCredentials() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		item.getCustomer().setPriority(true);

		PairValue code2 = new PairValue();
		code2.setKey(MasterType.AGENCY_GROUP.name());
		code2.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue code3 = new PairValue();
		code3.setKey(MasterType.BRANCH_OFFICE.name());
		code3.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue code4 = new PairValue();
		code4.setKey(MasterType.AGENT.name());
		code4.setValue(new LinkedHashSet<>(Set.of("1")));

		item.getCustomer().setCodes(Set.of(code2, code3, code4));

		CredentialItem credential = new CredentialItem();
		credential.setId("1");
		credential.setClients(Set.of(new CustomerItem()));
		CredentialItem credential2 = new CredentialItem();
		credential2.setId("2");
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(new LinkedHashSet<>(Set.of(credential, credential2)));

		when(this.credentials.getAllItems(any(), any())).thenReturn(credentials);
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.createItem(customer(), monitor()));
	}

	@Test
	public void testCreateItemPriorityDiffCustomer() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		item.getCustomer().setCodes(null);
		item.getCustomer().setPriority(true);

		PairValue code = new PairValue();
		code.setKey(MasterType.AGENCY.name());
		code.setValue(new LinkedHashSet<>(Set.of("1")));

		CustomerItem custom = new CustomerItem();
		custom.setCodes(Set.of(code));

		code = new PairValue();
		code.setKey(MasterType.AGENCY.name());
		code.setValue(new LinkedHashSet<>(Set.of("2")));

		CustomerItem custom2 = new CustomerItem();
		custom2.setCodes(Set.of(code));
		custom2.setPriority(true);

		CredentialItem credential = new CredentialItem();
		credential.setId("2");
		credential.setClients(Set.of(custom, custom2));

		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(new LinkedHashSet<>(Set.of(credential)));

		when(this.credentials.getAllItems(any(), any())).thenReturn(credentials);
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.createItem(customer(), monitor()));
	}

	@Test
	public void testCreateItemPriority() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		item.getCustomer().setPriority(true);

		PairValue code = new PairValue();
		code.setKey(MasterType.AGENCY.name());
		code.setValue(new LinkedHashSet<>(Set.of("1")));
		item.getCustomer().setCodes(Set.of(code));

		CustomerItem custom = new CustomerItem();
		custom.setCodes(Set.of(code));
		custom.setPriority(true);

		CredentialItem credential = new CredentialItem();
		credential.setId("2");
		credential.setClients(Set.of(custom));

		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(new LinkedHashSet<>(Set.of(credential)));

		when(this.credentials.getAllItems(any(), any())).thenReturn(credentials);
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.createItem(customer(), monitor()));
	}

	@Test
	public void testCreateItemNoPriority() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		item.getCustomer().setCodes(null);
		item.getCustomer().setPriority(true);
		item.getCustomer().setDeleted(true);
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.createItem(customer(), monitor()));
	}

	@Test
	public void testCreateItemNoPriorityIsDeleted() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		item.getCustomer().setCodes(null);
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.createItem(customer(), monitor()));
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveCustomer.updateItem(customer(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveCustomer.deleteItem(customer(), monitor()).getError());
	}

	@Test
	public void testMassiveCustomersNull() {
		assertNull(this.saveCustomer.massive(new MassiveItem(), monitor()).getCustomers());
	}

	@Test
	public void testMassiveErrorRedis() {
		MassiveItem massive = new MassiveItem();
		massive.setCustomers(Set.of(customer()));

		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.massive(massive, monitor()).getCustomers());
	}

	@Test
	public void testMassive() {
		CustomerItem customer1 = customer();
		customer1.setId(null);
		customer1.setCredential("1");

		MassiveItem massive = new MassiveItem();
		massive.setCustomers(Set.of(customer(), customer1));
/*		SupplierItem supplier = new SupplierItem();

		MasterItem item = new MasterItem();
		item.setId("1");
		PairValue code = new PairValue();
		code.setKey(MasterType.AGENCY.name());
		code.setValue(new LinkedHashSet<>(Set.of("1")));
		supplier.setCodes(Set.of(code));

		SupplierItems suppliers = new SupplierItems();
		suppliers.setSuppliers(Set.of(supplier));*/

		//when(this.customer.getItem(anyString())).thenReturn(Set.of(this.customer));
		ItemResponse item = new ItemResponse();
		item.setCustomer(customer());
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.customer.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveCustomer.massive(massive, monitor()).getCustomers());
	}
}
