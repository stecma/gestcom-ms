package com.globalia.application.service.customer;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.repository.customer.GetCustomer;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomersTest extends HelperTest {

	@InjectMocks
	private Customers customers;
	@Mock
	private GetCustomer getCustomer;
	@Mock
	private Customer customer;
	@Mock
	private Errors error;
	@Mock
	private Suppliers getSuppliers;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemNullResponse() {
		when(this.getCustomer.getItem(anyString())).thenReturn(null);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.customers.getAllItems(customer(), monitor()).getError());
	}

	@Test
	public void testGetAllItemNoResponse() {
		when(this.getCustomer.getItem(anyString())).thenReturn(new HashSet<>());
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.customers.getAllItems(customer(), monitor()).getError());
	}

	@Test
	public void testGetAllItem() {
		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(customer()));
		assertNotNull(this.customers.getAllItems(customer(), true, monitor()));
	}

	@Test
	public void testGetAllItemComplete() {
		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(customer()));
		when(this.getSuppliers.getAllItems(any())).thenReturn(new AllItemsResponse());
		assertNotNull(this.customers.getAllItems(customer(), monitor()));
	}
}
