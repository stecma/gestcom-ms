package com.globalia.application.service.customer;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.Status;
import com.globalia.application.repository.customer.GetCustomer;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerTest extends HelperTest {

	@InjectMocks
	private Customer customer;
	@Mock
	private GetCustomer getCustomer;
	@Mock
	private Master master;
	@Mock
	private Status status;
	@Mock
	private Suppliers suppliers;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemNoResponse() {
		when(this.getCustomer.getItem(anyString())).thenReturn(null);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.customer.getItem(customer(), monitor()).getError());
	}

	@Test
	public void testGetItemSuppliersError() {
		CustomerItem custom = customer();
		custom.setMasters(null);

		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setError(new Error());

		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(custom));
		when(this.suppliers.getAllItems(any())).thenReturn(suppliers);
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.RED);
		ItemResponse response = this.customer.getItem(custom, monitor());
		assertNotNull(response);
		assertEquals(MatchLevel.RED, response.getCustomer().getStatus());
		assertFalse(response.getCustomer().isXsell());
	}

	@Test
	public void testGetItemREDNullMaster() {
		CustomerItem custom = customer();
		custom.setMasters(null);
		CustomerItem custom2 = customer();
		custom.setId("213");

		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(custom2, custom));
		when(this.suppliers.getAllItems(any())).thenReturn(new AllItemsResponse());
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.RED);
		when(this.master.getMaster(anyString(), any(), any())).thenReturn(null);
		ItemResponse response = this.customer.getItem(custom, monitor());
		assertNotNull(response);
		assertEquals(MatchLevel.RED, response.getCustomer().getStatus());
		assertFalse(response.getCustomer().isXsell());
	}

	@Test
	public void testGetItemNullSuppliers() {
		CustomerItem customer = customer();
		customer.setMasters(null);

		MasterItem item = new MasterItem();
		item.setId("2");

		PairValue code = new PairValue();
		code.setKey(MasterType.AGENCY.name());
		code.setValue(new LinkedHashSet<>(Set.of("1")));

		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(customer));
		when(this.master.getMaster(anyString(), any(), any())).thenReturn(item);
		when(this.suppliers.getAllItems(any())).thenReturn(new AllItemsResponse());
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.YELLOW);

		ItemResponse response = this.customer.getItem(customer, monitor());
		assertNotNull(response);
		assertEquals(MatchLevel.YELLOW, response.getCustomer().getStatus());
		assertFalse(response.getCustomer().isXsell());
	}

	@Test
	public void testGetItemSuppliersNoCodes() {
		CustomerItem customer = customer();
		customer.setMasters(null);

		MasterItem item = new MasterItem();
		item.setId("2");

		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setSuppliers(Set.of(new SupplierItem()));

		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(customer));
		when(this.master.getMaster(anyString(), any(), any())).thenReturn(item);
		when(this.suppliers.getAllItems(any())).thenReturn(suppliers);
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.YELLOW);

		ItemResponse response = this.customer.getItem(customer, monitor());
		assertNotNull(response);
		assertEquals(MatchLevel.YELLOW, response.getCustomer().getStatus());
		assertFalse(response.getCustomer().isXsell());
	}

	@Test
	public void testGetItemYellow() {
		CustomerItem customer = customer();
		customer.setMasters(null);
		customer.setCodes(null);

		MasterItem item = new MasterItem();
		item.setId("2");

		PairValue code = new PairValue();
		code.setKey(MasterType.AGENCY.name());
		code.setValue(new LinkedHashSet<>(Set.of("1")));

		SupplierItem supplier = new SupplierItem();
		supplier.setCodes(Set.of(code));
		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setSuppliers(Set.of(supplier));

		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(customer));
		when(this.master.getMaster(anyString(), any(), any())).thenReturn(item);
		when(this.suppliers.getAllItems(any())).thenReturn(suppliers);
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.YELLOW);

		ItemResponse response = this.customer.getItem(customer, monitor());
		assertNotNull(response);
		assertEquals(MatchLevel.YELLOW, response.getCustomer().getStatus());
		assertFalse(response.getCustomer().isXsell());
	}

	@Test
	public void testGetItem() {
		CustomerItem customer = customer();
		customer.setMasters(null);

		MasterItem item = new MasterItem();
		item.setId("1");

		PairValue code = new PairValue();
		code.setKey(MasterType.AGENCY.name());
		code.setValue(new LinkedHashSet<>(Set.of("1")));

		SupplierItem supplier = new SupplierItem();
		supplier.setCodes(Set.of(code));
		AllItemsResponse suppliers = new AllItemsResponse();
		suppliers.setSuppliers(Set.of(supplier));

		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.GREEN);
		when(this.getCustomer.getItem(anyString())).thenReturn(Set.of(customer));
		when(this.master.getMaster(anyString(), any(), any())).thenReturn(item);
		when(this.suppliers.getAllItems(any())).thenReturn(suppliers);
		ItemResponse response = this.customer.getItem(customer, monitor());
		assertNotNull(response);
		assertEquals(MatchLevel.GREEN, response.getCustomer().getStatus());
		assertTrue(response.getCustomer().isXsell());
	}
}
