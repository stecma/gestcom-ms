package com.globalia.application.service.group;

import com.globalia.HelperTest;
import com.globalia.application.repository.group.GetGroups;
import com.globalia.application.service.group.Groups;
import com.globalia.dto.credential.AllItemsResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupsTest extends HelperTest {

	@InjectMocks
	private Groups groups;
	@Mock
	private GetGroups getGroups;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemsNotImplemented() {
		assertNull(this.groups.getAllItems(group(), monitor()));
	}

	@Test
	public void testGetAllItem() {
		when(this.getGroups.getAllItems(any(), anyBoolean(), any())).thenReturn(new AllItemsResponse());
		assertNull(this.groups.getAllItems(group(), true, monitor()).getError());
	}
}
