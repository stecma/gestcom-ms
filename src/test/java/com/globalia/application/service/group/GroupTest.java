package com.globalia.application.service.group;

import com.globalia.HelperTest;
import com.globalia.application.repository.group.GetAssignGroup;
import com.globalia.application.repository.group.GetGroup;
import com.globalia.application.service.group.Group;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupTest extends HelperTest {

	@InjectMocks
	private Group group;
	@Mock
	private GetGroup getGroup;
	@Mock
	private GetAssignGroup assignGroup;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItem() {
		when(this.getGroup.getItem(any(), any())).thenReturn(null);
		assertNull(this.group.getItem(group(), monitor()));
	}

	@Test
	public void testGetAssignItem() {
		when(this.assignGroup.getItem(anyString())).thenReturn(new HashSet<>());
		assertTrue(this.group.getAssignItem("test").isEmpty());
	}
}
