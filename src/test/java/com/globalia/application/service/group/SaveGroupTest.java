package com.globalia.application.service.group;

import com.globalia.HelperTest;
import com.globalia.application.repository.group.SaveGroupRedis;
import com.globalia.dto.credential.ItemResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveGroupTest extends HelperTest {

	@InjectMocks
	private SaveGroup saveGroup;
	@Mock
	private SaveGroupRedis save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveGroup.createItem(group(), monitor()).getError());
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveGroup.updateItem(group(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveGroup.deleteItem(group(), monitor()).getError());
	}

	@Test
	public void testAddAssignItem() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItemAssign(anyString(), any(), any())).thenReturn(item);
		assertNull(this.saveGroup.addAssignItem("credential", group(), monitor()).getError());
	}
}
