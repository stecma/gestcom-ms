package com.globalia.application.service.supplier;

import com.globalia.HelperTest;
import com.globalia.application.repository.supplier.SaveSupplierByZoneRedis;
import com.globalia.application.service.supplier.SaveSupplierByZone;
import com.globalia.dto.credential.ItemResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveSupplierByZoneTest extends HelperTest {

	@InjectMocks
	private SaveSupplierByZone supplierByZone;
	@Mock
	private SaveSupplierByZoneRedis save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreate() {
		when(this.save.addItem(any(), any())).thenReturn(new ItemResponse());
		assertNull(this.supplierByZone.createItem(supplierByZone(), monitor()).getError());
	}

	@Test
	public void testUpdate() {
		when(this.save.addItem(any(), any())).thenReturn(new ItemResponse());
		assertNull(this.supplierByZone.updateItem(supplierByZone(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		when(this.save.addItem(any(), any())).thenReturn(new ItemResponse());
		assertNull(this.supplierByZone.deleteItem(supplierByZone(), monitor()).getError());
	}
}
