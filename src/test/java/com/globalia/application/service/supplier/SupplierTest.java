package com.globalia.application.service.supplier;

import com.globalia.HelperTest;
import com.globalia.application.repository.supplier.GetSupplier;
import com.globalia.application.service.connector.Connectors;
import com.globalia.application.service.group.Group;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.master.Master;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SupplierTest extends HelperTest {

	@InjectMocks
	private Supplier supplier;
	@Mock
	private GetSupplier getSupplier;
	@Mock
	private Master master;
	@Mock
	private SupplierByZones supplierByZones;
	@Mock
	private Groups groups;
	@Mock
	private Connectors connectors;
	@Mock
	private Group group;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.getSupplier.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.supplier.getItem(supplier(), monitor()).getError());
	}

	@Test
	public void testGetItemNoAddMasterNoGroups() {
		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());
		AllItemsResponse groups = new AllItemsResponse();
		groups.setError(new Error());

		when(this.master.getMaster(anyString(), any(), any())).thenReturn(master(MasterType.EXTERNAL_SYSTEM, "1"));
		when(this.groups.getAllItems(any(), anyBoolean(), any())).thenReturn(groups);
		when(this.getSupplier.getItem(any(), any())).thenReturn(item);
		assertNull(this.supplier.getItem(supplier(), false, monitor()).getError());
	}

	@Test
	public void testGetItemNoAddMasterNoExternals() {
		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());
		AllItemsResponse groups = new AllItemsResponse();
		groups.setGroups(item.getSupplier().getExternalSys());
		AllItemsResponse connectors = new AllItemsResponse();
		connectors.setError(new Error());

		when(this.master.getMaster(anyString(), any(), any())).thenReturn(master(MasterType.EXTERNAL_SYSTEM, "1"));
		when(this.groups.getAllItems(any(), anyBoolean(), any())).thenReturn(groups);
		when(this.connectors.getAllItems(any(), anyBoolean(), any())).thenReturn(groups);
		when(this.getSupplier.getItem(any(), any())).thenReturn(item);
		assertNull(this.supplier.getItem(supplier(), false, monitor()).getError());
	}

	@Test
	public void testGetItemDisable() {
		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());

		GroupCredentialItem newGroup = new GroupCredentialItem();
		newGroup.setId("1");
		AllItemsResponse groups = new AllItemsResponse();
		groups.setGroups(Set.of(newGroup));

		ExternalCredentialItem newExt = new ExternalCredentialItem();
		newExt.setId("1");
		AllItemsResponse connectors = new AllItemsResponse();
		connectors.setExternals(Set.of(newExt));

		when(this.master.getMaster(anyString(), any(), any())).thenReturn(master(MasterType.EXTERNAL_SYSTEM, "1"));
		when(this.groups.getAllItems(any(), anyBoolean(), any())).thenReturn(groups);
		when(this.connectors.getAllItems(any(), anyBoolean(), any())).thenReturn(connectors);
		when(this.getSupplier.getItem(any(), any())).thenReturn(item);

		ItemResponse supplier = this.supplier.getItem(supplier(), false, monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
		assertEquals(SupplierStatus.DISABLED, supplier.getSupplier().getStatus());
	}

	@Test
	public void testGetItemDisable2() {
		masterSupplier(this.supplier, "serviceType,SERVICE_TYPE,false#buyModel,BUY_MODEL,false#user,USERS,false#clientPref,CLIENT_PREF,false#clients,AGENCY,true#topDestinations,TOP_DESTINATION,true");
		masterGroup(this.supplier, "countries,COUNTRY,true#excludedCountries,COUNTRY¬exc,true#currencies,CURRENCY,true#distriModels,DISTRIBUTION_MODEL,true#rates,RATE_TYPE,true#brand,BRAND,false#clientType,CLIENT_TYPE,false");
		masterConnector(this.supplier, "corpora,CORPORATION,false#invoicingType,BILLING_TYPE,false");

		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());

		GroupCredentialItem newGroup = new GroupCredentialItem();
		newGroup.setId("1");
		newGroup.setActive(true);
		newGroup.setTest(true);
		AllItemsResponse groups = new AllItemsResponse();
		groups.setGroups(Set.of(newGroup));

		ExternalCredentialItem newExt = new ExternalCredentialItem();
		newExt.setId("1");
		AllItemsResponse connectors = new AllItemsResponse();
		connectors.setExternals(Set.of(newExt));

		when(this.master.getMaster(anyString(), any(), any())).thenReturn(master(MasterType.EXTERNAL_SYSTEM, "1¬1"));
		when(this.groups.getAllItems(any(), anyBoolean(), any())).thenReturn(groups);
		when(this.connectors.getAllItems(any(), anyBoolean(), any())).thenReturn(connectors);
		when(this.getSupplier.getItem(any(), any())).thenReturn(item);
		when(this.supplierByZones.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.supplier.getItem(supplier(), true, monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
		assertEquals(SupplierStatus.DISABLED, supplier.getSupplier().getStatus());
	}

	@Test
	public void testGetItemEnable() {
		masterSupplier(this.supplier, "serviceType,SERVICE_TYPE,false#buyModel,BUY_MODEL,false#user,USERS,false#clientPref,CLIENT_PREF,false#clients,AGENCY,true#topDestinations,TOP_DESTINATION,true");
		masterGroup(this.supplier, "countries,COUNTRY,true#excludedCountries,COUNTRY¬exc,true#currencies,CURRENCY,true#distriModels,DISTRIBUTION_MODEL,true#rates,RATE_TYPE,true#brand,BRAND,false#clientType,CLIENT_TYPE,false");
		masterConnector(this.supplier, "corpora,CORPORATION,false#invoicingType,BILLING_TYPE,false");

		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());

		GroupCredentialItem newGroup = new GroupCredentialItem();
		newGroup.setId("1");
		newGroup.setActive(true);
		AllItemsResponse groups = new AllItemsResponse();
		groups.setGroups(Set.of(newGroup));

		ExternalCredentialItem newExt = new ExternalCredentialItem();
		newExt.setId("1");
		AllItemsResponse connectors = new AllItemsResponse();
		connectors.setExternals(Set.of(newExt));

		when(this.master.getMaster(anyString(), any(), any())).thenReturn(master(MasterType.EXTERNAL_SYSTEM, "1"));
		when(this.groups.getAllItems(any(), anyBoolean(), any())).thenReturn(groups);
		when(this.connectors.getAllItems(any(), anyBoolean(), any())).thenReturn(connectors);
		when(this.getSupplier.getItem(any(), any())).thenReturn(item);
		when(this.supplierByZones.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.supplier.getItem(supplier(), true, monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
		assertEquals(SupplierStatus.ENABLED, supplier.getSupplier().getStatus());
	}

	@Test
	public void testGetItemPartially() {
		masterSupplier(this.supplier, "serviceType,SERVICE_TYPE,false#buyModel,BUY_MODEL,false#user,USERS,false#clientPref,CLIENT_PREF,false#clients,AGENCY,true#topDestinations,TOP_DESTINATION,true");
		masterGroup(this.supplier, "countries,COUNTRY,true#excludedCountries,COUNTRY¬exc,true#currencies,CURRENCY,true#distriModels,DISTRIBUTION_MODEL,true#rates,RATE_TYPE,true#brand,BRAND,false#clientType,CLIENT_TYPE,false");
		masterConnector(this.supplier, "corpora,CORPORATION,false#invoicingType,BILLING_TYPE,false");

		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());

		GroupCredentialItem newGroup = new GroupCredentialItem();
		newGroup.setId("1");
		newGroup.setActive(true);
		GroupCredentialItem updGroup = new GroupCredentialItem();
		updGroup.setId("1");
		updGroup.setTest(true);
		AllItemsResponse groups = new AllItemsResponse();
		groups.setGroups(Set.of(newGroup, updGroup));

		ExternalCredentialItem newExt = new ExternalCredentialItem();
		newExt.setId("1");
		AllItemsResponse connectors = new AllItemsResponse();
		connectors.setExternals(Set.of(newExt));

		when(this.master.getMaster(anyString(), any(), any())).thenReturn(master(MasterType.EXTERNAL_SYSTEM, "1"));
		when(this.groups.getAllItems(any(), anyBoolean(), any())).thenReturn(groups);
		when(this.connectors.getAllItems(any(), anyBoolean(), any())).thenReturn(connectors);
		when(this.getSupplier.getItem(any(), any())).thenReturn(item);
		when(this.supplierByZones.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.supplier.getItem(supplier(), true, monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
		assertEquals(SupplierStatus.PARTIALLY, supplier.getSupplier().getStatus());
	}

	@Test
	public void testGetGroupError() {
		SupplierItem supplier = new SupplierItem();
		supplier.setId("1¬1");

		ItemResponse group = new ItemResponse();
		group.setError(new Error());

		when(this.group.getItem(any(), any())).thenReturn(group);
		assertNotNull(this.supplier.getGroup(supplier, monitor()).getError());
	}

	@Test
	public void testGetGroup() {
		SupplierItem supplier = new SupplierItem();
		supplier.setId("1¬1");

		GroupCredentialItem group = new GroupCredentialItem();
		group.setSupplierId("1");
		group.setId("1");
		ItemResponse item = new ItemResponse();
		item.setGroup(group);
		ExternalCredentialItem newExt = new ExternalCredentialItem();
		newExt.setId("1");
		AllItemsResponse connectors = new AllItemsResponse();
		connectors.setExternals(Set.of(newExt));

		when(this.group.getItem(any(), any())).thenReturn(item);
		when(this.connectors.getAllItems(any(), anyBoolean(), any())).thenReturn(connectors);
		assertNull(this.supplier.getGroup(supplier, monitor()).getError());
	}

	@Test
	public void testGetMapKey() {
		when(this.getSupplier.getMapKey(anyString(), any())).thenReturn(new ItemResponse());
		assertNotNull(this.supplier.getMapKey(supplier(), monitor()));
	}
}
