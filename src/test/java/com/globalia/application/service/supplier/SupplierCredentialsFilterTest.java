package com.globalia.application.service.supplier;

import com.globalia.Dates;
import com.globalia.HelperTest;
import com.globalia.dto.DateRange;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ExternalFilter;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.GroupFilter;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierFilter;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.HotelX;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SupplierCredentialsFilterTest extends HelperTest {

	@InjectMocks
	private SupplierCredentialFilter supplierCredentialFilter;
	@Mock
	private Dates dates;

	private SupplierItem supplier;
	private GroupCredentialItem newGroup;
	private ExternalCredentialItem newExt;
	private MasterItem system;
	private Monitor monitor;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		PairValue srvType = new PairValue();
		srvType.setKey(MasterType.SERVICE_TYPE.name());
		srvType.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue sys = new PairValue();
		sys.setKey(MasterType.EXTERNAL_SYSTEM.name());
		sys.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue mce = new PairValue();
		mce.setKey(MasterType.BUY_MODEL.name());
		mce.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue user = new PairValue();
		user.setKey(MasterType.USERS.name());
		user.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue cpc = new PairValue();
		cpc.setKey(MasterType.CLIENT_PREF.name());
		cpc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue agc = new PairValue();
		agc.setKey(MasterType.AGENCY.name());
		agc.setValue(new LinkedHashSet<>(Set.of("1")));
		// Pair value para los grupos de credenciales
		PairValue cous = new PairValue();
		cous.setKey(MasterType.COUNTRY.name());
		cous.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue rates = new PairValue();
		rates.setKey(MasterType.RATE_TYPE.name());
		rates.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue models = new PairValue();
		models.setKey(MasterType.DISTRIBUTION_MODEL.name());
		models.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue divs = new PairValue();
		divs.setKey(MasterType.CURRENCY.name());
		divs.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue brand = new PairValue();
		brand.setKey(MasterType.BRAND.name());
		brand.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue cliType = new PairValue();
		cliType.setKey(MasterType.CLIENT_TYPE.name());
		cliType.setValue(new LinkedHashSet<>(Set.of("1")));
		// Pair value para las credenciales externas
		PairValue corp = new PairValue();
		corp.setKey(MasterType.CORPORATION.name());
		corp.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue invoice = new PairValue();
		invoice.setKey(MasterType.BILLING_TYPE.name());
		invoice.setValue(new LinkedHashSet<>(Set.of("1")));

		MasterItem master = new MasterItem();
		master.setId("1");
		// Credenciales externas
		this.newExt = new ExternalCredentialItem();
		this.newExt.setUser("test");
		this.newExt.setConnector("test");
		this.newExt.setCheckIn(new DateRange());
		this.newExt.setBook(new DateRange());
		this.newExt.getCheckIn().setFrom("20/09/2020");
		this.newExt.getCheckIn().setTo("27/09/2020");
		this.newExt.getBook().setFrom("20/09/2020");
		this.newExt.getBook().setTo("27/09/2020");
		this.newExt.setCreate(true);
		this.newExt.setCorporation(master);
		this.newExt.setInvoicingType(master);
		this.newExt.setCodes(new HashSet<>());
		this.newExt.getCodes().add(corp);
		this.newExt.getCodes().add(invoice);

		ExternalCredentialItem updExt = new ExternalCredentialItem();
		updExt.setCorporation(master);
		updExt.setInvoicingType(master);

		ExternalCredentialItem delExt = new ExternalCredentialItem();
		delExt.setDeleted(true);
		delExt.setCorporation(master);
		delExt.setInvoicingType(master);

		// Grupos de credenciales
		this.newGroup = new GroupCredentialItem();
		this.newGroup.setSysName("test");
		this.newGroup.setCreate(true);
		this.newGroup.setRates(Set.of(master));
		this.newGroup.setCurrencies(Set.of(master));
		this.newGroup.setDistriModels(Set.of(master));
		this.newGroup.setCountries(Set.of(master));
		this.newGroup.setClientType(master);
		this.newGroup.setHotelX(HotelX.AMBOS);
		this.newGroup.setBrand(master);
		this.newGroup.setCodes(new HashSet<>());
		this.newGroup.getCodes().add(cous);
		this.newGroup.getCodes().add(rates);
		this.newGroup.getCodes().add(models);
		this.newGroup.getCodes().add(divs);
		this.newGroup.getCodes().add(brand);
		this.newGroup.getCodes().add(cliType);
		this.newGroup.setExternalConfig(Set.of(this.newExt, updExt, delExt));

		GroupCredentialItem updGroup = new GroupCredentialItem();
		updGroup.setRates(Set.of(master));
		updGroup.setCurrencies(Set.of(master));
		updGroup.setDistriModels(Set.of(master));
		updGroup.setCountries(Set.of(master));
		updGroup.setHotelX(HotelX.AMBOS);

		GroupCredentialItem delGroup = new GroupCredentialItem();
		delGroup.setDeleted(true);
		delGroup.setRates(Set.of(master));
		delGroup.setCurrencies(Set.of(master));
		delGroup.setDistriModels(Set.of(master));
		delGroup.setCountries(Set.of(master));
		delGroup.setHotelX(HotelX.AMBOS);

		MasterItem filter = new MasterItem();
		filter.setId("1");
		filter.setOwnProduct(true);
		this.supplier = new SupplierItem();
		this.supplier.setId("1");
		this.supplier.setStatus(SupplierStatus.DISABLED);
		this.supplier.setCodes(new HashSet<>());
		this.supplier.getCodes().add(srvType);
		this.supplier.getCodes().add(sys);
		this.supplier.getCodes().add(mce);
		this.supplier.getCodes().add(user);
		this.supplier.getCodes().add(cpc);
		this.supplier.getCodes().add(agc);
		this.supplier.setBuyModel(filter);
		this.supplier.setClientPref(filter);
		this.supplier.setUser(filter);
		this.supplier.setServiceType(filter);
		this.supplier.setSystem(filter);
		this.supplier.setClients(Set.of(filter));
		this.supplier.setTopDestinations(Set.of(filter));
		this.supplier.setExternalSys(Set.of(this.newGroup, updGroup, delGroup));
		this.supplier.setGiata("false");

		this.system = new MasterItem();
		this.system.setId("1¬1");
		this.system.setOwnProduct(true);
		this.system.setEntity(MasterType.EXTERNAL_SYSTEM.name());
		this.system.setSapCode("1");
		this.system.setServiceOther("1");
		this.system.setWbdsCode("1");

		dateTo(SupplierCredentialFilter.class, this.supplierCredentialFilter, false);
		timeTo(SupplierCredentialFilter.class, this.supplierCredentialFilter, false);
		timeFrom(SupplierCredentialFilter.class, this.supplierCredentialFilter, false);
		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());
	}

	@Test
	public void testFilterWrong() {
		MasterItem filter2 = new MasterItem();
		filter2.setId("2");

		SupplierFilter filter = new SupplierFilter();
		filter.setSupplier("1");
		filter.setSubSupplier("1");
		filter.setBuyModel(this.supplier.getBuyModel().getId());
		filter.setClientPref(this.supplier.getClientPref().getId());
		filter.setUser(this.supplier.getUser().getId());
		filter.setServiceType(this.supplier.getServiceType().getId());
		filter.setSystem(this.supplier.getSystem().getId());
		filter.setClient(filter2.getId());
		filter.setOwnProduct("true");
		filter.setStatus(SupplierStatus.DISABLED.name());

		this.supplier.setTopDestinations(null);

		assertFalse(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}

	@Test
	public void testFilterNoGroup() {
		GroupFilter grpFilter = new GroupFilter();
		grpFilter.setBrand(this.newGroup.getBrand().getId());
		grpFilter.setClientType(this.newGroup.getClientType().getId());
		grpFilter.setActive("true");
		grpFilter.setByDefault("true");
		grpFilter.setTest("true");
		grpFilter.setHotelx(HotelX.AMBOS.name());
		grpFilter.setTagdiv("true");
		grpFilter.setCurrency(this.newGroup.getCurrencies().iterator().next().getId());
		grpFilter.setTagnac("true");
		grpFilter.setCountry(this.newGroup.getCountries().iterator().next().getId());
		grpFilter.setDistributionModel(this.newGroup.getDistriModels().iterator().next().getId());
		grpFilter.setRates(this.newGroup.getRates().iterator().next().getId());

		SupplierFilter filter = new SupplierFilter();
		filter.setSupplier("1");
		filter.setSubSupplier("1");
		filter.setBuyModel(this.supplier.getBuyModel().getId());
		filter.setClientPref(this.supplier.getClientPref().getId());
		filter.setUser(this.supplier.getUser().getId());
		filter.setServiceType(this.supplier.getServiceType().getId());
		filter.setSystem(this.supplier.getSystem().getId());
		filter.setClient(this.supplier.getClients().iterator().next().getId());
		filter.setTopDestination(this.supplier.getTopDestinations().iterator().next().getId());
		filter.setStatus(SupplierStatus.DISABLED.name());
		filter.setOwnProduct("true");
		filter.setOtherSrv("1");
		filter.setSapCode("1");
		filter.setWbds("1");
		filter.setGroupFilter(grpFilter);

		PairValue top = new PairValue();
		top.setKey(MasterType.TOP_DESTINATION.name());
		top.setValue(new LinkedHashSet<>(Set.of("1")));
		this.supplier.getCodes().add(top);

		assertFalse(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}

	@Test
	public void testFilterNoExternalParseError() {
		ExternalFilter extFilter = new ExternalFilter();
		extFilter.setCorp(this.newExt.getCorporation().getId());
		extFilter.setUser(this.newExt.getUser());
		extFilter.setFactType(this.newExt.getInvoicingType().getId());
		extFilter.setConnector("false");
		extFilter.setCheckIn(new DateRange());
		extFilter.setBook(new DateRange());
		extFilter.getCheckIn().setFrom("20/09/2020");
		extFilter.getCheckIn().setTo("27/09/2020");
		extFilter.getBook().setFrom("20/09/2020");
		extFilter.getBook().setTo("27/09/2020");

		SupplierFilter filter = new SupplierFilter();
		filter.setSupplier("1");
		filter.setSubSupplier("1");
		filter.setBuyModel(this.supplier.getBuyModel().getId());
		filter.setClientPref(this.supplier.getClientPref().getId());
		filter.setUser(null);
		filter.setServiceType(this.supplier.getServiceType().getId());
		filter.setSystem(this.supplier.getSystem().getId());
		filter.setClient(this.supplier.getClients().iterator().next().getId());
		filter.setTopDestination(this.supplier.getTopDestinations().iterator().next().getId());
		filter.setStatus(SupplierStatus.DISABLED.name());
		filter.setOwnProduct("true");
		filter.setOtherSrv("1");
		filter.setSapCode("1");
		filter.setWbds("1");
		filter.setExternalFilter(extFilter);

		PairValue top = new PairValue();
		top.setKey(MasterType.TOP_DESTINATION.name());
		top.setValue(new LinkedHashSet<>(Set.of("1")));
		this.supplier.getCodes().add(top);

		when(this.dates.getDateTime(anyString(), anyString(), any(Monitor.class))).thenThrow(ValidateException.class);
		assertFalse(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}

	@Test
	public void testFilterNoExternal() {
		ExternalFilter extFilter = new ExternalFilter();
		extFilter.setCorp(this.newExt.getCorporation().getId());
		extFilter.setUser(this.newExt.getUser());
		extFilter.setFactType(this.newExt.getInvoicingType().getId());
		extFilter.setConnector("false");
		extFilter.setCheckIn(new DateRange());
		extFilter.setBook(new DateRange());
		extFilter.getCheckIn().setFrom("20/09/2020");
		extFilter.getCheckIn().setTo("27/09/2020");
		extFilter.getBook().setFrom("20/09/2020");
		extFilter.getBook().setTo("27/09/2020");

		SupplierFilter filter = new SupplierFilter();
		filter.setSupplier("1");
		filter.setSubSupplier("1");
		filter.setBuyModel(this.supplier.getBuyModel().getId());
		filter.setClientPref(this.supplier.getClientPref().getId());
		filter.setUser(null);
		filter.setServiceType(this.supplier.getServiceType().getId());
		filter.setSystem(this.supplier.getSystem().getId());
		filter.setClient(this.supplier.getClients().iterator().next().getId());
		filter.setTopDestination(this.supplier.getTopDestinations().iterator().next().getId());
		filter.setStatus(SupplierStatus.DISABLED.name());
		filter.setOwnProduct("true");
		filter.setOtherSrv("1");
		filter.setSapCode("1");
		filter.setWbds("1");
		filter.setExternalFilter(extFilter);

		PairValue top = new PairValue();
		top.setKey(MasterType.TOP_DESTINATION.name());
		top.setValue(new LinkedHashSet<>(Set.of("1")));
		this.supplier.getCodes().add(top);

		when(this.dates.rangeOverlap(any(), any(), any(), any())).thenReturn(false);
		assertFalse(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}

	@Test
	public void testFilterParseError() {
		ExternalFilter extFilter = new ExternalFilter();
		extFilter.setCorp(this.newExt.getCorporation().getId());
		extFilter.setUser(this.newExt.getUser());
		extFilter.setFactType(this.newExt.getInvoicingType().getId());
		extFilter.setConnector("false");
		extFilter.setCheckIn(new DateRange());
		extFilter.setBook(new DateRange());
		extFilter.getCheckIn().setFrom("20/09/2020");
		extFilter.getCheckIn().setTo("27/09/2020");
		extFilter.getBook().setFrom("20/09/2020");
		extFilter.getBook().setTo("27/09/2020");

		SupplierFilter filter = new SupplierFilter();
		filter.setSupplier("1");
		filter.setSubSupplier("1");
		filter.setBuyModel(this.supplier.getBuyModel().getId());
		filter.setClientPref(this.supplier.getClientPref().getId());
		filter.setUser(null);
		filter.setServiceType(this.supplier.getServiceType().getId());
		filter.setSystem(this.supplier.getSystem().getId());
		filter.setClient(this.supplier.getClients().iterator().next().getId());
		filter.setTopDestination(this.supplier.getTopDestinations().iterator().next().getId());
		filter.setStatus(SupplierStatus.DISABLED.name());
		filter.setOwnProduct("true");
		filter.setOtherSrv("1");
		filter.setSapCode("1");
		filter.setWbds("1");
		filter.setExternalFilter(extFilter);

		PairValue top = new PairValue();
		top.setKey(MasterType.TOP_DESTINATION.name());
		top.setValue(new LinkedHashSet<>(Set.of("1")));
		this.supplier.getCodes().add(top);

		when(this.dates.getDateTime(anyString(), anyString(), any())).thenThrow(ValidateException.class);
		assertFalse(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}

	@Test
	public void testFilter() {
		GroupFilter grpFilter = new GroupFilter();
		grpFilter.setBrand(this.newGroup.getBrand().getId());
		grpFilter.setClientType(this.newGroup.getClientType().getId());
		grpFilter.setName("test");
		grpFilter.setActive("false");
		grpFilter.setByDefault("false");
		grpFilter.setTest("false");
		grpFilter.setHotelx(HotelX.AMBOS.name());
		grpFilter.setTagdiv("false");
		grpFilter.setCurrency(this.newGroup.getCurrencies().iterator().next().getId());
		grpFilter.setTagnac("false");
		grpFilter.setCountry(this.newGroup.getCountries().iterator().next().getId());
		grpFilter.setDistributionModel(this.newGroup.getDistriModels().iterator().next().getId());
		grpFilter.setRates(this.newGroup.getRates().iterator().next().getId());

		ExternalFilter extFilter = new ExternalFilter();
		extFilter.setCorp(this.newExt.getCorporation().getId());
		extFilter.setUser(this.newExt.getUser());
		extFilter.setFactType(this.newExt.getInvoicingType().getId());
		extFilter.setConnector("test");
		extFilter.setCheckIn(new DateRange());
		extFilter.setBook(new DateRange());
		extFilter.getCheckIn().setFrom("20/09/2020");
		extFilter.getCheckIn().setTo("27/09/2020");
		extFilter.getBook().setFrom("20/09/2020");
		extFilter.getBook().setTo("27/09/2020");

		SupplierFilter filter = new SupplierFilter();
		filter.setSupplier("1");
		filter.setSubSupplier("1");
		filter.setBuyModel(this.supplier.getBuyModel().getId());
		filter.setClientPref(this.supplier.getClientPref().getId());
		filter.setUser(null);
		filter.setServiceType(this.supplier.getServiceType().getId());
		filter.setSystem(this.supplier.getSystem().getId());
		filter.setClient(this.supplier.getClients().iterator().next().getId());
		filter.setTopDestination(this.supplier.getTopDestinations().iterator().next().getId());
		filter.setStatus(SupplierStatus.DISABLED.name());
		filter.setOwnProduct("true");
		filter.setOtherSrv("1");
		filter.setSapCode("1");
		filter.setWbds("1");
		filter.setXsell("false");
		filter.setMultiHotel("false");
		filter.setAvailCost("false");
		filter.setGeneralRegimen("false");
		filter.setSla("false");
		filter.setGiata("false");
		filter.setTimeout("false");
		filter.setGroupFilter(grpFilter);
		filter.setExternalFilter(extFilter);

		PairValue top = new PairValue();
		top.setKey(MasterType.TOP_DESTINATION.name());
		top.setValue(new LinkedHashSet<>(Set.of("1")));
		this.supplier.getCodes().add(top);

		when(this.dates.rangeOverlap(any(), any(), any(), any())).thenReturn(true);
		assertTrue(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}

	@Test
	public void testFilterOnlyGroup() {
		GroupFilter grpFilter = new GroupFilter();
		grpFilter.setBrand(this.newGroup.getBrand().getId());

		SupplierFilter filter = new SupplierFilter();
		filter.setGroupFilter(grpFilter);

		when(this.dates.rangeOverlap(any(), any(), any(), any())).thenReturn(true);
		assertTrue(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}

	@Test
	public void testFilterOnlyExternal() {
		ExternalFilter extFilter = new ExternalFilter();
		extFilter.setCorp(this.newExt.getCorporation().getId());

		SupplierFilter filter = new SupplierFilter();
		filter.setExternalFilter(extFilter);

		when(this.dates.rangeOverlap(any(), any(), any(), any())).thenReturn(true);
		assertTrue(this.supplierCredentialFilter.filter(filter, this.supplier, this.system, this.monitor));
	}
}
