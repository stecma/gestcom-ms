package com.globalia.application.service.supplier;

import com.globalia.HelperTest;
import com.globalia.application.repository.supplier.SaveSupplierRedis;
import com.globalia.application.service.connector.SaveConnector;
import com.globalia.application.service.group.SaveGroup;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.supplier.SaveSupplier;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveSupplierTest extends HelperTest {

	@InjectMocks
	private SaveSupplier saveSupplier;
	@Mock
	private SaveSupplierRedis save;
	@Mock
	private Supplier supplier;
	@Mock
	private SaveGroup saveGroup;
	@Mock
	private SaveConnector saveConnector;
	@Mock
	private Master master;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveSupplier.createItem(supplier(), monitor()).getError());
	}

	@Test
	public void testCreate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.supplier.getItem(any(), any())).thenReturn(item);
		when(this.saveGroup.createItem(any(), any())).thenReturn(item);
		when(this.saveConnector.createItem(any(), any())).thenReturn(item);
		assertNull(this.saveSupplier.createItem(supplier(), monitor()).getError());
	}

	@Test
	public void testUpdateError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveSupplier.updateItem(supplier(), monitor()).getError());
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.supplier.getItem(any(), any())).thenReturn(item);
		when(this.saveGroup.createItem(any(), any())).thenReturn(item);
		when(this.saveConnector.createItem(any(), any())).thenReturn(item);
		assertNull(this.saveSupplier.updateItem(supplier(), monitor()).getError());
	}

	@Test
	public void testDeleteError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveSupplier.deleteItem(supplier(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.saveGroup.createItem(any(), any())).thenReturn(item);
		when(this.saveConnector.createItem(any(), any())).thenReturn(item);
		assertNull(this.saveSupplier.deleteItem(supplier(), monitor()).getError());
	}

	@Test
	public void testUpdateSapError() {
		SupplierItem supplier = supplier();
		ItemResponse master = new ItemResponse();
		master.setError(new Error());
		when(this.master.getItem(any(), any())).thenReturn(master);
		assertNotNull(this.saveSupplier.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), true, monitor()).getError());
	}

	@Test
	public void testUpdateSap() {
		SupplierItem supplier = supplier();
		supplier.getSystem().setId("1¬1");
		ItemResponse master = new ItemResponse();
		master.setMaster(new MasterItem());
		when(this.master.getItem(any(), any())).thenReturn(master);
		assertNull(this.saveSupplier.updateSap(supplier.getSystem().getId(), supplier.getSystem().getSapCode(), true, monitor()).getError());
	}

	@Test
	public void testUpdateServiceError() {
		SupplierItem supplier = supplier();
		ItemResponse master = new ItemResponse();
		master.setError(new Error());
		when(this.master.getItem(any(), any())).thenReturn(master);
		assertNotNull(this.saveSupplier.updateService(supplier.getSystem().getId(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther(), monitor()).getError());
	}

	@Test
	public void testUpdateService() {
		SupplierItem supplier = supplier();
		supplier.getSystem().setId("1¬1");
		ItemResponse master = new ItemResponse();
		master.setMaster(new MasterItem());
		when(this.master.getItem(any(), any())).thenReturn(master);
		assertNull(this.saveSupplier.updateService(supplier.getSystem().getId(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther(), supplier.getSystem().getServiceOther(), supplier.getSystem().getDescSrvOther(), monitor()).getError());
	}
}
