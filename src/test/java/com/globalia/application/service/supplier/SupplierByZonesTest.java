package com.globalia.application.service.supplier;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.repository.supplier.GetSupplierByZones;
import com.globalia.application.service.supplier.SupplierByZones;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SupplierByZonesTest extends HelperTest {

	@InjectMocks
	private SupplierByZones supplierByZones;
	@Mock
	private GetSupplierByZones getSupplierByZones;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemsNoResponse() {
		AllItemsResponse items = new AllItemsResponse();
		items.setError(new Error());
		when(this.getSupplierByZones.getAllItems(any())).thenReturn(items);
		assertNotNull(this.supplierByZones.getAllItems(supplierByZone(), monitor()).getError());
	}

	@Test
	public void testGetAllItemsNoFilterCODSIS() {
		SupplierByZoneItem filter = supplierByZone();
		filter.setCodsis("2");

		AllItemsResponse items = new AllItemsResponse();
		items.setSuppliersByZone(Set.of(supplierByZone()));
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.getSupplierByZones.getAllItems(any())).thenReturn(items);
		assertNotNull(this.supplierByZones.getAllItems(filter, monitor()).getError());
	}

	@Test
	public void testGetAllItemNoFilterSUBPRV() {
		SupplierByZoneItem filter = new SupplierByZoneItem();
		filter.setCodsis("1");
		filter.setSubprv("2");

		AllItemsResponse items = new AllItemsResponse();
		items.setSuppliersByZone(Set.of(supplierByZone()));
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.getSupplierByZones.getAllItems(any())).thenReturn(items);
		when(this.getSupplierByZones.getAllItems(any())).thenReturn(items);
		assertNotNull(this.supplierByZones.getAllItems(filter, monitor()).getError());
	}

	@Test
	public void testGetAllItem() {
		AllItemsResponse items = new AllItemsResponse();
		items.setSuppliersByZone(Set.of(supplierByZone()));

		SupplierByZoneItem filter = new SupplierByZoneItem();
		filter.setCodsis("1");
		filter.setSubprv("1");
		when(this.getSupplierByZones.getAllItems(any())).thenReturn(items);
		assertNull(this.supplierByZones.getAllItems(filter, monitor()).getError());
	}
}
