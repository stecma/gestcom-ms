package com.globalia.application.service.supplier;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.repository.supplier.GetSuppliers;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.application.service.supplier.SupplierCredentialFilter;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SuppliersTest extends HelperTest {

	@InjectMocks
	private Suppliers suppliers;
	@Mock
	private GetSuppliers getSuppliers;
	@Mock
	private Master master;
	@Mock
	private Supplier supplier;
	@Mock
	private SupplierCredentialFilter supplierHelper;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemNoResponse() {
		AllItemsResponse items = new AllItemsResponse();
		items.setError(new Error());
		when(this.getSuppliers.getAllItems(any())).thenReturn(items);
		assertNotNull(this.suppliers.getAllItems(supplier(), monitor()));
	}

	@Test
	public void testGetAllItemNoAddMaster() {
		AllItemsResponse items = new AllItemsResponse();
		items.setSuppliers(Set.of(supplier()));

		when(this.getSuppliers.getAllItems(any())).thenReturn(items);
		when(this.supplierHelper.filter(any(), any(), any(), any())).thenReturn(false);
		assertNotNull(this.suppliers.getAllItems(supplier(), monitor()));
	}

	@Test
	public void testGetAllItemNoAddMaster2() {
		AllItemsResponse items = new AllItemsResponse();
		items.setSuppliers(Set.of(supplier()));

		when(this.getSuppliers.getAllItems(any())).thenReturn(items);
		when(this.supplierHelper.filter(any(), any(), any(), any())).thenReturn(true);
		assertNotNull(this.suppliers.getAllItems(supplier(), monitor()));
	}

	@Test
	public void testGetAllItem() {
		AllItemsResponse items = new AllItemsResponse();
		items.setSuppliers(Set.of(supplier()));

		when(this.getSuppliers.getAllItems(any())).thenReturn(items);
		when(this.supplierHelper.filter(any(), any(), any(), any())).thenReturn(true);
		assertNotNull(this.suppliers.getAllItems(supplier(), true, monitor()));
	}

}
