package com.globalia.application.service.supplier;

import com.globalia.HelperTest;
import com.globalia.application.repository.supplier.GetSupplierByZone;
import com.globalia.dto.credential.ItemResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SupplierByZoneTest extends HelperTest {

	@InjectMocks
	private SupplierByZone supplierByZone;
	@Mock
	private GetSupplierByZone getSupplierByZone;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItem() {
		when(this.getSupplierByZone.getItem(any(), any())).thenReturn(new ItemResponse());
		assertNull(this.supplierByZone.getItem(supplierByZone(), monitor()).getError());
	}
}
