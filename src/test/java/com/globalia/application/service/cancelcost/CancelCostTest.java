package com.globalia.application.service.cancelcost;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.Status;
import com.globalia.application.repository.cancelcost.GetCancelCost;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CancelCostTest extends HelperTest {

	@InjectMocks
	private CancelCost cancelCost;
	@Mock
	private GetCancelCost getCancelCost;
	@Mock
	private Status status;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemError() {
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.getCancelCost.getItem(anyString())).thenReturn(null);
		assertNotNull(this.cancelCost.getItem(cancelCost(), monitor()).getError());
	}

	@Test
	public void testGetItemDiffID() {
		CancelCostItem aux = cancelCost();
		aux.setId("1");

		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.getCancelCost.getItem(anyString())).thenReturn(Set.of(aux));
		assertNotNull(this.cancelCost.getItem(cancelCost(), monitor()).getError());
	}

	@Test
	public void testGetItem() {
		CancelCostItem aux = cancelCost();
		when(this.getCancelCost.getItem(anyString())).thenReturn(Set.of(aux));
		assertNull(this.cancelCost.getItem(aux, monitor()).getError());
	}
}
