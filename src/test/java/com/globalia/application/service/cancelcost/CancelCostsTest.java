package com.globalia.application.service.cancelcost;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.Status;
import com.globalia.application.repository.cancelcost.GetCancelCost;
import com.globalia.application.service.cancelcost.CancelCosts;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CancelCostsTest extends HelperTest {

	@InjectMocks
	private CancelCosts cancelCosts;
	@Mock
	private GetCancelCost getCancelCost;
	@Mock
	private Status status;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemsEmptyResponse() {
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.getCancelCost.getItem(anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.cancelCosts.getAllItems(cancelCost(), monitor()).getError());
	}

	@Test
	public void testGetAllItem() {
		CancelCostItem aux = cancelCost();
		when(this.getCancelCost.getItem(anyString())).thenReturn(Set.of(aux));
		assertNull(this.cancelCosts.getAllItems(cancelCost(), monitor()).getError());
	}
}
