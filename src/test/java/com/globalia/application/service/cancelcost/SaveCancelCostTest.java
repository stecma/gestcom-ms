package com.globalia.application.service.cancelcost;

import com.globalia.HelperTest;
import com.globalia.application.Status;
import com.globalia.application.repository.cancelcost.SaveCancelCostRedis;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MatchLevel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveCancelCostTest extends HelperTest {

	@InjectMocks
	private SaveCancelCost saveCancelCost;
	@Mock
	private SaveCancelCostRedis save;
	@Mock
	private Status status;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreate() {
		ItemResponse item = new ItemResponse();
		item.setCancelCost(new CancelCostItem());
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.status.getStatus(any(),any(),any())).thenReturn(MatchLevel.RED);
		assertNull(this.saveCancelCost.createItem(cancelCost(), monitor()).getError());
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		item.setCancelCost(new CancelCostItem());
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.status.getStatus(any(),any(),any())).thenReturn(MatchLevel.RED);
		assertNull(this.saveCancelCost.updateItem(cancelCost(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveCancelCost.deleteItem(cancelCost(), monitor()).getError());
	}
}
