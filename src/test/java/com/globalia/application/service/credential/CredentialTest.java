package com.globalia.application.service.credential;

import com.globalia.Dates;
import com.globalia.HelperTest;
import com.globalia.application.repository.credential.GetCredential;
import com.globalia.application.service.master.ChildCodes;
import com.globalia.application.service.currency.Currencies;
import com.globalia.application.service.customer.Customers;
import com.globalia.application.service.group.Group;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.release.Releases;
import com.globalia.application.service.trade.Trade;
import com.globalia.application.service.cancelcost.CancelCost;
import com.globalia.dto.DateRange;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.Assignment;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CredentialTest extends HelperTest {

	@InjectMocks
	private Credential credential;
	@Mock
	private GetCredential getCredential;
	@Mock
	private Master master;
	@Mock
	private ChildCodes getChild;
	@Mock
	private Group group;
	@Mock
	private Trade trade;
	@Mock
	private Customers customers;
	@Mock
	private CancelCost cancelCost;
	@Mock
	private Currencies currencies;
	@Mock
	private Releases releases;
	@Mock
	private Dates dates;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		dateTo(Credential.class, this.credential, false);
		timeFrom(Credential.class, this.credential, false);
		timeTo(Credential.class, this.credential, false);
	}

	@Test
	public void testGetItemRedisError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.getCredential.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.credential.getItem(credential(), monitor()));
	}

	@Test
	public void testGetItemTrades() {
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());

		TradePolicyItem trade = new TradePolicyItem();
		trade.setDateApl(new DateRange());

		when(this.dates.getDateTime(nullable(String.class), anyString(), any())).thenThrow(ValidateException.class);
		when(this.getCredential.getItem(any(), any())).thenReturn(item);
		when(this.master.getItem(any(), any())).thenReturn(new ItemResponse());
		when(this.trade.getItem(anyString())).thenReturn(Set.of(trade));
		when(this.group.getAssignItem(anyString())).thenReturn(Set.of(new GroupCredentialItem()));
		when(this.customers.getAllItems(any(), anyBoolean(), any())).thenReturn(new AllItemsResponse());
		when(this.releases.getAllItems(any(), any())).thenReturn(new AllItemsResponse());
		when(this.cancelCost.getItem(anyString())).thenReturn(Set.of(new CancelCostItem()));
		when(this.currencies.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.credential.getItem(credential(), monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
	}

	@Test
	public void testGetItemReleses() {
		masterBrand(this.credential, "DEPTH#SERVICE_DISTRIBUTION");
		masterServiceType(this.credential, "PLATFORM#SALES_MODEL#PRODUCT_ORIGIN#RATE_TYPE");
		masterSalesChannel(this.credential, "ENVIRONMENTS#REQUEST#DISTRIBUTION_MODEL");
		masterCredential(this.credential, "customers,AGENCY,true#brand,BRAND,false#serviceType,SERVICE_TYPE,false#saleChannel,SALES_CHANNEL,false#distributionModel,DISTRIBUTION_MODEL,false#market,COUNTRY,false#depth,DEPTH,false#salesModel,SALES_MODEL,false#language,LANGUAGES,false#environment,ENVIRONMENTS,false#request,REQUEST,false#user,USERS,false#platforms,PLATFORM,true#rates,RATE_TYPE,true#products,PRODUCT_ORIGIN,true");

		ItemResponse master = new ItemResponse();
		master.setMaster(master());
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());

		ReleaseItem release = new ReleaseItem();
		release.setAssignment(new Assignment());
		release.getAssignment().setBook(new DateRange());
		AllItemsResponse releases = new AllItemsResponse();
		releases.setReleases(Set.of(release));

		when(this.dates.getDateTime(nullable(String.class), anyString(), any())).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(false);
		when(this.getCredential.getItem(any(), any())).thenReturn(item);
		when(this.master.getItem(any(), any())).thenReturn(master);
		when(this.getChild.getChild(nullable(String.class), anyString())).thenReturn(item.getCredential().getCodes());
		when(this.trade.getItem(anyString())).thenReturn(null);
		when(this.group.getAssignItem(anyString())).thenReturn(Set.of(new GroupCredentialItem()));
		when(this.customers.getAllItems(any(), anyBoolean(), any())).thenReturn(new AllItemsResponse());
		when(this.releases.getAllItems(any(), any())).thenReturn(releases);
		when(this.cancelCost.getItem(anyString())).thenReturn(Set.of(new CancelCostItem()));
		when(this.currencies.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.credential.getItem(credential(), monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
	}

	@Test
	public void testGetItemCustomers() {
		masterBrand(this.credential, "DEPTH#SERVICE_DISTRIBUTION");
		masterServiceType(this.credential, "PLATFORM#SALES_MODEL#PRODUCT_ORIGIN#RATE_TYPE");
		masterSalesChannel(this.credential, "ENVIRONMENTS#REQUEST#DISTRIBUTION_MODEL");
		masterCredential(this.credential, "customers,AGENCY,true#brand,BRAND,false#serviceType,SERVICE_TYPE,false#saleChannel,SALES_CHANNEL,false#distributionModel,DISTRIBUTION_MODEL,false#market,COUNTRY,false#depth,DEPTH,false#salesModel,SALES_MODEL,false#language,LANGUAGES,false#environment,ENVIRONMENTS,false#request,REQUEST,false#user,USERS,false#platforms,PLATFORM,true#rates,RATE_TYPE,true#products,PRODUCT_ORIGIN,true");

		ItemResponse master = new ItemResponse();
		master.setMaster(master());
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());

		CustomerItem customer = new CustomerItem();
		customer.setAssignment(new Assignment());
		customer.getAssignment().setBook(new DateRange());
		AllItemsResponse customers = new AllItemsResponse();
		customers.setCustomers(Set.of(customer));

		when(this.dates.getDateTime(nullable(String.class), anyString(), any())).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(false);
		when(this.getCredential.getItem(any(), any())).thenReturn(item);
		when(this.master.getItem(any(), any())).thenReturn(master);
		when(this.getChild.getChild(nullable(String.class), anyString())).thenReturn(item.getCredential().getCodes());
		when(this.trade.getItem(anyString())).thenReturn(null);
		when(this.group.getAssignItem(anyString())).thenReturn(Set.of(new GroupCredentialItem()));
		when(this.customers.getAllItems(any(), anyBoolean(), any())).thenReturn(customers);
		when(this.releases.getAllItems(any(), any())).thenReturn(new AllItemsResponse());
		when(this.cancelCost.getItem(anyString())).thenReturn(Set.of(new CancelCostItem()));
		when(this.currencies.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.credential.getItem(credential(), monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
	}

	@Test
	public void testGetItemGroups() {
		masterBrand(this.credential, "DEPTH#SERVICE_DISTRIBUTION");
		masterServiceType(this.credential, "PLATFORM#SALES_MODEL#PRODUCT_ORIGIN#RATE_TYPE");
		masterSalesChannel(this.credential, "ENVIRONMENTS#REQUEST#DISTRIBUTION_MODEL");
		masterCredential(this.credential, "customers,AGENCY,true#brand,BRAND,false#serviceType,SERVICE_TYPE,false#saleChannel,SALES_CHANNEL,false#distributionModel,DISTRIBUTION_MODEL,false#market,COUNTRY,false#depth,DEPTH,false#salesModel,SALES_MODEL,false#language,LANGUAGES,false#environment,ENVIRONMENTS,false#request,REQUEST,false#user,USERS,false#platforms,PLATFORM,true#rates,RATE_TYPE,true#products,PRODUCT_ORIGIN,true");

		ItemResponse master = new ItemResponse();
		master.setMaster(master());
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());

		GroupCredentialItem group = new GroupCredentialItem();
		group.setAssignment(new Assignment());
		group.getAssignment().setBook(new DateRange());

		Optional<PairValue> code = item.getCredential().getCodes().stream().filter(c -> MasterType.PRODUCT_ORIGIN.name().equalsIgnoreCase(c.getKey())).findFirst();
		code.ifPresent(pairValue -> pairValue.getValue().add("PPPULL"));

		when(this.dates.getDateTime(nullable(String.class), anyString(), any())).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(false);
		when(this.getCredential.getItem(any(), any())).thenReturn(item);
		when(this.master.getItem(any(), any())).thenReturn(master);
		when(this.getChild.getChild(nullable(String.class), anyString())).thenReturn(item.getCredential().getCodes());
		when(this.trade.getItem(anyString())).thenReturn(null);
		when(this.group.getAssignItem(anyString())).thenReturn(Set.of(group));
		when(this.customers.getAllItems(any(), anyBoolean(), any())).thenReturn(new AllItemsResponse());
		when(this.releases.getAllItems(any(), any())).thenReturn(new AllItemsResponse());
		when(this.cancelCost.getItem(anyString())).thenReturn(Set.of(new CancelCostItem()));
		when(this.currencies.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.credential.getItem(credential(), monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
	}

	@Test
	public void testGetItem() {
		masterBrand(this.credential, "DEPTH#SERVICE_DISTRIBUTION");
		masterServiceType(this.credential, "PLATFORM#SALES_MODEL#PRODUCT_ORIGIN#RATE_TYPE");
		masterSalesChannel(this.credential, "ENVIRONMENTS#REQUEST#DISTRIBUTION_MODEL");
		masterCredential(this.credential, "customers,AGENCY,true#brand,BRAND,false#serviceType,SERVICE_TYPE,false#saleChannel,SALES_CHANNEL,false#distributionModel,DISTRIBUTION_MODEL,false#market,COUNTRY,false#depth,DEPTH,false#salesModel,SALES_MODEL,false#language,LANGUAGES,false#environment,ENVIRONMENTS,false#request,REQUEST,false#user,USERS,false#currency,CURRENCY,false#platforms,PLATFORM,true#rates,RATE_TYPE,true#products,PRODUCT_ORIGIN,true");

		ItemResponse master = new ItemResponse();
		master.setMaster(master());
		ItemResponse item = new ItemResponse();
		item.setCredential(credential());
		item.getCredential().setActive(true);

		when(this.dates.getDateTime(nullable(String.class), anyString(), any())).thenReturn(new Date());
		when(this.dates.currentBetweenRange(any(), any())).thenReturn(false);
		when(this.getCredential.getItem(any(), any())).thenReturn(item);
		when(this.master.getItem(any(), any())).thenReturn(master);
		when(this.getChild.getChild(nullable(String.class), anyString())).thenReturn(item.getCredential().getCodes());
		when(this.trade.getItem(anyString())).thenReturn(null);
		when(this.group.getAssignItem(anyString())).thenReturn(null);
		when(this.customers.getAllItems(any(), anyBoolean(), any())).thenReturn(new AllItemsResponse());
		when(this.releases.getAllItems(any(), any())).thenReturn(new AllItemsResponse());
		when(this.cancelCost.getItem(anyString())).thenReturn(Set.of(new CancelCostItem()));
		when(this.currencies.getAllItems(any(), any())).thenReturn(new AllItemsResponse());

		ItemResponse supplier = this.credential.getItem(credential(), monitor());
		assertNotNull(supplier);
		assertNull(supplier.getError());
	}
}
