package com.globalia.application.service.credential;

import com.globalia.HelperTest;
import com.globalia.application.repository.credential.GetCredentialCount;
import com.globalia.application.repository.credential.SaveCredentialRedis;
import com.globalia.dto.credential.ItemResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveCredentialTest extends HelperTest {

	@InjectMocks
	private SaveCredential saveCredential;
	@Mock
	private SaveCredentialRedis save;
	@Mock
	private GetCredentialCount getCredentialCount;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveCredential.createItem(credential(), monitor()).getError());
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveCredential.updateItem(credential(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveCredential.deleteItem(credential(), monitor()).getError());
	}
}
