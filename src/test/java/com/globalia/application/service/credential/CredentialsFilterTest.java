package com.globalia.application.service.credential;

import com.globalia.application.service.credential.CredentialsFilter;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CredentialFilter;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CredentialsFilterTest {

	@InjectMocks
	private CredentialsFilter credentialFilter;

	private CredentialItem credential;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		PairValue brd = new PairValue();
		brd.setKey(MasterType.BRAND.name());
		brd.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue srvc = new PairValue();
		srvc.setKey(MasterType.SERVICE_TYPE.name());
		srvc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue channelc = new PairValue();
		channelc.setKey(MasterType.SALES_CHANNEL.name());
		channelc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue modelc = new PairValue();
		modelc.setKey(MasterType.DISTRIBUTION_MODEL.name());
		modelc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue rqc = new PairValue();
		rqc.setKey(MasterType.REQUEST.name());
		rqc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue envc = new PairValue();
		envc.setKey(MasterType.ENVIRONMENTS.name());
		envc.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue rates = new PairValue();
		rates.setKey(MasterType.RATE_TYPE.name());
		rates.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue cous = new PairValue();
		cous.setKey(MasterType.COUNTRY.name());
		cous.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue depth = new PairValue();
		depth.setKey(MasterType.DEPTH.name());
		depth.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue sales = new PairValue();
		sales.setKey(MasterType.SALES_MODEL.name());
		sales.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue lang = new PairValue();
		lang.setKey(MasterType.LANGUAGES.name());
		lang.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue platform = new PairValue();
		platform.setKey(MasterType.PLATFORM.name());
		platform.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue prd = new PairValue();
		prd.setKey(MasterType.PRODUCT_ORIGIN.name());
		prd.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue client = new PairValue();
		client.setKey(MasterType.AGENCY.name());
		client.setValue(new LinkedHashSet<>(Set.of("1")));

		MasterItem master = new MasterItem();
		master.setId("1");
		this.credential = new CredentialItem();
		this.credential.setId("test");
		this.credential.setAlias("test");
		this.credential.setEnvironment(master);
		this.credential.setBrand(master);
		this.credential.setServiceType(master);
		this.credential.setSaleChannel(master);
		this.credential.setDistributionModel(master);
		this.credential.setMarket(master);
		this.credential.setDepth(master);
		this.credential.setSalesModel(master);
		this.credential.setLanguage(master);
		this.credential.setRequest(master);
		this.credential.setPlatforms(new HashSet<>(Set.of(master)));
		this.credential.setRates(new HashSet<>(Set.of(master)));
		this.credential.setProducts(new HashSet<>(Set.of(master)));
		this.credential.setCodes(new HashSet<>(Set.of(brd, srvc, channelc, modelc, rqc, envc, rates, cous, depth, sales, lang, platform, prd, client)));
	}

	@Test
	public void testFilterWrong() {
		MasterItem filter2 = new MasterItem();
		filter2.setId("2");

		CredentialFilter filter = new CredentialFilter();
		filter.setBrand(this.credential.getBrand().getId());
		filter.setServiceType(this.credential.getServiceType().getId());
		filter.setDepth(this.credential.getDepth().getId());
		filter.setSaleChannel(this.credential.getSaleChannel().getId());
		filter.setDistributionModel(this.credential.getDistributionModel().getId());
		filter.setSalesModel(this.credential.getSalesModel().getId());
		filter.setLanguage(this.credential.getLanguage().getId());
		filter.setEnvironment(this.credential.getEnvironment().getId());
		filter.setRequest(this.credential.getRequest().getId());
		filter.setPlatform(this.credential.getPlatforms().iterator().next().getId());
		filter.setRate(null);
		filter.setProductOrigin(this.credential.getProducts().iterator().next().getId());
		filter.setCustomer(filter2.getId());

		this.credential.setPlatforms(null);

		assertFalse(this.credentialFilter.filter(filter, this.credential));
	}

	@Test
	public void testFilterWitOutClients() {
		CredentialFilter filter = new CredentialFilter();
		filter.setBrand(this.credential.getBrand().getId());
		filter.setServiceType(this.credential.getServiceType().getId());
		filter.setDepth(this.credential.getDepth().getId());
		filter.setSaleChannel(this.credential.getSaleChannel().getId());
		filter.setDistributionModel(this.credential.getDistributionModel().getId());
		filter.setSalesModel(this.credential.getSalesModel().getId());
		filter.setLanguage(this.credential.getLanguage().getId());
		filter.setEnvironment(this.credential.getEnvironment().getId());
		filter.setRequest(this.credential.getRequest().getId());
		filter.setPlatform(this.credential.getPlatforms().iterator().next().getId());
		filter.setRate(null);
		filter.setProductOrigin(this.credential.getProducts().iterator().next().getId());
		filter.setHotelX("false");
		filter.setAvailCost("false");
		filter.setPriceChange("false");

		assertTrue(this.credentialFilter.filter(filter, this.credential));
	}

	@Test
	public void testFilterNoAgency() {
		CredentialFilter filter = new CredentialFilter();
		filter.setBrand(this.credential.getBrand().getId());
		filter.setServiceType(this.credential.getServiceType().getId());
		filter.setDepth(this.credential.getDepth().getId());
		filter.setSaleChannel(this.credential.getSaleChannel().getId());
		filter.setDistributionModel(this.credential.getDistributionModel().getId());
		filter.setSalesModel(this.credential.getSalesModel().getId());
		filter.setLanguage(this.credential.getLanguage().getId());
		filter.setEnvironment(this.credential.getEnvironment().getId());
		filter.setRequest(this.credential.getRequest().getId());
		filter.setPlatform(this.credential.getPlatforms().iterator().next().getId());
		filter.setRate(null);
		filter.setProductOrigin(this.credential.getProducts().iterator().next().getId());
		filter.setHotelX("false");
		filter.setAvailCost("false");
		filter.setPriceChange("false");

		PairValue client = new PairValue();
		client.setKey(MasterType.AGENCY_GROUP.name());
		client.setValue(new LinkedHashSet<>(Set.of("1")));

		CustomerItem customer = new CustomerItem();
		customer.setCodes(Set.of(client));
		this.credential.setClients(Set.of(customer));
		assertTrue(this.credentialFilter.filter(filter, this.credential));
	}

	@Test
	public void testFilter() {
		CredentialFilter filter = new CredentialFilter();
		filter.setBrand(this.credential.getBrand().getId());
		filter.setServiceType(this.credential.getServiceType().getId());
		filter.setDepth(this.credential.getDepth().getId());
		filter.setSaleChannel(this.credential.getSaleChannel().getId());
		filter.setDistributionModel(this.credential.getDistributionModel().getId());
		filter.setSalesModel(this.credential.getSalesModel().getId());
		filter.setLanguage(this.credential.getLanguage().getId());
		filter.setEnvironment(this.credential.getEnvironment().getId());
		filter.setRequest(this.credential.getRequest().getId());
		filter.setPlatform(this.credential.getPlatforms().iterator().next().getId());
		filter.setRate(null);
		filter.setProductOrigin(this.credential.getProducts().iterator().next().getId());
		filter.setHotelX("false");
		filter.setAvailCost("false");
		filter.setPriceChange("false");

		PairValue client = new PairValue();
		client.setKey(MasterType.AGENCY.name());
		client.setValue(new LinkedHashSet<>(Set.of("1")));

		CustomerItem customer = new CustomerItem();
		customer.setCodes(Set.of(client));
		this.credential.setClients(Set.of(customer));
		assertTrue(this.credentialFilter.filter(filter, this.credential));
	}
}
