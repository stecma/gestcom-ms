package com.globalia.application.service.credential;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.repository.credential.GetCredentials;
import com.globalia.application.service.customer.Customers;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CredentialsTest extends HelperTest {

	@InjectMocks
	private Credentials credentials;
	@Mock
	private GetCredentials getCredentials;
	@Mock
	private Credential credential;
	@Mock
	private Errors error;
	@Mock
	private Customers customers;
	@Mock
	private CredentialsFilter credentialsFilter;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		dateTo(Credential.class, this.credential, false);
		timeFrom(Credential.class, this.credential, false);
		timeTo(Credential.class, this.credential, false);
	}

	@Test
	public void testGetAllItemErrorResponse() {
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setError(new Error());
		when(this.getCredentials.getAllItems(any())).thenReturn(credentials);
		assertNotNull(this.credentials.getAllItems(credential(), monitor()).getError());
	}

	@Test
	public void testGetAllItemNoFilter() {
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(Set.of(credential()));
		AllItemsResponse customers = new AllItemsResponse();
		customers.setCustomers(Set.of(customer()));
		when(this.getCredentials.getAllItems(any())).thenReturn(credentials);
		when(this.credentialsFilter.filter(any(), any())).thenReturn(false);
		when(this.customers.getAllItems(any(), anyBoolean(), any())).thenReturn(customers);
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.credentials.getAllItems(credential(), monitor()).getError());
	}

	@Test
	public void testGetAllItem() {
		AllItemsResponse credentials = new AllItemsResponse();
		credentials.setCredentials(Set.of(credential()));
		AllItemsResponse customers = new AllItemsResponse();
		customers.setCustomers(Set.of(customer()));
		when(this.getCredentials.getAllItems(any())).thenReturn(credentials);
		when(this.credentialsFilter.filter(any(), any())).thenReturn(true);
		when(this.customers.getAllItems(any(), anyBoolean(), any())).thenReturn(customers);
		assertNull(this.credentials.getAllItems(credential(), monitor()).getError());
	}
}
