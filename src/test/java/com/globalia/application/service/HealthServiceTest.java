package com.globalia.application.service;

import com.globalia.Context;
import com.globalia.HelperTest;
import com.globalia.redis.RedisClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.actuate.health.Status;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class HealthServiceTest extends HelperTest {

	@InjectMocks
	private HealthService healthCheck;
	@Mock
	protected RedisClient client;
	@Mock
	private Context context;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testHealthDown() {
		when(this.client.patternKeys(anyString())).thenThrow(RuntimeException.class);
		assertEquals(Status.DOWN, this.healthCheck.health().getStatus());
	}

	@Test
	public void testHealthDataNullDown() {
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("1"));
		when(this.producer.reply(any())).thenReturn(null);
		assertEquals(Status.DOWN, this.healthCheck.health().getStatus());
	}

	@Test
	public void testHealthDataDown() {
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("1"));
		when(this.producer.reply(any())).thenReturn("DOWN");
		assertEquals(Status.DOWN, this.healthCheck.health().getStatus());
	}

	@Test
	public void testHealthUp() {
		when(this.client.patternKeys(anyString())).thenReturn(Set.of("1"));
		when(this.producer.reply(any())).thenReturn("UP");
		assertEquals(Status.UP, this.healthCheck.health().getStatus());
	}
}
