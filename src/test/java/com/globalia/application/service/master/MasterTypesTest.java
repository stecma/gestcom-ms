package com.globalia.application.service.master;

import com.globalia.HelperTest;
import com.globalia.application.repository.master.GetTypes;
import com.globalia.application.service.master.MasterTypes;
import com.globalia.enumeration.credential.MasterType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MasterTypesTest extends HelperTest {

	@InjectMocks
	private MasterTypes masterTypes;
	@Mock
	private GetTypes getTypes;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetTypes() {
		when(this.getTypes.getType(any())).thenReturn("permision");
		assertNotNull(this.masterTypes.getType(MasterType.ACTIVITY));
	}
}
