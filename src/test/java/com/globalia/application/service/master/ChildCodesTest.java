package com.globalia.application.service.master;

import com.globalia.HelperTest;
import com.globalia.application.repository.master.GetChildCodes;
import com.globalia.application.service.master.ChildCodes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ChildCodesTest extends HelperTest {

	@InjectMocks
	private ChildCodes childCodes;
	@Mock
	private GetChildCodes getChildCodes;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testChildCodes() {
		when(this.getChildCodes.getChildCodes(anyString(), anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.childCodes.getChild("key", "entity"));
	}
}
