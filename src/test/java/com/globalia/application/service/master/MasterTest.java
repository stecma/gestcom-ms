package com.globalia.application.service.master;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.repository.master.GetMaster;
import com.globalia.application.service.master.ChildCodes;
import com.globalia.application.service.master.Master;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MasterTest extends HelperTest {

	@InjectMocks
	private Master master;
	@Mock
	private ChildCodes childCodes;
	@Mock
	private GetMaster getMaster;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemNoResponse() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error());
		when(this.getMaster.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetItemNoCodes() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());
		response.getMaster().setMasters(null);
		response.getMaster().setCodes(null);

		PairValue brand = new PairValue();
		brand.setKey(MasterType.BRAND.name());
		brand.setValue(new LinkedHashSet<>(Set.of("2")));

		when(this.childCodes.getChild(anyString(), anyString())).thenReturn(Set.of(brand));
		when(this.getMaster.getItem(any(), any())).thenReturn(response);
		assertNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetItemNoChildren() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());
		response.getMaster().setMasters(null);

		when(this.childCodes.getChild(anyString(), anyString())).thenReturn(new HashSet<>());
		when(this.getMaster.getItem(any(), any())).thenReturn(response);
		assertNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testConvertCodeToMasterNoCodes() {
		Set<MasterItem> masters = new HashSet<>();
		this.master.convertCodeToMaster(masters, null, monitor());
		assertTrue(masters.isEmpty());
	}

	@Test
	public void testConvertCodeToMasterNoRedis() {
		Set<MasterItem> masters = new HashSet<>();

		ItemResponse response = new ItemResponse();
		response.setMaster(master());

		when(this.getMaster.getItem(any(), any())).thenReturn(response);
		this.master.convertCodeToMaster(masters, master().getCodes(), monitor());
		assertFalse(masters.isEmpty());
	}

	@Test
	public void testAddMasterValuesNoSuchFieldException() {
		CredentialItem item = new CredentialItem();
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.master.addMasterValues(CredentialItem.class, item, "customers,AGENCY,true", null, monitor()));
	}

	@Test
	public void testAddMasterValuesNoCodesMaster() {
		CredentialItem item = new CredentialItem();
		when(this.error.addError(anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNull(this.master.addMasterValues(CredentialItem.class, item, "brand,BRAND,false", null, monitor()));
	}

	@Test
	public void testAddMasterValuesNoCodesMasters() {
		CredentialItem item = new CredentialItem();
		when(this.error.addError(anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNull(this.master.addMasterValues(CredentialItem.class, item, "brand,BRAND,true", null, monitor()));
	}

	@Test
	public void testAddMasterValues() {
		CredentialItem item = new CredentialItem();
		ItemResponse response = new ItemResponse();
		response.setMaster(master());

		PairValue rate = new PairValue();
		rate.setKey(MasterType.RATE_TYPE.name());
		rate.setValue(new LinkedHashSet<>(Set.of("1")));

		when(this.getMaster.getItem(any(), any())).thenReturn(response);
		when(this.error.addError(anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNull(this.master.addMasterValues(CredentialItem.class, item, "rates,RATE_TYPE,true", Set.of(rate), monitor()));
	}
}
