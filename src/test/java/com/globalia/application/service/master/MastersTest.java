package com.globalia.application.service.master;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.repository.master.GetMasters;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.I18n;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MastersTest extends HelperTest {

	@InjectMocks
	private Masters masters;
	@Mock
	private GetMasters getMasters;
	@Mock
	private Master master;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemNoResponse() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error());
		when(this.getMasters.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.masters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testGetAllItem() {
		AllItemsResponse response = new AllItemsResponse();
		response.setMasters(new LinkedHashSet<>(Set.of(master())));
		when(this.getMasters.getAllItems(any(), any())).thenReturn(response);
		assertNull(this.masters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testValidateKeyNotExistFalse() {
		when(this.getMasters.findCodes(anyString())).thenReturn(Set.of("IT"));
		assertFalse(this.masters.validateKey(MasterType.LANGUAGES.name(), "ES"));
	}

	@Test
	public void testValidateKeyTrue() {
		when(this.getMasters.findCodes(anyString())).thenReturn(Set.of("ES"));
		assertTrue(this.masters.validateKey(MasterType.LANGUAGES.name(), "ES"));
	}

	@Test
	public void testSearchItemNOEntity() {
		entityScaned(Masters.class, this.masters, "HOTEL_SERVICE");
		when(this.getMasters.getAllItems(any(), any())).thenReturn(null);
		assertNull(this.masters.searchItems(null, "test", "test", monitor()));
	}

	@Test
	public void testSearchItemNOText() {
		entityScaned(Masters.class, this.masters, "HOTEL_SERVICE");
		when(this.getMasters.getAllItems(any(), any())).thenReturn(null);
		assertNull(this.masters.searchItems("BRAND", null, "test", monitor()));
	}

	@Test
	public void testSearchItemNOResponse() {
		AllItemsResponse items = new AllItemsResponse();
		items.setError(new Error());
		entityScaned(Masters.class, this.masters, "HOTEL_SERVICE");
		when(this.getMasters.getAllItems(any(), any())).thenReturn(items);
		assertNotNull(this.masters.searchItems("BRAND", "test", "test", monitor()));
	}

	@Test
	public void testSearchItemNOItem() {
		I18n i18n = new I18n();
		i18n.setValue("test");

		MasterItem master1 = new MasterItem();
		master1.setNames(new LinkedHashSet<>(Set.of(i18n)));

		AllItemsResponse items = new AllItemsResponse();
		items.setMasters(new LinkedHashSet<>(Set.of(master1)));

		entityScaned(Masters.class, this.masters, "HOTEL_SERVICE");
		when(this.getMasters.getAllItems(any(), any())).thenReturn(items);
		assertNotNull(this.masters.searchItems("BRAND", "test", "test", monitor()));
	}

	@Test
	public void testSearchItemScan() {
		I18n i18n = new I18n();
		i18n.setValue("test");

		MasterItem master1 = new MasterItem();
		master1.setNames(new LinkedHashSet<>(Set.of(i18n)));

		AllItemsResponse items = new AllItemsResponse();
		items.setMasters(new LinkedHashSet<>(Set.of(master1)));

		entityScaned(Masters.class, this.masters, "HOTEL_SERVICE");
		when(this.getMasters.getAllScan(anyString(), anyString(), anyString(), any())).thenReturn(items);
		assertNotNull(this.masters.searchItems("HOTEL_SERVICE", "test", "test", monitor()));
	}
}
