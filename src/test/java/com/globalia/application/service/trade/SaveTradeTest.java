package com.globalia.application.service.trade;

import com.globalia.HelperTest;
import com.globalia.application.repository.trade.SaveTradeRedis;
import com.globalia.application.service.trade.SaveTrade;
import com.globalia.application.service.trade.Trade;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MassiveItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveTradeTest extends HelperTest {

	@InjectMocks
	private SaveTrade saveTrade;
	@Mock
	private Trade trade;
	@Mock
	private SaveTradeRedis save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveTrade.createItem(trade(), monitor()).getError());
	}

	@Test
	public void testCreate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.trade.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveTrade.createItem(trade(), monitor()).getError());
	}

	@Test
	public void testUpdateError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveTrade.updateItem(trade(), monitor()).getError());
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.trade.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveTrade.updateItem(trade(), monitor()).getError());
	}

	@Test
	public void testDeleteError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveTrade.deleteItem(trade(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.trade.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveTrade.deleteItem(trade(), monitor()).getError());
	}

	@Test
	public void testMassiveNoTrades() {
		assertNotNull(this.saveTrade.massive(new MassiveItem(), monitor()));
	}

	@Test
	public void testMassiveNoCredential() {
		TradePolicyItem trade1 = trade();
		trade1.setCredential(null);

		MassiveItem trades = new MassiveItem();
		trades.setTrades(Set.of(trade1));

		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveTrade.massive(trades, monitor()));
	}

	@Test
	public void testMassive() {
		MassiveItem trades = new MassiveItem();
		trades.setTrades(Set.of(trade()));

		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveTrade.massive(trades, monitor()).getError());
	}

	@Test
	public void testMassiveCreate() {
		TradePolicyItem trade = trade();
		trade.setId(null);

		MassiveItem trades = new MassiveItem();
		trades.setTrades(Set.of(trade));

		when(this.save.addItem(any(), any())).thenReturn(new ItemResponse());
		when(this.trade.getItem(any(), any())).thenReturn(new ItemResponse());
		assertNull(this.saveTrade.massive(trades, monitor()).getError());
	}
}
