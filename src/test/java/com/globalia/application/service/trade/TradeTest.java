package com.globalia.application.service.trade;

import com.globalia.Errors;
import com.globalia.HelperTest;
import com.globalia.application.repository.trade.GetTrade;
import com.globalia.application.service.master.Master;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class TradeTest extends HelperTest {

	@InjectMocks
	private Trade trade;
	@Mock
	private GetTrade getTrade;
	@Mock
	private Master master;
	@Mock
	private Errors error;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemError() {
		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.getTrade.getItem(anyString())).thenReturn(null);
		assertNotNull(this.trade.getItem(trade(), monitor()).getError());
	}

	@Test
	public void testGetItemDiffID() {
		TradePolicyItem aux = trade();
		aux.setId("1");
		aux.setMasters(null);

		when(this.error.addError(anyString(), any(), any())).thenReturn(new Error());
		when(this.getTrade.getItem(anyString())).thenReturn(Set.of(aux));
		assertNotNull(this.trade.getItem(trade(), monitor()).getError());
	}

	@Test
	public void testGetItemNoCodes() {
		TradePolicyItem aux = trade();
		aux.setCodes(null);

		when(this.getTrade.getItem(anyString())).thenReturn(Set.of(aux));
		assertNull(this.trade.getItem(trade(), monitor()).getError());
	}

	@Test
	public void testGetItem() {
		TradePolicyItem aux = trade();

		when(this.getTrade.getItem(anyString())).thenReturn(Set.of(aux));
		assertNull(this.trade.getItem(trade(), monitor()).getError());
	}

	@Test
	public void testGetItemRedis() {
		when(this.getTrade.getItem(anyString())).thenReturn(new HashSet<>());
		assertNotNull(this.trade.getItem(trade().getCredential()));
	}
}
