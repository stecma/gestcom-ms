package com.globalia.application.service.connector;

import com.globalia.HelperTest;
import com.globalia.application.repository.connector.SaveConnectorRedis;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveConnectorTest extends HelperTest {

	@InjectMocks
	private SaveConnector saveConnector;
	@Mock
	private SaveConnectorRedis saveConnectorRedis;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreate() {
		ItemResponse item = new ItemResponse();
		when(this.saveConnectorRedis.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveConnector.createItem(connector(), monitor()).getError());
	}

	@Test
	public void testCreateNullMaster() {
		ExternalCredentialItem connector = connector();
		connector.setMasters(null);
		ItemResponse item = new ItemResponse();
		when(this.saveConnectorRedis.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveConnector.createItem(connector, monitor()).getError());
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		when(this.saveConnectorRedis.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveConnector.updateItem(connector(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		when(this.saveConnectorRedis.addItem(any(), any())).thenReturn(item);
		assertNull(this.saveConnector.deleteItem(connector(), monitor()).getError());
	}
}
