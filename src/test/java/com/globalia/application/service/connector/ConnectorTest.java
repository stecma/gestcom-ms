package com.globalia.application.service.connector;

import com.globalia.HelperTest;
import com.globalia.application.repository.connector.GetConnector;
import com.globalia.dto.credential.ItemResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ConnectorTest extends HelperTest {

	@InjectMocks
	private Connector connector;
	@Mock
	private GetConnector getConnector;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItem() {
		when(this.getConnector.getItem(any(), any())).thenReturn(new ItemResponse());
		assertNotNull(this.connector.getItem(connector(), monitor()));
	}
}
