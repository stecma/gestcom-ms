package com.globalia.application.service.connector;

import com.globalia.HelperTest;
import com.globalia.application.repository.connector.GetConnectors;
import com.globalia.application.service.connector.Connectors;
import com.globalia.dto.credential.AllItemsResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ConnectorsTest extends HelperTest {

	@InjectMocks
	private Connectors connectors;
	@Mock
	private GetConnectors getConnectors;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemsNotImplemented() {
		assertNull(this.connectors.getAllItems(connector(), monitor()));
	}

	@Test
	public void testGetAllItem() {
		when(this.getConnectors.getAllItems(any(), anyBoolean(), any())).thenReturn(new AllItemsResponse());
		assertNotNull(this.connectors.getAllItems(connector(), true, monitor()));
	}
}
