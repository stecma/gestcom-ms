package com.globalia.application.service.currency;

import com.globalia.HelperTest;
import com.globalia.application.Status;
import com.globalia.application.repository.currency.GetCurrencies;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CurrenciesTest extends HelperTest {

	@InjectMocks
	private Currencies currencies;
	@Mock
	private GetCurrencies getCurrencies;
	@Mock
	private Status status;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItems() {
		when(this.getCurrencies.getAllItems(any())).thenReturn(new AllItemsResponse());
		assertNull(this.currencies.getAllItemsByCountry(monitor()).getError());
	}

	@Test
	public void testGetAllItemsByCredentialError() {
		AllItemsResponse items = new AllItemsResponse();
		items.setError(new Error());
		when(this.getCurrencies.getAllItems(anyString(), any())).thenReturn(items);
		assertNotNull(this.currencies.getAllItems(currency(), monitor()));
	}

	@Test
	public void testGetAllItemsByCredential() {
		AllItemsResponse items = new AllItemsResponse();
		items.setCurrencies(Set.of(currency()));
		when(this.getCurrencies.getAllItems(anyString(), any())).thenReturn(items);
		assertNotNull(this.currencies.getAllItems(currency(), monitor()));
	}
}
