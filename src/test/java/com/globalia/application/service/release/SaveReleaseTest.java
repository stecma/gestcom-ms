package com.globalia.application.service.release;

import com.globalia.HelperTest;
import com.globalia.application.repository.release.SaveReleaseRedis;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveReleaseTest extends HelperTest {

	@InjectMocks
	private SaveRelease saveRelease;
	@Mock
	private Release release;
	@Mock
	private SaveReleaseRedis save;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveRelease.createItem(release(), monitor()).getError());
	}

	@Test
	public void testCreate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.release.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveRelease.createItem(release(), monitor()).getError());
	}

	@Test
	public void testUpdateError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveRelease.updateItem(release(), monitor()).getError());
	}

	@Test
	public void testUpdate() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.release.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveRelease.updateItem(release(), monitor()).getError());
	}

	@Test
	public void testDeleteError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.save.addItem(any(), any())).thenReturn(item);
		assertNotNull(this.saveRelease.deleteItem(release(), monitor()).getError());
	}

	@Test
	public void testDelete() {
		ItemResponse item = new ItemResponse();
		when(this.save.addItem(any(), any())).thenReturn(item);
		when(this.release.getItem(any(), any())).thenReturn(item);
		assertNull(this.saveRelease.deleteItem(release(), monitor()).getError());
	}
}
