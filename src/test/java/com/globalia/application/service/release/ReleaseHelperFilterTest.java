package com.globalia.application.service.release;

import com.globalia.application.service.release.ReleaseHelperFilter;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ReleaseFilter;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.enumeration.credential.MasterType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ReleaseHelperFilterTest {

	@InjectMocks
	private ReleaseHelperFilter releasesFilter;

	private ReleaseItem release;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		PairValue brand = new PairValue();
		brand.setKey(MasterType.BRAND.name());
		brand.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue service = new PairValue();
		service.setKey(MasterType.SERVICE_TYPE.name());
		service.setValue(new LinkedHashSet<>(Set.of("1")));
		PairValue pay = new PairValue();
		pay.setKey(MasterType.PAYMENT.name());
		pay.setValue(new LinkedHashSet<>(Set.of("1")));

		this.release = new ReleaseItem();
		this.release.setId("1");
		this.release.setCodes(new HashSet<>(Set.of(brand, service, pay)));
		this.release.setCredential("1");
		this.release.setDays(1);
		this.release.setOperationDays("X");
	}

	@Test
	public void testFilterNoFilter() {
		assertTrue(this.releasesFilter.filter(new ReleaseFilter(), this.release));
	}

	@Test
	public void testFilterNoAllow() {
		ReleaseFilter filter = new ReleaseFilter();
		filter.setBrand("1");
		filter.setServiceType("1");
		filter.setPayType("1");
		filter.setCredential("1");
		filter.setDays(1);
		filter.setOperationDays("Y");

		assertFalse(this.releasesFilter.filter(filter, this.release));
	}

	@Test
	public void testFilter() {
		ReleaseFilter filter = new ReleaseFilter();
		filter.setBrand("1");
		filter.setServiceType("1");
		filter.setPayType("1");
		filter.setCredential("1");
		filter.setDays(1);
		filter.setOperationDays("X");

		assertTrue(this.releasesFilter.filter(filter, this.release));
	}
}
