package com.globalia.application.service.release;

import com.globalia.HelperTest;
import com.globalia.application.Status;
import com.globalia.application.repository.release.GetRelease;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.release.Release;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ReleaseTest extends HelperTest {

	@InjectMocks
	private Release release;
	@Mock
	private GetRelease getRelease;
	@Mock
	private Master master;
	@Mock
	private Status status;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());
		when(this.getRelease.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.release.getItem(release(), monitor()).getError());
	}

	@Test
	public void testGetItemNoCodes() {
		ItemResponse item = new ItemResponse();
		item.setRelease(release());
		item.getRelease().setCodes(null);

		when(this.getRelease.getItem(any(), any())).thenReturn(item);
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.GREEN);
		assertNull(this.release.getItem(release(), monitor()).getError());
	}

	@Test
	public void testGetItem() {
		ItemResponse item = new ItemResponse();
		item.setRelease(release());

		when(this.getRelease.getItem(any(), any())).thenReturn(item);
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.GREEN);
		assertNull(this.release.getItem(release(), monitor()).getError());
	}
}
