package com.globalia.application.service.release;

import com.globalia.HelperTest;
import com.globalia.application.Status;
import com.globalia.application.repository.release.GetReleases;
import com.globalia.application.service.release.Release;
import com.globalia.application.service.release.ReleaseHelperFilter;
import com.globalia.application.service.release.Releases;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ReleasesTest extends HelperTest {

	@InjectMocks
	private Releases releases;
	@Mock
	private GetReleases getReleases;
	@Mock
	private ReleaseHelperFilter releaseFilter;
	@Mock
	private Status status;
	@Mock
	private Release release;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemError() {
		AllItemsResponse item = new AllItemsResponse();
		item.setError(new Error());
		when(this.getReleases.getAllItems(any(), any())).thenReturn(item);
		assertNotNull(this.releases.getAllItems(release(), monitor()).getError());
	}

	@Test
	public void testGetAllItemNoFilter() {
		AllItemsResponse item = new AllItemsResponse();
		item.setReleases(Set.of(release()));
		when(this.getReleases.getAllItems(any(), any())).thenReturn(item);
		when(this.releaseFilter.filter(any(), any())).thenReturn(false);
		assertNull(this.releases.getAllItems(release(), monitor()).getError());
	}

	@Test
	public void testGetAllItem() {
		AllItemsResponse item = new AllItemsResponse();
		item.setReleases(Set.of(release()));
		when(this.getReleases.getAllItems(any(), any())).thenReturn(item);
		when(this.releaseFilter.filter(any(), any())).thenReturn(true);
		when(this.status.getStatus(any(), any(), any())).thenReturn(MatchLevel.GREEN);
		assertNull(this.releases.getAllItems(release(), monitor()).getError());
	}
}
