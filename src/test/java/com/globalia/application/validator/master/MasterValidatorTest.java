package com.globalia.application.validator.master;

import com.globalia.HelperTest;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class MasterValidatorTest extends HelperTest {

	@InjectMocks
	private MasterValidator validator;
	@Mock
	private Masters masters;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testMasterValidationNoValidKey() {
		when(this.masters.validateKey(anyString(), anyString())).thenReturn(false);
		this.validator.masterValidation("test", "test", monitor());
	}

	@Test
	public void testMasterValidation() {
		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.validator.masterValidation("test", "test", monitor());
		assertTrue(true);
	}

	@Test(expected = ValidateException.class)
	public void testValidateEntityError() {
		MasterItem request = master();
		request.setEntity(null);
		this.validator.validateEntity(request, monitor());
	}

	@Test
	public void testValidateEntity() {
		this.validator.validateEntity(master(), monitor());
		assertTrue(true);
	}

	@Test(expected = ValidateException.class)
	public void testValidateBranchError() {
		MasterItem request = master();
		request.setEntity(MasterType.BRANCH_OFFICE.name());
		request.setFilter(null);
		this.validator.validateMaster(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testValidateDistributionError() {
		MasterItem request = master();
		request.setEntity(MasterType.DISTRIBUTION_MODEL.name());
		request.setAlias(null);
		this.validator.validateMaster(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testValidateRateError() {
		MasterItem request = master();
		request.setEntity(MasterType.RATE_TYPE.name());
		request.setRateCategory(null);
		this.validator.validateMaster(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testValidateNoNameError() {
		MasterItem request = master();
		request.setNames(null);
		this.validator.validateMaster(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testValidateEmptyNameError() {
		MasterItem request = master();
		request.setNames(new LinkedHashSet<>());
		this.validator.validateMaster(request, monitor());
	}

	@Test
	public void testValidateNoChildrenError() {
		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.validator.validateMaster(master(), monitor());
		assertTrue(true);
	}

	@Test
	public void testValidateEmptyChildrenError() {
		MasterItem request = master();
		request.setMasters(new LinkedHashSet<>());
		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.validator.validateMaster(request, monitor());
		assertTrue(true);
	}

	@Test
	public void testValidate() {
		MasterItem request = master();
		request.setMasters(new LinkedHashSet<>(Set.of(master())));
		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.validator.validateMaster(request, monitor());
		assertTrue(true);
	}
}