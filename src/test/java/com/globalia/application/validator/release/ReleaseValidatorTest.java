package com.globalia.application.validator.release;

import com.globalia.HelperTest;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ReleaseValidatorTest extends HelperTest {

	@InjectMocks
	private ReleaseValidator validator;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testReleaseValidationNoMaster() {
		ReleaseItem request = release();
		request.setMasters(null);
		request.setCredential(null);
		this.validator.releaseValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testReleaseValidationNoMasterBrand() {
		ReleaseItem request = release();
		request.setMasters(Set.of(master(MasterType.CLIENT, "1")));
		request.setCredential(null);
		this.validator.releaseValidation(request, monitor());
	}

	@Test
	public void testReleaseValidation() {
		this.validator.releaseValidation(release(), monitor());
		assertTrue(true);
	}
}
