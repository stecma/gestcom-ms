package com.globalia.application.validator.customer;

import com.globalia.HelperTest;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerValidatorTest extends HelperTest {

	@InjectMocks
	private CustomerValidator validator;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCustomerValidationNoCredential() {
		CustomerItem request = customer();
		request.setCredential(null);
		this.validator.customerValidation(request, false, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testCustomerValidationNoMaster() {
		CustomerItem request = customer();
		request.setMasters(null);
		this.validator.customerValidation(request, false, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testCustomerValidationBothMaster() {
		CustomerItem request = customer();
		request.setMasters(Set.of(master(MasterType.AGENCY, "1"), master(MasterType.AGENCY_GROUP, "1")));
		this.validator.customerValidation(request, false, monitor());
	}

	@Test
	public void testTradeValidation() {
		this.validator.customerValidation(customer(), true, monitor());
		assertTrue(true);
	}
}
