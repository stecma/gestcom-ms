package com.globalia.application.validator.currency;

import com.globalia.HelperTest;
import com.globalia.dto.Parameter;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CurrencyByCountryValidatorTest extends HelperTest {

	@InjectMocks
	private CurrencyByCountryValidator validator;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCurrencyByCountryValidationNoParams() {
		this.validator.currencyValidation(currency(), monitor());
	}

	@Test(expected = ValidateException.class)
	public void testCurrencyByCountryValidationEmptyParams() {
		CurrencyItem request = currency();
		request.setParams(new HashSet<>());
		this.validator.currencyValidation(request, monitor());
	}

	@Test
	public void testCurrencyByCountryValidation() {
		CurrencyItem request = currency();
		request.setParams(Set.of(new Parameter()));
		this.validator.currencyValidation(request, monitor());
		assertTrue(true);
	}
}
