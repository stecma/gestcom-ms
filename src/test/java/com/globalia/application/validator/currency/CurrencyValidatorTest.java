package com.globalia.application.validator.currency;

import com.globalia.HelperTest;
import com.globalia.dto.Parameter;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CurrencyValidatorTest extends HelperTest {

	@InjectMocks
	private CurrencyValidator validator;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCurrencyValidationNoCredential() {
		CurrencyItem request = currency();
		request.setCredential(null);
		this.validator.currencyValidation(request, true, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testCurrencyValidationNoParams() {
		this.validator.currencyValidation(currency(), false, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testCurrencyByCountryValidationEmptyParams() {
		CurrencyItem request = currency();
		request.setParams(new HashSet<>());
		this.validator.currencyValidation(request, false, monitor());
	}

	@Test
	public void testCurrencyByCountryValidation() {
		CurrencyItem request = currency();
		request.setParams(Set.of(new Parameter()));
		this.validator.currencyValidation(request, false, monitor());
		assertTrue(true);
	}
}
