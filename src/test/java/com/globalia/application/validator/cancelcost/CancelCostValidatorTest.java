package com.globalia.application.validator.cancelcost;

import com.globalia.HelperTest;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class CancelCostValidatorTest extends HelperTest {

	@InjectMocks
	private CancelCostValidator validator;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCancelCostValidationError() {
		CancelCostItem request = cancelCost();
		request.setCredential(null);
		this.validator.cancelCostValidation(request, monitor());
	}

	@Test
	public void testCancelCostValidation() {
		this.validator.cancelCostValidation(cancelCost(), monitor());
		assertTrue(true);
	}
}
