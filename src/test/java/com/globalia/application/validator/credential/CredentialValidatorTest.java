package com.globalia.application.validator.credential;

import com.globalia.HelperTest;
import com.globalia.application.validator.master.MasterValidator;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CredentialValidatorTest extends HelperTest {

	@InjectMocks
	private CredentialValidator validator;
	@Mock
	private MasterValidator masterValidator;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCredentialValidationNoAlias() {
		CredentialItem request = credential();
		request.setAlias(null);
		this.validator.credentialValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testCredentialValidationNoDesc() {
		CredentialItem request = credential();
		request.setDescription(null);
		this.validator.credentialValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testCredentialValidationNullMaster() {
		CredentialItem request = credential();
		request.setBrand(null);
		this.validator.credentialValidation(request, monitor());
	}

	@Test
	public void testCredentialValidationNoMarket() {
		CredentialItem request = credential();
		request.setTagNationality(false);
		request.getSaleChannel().setId("API");
		this.validator.credentialValidation(request, monitor());
		assertTrue(true);
	}

	@Test
	public void testCredentialValidation() {
		this.validator.credentialValidation(credential(), monitor());
		assertTrue(true);
	}
}