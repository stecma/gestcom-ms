package com.globalia.application.validator.trade;

import com.globalia.HelperTest;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class TradeValidatorTest extends HelperTest {

	@InjectMocks
	private TradeValidator validator;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testTradeValidationNoCredential() {
		TradePolicyItem request = trade();
		request.setCredential(null);
		this.validator.tradeValidation(request, false, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testTradeValidationNoPercentage() {
		TradePolicyItem request = trade();
		request.setPercentage(null);
		this.validator.tradeValidation(request, false, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testTradeValidationNoPercentageType() {
		TradePolicyItem request = trade();
		request.setPercentageType(null);
		this.validator.tradeValidation(request, false, monitor());
	}

	@Test
	public void testTradeValidationNoAll() {
		this.validator.tradeValidation(trade(), true, monitor());
		assertTrue(true);
	}

	@Test
	public void testTradeValidation() {
		this.validator.tradeValidation(trade(), true, monitor());
		assertTrue(true);
	}
}
