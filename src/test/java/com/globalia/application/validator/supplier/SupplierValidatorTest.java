package com.globalia.application.validator.supplier;

import com.globalia.Dates;
import com.globalia.HelperTest;
import com.globalia.application.validator.master.MasterValidator;
import com.globalia.dto.DateRange;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class SupplierValidatorTest extends HelperTest {

	@InjectMocks
	private SupplierValidator validator;
	@Mock
	private Dates dates;
	@Mock
	private MasterValidator masterValidator;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationNoClientPref() {
		SupplierItem request = supplier();
		request.setClientPref(null);
		this.validator.supplierValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationNoRoomMAx() {
		this.validator.supplierValidation(supplier(), monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationNoServiceType() {
		SupplierItem request = supplier();
		request.setRoomMax(1);
		request.setServiceType(null);
		this.validator.supplierValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationNoBuyModel() {
		SupplierItem request = supplier();
		request.setRoomMax(1);
		request.setBuyModel(null);
		this.validator.supplierValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationNoUser() {
		SupplierItem request = supplier();
		request.setRoomMax(1);
		request.setUser(null);
		this.validator.supplierValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationNoClients() {
		SupplierItem request = supplier();
		request.setRoomMax(1);
		request.setXsell(true);
		request.setClients(null);
		this.validator.supplierValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationEmptyClients() {
		SupplierItem request = supplier();
		request.setRoomMax(1);
		request.setXsell(true);
		request.setClients(new LinkedHashSet<>());
		this.validator.supplierValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierValidationNoSystem() {
		SupplierItem request = supplier();
		request.setRoomMax(1);
		request.setSystem(null);
		this.validator.supplierValidation(request, monitor());
	}

	@Test
	public void testSupplierValidation() {
		SupplierItem request = supplier();
		request.setRoomMax(1);
		this.validator.supplierValidation(request, monitor());
		assertTrue(true);
	}

	@Test
	public void testSupplierGroupsNoGroup() {
		SupplierItem request = supplier();
		request.setExternalSys(null);
		this.validator.supplierGroups(request, monitor());
		assertTrue(true);
	}

	@Test(expected = ValidateException.class)
	public void testSupplierGroupsNoModel() {
		GroupCredentialItem group = group();
		group.setDistriModels(null);
		group.setAllMasters(Set.of("test"));

		SupplierItem request = supplier();
		request.setExternalSys(Set.of(group));
		this.validator.supplierGroups(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierGroupsNoRates() {
		GroupCredentialItem group = group();
		group.setRates(null);
		group.setAllMasters(Set.of("test"));

		SupplierItem request = supplier();
		request.setExternalSys(Set.of(group));
		this.validator.supplierGroups(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierGroupsNoBrand() {
		GroupCredentialItem group = group();
		group.setBrand(null);

		SupplierItem request = supplier();
		request.setExternalSys(Set.of(group));
		this.validator.supplierGroups(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierGroupsNoClientType() {
		GroupCredentialItem group = group();
		group.setClientType(null);

		SupplierItem request = supplier();
		request.setExternalSys(Set.of(group));
		this.validator.supplierGroups(request, monitor());
	}

	@Test
	public void testSupplierGroupsNoConnectors() {
		GroupCredentialItem group = group();
		group.setExternalConfig(null);

		SupplierItem request = supplier();
		request.setExternalSys(Set.of(group));
		this.validator.supplierGroups(request, monitor());
		assertTrue(true);
	}

	@Test
	public void testSupplierGroupsEmptyConnector() {
		GroupCredentialItem group = group();
		group.setExternalConfig(Set.of(new ExternalCredentialItem()));

		SupplierItem request = supplier();
		request.setExternalSys(Set.of(group));
		this.validator.supplierGroups(request, monitor());
		assertTrue(true);
	}

	@Test
	public void testSupplierGroups() {
		ExternalCredentialItem external = new ExternalCredentialItem();
		external.setCheckIn(new DateRange());
		external.setBook(new DateRange());
		external.getCheckIn().setFrom("20/06/2020");
		external.getCheckIn().setTo("21/06/2020");
		external.setCorporation(master(MasterType.CORPORATION, "1"));
		external.setInvoicingType(master(MasterType.BILLING_TYPE, "1"));

		GroupCredentialItem group = group();
		group.setExternalConfig(Set.of(external));

		SupplierItem request = supplier();
		request.setExternalSys(Set.of(group));
		this.validator.supplierGroups(request, monitor());
		assertTrue(true);
	}
}