package com.globalia.application.validator.supplier;

import com.globalia.HelperTest;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SupplierByZoneValidatorTest extends HelperTest {

	@InjectMocks
	private SupplierByZoneValidator validator;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testSupplierByZoneValidationNoCodPro() {
		SupplierByZoneItem request = supplierByZone();
		request.setCodpro(null);
		this.validator.supplierByZoneValidation(request, monitor());
	}

	@Test(expected = ValidateException.class)
	public void testSupplierByZoneValidationNoUser() {
		SupplierByZoneItem request = supplierByZone();
		request.setCoduser(null);
		this.validator.supplierByZoneValidation(request, monitor());
	}

	@Test
	public void testSupplierByZoneValidation() {
		this.validator.supplierByZoneValidation(supplierByZone(), monitor());
		assertTrue(true);
	}
}
