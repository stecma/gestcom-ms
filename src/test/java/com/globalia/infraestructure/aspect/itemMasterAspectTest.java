package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.master.MasterValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class itemMasterAspectTest extends HelperTest {

	@InjectMocks
	private ItemMasterAspect validator;
	@Mock
	private MasterValidator masterValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testGetItemNullRequest() throws Throwable {
		this.validator.getItem(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testGetItemNullMaster() throws Throwable {
		this.validator.getItem(this.joinPoint, new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testGetItemNoId() throws Throwable {
		ApiRQ item = HelperTest.masterRQ();
		item.getMaster().setId(null);
		this.validator.getItem(this.joinPoint, item);
	}

	@Test
	public void testGetItem() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.getItem(this.joinPoint, HelperTest.masterRQ()));
	}

	@Test(expected = ValidateException.class)
	public void testGetAllItemsNullRequest() throws Throwable {
		this.validator.getAllItems(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testGetAllItemsNullMaster() throws Throwable {
		this.validator.getAllItems(this.joinPoint, new ApiRQ());
	}

	@Test
	public void testGetAllItems() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.getAllItems(this.joinPoint, HelperTest.masterRQ()));
	}
}