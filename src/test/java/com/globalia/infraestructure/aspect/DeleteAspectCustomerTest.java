package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.customer.CustomerValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class DeleteAspectCustomerTest extends HelperTest {

	@InjectMocks
	private DeleteAspect validator;
	@Mock
	private CustomerValidator customerValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerCustomerNullRequest() throws Throwable {
		this.validator.deleteController(this.joinPoint, "customer", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullCustomer() throws Throwable {
		this.validator.deleteController(this.joinPoint, "customer", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerCustomerNoId() throws Throwable {
		ApiRQ item = HelperTest.customerRQ();
		item.getCustom().setId(null);

		this.validator.deleteController(this.joinPoint, "customer", item);
	}

	@Test
	public void testDeleteController() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.deleteController(this.joinPoint, "customer", HelperTest.customerRQ()));
	}
}