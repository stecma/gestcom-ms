package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.credential.CredentialValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class DeleteAspectCredentialTest extends HelperTest {

	@InjectMocks
	private DeleteAspect validator;
	@Mock
	private CredentialValidator credentialValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerCredentialNullRequest() throws Throwable {
		this.validator.deleteController(this.joinPoint, "credential", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullCredential() throws Throwable {
		this.validator.deleteController(this.joinPoint, "credential", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerCredentialNoId() throws Throwable {
		ApiRQ item = HelperTest.credentialRQ();
		item.getCredential().setId(null);
		this.validator.deleteController(this.joinPoint, "credential", item);
	}

	@Test
	public void testDeleteControllerCredential() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.deleteController(this.joinPoint, "credential", HelperTest.credentialRQ()));
	}
}