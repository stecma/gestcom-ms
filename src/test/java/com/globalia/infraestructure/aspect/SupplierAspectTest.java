package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class SupplierAspectTest extends HelperTest {

	@InjectMocks
	private SupplierAspect validator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testGetMethodNullRequest() throws Throwable {
		this.validator.getMethod(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testGetMethodNullSupplier() throws Throwable {
		this.validator.getMethod(this.joinPoint, new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testGetMethodNoId() throws Throwable {
		ApiRQ item = HelperTest.supplierRQ();
		item.getSupplier().setId(null);
		this.validator.getMethod(this.joinPoint, item);
	}

	@Test
	public void testGetMethod() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.getMethod(this.joinPoint, HelperTest.supplierRQ()));
	}

	@Test(expected = ValidateException.class)
	public void testUpdateSapMethodError() throws Throwable {
		when(this.joinPoint.proceed()).thenReturn("OK");
		this.validator.updateSapMethod(this.joinPoint, "", "", "test", true, HelperTest.supplierRQ());
	}

	@Test
	public void testUpdateSapMethod() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.updateSapMethod(this.joinPoint, "system", "", "test", true, HelperTest.supplierRQ()));
	}

	@Test(expected = ValidateException.class)
	public void testUpdateServiceMethodError() throws Throwable {
		when(this.joinPoint.proceed()).thenReturn("OK");
		this.validator.updateServiceMethod(this.joinPoint, "", "", "test", "test", HelperTest.supplierRQ());
	}

	@Test
	public void testUpdateServiceMethod() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.updateServiceMethod(this.joinPoint, "system", "", "test", "test", HelperTest.supplierRQ()));
	}
}