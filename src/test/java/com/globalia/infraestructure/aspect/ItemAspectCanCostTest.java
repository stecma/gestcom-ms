package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class ItemAspectCanCostTest extends HelperTest {

	@InjectMocks
	private ItemAspect validator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerCancelCostNullRequest() throws Throwable {
		this.validator.itemController(this.joinPoint, "cancelcost", null);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerNullCancelCost() throws Throwable {
		this.validator.itemController(this.joinPoint, "cancelcost", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerCancelCostNoId() throws Throwable {
		ApiRQ item = HelperTest.cancelCostRQ();
		item.getCancelCost().setId(null);

		this.validator.itemController(this.joinPoint, "cancelcost", item);
	}

	@Test
	public void testItemControllerCancelCost() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.itemController(this.joinPoint, "cancelcost", HelperTest.cancelCostRQ()));
	}
}