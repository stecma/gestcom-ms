package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.currency.CurrencyValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CreateAspectCurrencyTest extends HelperTest {

	@InjectMocks
	private CreateAspect validator;
	@Mock
	private CurrencyValidator currencyValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerCurrencyNullRequest() throws Throwable {
		this.validator.createController(this.joinPoint, "currency", null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerNullCurrency() throws Throwable {
		this.validator.createController(this.joinPoint, "currency", new ApiRQ());
	}

	@Test
	public void testCreateControllerCurrency() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.createController(this.joinPoint, "currency", HelperTest.currencyRQ()));
	}
}