package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.currency.CurrencyByCountryValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CurrencyByCountrySaveAspectTest extends HelperTest {

	@InjectMocks
	private CurrencyByCountrySaveAspect validator;
	@Mock
	private CurrencyByCountryValidator currencyValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCreateNullRequest() throws Throwable {
		this.validator.create(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateNullCurrency() throws Throwable {
		this.validator.create(this.joinPoint, new ApiRQ());
	}

	@Test
	public void testCreate() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.create(this.joinPoint, HelperTest.currencyRQ()));
	}

	@Test(expected = ValidateException.class)
	public void testUpdateDeleteNullRequest() throws Throwable {
		this.validator.updateDelete(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateDeleteNullCurrency() throws Throwable {
		this.validator.updateDelete(this.joinPoint, new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateDeleteNoId() throws Throwable {
		ApiRQ item = HelperTest.currencyRQ();
		item.getCurrency().setId(null);
		this.validator.updateDelete(this.joinPoint, item);
	}

	@Test
	public void testUpdateDelete() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.updateDelete(this.joinPoint, HelperTest.currencyRQ()));
	}
}