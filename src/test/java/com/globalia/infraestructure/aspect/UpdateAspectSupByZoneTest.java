package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.supplier.SupplierByZoneValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class UpdateAspectSupByZoneTest extends HelperTest {

	@InjectMocks
	private UpdateAspect validator;
	@Mock
	private SupplierByZoneValidator supplierByZoneValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerSupByZoneNullRequest() throws Throwable {
		this.validator.updateController(this.joinPoint, "supplierbyzone", null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerNullSupByZone() throws Throwable {
		this.validator.updateController(this.joinPoint, "supplierbyzone", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerSupplierByZoneNoId() throws Throwable {
		ApiRQ item = HelperTest.supplierByZoneRQ();
		item.getSupplierbyzone().setId(null);

		this.validator.updateController(this.joinPoint, "supplierbyzone", item);
	}

	@Test
	public void testUpdateControllerTrade() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.updateController(this.joinPoint, "supplierbyzone", HelperTest.supplierByZoneRQ()));
	}
}