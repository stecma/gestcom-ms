package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.trade.TradeValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CreateAspectTradeTest extends HelperTest {

	@InjectMocks
	private CreateAspect validator;
	@Mock
	private TradeValidator tradeValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerTradeNullRequest() throws Throwable {
		this.validator.createController(this.joinPoint, "trade", null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerNullTrade() throws Throwable {
		this.validator.createController(this.joinPoint, "trade", new ApiRQ());
	}

	@Test
	public void testCreateControllerTrade() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.createController(this.joinPoint, "trade", HelperTest.tradeRQ()));
	}
}