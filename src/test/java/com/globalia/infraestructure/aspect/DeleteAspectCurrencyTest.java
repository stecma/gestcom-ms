package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.currency.CurrencyValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class DeleteAspectCurrencyTest extends HelperTest {

	@InjectMocks
	private DeleteAspect validator;
	@Mock
	private CurrencyValidator currencyValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerCurrencyNullRequest() throws Throwable {
		this.validator.deleteController(this.joinPoint, "currency", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullCurrency() throws Throwable {
		this.validator.deleteController(this.joinPoint, "currency", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerCurrencyNoId() throws Throwable {
		ApiRQ item = HelperTest.currencyRQ();
		item.getCurrency().setId(null);

		this.validator.deleteController(this.joinPoint, "currency", item);
	}

	@Test
	public void testDeleteController() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.deleteController(this.joinPoint, "currency", HelperTest.currencyRQ()));
	}
}