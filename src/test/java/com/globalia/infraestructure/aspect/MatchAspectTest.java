package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.dto.credential.CredentialFilter;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class MatchAspectTest extends HelperTest {

	@InjectMocks
	private MatchAspect validator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testMatchSupplierNullRequest() throws Throwable {
		this.validator.matchSupplier(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testMatchSupplierEmptyRequest() throws Throwable {
		this.validator.matchSupplier(this.joinPoint, new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testMatchSupplierNoGroup() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setGroupId(null);
		this.validator.matchSupplier(this.joinPoint, item);
	}

	@Test(expected = ValidateException.class)
	public void testMatchSupplierNoSupplier() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setSupplierId(null);
		this.validator.matchSupplier(this.joinPoint, item);
	}

	@Test(expected = ValidateException.class)
	public void testMatchSupplierNoCustomer() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setCustomer(null);
		this.validator.matchSupplier(this.joinPoint, item);
	}

	@Test(expected = ValidateException.class)
	public void testMatchSupplierNoCustomerFilter() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setCustomer(new CredentialItem());
		this.validator.matchSupplier(this.joinPoint, item);
	}

	@Test
	public void testMatchSupplier() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setCustomer(new CredentialItem());
		item.getMatch().getCustomer().setFilter(new CredentialFilter());
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.matchSupplier(this.joinPoint, item));
	}

	@Test(expected = ValidateException.class)
	public void testMatchCustomerNullRequest() throws Throwable {
		this.validator.matchCustomer(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testMatchCustomerEmptyRequest() throws Throwable {
		this.validator.matchCustomer(this.joinPoint, new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testMatchCustomerNoCredential() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setCredentialId(null);
		this.validator.matchCustomer(this.joinPoint, item);
	}

	@Test(expected = ValidateException.class)
	public void testMatchCustomerNoSuppier() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setSupplier(null);
		this.validator.matchCustomer(this.joinPoint, item);
	}

	@Test(expected = ValidateException.class)
	public void testMatchCustomerNoSupplierFilter() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().getSupplier().setFilter(null);
		this.validator.matchCustomer(this.joinPoint, item);
	}

	@Test
	public void testMatchCustomer() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.matchCustomer(this.joinPoint, matchRQ()));
	}

	@Test(expected = ValidateException.class)
	public void testAssignNullRequest() throws Throwable {
		this.validator.assign(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testAssignEmptyRequest() throws Throwable {
		this.validator.assign(this.joinPoint, new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAssignNoCodes() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setCodes(null);
		this.validator.assign(this.joinPoint, item);
	}

	@Test(expected = ValidateException.class)
	public void testAssignNoGroupNoCredential() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setGroupId(null);
		item.getMatch().setCredentialId(null);
		this.validator.assign(this.joinPoint, item);
	}

	@Test(expected = ValidateException.class)
	public void testAssignNoAssigment() throws Throwable {
		ApiRQ item = HelperTest.matchRQ();
		item.getMatch().setAssignment(null);
		this.validator.assign(this.joinPoint, item);
	}

	@Test
	public void testAssign() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.assign(this.joinPoint, HelperTest.matchRQ()));
	}
}