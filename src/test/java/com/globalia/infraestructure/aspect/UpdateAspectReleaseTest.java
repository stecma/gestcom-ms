package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.release.ReleaseValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class UpdateAspectReleaseTest extends HelperTest {

	@InjectMocks
	private UpdateAspect validator;
	@Mock
	private ReleaseValidator releaseValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerReleaseNullRequest() throws Throwable {
		this.validator.updateController(this.joinPoint, "release", null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerNullRelease() throws Throwable {
		this.validator.updateController(this.joinPoint, "release", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerReleaseNoId() throws Throwable {
		ApiRQ item = HelperTest.releaseRQ();
		item.getRelease().setId(null);
		this.validator.updateController(this.joinPoint, "release", item);
	}

	@Test
	public void testUpdateControllerRelease() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.updateController(this.joinPoint, "release", HelperTest.releaseRQ()));
	}
}