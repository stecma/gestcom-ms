package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.trade.TradeValidator;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class DeleteAspectTradeTest extends HelperTest {

	@InjectMocks
	private DeleteAspect validator;
	@Mock
	private TradeValidator tradeValidator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerTradeNullRequest() throws Throwable {
		this.validator.deleteController(this.joinPoint, "trade", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullTrade() throws Throwable {
		this.validator.deleteController(this.joinPoint, "trade", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerTradeNoId() throws Throwable {
		ApiRQ item = HelperTest.tradeRQ();
		item.getTrade().setId(null);

		this.validator.deleteController(this.joinPoint, "trade", item);
	}

	@Test
	public void testDeleteControllerTrade() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.deleteController(this.joinPoint, "trade", HelperTest.tradeRQ()));
	}
}