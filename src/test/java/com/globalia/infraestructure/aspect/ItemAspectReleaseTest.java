package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.credential.ApiRQ;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class ItemAspectReleaseTest extends HelperTest {

	@InjectMocks
	private ItemAspect validator;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private AspectHandler aspectHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerReleaseNullRequest() throws Throwable {
		this.validator.itemController(this.joinPoint, "release", null);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerNullRelease() throws Throwable {
		this.validator.itemController(this.joinPoint, "release", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerReleaseNoId() throws Throwable {
		ApiRQ item = HelperTest.releaseRQ();
		item.getRelease().setId(null);

		this.validator.itemController(this.joinPoint, "release", item);
	}

	@Test
	public void testItemControllerRelease() throws Throwable {
		when(this.aspectHandler.execute(any(), any())).thenReturn("OK");
		assertNotNull(this.validator.itemController(this.joinPoint, "release", HelperTest.releaseRQ()));
	}
}