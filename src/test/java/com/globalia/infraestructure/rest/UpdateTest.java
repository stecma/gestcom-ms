package com.globalia.infraestructure.rest;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.infraestructure.rest.converter.SaveConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class UpdateTest extends HelperTest {

	@InjectMocks
	private Update update;
	@Mock
	private SaveConverter converter;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testDeleteCreateError() {
		when(this.converter.save(any(), any(), any())).thenReturn(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
		ResponseEntity<ApiRS> response = this.update.update("trade", tradeRQ());
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testDelete() {
		when(this.converter.save(any(), any(), any())).thenReturn(new ItemResponse());
		ResponseEntity<ApiRS> response = this.update.update("trade", tradeRQ());
		assertNotNull(response);
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
	}
}