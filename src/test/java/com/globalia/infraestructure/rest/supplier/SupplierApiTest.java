package com.globalia.infraestructure.rest.supplier;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.supplier.SaveSupplier;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class SupplierApiTest extends HelperTest {

	@InjectMocks
	private SupplierApi supplierApi;
	@Mock
	private Supplier supplier;
	@Mock
	private SaveSupplier save;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testMapKeyError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.supplier.getMapKey(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.supplierApi.getMapKey(supplierRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testMapKey() {
		ItemResponse response = new ItemResponse();
		response.setSupplier(supplier());

		when(this.supplier.getMapKey(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.supplierApi.getMapKey(supplierRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testGetGroupError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.supplier.getGroup(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.supplierApi.getGroup(supplierRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testGetGroup() {
		ItemResponse response = new ItemResponse();
		response.setGroup(group());

		when(this.supplier.getGroup(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.supplierApi.getGroup(supplierRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testUpdateSapError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.save.updateSap(anyString(), anyString(), anyBoolean(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.supplierApi.updateSap("system", "null", "sap", "true", supplierRQ());
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testUpdateSap() {
		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());

		when(this.save.updateSap(anyString(), anyString(), anyBoolean(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.supplierApi.updateSap("system", "subsystem", "sap", "true", supplierRQ());
		assertNotNull(response);
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
	}

	@Test
	public void testUpdateServiceError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.save.updateService(anyString(), anyString(), anyString(), anyString(), anyString(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.supplierApi.updateService("system", "", "refser", "tipser", "srv", "desc", supplierRQ());
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testUpdateService() {
		ItemResponse item = new ItemResponse();
		item.setSupplier(supplier());

		when(this.save.updateService(anyString(), anyString(), anyString(), anyString(), anyString(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.supplierApi.updateService("system", "", "refser", "tipser", "srv", "desc", supplierRQ());
		assertNotNull(response);
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
	}
}