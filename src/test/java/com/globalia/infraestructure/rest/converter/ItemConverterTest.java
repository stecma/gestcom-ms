package com.globalia.infraestructure.rest.converter;

import com.globalia.HelperTest;
import com.globalia.application.service.cancelcost.CancelCost;
import com.globalia.application.service.credential.Credential;
import com.globalia.application.service.customer.Customer;
import com.globalia.application.service.release.Release;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.application.service.supplier.SupplierByZone;
import com.globalia.application.service.trade.Trade;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ItemConverterTest extends HelperTest {

	@InjectMocks
	private ItemConverter result;
	@Mock
	private Trade trade;
	@Mock
	private SupplierByZone supplierByZone;
	@Mock
	private Release release;
	@Mock
	private Customer customer;
	@Mock
	private Supplier supplier;
	@Mock
	private Credential credential;
	@Mock
	private CancelCost cancelCost;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error());
		when(this.trade.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.TRADE, tradeRQ()));
	}

	@Test
	public void testGetItemTrade() {
		ItemResponse response = new ItemResponse();
		response.setTradePolicy(trade());
		when(this.trade.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.TRADE, tradeRQ()));
	}

	@Test
	public void testGetItemSupplierByZone() {
		ItemResponse response = new ItemResponse();
		response.setSupplierByZone(supplierByZone());
		when(this.supplierByZone.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.SUPPLIERBYZONE, supplierByZoneRQ()));
	}

	@Test
	public void testGetItemCustomer() {
		ItemResponse response = new ItemResponse();
		response.setCustomer(customer());
		when(this.customer.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.CUSTOMER, customerRQ()));
	}

	@Test
	public void testGetItemRelease() {
		ItemResponse response = new ItemResponse();
		response.setRelease(release());
		when(this.release.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.RELEASE, releaseRQ()));
	}

	@Test
	public void testGetItemSupplier() {
		ItemResponse response = new ItemResponse();
		response.setSupplier(supplier());
		when(this.supplier.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.SUPPLIER, supplierRQ()));
	}

	@Test
	public void testGetItemCredential() {
		ItemResponse response = new ItemResponse();
		response.setCredential(credential());
		when(this.credential.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.CREDENTIAL, credentialRQ()));
	}

	@Test
	public void testGetItemCancelCost() {
		ItemResponse response = new ItemResponse();
		response.setCancelCost(cancelCost());
		when(this.cancelCost.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.CANCELCOST, cancelCostRQ()));
	}
}
