package com.globalia.infraestructure.rest.converter;

import com.globalia.HelperTest;
import com.globalia.application.service.cancelcost.CancelCosts;
import com.globalia.application.service.credential.Credentials;
import com.globalia.application.service.currency.Currencies;
import com.globalia.application.service.customer.Customers;
import com.globalia.application.service.release.Releases;
import com.globalia.application.service.supplier.SupplierByZones;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.application.service.trade.Trades;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AllItemsConverterTest extends HelperTest {

	@InjectMocks
	private AllItemsConverter result;
	@Mock
	private Trades trades;
	@Mock
	private SupplierByZones supplierByZones;
	@Mock
	private Releases releases;
	@Mock
	private Customers customers;
	@Mock
	private Suppliers suppliers;
	@Mock
	private Credentials credentials;
	@Mock
	private CancelCosts cancelCosts;
	@Mock
	private Currencies currencies;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemsError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error());
		when(this.trades.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.TRADE, tradeRQ()));
	}

	@Test
	public void testGetAllItemsTrade() {
		AllItemsResponse response = new AllItemsResponse();
		response.setTradePolicies(new LinkedHashSet<>(Set.of(trade())));
		when(this.trades.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.TRADE, tradeRQ()));
	}

	@Test
	public void testGetAllItemsSupplierByZone() {
		AllItemsResponse response = new AllItemsResponse();
		response.setSuppliersByZone(new LinkedHashSet<>(Set.of(supplierByZone())));
		when(this.supplierByZones.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.SUPPLIERBYZONE, supplierRQ()));
	}

	@Test
	public void testGetAllItemsRelease() {
		AllItemsResponse response = new AllItemsResponse();
		response.setReleases(new LinkedHashSet<>(Set.of(release())));
		when(this.releases.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.RELEASE, releaseRQ()));
	}

	@Test
	public void testGetAllItemsCustomer() {
		AllItemsResponse response = new AllItemsResponse();
		response.setCustomers(new LinkedHashSet<>(Set.of(customer())));
		when(this.customers.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.CUSTOMER, customerRQ()));
	}

	@Test
	public void testGetAllItemsSupplier() {
		AllItemsResponse response = new AllItemsResponse();
		response.setSuppliers(new LinkedHashSet<>(Set.of(supplier())));
		when(this.suppliers.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.SUPPLIER, supplierRQ()));
	}

	@Test
	public void testGetAllItemsCredential() {
		AllItemsResponse response = new AllItemsResponse();
		response.setCredentials(new LinkedHashSet<>(Set.of(credential())));
		when(this.credentials.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.CREDENTIAL, credentialRQ()));
	}

	@Test
	public void testGetAllItemsCancelCost() {
		AllItemsResponse response = new AllItemsResponse();
		response.setCancelCosts(new LinkedHashSet<>(Set.of(cancelCost())));
		when(this.cancelCosts.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.CANCELCOST, cancelCostRQ()));
	}

	@Test
	public void testGetAllItemsCurrency() {
		AllItemsResponse response = new AllItemsResponse();
		response.setCurrencies(new LinkedHashSet<>(Set.of(currency())));
		when(this.currencies.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.CURRENCY, currencyRQ()));
	}
}
