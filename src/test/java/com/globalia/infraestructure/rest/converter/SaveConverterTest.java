package com.globalia.infraestructure.rest.converter;

import com.globalia.HelperTest;
import com.globalia.application.service.cancelcost.SaveCancelCost;
import com.globalia.application.service.credential.SaveCredential;
import com.globalia.application.service.currency.SaveCurrency;
import com.globalia.application.service.customer.SaveCustomer;
import com.globalia.application.service.master.SaveMaster;
import com.globalia.application.service.release.SaveRelease;
import com.globalia.application.service.supplier.SaveSupplier;
import com.globalia.application.service.supplier.SaveSupplierByZone;
import com.globalia.application.service.trade.SaveTrade;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveConverterTest extends HelperTest {

	@InjectMocks
	private SaveConverter result;
	@Mock
	private SaveTrade saveTrade;
	@Mock
	private SaveSupplierByZone saveSupplierByZone;
	@Mock
	private SaveRelease saveRelease;
	@Mock
	private SaveCustomer saveCustomer;
	@Mock
	private SaveMaster saveMaster;
	@Mock
	private SaveSupplier saveSupplier;
	@Mock
	private SaveCredential saveCredential;
	@Mock
	private SaveCancelCost saveCancelCost;
	@Mock
	private SaveCurrency saveCurrency;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testSaveError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error());
		when(this.saveTrade.createItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.TRADE, SaveConverter.saveAction.CREATE, tradeRQ()));
	}

	@Test
	public void testSaveTrade() {
		ItemResponse response = new ItemResponse();
		response.setTradePolicy(trade());
		when(this.saveTrade.createItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.TRADE, SaveConverter.saveAction.CREATE, tradeRQ()));
	}

	@Test
	public void testSaveSupplierByZone() {
		ItemResponse response = new ItemResponse();
		response.setSupplierByZone(supplierByZone());
		when(this.saveSupplierByZone.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.SUPPLIERBYZONE, SaveConverter.saveAction.UPDATE, supplierByZoneRQ()));
	}

	@Test
	public void testSaveRelease() {
		ItemResponse response = new ItemResponse();
		response.setRelease(release());
		when(this.saveRelease.deleteItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.RELEASE, SaveConverter.saveAction.DELETE, releaseRQ()));
	}

	@Test
	public void testSaveCustomer() {
		ItemResponse response = new ItemResponse();
		response.setCustomer(customer());
		when(this.saveCustomer.createItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.CUSTOMER, SaveConverter.saveAction.CREATE, customerRQ()));
	}

	@Test
	public void testSaveMaster() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());
		when(this.saveMaster.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.MASTER, SaveConverter.saveAction.UPDATE, masterRQ()));
	}

	@Test
	public void testSaveSupplier() {
		ItemResponse response = new ItemResponse();
		response.setSupplier(supplier());
		when(this.saveSupplier.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.SUPPLIER, SaveConverter.saveAction.UPDATE, supplierRQ()));
	}

	@Test
	public void testSaveCredential() {
		ItemResponse response = new ItemResponse();
		response.setCredential(credential());
		when(this.saveCredential.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.CREDENTIAL, SaveConverter.saveAction.UPDATE, credentialRQ()));
	}

	@Test
	public void testSaveCancelCost() {
		ItemResponse response = new ItemResponse();
		response.setCancelCost(cancelCost());
		when(this.saveCancelCost.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.CANCELCOST, SaveConverter.saveAction.UPDATE, cancelCostRQ()));
	}

	@Test
	public void testSaveCurrency() {
		ItemResponse response = new ItemResponse();
		response.setCurrency(currency());
		when(this.saveCurrency.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.CURRENCY, SaveConverter.saveAction.UPDATE, currencyRQ()));
	}
}
