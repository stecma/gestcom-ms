package com.globalia.infraestructure.rest;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.HealthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class HealthTest extends HelperTest {

	@InjectMocks
	private Health healthController;
	@Mock
	private HealthService healthService;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testHealth() {
		when(this.healthService.health()).thenReturn(org.springframework.boot.actuate.health.Health.down().build());
		ResponseEntity<ApiRS> response = this.healthController.health();
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void testReload() {
		ResponseEntity<Void> response = this.healthController.reload();
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

}
