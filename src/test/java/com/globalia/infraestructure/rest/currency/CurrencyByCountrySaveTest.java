package com.globalia.infraestructure.rest.currency;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.currency.SaveCurrency;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CurrencyByCountrySaveTest extends HelperTest {

	@InjectMocks
	private CurrencyByCountrySave save;
	@Mock
	private SaveCurrency currency;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateItemError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.currency.createItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.createCurrency(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testCreateItem() {
		ItemResponse response = new ItemResponse();
		response.setCurrency(currency());

		when(this.currency.createItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.createCurrency(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.CREATED, apiRS.getStatusCode());
	}

	@Test
	public void testUpdateItemError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.currency.updateItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.updateCurrency(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testUpdateItem() {
		ItemResponse response = new ItemResponse();
		response.setCurrency(currency());

		when(this.currency.updateItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.updateCurrency(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.ACCEPTED, apiRS.getStatusCode());
	}

	@Test
	public void testDeleteItemError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.currency.deleteItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.deleteCurrency(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testDeleteItem() {
		ItemResponse response = new ItemResponse();
		response.setCurrency(currency());

		when(this.currency.deleteItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.deleteCurrency(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.ACCEPTED, apiRS.getStatusCode());
	}
}