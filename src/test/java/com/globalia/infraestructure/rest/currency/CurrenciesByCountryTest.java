package com.globalia.infraestructure.rest.currency;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.currency.Currencies;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CurrenciesByCountryTest extends HelperTest {

	@InjectMocks
	private CurrenciesByCountry items;
	@Mock
	private Currencies currencies;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemsError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.currencies.getAllItemsByCountry(any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.items.getAllItems(currencyRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testGetAllItems() {
		AllItemsResponse response = new AllItemsResponse();
		response.setCurrencies(new LinkedHashSet<>(Set.of(currency())));

		when(this.currencies.getAllItemsByCountry(any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.items.getAllItems(currencyRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}
}