package com.globalia.infraestructure.rest.match;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.match.AssignMatch;
import com.globalia.application.service.match.CustomerMatch;
import com.globalia.application.service.match.SupplierMatch;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class MatchApiTest extends HelperTest {

	@InjectMocks
	private MatchApi matchApi;
	@Mock
	private SupplierMatch supplierMatch;
	@Mock
	private CustomerMatch customerMatch;
	@Mock
	private AssignMatch assignMatch;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testMatchSupplierError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.supplierMatch.matchSupplier(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.matchApi.matchSupplier(matchRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testMatchSupplier() {
		ItemResponse response = new ItemResponse();
		response.setMatch(match());

		when(this.supplierMatch.matchSupplier(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.matchApi.matchSupplier(matchRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testMatchCustomerError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.customerMatch.matchCustomer(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.matchApi.matchCustomer(matchRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testMatchCustomer() {
		ItemResponse response = new ItemResponse();
		response.setMatch(match());

		when(this.customerMatch.matchCustomer(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.matchApi.matchCustomer(matchRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testAssignError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.assignMatch.assign(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.matchApi.assign(customerRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testItemCustomer() {
		ItemResponse response = new ItemResponse();
		response.setMatch(match());

		when(this.assignMatch.assign(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.matchApi.assign(customerRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}
}