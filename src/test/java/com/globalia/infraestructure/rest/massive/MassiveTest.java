package com.globalia.infraestructure.rest.massive;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.customer.SaveCustomer;
import com.globalia.application.service.trade.SaveTrade;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class MassiveTest extends HelperTest {

	@InjectMocks
	private Massive massive;
	@Mock
	private SaveTrade trade;
	@Mock
	private SaveCustomer customer;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testItemTradeError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.trade.massive(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.massive.massive("trades", tradeRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testItemTrade() {
		AllItemsResponse response = new AllItemsResponse();
		response.setTradePolicies(Set.of(trade()));

		when(this.trade.massive(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.massive.massive("trades", tradeRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.CREATED, apiRS.getStatusCode());
	}

	@Test
	public void testItemCustomerError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.customer.massive(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.massive.massive("customers", customerRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testItemCustomer() {
		AllItemsResponse response = new AllItemsResponse();
		response.setCustomers(Set.of(customer()));

		when(this.customer.massive(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.massive.massive("customers", customerRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.CREATED, apiRS.getStatusCode());
	}
}