package com.globalia.infraestructure.rest.master;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.infraestructure.rest.converter.SaveConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class MasterSaveTest extends HelperTest {

	@InjectMocks
	private MasterSave save;
	@Mock
	private SaveConverter converter;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateItemError() {
		when(this.converter.save(any(), any(), any())).thenReturn(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
		ResponseEntity<ApiRS> apiRS = this.save.create(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testCreateItem() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());

		when(this.converter.save(any(), any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.create(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.CREATED, apiRS.getStatusCode());
	}

	@Test
	public void testUpdateItemError() {
		when(this.converter.save(any(), any(), any())).thenReturn(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
		ResponseEntity<ApiRS> apiRS = this.save.update(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testUpdateItem() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());

		when(this.converter.save(any(), any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.update(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.ACCEPTED, apiRS.getStatusCode());
	}

	@Test
	public void testDeleteItemError() {
		when(this.converter.save(any(), any(), any())).thenReturn(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
		ResponseEntity<ApiRS> apiRS = this.save.delete(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testDeleteItem() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());

		when(this.converter.save(any(), any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.save.delete(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.ACCEPTED, apiRS.getStatusCode());
	}
}