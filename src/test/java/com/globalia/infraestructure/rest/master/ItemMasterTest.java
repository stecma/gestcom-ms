package com.globalia.infraestructure.rest.master;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.master.MasterTypes;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class ItemMasterTest extends HelperTest {

	@InjectMocks
	private ItemMaster item;
	@Mock
	private Master master;
	@Mock
	private MasterTypes masterTypes;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.master.getItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.item.getItem(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testGetItem() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());

		when(this.master.getItem(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.item.getItem(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testTypes() {
		when(this.masterTypes.getType(any(MasterType.class))).thenReturn("permission");
		assertNotNull(this.item.types(masterRQ()));
	}
}