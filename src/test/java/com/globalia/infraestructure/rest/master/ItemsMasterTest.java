package com.globalia.infraestructure.rest.master;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class ItemsMasterTest extends HelperTest {

	@InjectMocks
	private ItemsMaster items;
	@Mock
	private Masters masters;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testSearchItemsError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.masters.searchItems(anyString(), anyString(), nullable(String.class), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.items.searchItems("test", "test", "null", masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testSearchItems() {
		AllItemsResponse response = new AllItemsResponse();
		response.setMasters(new LinkedHashSet<>(Set.of(master())));

		when(this.masters.searchItems(anyString(), anyString(), anyString(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.items.searchItems("test", "test", "test", masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}

	@Test
	public void testGetAllItemsError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));

		when(this.masters.getAllItems(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.items.getAllItems(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, apiRS.getStatusCode());
	}

	@Test
	public void testGetAllItems() {
		AllItemsResponse response = new AllItemsResponse();
		response.setMasters(new LinkedHashSet<>(Set.of(master())));

		when(this.masters.getAllItems(any(), any())).thenReturn(response);
		ResponseEntity<ApiRS> apiRS = this.items.getAllItems(masterRQ());
		assertNotNull(apiRS);
		assertEquals(HttpStatus.OK, apiRS.getStatusCode());
	}
}