package com.globalia.infraestructure;

import com.globalia.api.credential.ApiRQ;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.infraestructure.rest.converter.ItemConverter;

public abstract class AbstractConverter {

	protected Object getRequest(final ItemConverter.apiRequest entity, final ApiRQ request) {
		switch (entity) {
			case CURRENCY:
				return request.getCurrency();
			case SUPPLIERBYZONE:
				return request.getSupplierbyzone();
			case RELEASE:
				return request.getRelease();
			case CUSTOMER:
				return request.getCustom();
			case CREDENTIAL:
				return request.getCredential();
			case SUPPLIER:
				return request.getSupplier();
			case TRADE:
				return request.getTrade();
			case MASTER:
				return request.getMaster();
			case CANCELCOST:
			default:
				return request.getCancelCost();
		}
	}

	protected Object getResponse(final ItemConverter.apiRequest entity, final ItemResponse response) {
		switch (entity) {
			case CURRENCY:
				return response.getCurrency();
			case RELEASE:
				return response.getRelease();
			case CUSTOMER:
				return response.getCustomer();
			case CREDENTIAL:
				return response.getCredential();
			case SUPPLIER:
				return response.getSupplier();
			case TRADE:
				return response.getTradePolicy();
			case SUPPLIERBYZONE:
				return response.getSupplierByZone();
			case MASTER:
				return response.getMaster();
			case CANCELCOST:
			default:
				return response.getCancelCost();
		}
	}

}