package com.globalia.infraestructure;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.cancelcost.CancelCostValidator;
import com.globalia.application.validator.credential.CredentialValidator;
import com.globalia.application.validator.currency.CurrencyValidator;
import com.globalia.application.validator.customer.CustomerValidator;
import com.globalia.application.validator.release.ReleaseValidator;
import com.globalia.application.validator.supplier.SupplierByZoneValidator;
import com.globalia.application.validator.supplier.SupplierValidator;
import com.globalia.application.validator.trade.TradeValidator;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.aspect.AspectHandler;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.util.StringUtils;

@SuppressWarnings("BooleanMethodIsAlwaysInverted")
@RefreshScope
public abstract class AbstractAspect {

	protected static final String ID_MANDATORY = "Field Id is mandatory";

	@Getter
	@Autowired
	private AspectHandler handler;
	@Autowired
	private SupplierValidator supplierValidator;
	@Autowired
	private CredentialValidator credentialValidator;
	@Autowired
	private CancelCostValidator cancelCostValidator;
	@Autowired
	private ReleaseValidator releaseValidator;
	@Autowired
	private TradeValidator tradeValidator;
	@Autowired
	private SupplierByZoneValidator supplierByZoneValidator;
	@Autowired
	private CustomerValidator customerValidator;
	@Autowired
	private CurrencyValidator currencyValidator;

	protected void validateRequest(final ItemConverter.apiRequest entity, final ApiRQ request) {
		if (request == null || !this.isValidRQ(entity, request)) {
			throw new ValidateException(new Error("Invalid request", ErrorLayer.API_LAYER), null);
		}
	}

	protected boolean isEmptyID(final ItemConverter.apiRequest entity, final ApiRQ request) {
		switch (entity) {
			case CURRENCY:
				return !StringUtils.hasText(request.getCurrency().getId());
			case MASTER:
				return !StringUtils.hasText(request.getMaster().getId());
			case TRADE:
				return !StringUtils.hasText(request.getTrade().getId());
			case SUPPLIERBYZONE:
				return !StringUtils.hasText(request.getSupplierbyzone().getId());
			case RELEASE:
				return !StringUtils.hasText(request.getRelease().getId());
			case CUSTOMER:
				return !StringUtils.hasText(request.getCustom().getId());
			case SUPPLIER:
				return !StringUtils.hasText(request.getSupplier().getId());
			case CREDENTIAL:
				return !StringUtils.hasText(request.getCredential().getId());
			case CANCELCOST:
			default:
				return !StringUtils.hasText(request.getCancelCost().getId());
		}
	}

	protected void validateContent(final ItemConverter.apiRequest entity, final ApiRQ request) {
		this.validateContent(entity, request, false);
	}

	protected void validateContent(final ItemConverter.apiRequest entity, final ApiRQ request, final boolean done) {
		switch (entity) {
			case CURRENCY:
				this.currencyValidator.currencyValidation(request.getCurrency(), done, request.getMonitor());
				break;
			case TRADE:
				this.tradeValidator.tradeValidation(request.getTrade(), done, request.getMonitor());
				break;
			case SUPPLIERBYZONE:
				if (!done) {
					this.supplierByZoneValidator.supplierByZoneValidation(request.getSupplierbyzone(), request.getMonitor());
				}
				break;
			case RELEASE:
				if (!done) {
					this.releaseValidator.releaseValidation(request.getRelease(), request.getMonitor());
				}
				break;
			case CUSTOMER:
				this.customerValidator.customerValidation(request.getCustom(), done, request.getMonitor());
				break;
			case SUPPLIER:
				if (!done) {
					this.supplierValidator.supplierValidation(request.getSupplier(), request.getMonitor());
					this.supplierValidator.supplierGroups(request.getSupplier(), request.getMonitor());
				}
				break;
			case CREDENTIAL:
				if (!done) {
					this.credentialValidator.credentialValidation(request.getCredential(), request.getMonitor());
				}
				break;
			case CANCELCOST:
			default:
				if (!done) {
					this.cancelCostValidator.cancelCostValidation(request.getCancelCost(), request.getMonitor());
				}
		}
	}

	private boolean isValidRQ(final ItemConverter.apiRequest entity, final ApiRQ request) {
		switch (entity) {
			case CURRENCY:
				return request.getCurrency() != null;
			case MASTER:
				return request.getMaster() != null;
			case MATCH:
				return request.getMatch() != null;
			case MASSIVE:
				return request.getMassive() != null;
			case TRADE:
				return request.getTrade() != null;
			case SUPPLIERBYZONE:
				return request.getSupplierbyzone() != null;
			case RELEASE:
				return request.getRelease() != null;
			case CUSTOMER:
				return request.getCustom() != null;
			case SUPPLIER:
				return request.getSupplier() != null;
			case CREDENTIAL:
				return request.getCredential() != null;
			case CANCELCOST:
			default:
				return request.getCancelCost() != null;
		}
	}
}