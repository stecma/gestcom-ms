package com.globalia.infraestructure;

import com.globalia.api.ApiRS;
import com.globalia.exception.Error;
import com.globalia.monitoring.Monitor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {

	protected ResponseEntity<ApiRS> buildResponse(final Object obj, final HttpStatus status, final Monitor monitor) {
		HttpStatus statusRS = status;
		if (obj instanceof Error) {
			statusRS = HttpStatus.valueOf(((Error) obj).getStatus());
			if (statusRS == HttpStatus.NO_CONTENT) {
				statusRS = HttpStatus.OK;
			}
		}

		ApiRS response = new ApiRS();
		response.setMonitor(monitor);
		response.setResponse(obj);
		response.setStatus(statusRS.value());
		return new ResponseEntity<>(response, statusRS);
	}
}