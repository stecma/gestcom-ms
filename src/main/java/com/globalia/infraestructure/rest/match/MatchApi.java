package com.globalia.infraestructure.rest.match;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.match.AssignMatch;
import com.globalia.application.service.match.CustomerMatch;
import com.globalia.application.service.match.SupplierMatch;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.infraestructure.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class MatchApi extends AbstractController {

	@Autowired
	private SupplierMatch supplierMatch;
	@Autowired
	private CustomerMatch customerMatch;
	@Autowired
	private AssignMatch assignMatch;

	@PostMapping(value = "/match/supplier")
	public ResponseEntity<ApiRS> matchSupplier(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("supplier", request), HttpStatus.OK, request.getMonitor());
	}

	@PostMapping(value = "/match/customer")
	public ResponseEntity<ApiRS> matchCustomer(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("customer", request), HttpStatus.OK, request.getMonitor());
	}

	@PostMapping(value = "/match/assign")
	public ResponseEntity<ApiRS> assign(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("assign", request), HttpStatus.OK, request.getMonitor());
	}

	protected Object getResponse(final String entity, final ApiRQ request) {
		ItemResponse response;
		if ("customer".equals(entity)) {
			response = this.customerMatch.matchCustomer(request.getMatch(), request.getMonitor());
		} else if ("assign".equals(entity)) {
			response = this.assignMatch.assign(request.getMatch(), request.getMonitor());
		} else {
			response = this.supplierMatch.matchSupplier(request.getMatch(), request.getMonitor());
		}
		return response.getError() != null ? response.getError() : response.getMatch();
	}
}
