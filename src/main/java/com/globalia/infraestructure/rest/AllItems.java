package com.globalia.infraestructure.rest;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.infraestructure.AbstractController;
import com.globalia.infraestructure.rest.converter.AllItemsConverter;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class AllItems extends AbstractController {

	@Autowired
	private AllItemsConverter converter;

	@PostMapping(value = "/{entity}/all")
	public ResponseEntity<ApiRS> getAllItems(@PathVariable("entity") final String entity, @RequestBody final ApiRQ request) {
		return buildResponse(this.converter.getAllItems(ItemConverter.apiRequest.valueOf(entity.toUpperCase()), request), HttpStatus.OK, request.getMonitor());
	}
}
