package com.globalia.infraestructure.rest.massive;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.customer.SaveCustomer;
import com.globalia.application.service.trade.SaveTrade;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.infraestructure.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class Massive extends AbstractController {

	@Autowired
	private SaveTrade trade;
	@Autowired
	private SaveCustomer customer;

	@PostMapping(value = "/{entity}/massive")
	public ResponseEntity<ApiRS> massive(@PathVariable("entity") final String entity, @RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse(entity, request), HttpStatus.CREATED, request.getMonitor());
	}

	protected Object getResponse(final String entity, final ApiRQ request) {
		AllItemsResponse massive;
		switch (entity) {
			case "trades":
				massive = this.trade.massive(request.getMassive(), request.getMonitor());
				return massive.getError() != null ? massive.getError() : massive.getTradePolicies();
			case "customers":
			default:
				massive = this.customer.massive(request.getMassive(), request.getMonitor());
				return massive.getError() != null ? massive.getError() : massive.getCustomers();
		}
	}
}
