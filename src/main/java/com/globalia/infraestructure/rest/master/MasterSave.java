package com.globalia.infraestructure.rest.master;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.infraestructure.AbstractController;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import com.globalia.infraestructure.rest.converter.SaveConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class MasterSave extends AbstractController {

	@Autowired
	private SaveConverter converter;

	@PostMapping(value = "/master/credential/{address}/add")
	public ResponseEntity<ApiRS> create(@RequestBody final ApiRQ request) {
		return buildResponse(this.converter.save(ItemConverter.apiRequest.MASTER, SaveConverter.saveAction.CREATE, request), HttpStatus.CREATED, request.getMonitor());
	}

	@PutMapping(value = "/master/credential/{address}")
	public ResponseEntity<ApiRS> update(@RequestBody final ApiRQ request) {
		return buildResponse(this.converter.save(ItemConverter.apiRequest.MASTER, SaveConverter.saveAction.UPDATE, request), HttpStatus.ACCEPTED, request.getMonitor());
	}

	@DeleteMapping(value = "/master/credential/{address}")
	public ResponseEntity<ApiRS> delete(@RequestBody final ApiRQ request) {
		return buildResponse(this.converter.save(ItemConverter.apiRequest.MASTER, SaveConverter.saveAction.DELETE, request), HttpStatus.ACCEPTED, request.getMonitor());
	}
}
