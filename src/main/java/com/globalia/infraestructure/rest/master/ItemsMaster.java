package com.globalia.infraestructure.rest.master;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.infraestructure.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ItemsMaster extends AbstractController {

	@Autowired
	private Masters masters;

	@PostMapping(value = "/master/credential/{entity}/{text}/{refser}")
	public ResponseEntity<ApiRS> searchItems(@PathVariable("entity") final String entity, @PathVariable("text") final String text, @PathVariable("refser") final String refser, @RequestBody final ApiRQ request) {
		String ref = StringUtils.hasText(refser) && !"null".equalsIgnoreCase(refser) ? refser : null;
		return buildResponse(this.getResponse(entity, text, ref, request), HttpStatus.OK, request.getMonitor());
	}

	@PostMapping(value = "/master/credential/{address}/all")
	public ResponseEntity<ApiRS> getAllItems(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse(request), HttpStatus.OK, request.getMonitor());
	}

	protected Object getResponse(final ApiRQ request) {
		AllItemsResponse items = this.masters.getAllItems(request.getMaster(), request.getMonitor());
		return items.getError() != null ? items.getError() : items.getMasters();
	}

	private Object getResponse(final String entity, final String text, final String ref, final ApiRQ request) {
		AllItemsResponse items = this.masters.searchItems(entity, text, ref, request.getMonitor());
		return items.getError() != null ? items.getError() : items.getMasters();
	}
}
