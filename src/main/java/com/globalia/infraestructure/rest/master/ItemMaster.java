package com.globalia.infraestructure.rest.master;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.master.MasterTypes;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.infraestructure.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ItemMaster extends AbstractController {

	@Autowired
	private Master master;
	@Autowired
	private MasterTypes masterTypes;

	@PostMapping(value = "/master/credential/{address}")
	public ResponseEntity<ApiRS> getItem(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("", request), HttpStatus.OK, request.getMonitor());
	}

	@PostMapping(value = "/permission")
	public ResponseEntity<ApiRS> types(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("permission", request), HttpStatus.OK, request.getMonitor());
	}

	protected Object getResponse(final String entity, final ApiRQ request) {
		if ("permission".equals(entity)) {
			return this.masterTypes.getType(MasterType.valueOf(request.getMaster().getEntity().toUpperCase()));
		}
		ItemResponse item = this.master.getItem(request.getMaster(), request.getMonitor());
		return item.getError() != null ? item.getError() : item.getMaster();
	}
}
