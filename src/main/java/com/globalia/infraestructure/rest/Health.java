package com.globalia.infraestructure.rest;

import com.globalia.api.ApiRS;
import com.globalia.application.producer.MessageProducer;
import com.globalia.application.service.HealthService;
import com.globalia.dto.KafkaItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/gescom")
class Health {

	@Autowired
	private HealthService healthService;
	@Autowired
	private MessageProducer producer;

	@PostMapping(value = "/health", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiRS> health() {
		ApiRS response = new ApiRS();
		response.setResponse(this.healthService.health());
		response.setStatus(HttpStatus.OK.value());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/reload", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> reload() {
		KafkaItem message = new KafkaItem();
		message.setKey(UUID.randomUUID().toString());
		message.setEntity("reload");
		this.producer.reply(message);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
