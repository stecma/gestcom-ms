package com.globalia.infraestructure.rest;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.infraestructure.AbstractController;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class Item extends AbstractController {

	@Autowired
	private ItemConverter converter;

	@PostMapping(value = "/{entity}")
	public ResponseEntity<ApiRS> getItem(@PathVariable("entity") final String entity, @RequestBody final ApiRQ request) {
		return buildResponse(this.converter.getItem(ItemConverter.apiRequest.valueOf(entity.toUpperCase()), request), HttpStatus.OK, request.getMonitor());
	}
}
