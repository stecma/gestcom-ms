package com.globalia.infraestructure.rest.converter;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.cancelcost.SaveCancelCost;
import com.globalia.application.service.credential.SaveCredential;
import com.globalia.application.service.currency.SaveCurrency;
import com.globalia.application.service.customer.SaveCustomer;
import com.globalia.application.service.master.SaveMaster;
import com.globalia.application.service.release.SaveRelease;
import com.globalia.application.service.supplier.SaveSupplier;
import com.globalia.application.service.supplier.SaveSupplierByZone;
import com.globalia.application.service.trade.SaveTrade;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.ISave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class SaveConverter extends AbstractConverter {

	@Autowired
	private SaveTrade saveTrade;
	@Autowired
	private SaveSupplierByZone saveSupplierByZone;
	@Autowired
	private SaveRelease saveRelease;
	@Autowired
	private SaveCustomer saveCustomer;
	@Autowired
	private SaveMaster saveMaster;
	@Autowired
	private SaveSupplier saveSupplier;
	@Autowired
	private SaveCredential saveCredential;
	@Autowired
	private SaveCancelCost saveCancelCost;
	@Autowired
	private SaveCurrency saveCurrency;

	public Object save(final ItemConverter.apiRequest entity, final saveAction action, final ApiRQ request) {
		ItemResponse response;
		switch (action) {
			case CREATE:
				response = (ItemResponse) this.getSave(entity).createItem(getRequest(entity, request), request.getMonitor());
				break;
			case UPDATE:
				response = (ItemResponse) this.getSave(entity).updateItem(getRequest(entity, request), request.getMonitor());
				break;
			default:
				response = (ItemResponse) this.getSave(entity).deleteItem(getRequest(entity, request), request.getMonitor());
		}

		return response.getError() != null ? response.getError() : getResponse(entity, response);
	}

	private ISave getSave(final ItemConverter.apiRequest entity) {
		switch (entity) {
			case TRADE:
				return this.saveTrade;
			case SUPPLIERBYZONE:
				return this.saveSupplierByZone;
			case RELEASE:
				return this.saveRelease;
			case CUSTOMER:
				return this.saveCustomer;
			case CREDENTIAL:
				return this.saveCredential;
			case SUPPLIER:
				return this.saveSupplier;
			case CANCELCOST:
				return this.saveCancelCost;
			case CURRENCY:
				return this.saveCurrency;
			case MASTER:
			default:
				return this.saveMaster;
		}
	}

	public enum saveAction {
		CREATE,
		UPDATE,
		DELETE
	}
}