package com.globalia.infraestructure.rest.converter;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.cancelcost.CancelCost;
import com.globalia.application.service.credential.Credential;
import com.globalia.application.service.customer.Customer;
import com.globalia.application.service.release.Release;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.application.service.supplier.SupplierByZone;
import com.globalia.application.service.trade.Trade;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.IFind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class ItemConverter extends AbstractConverter {

	@Autowired
	private Trade trade;
	@Autowired
	private SupplierByZone supplierByZone;
	@Autowired
	private Release release;
	@Autowired
	private Customer customer;
	@Autowired
	private Supplier supplier;
	@Autowired
	private Credential credential;
	@Autowired
	private CancelCost cancelCost;

	public Object getItem(final apiRequest entity, final ApiRQ request) {
		ItemResponse response = (ItemResponse) this.getFind(entity).getItem(getRequest(entity, request), request.getMonitor());
		return response.getError() != null ? response.getError() : getResponse(entity, response);
	}

	private IFind getFind(final apiRequest entity) {
		switch (entity) {
			case TRADE:
				return this.trade;
			case SUPPLIERBYZONE:
				return this.supplierByZone;
			case RELEASE:
				return this.release;
			case CUSTOMER:
				return this.customer;
			case CREDENTIAL:
				return this.credential;
			case SUPPLIER:
				return this.supplier;
			case CANCELCOST:
			default:
				return this.cancelCost;
		}
	}

	public enum apiRequest {
		TRADE,
		SUPPLIER,
		SUPPLIERBYZONE,
		RELEASE,
		CUSTOMER,
		CURRENCY,
		CANCELCOST,
		CREDENTIAL,
		MASTER,
		MATCH,
		MASSIVE
	}
}