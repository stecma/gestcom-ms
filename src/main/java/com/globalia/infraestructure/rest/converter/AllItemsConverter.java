package com.globalia.infraestructure.rest.converter;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.cancelcost.CancelCosts;
import com.globalia.application.service.credential.Credentials;
import com.globalia.application.service.currency.Currencies;
import com.globalia.application.service.customer.Customers;
import com.globalia.application.service.release.Releases;
import com.globalia.application.service.supplier.SupplierByZones;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.application.service.trade.Trades;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.IFindAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class AllItemsConverter extends AbstractConverter {

	@Autowired
	private Trades trades;
	@Autowired
	private SupplierByZones supplierByZones;
	@Autowired
	private Releases releases;
	@Autowired
	private Customers customers;
	@Autowired
	private Suppliers suppliers;
	@Autowired
	private Credentials credentials;
	@Autowired
	private CancelCosts cancelCosts;
	@Autowired
	private Currencies currencies;

	public Object getAllItems(final ItemConverter.apiRequest entity, final ApiRQ request) {
		AllItemsResponse response = (AllItemsResponse) this.getFindAll(entity).getAllItems(getRequest(entity, request), request.getMonitor());
		return response.getError() != null ? response.getError() : this.getResponse(entity, response);
	}

	private Object getResponse(final ItemConverter.apiRequest entity, final AllItemsResponse response) {
		switch (entity) {
			case TRADE:
				return response.getTradePolicies();
			case SUPPLIER:
				return response.getSuppliers();
			case SUPPLIERBYZONE:
				return response.getSuppliersByZone();
			case RELEASE:
				return response.getReleases();
			case CUSTOMER:
				return response.getCustomers();
			case CURRENCY:
				return response.getCurrencies();
			case CANCELCOST:
				return response.getCancelCosts();
			case CREDENTIAL:
			default:
				return response.getCredentials();
		}
	}

	private IFindAll getFindAll(final ItemConverter.apiRequest entity) {
		switch (entity) {
			case TRADE:
				return this.trades;
			case SUPPLIER:
				return this.suppliers;
			case SUPPLIERBYZONE:
				return this.supplierByZones;
			case RELEASE:
				return this.releases;
			case CUSTOMER:
				return this.customers;
			case CURRENCY:
				return this.currencies;
			case CANCELCOST:
				return this.cancelCosts;
			case CREDENTIAL:
			default:
				return this.credentials;
		}
	}
}