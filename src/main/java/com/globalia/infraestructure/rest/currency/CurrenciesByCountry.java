package com.globalia.infraestructure.rest.currency;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.currency.Currencies;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.infraestructure.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class CurrenciesByCountry extends AbstractController {

	@Autowired
	private Currencies currencies;

	@PostMapping(value = "/currency/bycountry/all")
	public ResponseEntity<ApiRS> getAllItems(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse(request), HttpStatus.OK, request.getMonitor());
	}

	protected Object getResponse(final ApiRQ request) {
		AllItemsResponse items = this.currencies.getAllItemsByCountry(request.getMonitor());
		return items.getError() != null ? items.getError() : items.getCurrencies();
	}
}
