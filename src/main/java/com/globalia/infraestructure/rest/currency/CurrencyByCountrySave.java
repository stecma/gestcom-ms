package com.globalia.infraestructure.rest.currency;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.currency.SaveCurrency;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.infraestructure.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class CurrencyByCountrySave extends AbstractController {

	@Autowired
	private SaveCurrency currency;

	@PostMapping(value = "/currency/bycountry/add")
	public ResponseEntity<ApiRS> createCurrency(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("create", request), HttpStatus.CREATED, request.getMonitor());
	}

	@PutMapping(value = "/currency/bycountry")
	public ResponseEntity<ApiRS> updateCurrency(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("update", request), HttpStatus.ACCEPTED, request.getMonitor());
	}

	@DeleteMapping(value = "/currency/bycountry")
	public ResponseEntity<ApiRS> deleteCurrency(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("delete", request), HttpStatus.ACCEPTED, request.getMonitor());
	}

	protected Object getResponse(final String entity, final ApiRQ request) {
		ItemResponse item;
		if ("delete".equals(entity)) {
			item = this.currency.deleteItem(request.getCurrency(), request.getMonitor());
		} else if ("update".equals(entity)) {
			item = this.currency.updateItem(request.getCurrency(), request.getMonitor());
		} else {
			item = this.currency.createItem(request.getCurrency(), request.getMonitor());
		}
		return item.getError() != null ? item.getError() : item.getCurrency();
	}
}
