package com.globalia.infraestructure.rest.supplier;

import com.globalia.api.ApiRS;
import com.globalia.api.credential.ApiRQ;
import com.globalia.application.service.supplier.SaveSupplier;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.infraestructure.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/supplier", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class SupplierApi extends AbstractController {

	@Autowired
	private Supplier supplier;
	@Autowired
	private SaveSupplier save;

	@PostMapping(value = "/mapKey")
	public ResponseEntity<ApiRS> getMapKey(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("mapkey", request), HttpStatus.OK, request.getMonitor());
	}

	@PostMapping(value = "/group")
	public ResponseEntity<ApiRS> getGroup(@RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse("group", request), HttpStatus.OK, request.getMonitor());
	}

	@PutMapping(value = "/{system}/{subsystem}/{sap}/{isNew}/{dummy}")
	public ResponseEntity<ApiRS> updateSap(@PathVariable("system") final String system, @PathVariable("subsystem") final String subsystem, @PathVariable("sap") final String sap, @PathVariable("isNew") final String isNew, @RequestBody final ApiRQ request) {
		ItemResponse item = this.save.updateSap(this.getSystem(system, subsystem), StringUtils.hasText(subsystem) && !"null".equalsIgnoreCase(sap) ? sap : null, Boolean.parseBoolean(isNew), request.getMonitor());
		return buildResponse(item.getError() != null ? item.getError() : item.getSupplier(), HttpStatus.ACCEPTED, request.getMonitor());
	}

	@PutMapping(value = "/{system}/{subsystem}/{refser}/{tipser}/{srvOther}/{descSrv}")
	public ResponseEntity<ApiRS> updateService(@PathVariable("system") final String system, @PathVariable("subsystem") final String subsystem, @PathVariable("refser") final String refser, @PathVariable("tipser") final String tipser, @PathVariable("srvOther") final String srvOther, @PathVariable("descSrv") final String descSrv, @RequestBody final ApiRQ request) {
		String ref = StringUtils.hasText(refser) && !"null".equalsIgnoreCase(refser) ? refser : null;
		String tip = StringUtils.hasText(tipser) && !"null".equalsIgnoreCase(tipser) ? tipser : null;
		String srv = StringUtils.hasText(srvOther) && !"null".equalsIgnoreCase(srvOther) ? srvOther.replace("_", " ") : null;
		String desc = StringUtils.hasText(descSrv) && !"null".equalsIgnoreCase(descSrv) ? descSrv.replace("_", " ") : null;

		ItemResponse item = this.save.updateService(this.getSystem(system, subsystem), ref, tip, srv, desc, request.getMonitor());
		return buildResponse(item.getError() != null ? item.getError() : item.getSupplier(), HttpStatus.ACCEPTED, request.getMonitor());
	}

	private String getSystem(final String system, final String subsystem) {
		if (StringUtils.hasText(subsystem) && !"null".equalsIgnoreCase(subsystem)) {
			return String.format("%s¬%s", system, subsystem);
		}
		return system;
	}

	protected Object getResponse(final String entity, final ApiRQ request) {
		switch (entity) {
			case "mapkey":
				ItemResponse mapkey = this.supplier.getMapKey(request.getSupplier(), request.getMonitor());
				return mapkey.getError() != null ? mapkey.getError() : mapkey.getSupplier();
			case "group":
			default:
				ItemResponse group = this.supplier.getGroup(request.getSupplier(), request.getMonitor());
				return group.getError() != null ? group.getError() : group.getGroup();
		}
	}
}
