package com.globalia.infraestructure.aspect;

import com.globalia.api.credential.ApiRQ;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Aspect
public class MatchAspect extends AbstractAspect {

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.match.MatchApi.matchSupplier*(..)) && args(request)", argNames = "point,request")
	public Object matchSupplier(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.MATCH, request);
		this.validateMatchSupplier(request);
		return getHandler().execute(point, request.getMonitor());
	}

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.match.MatchApi.matchCustomer*(..)) && args(request)", argNames = "point,request")
	public Object matchCustomer(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.MATCH, request);
		this.validateMatchCustomer(request);
		return getHandler().execute(point, request.getMonitor());
	}

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.match.MatchApi.assign*(..)) && args(request)", argNames = "point,request")
	public Object assign(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.MATCH, request);
		this.validateAssignRequest(request);
		return getHandler().execute(point, request.getMonitor());
	}

	private void validateMatchSupplier(final ApiRQ request) {
		if (!StringUtils.hasText(request.getMatch().getGroupId())) {
			throw new ValidateException(new Error("Credential group ID is mandatory", ErrorLayer.API_LAYER), null);
		}
		if (!StringUtils.hasText(request.getMatch().getSupplierId())) {
			throw new ValidateException(new Error("Supplier ID is mandatory", ErrorLayer.API_LAYER), null);
		}
		if (request.getMatch().getCustomer() == null || request.getMatch().getCustomer().getFilter() == null) {
			throw new ValidateException(new Error("Customer filter is mandatory", ErrorLayer.API_LAYER), null);
		}
	}

	private void validateMatchCustomer(final ApiRQ request) {
		if (!StringUtils.hasText(request.getMatch().getCredentialId())) {
			throw new ValidateException(new Error("Credential ID is mandatory", ErrorLayer.API_LAYER), null);
		}
		if (request.getMatch().getSupplier() == null || request.getMatch().getSupplier().getFilter() == null) {
			throw new ValidateException(new Error("Supplier filter is mandatory", ErrorLayer.API_LAYER), null);
		}
	}

	private void validateAssignRequest(final ApiRQ request) {
		if (request.getMatch().getCodes() == null) {
			throw new ValidateException(new Error("Assignment codes are mandatory", ErrorLayer.API_LAYER), null);
		}
		if (!StringUtils.hasText(request.getMatch().getGroupId()) && !StringUtils.hasText(request.getMatch().getCredentialId())) {
			throw new ValidateException(new Error("Must indicate a group credential or a credential", ErrorLayer.API_LAYER), null);
		}
		if (request.getMatch().getAssignment() == null) {
			throw new ValidateException(new Error("Assignment values are mandatory", ErrorLayer.API_LAYER), null);
		}
	}
}
