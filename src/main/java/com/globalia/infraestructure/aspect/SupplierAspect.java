package com.globalia.infraestructure.aspect;

import com.globalia.api.credential.ApiRQ;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import com.globalia.monitoring.Monitor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Aspect
public class SupplierAspect extends AbstractAspect {

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.supplier.SupplierApi.get*(..)) && args(request)", argNames = "point,request")
	public Object getMethod(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.SUPPLIER, request);
		if (isEmptyID(ItemConverter.apiRequest.SUPPLIER, request)) {
			throw new ValidateException(new Error(AbstractAspect.ID_MANDATORY, ErrorLayer.API_LAYER), request.getMonitor());
		}
		return getHandler().execute(point, request.getMonitor());
	}

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.supplier.SupplierApi.updateSap*(..)) && args(system,subsystem,sap,isNew,request)", argNames = "point,system,subsystem,sap,isNew,request")
	public Object updateSapMethod(final ProceedingJoinPoint point, final String system, final String subsystem, final String sap, final boolean isNew, final ApiRQ request) throws Throwable {
		this.validateValues(system, request.getMonitor());
		return getHandler().execute(point, request.getMonitor());
	}

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.supplier.SupplierApi.updateService*(..)) && args(system,subsystem,refser,tipser,request)", argNames = "point,system,subsystem,refser,tipser,request")
	public Object updateServiceMethod(final ProceedingJoinPoint point, final String system, final String subsystem, final String refser, final String tipser, final ApiRQ request) throws Throwable {
		this.validateValues(system, request.getMonitor());
		return getHandler().execute(point, request.getMonitor());
	}

	private void validateValues(final String system, final Monitor monitor) {
		if (!StringUtils.hasText(system)) {
			throw new ValidateException(new Error("System code is mandatory", ErrorLayer.API_LAYER), monitor);
		}
	}
}
