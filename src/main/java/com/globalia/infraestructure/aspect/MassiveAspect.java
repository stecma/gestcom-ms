package com.globalia.infraestructure.aspect;

import com.globalia.api.credential.ApiRQ;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MassiveAspect extends AbstractAspect {

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.massive.Massive.*(..)) && args(entity,request)", argNames = "point,entity,request")
	public Object massiveController(final ProceedingJoinPoint point, final String entity, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.MASSIVE, request);
		return getHandler().execute(point, request.getMonitor());
	}
}
