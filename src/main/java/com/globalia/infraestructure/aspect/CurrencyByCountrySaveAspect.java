package com.globalia.infraestructure.aspect;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.currency.CurrencyByCountryValidator;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class CurrencyByCountrySaveAspect extends AbstractAspect {

	@Autowired
	private CurrencyByCountryValidator validator;

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.currency.CurrencyByCountrySave.create*(..)) && args(request)", argNames = "point,request")
	public Object create(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.CURRENCY, request);
		this.validator.currencyValidation(request.getCurrency(), request.getMonitor());
		return getHandler().execute(point, request.getMonitor());
	}

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && (execution(* com.globalia.infraestructure.rest.currency.CurrencyByCountrySave.update*(..)) || execution(* com.globalia.infraestructure.rest.currency.CurrencyByCountrySave.delete*(..))) && args(request)", argNames = "point,request")
	public Object updateDelete(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.CURRENCY, request);
		if (isEmptyID(ItemConverter.apiRequest.CURRENCY, request)) {
			throw new ValidateException(new Error(AbstractAspect.ID_MANDATORY, ErrorLayer.API_LAYER), request.getMonitor());
		}
		this.validator.currencyValidation(request.getCurrency(), request.getMonitor());
		return getHandler().execute(point, request.getMonitor());
	}
}
