package com.globalia.infraestructure.aspect;

import com.globalia.api.credential.ApiRQ;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Aspect
public class CreateAspect extends AbstractAspect {

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.Create.*(..)) && args(entity,request)", argNames = "point,entity,request")
	public Object createController(final ProceedingJoinPoint point, final String entity, final ApiRQ request) throws Throwable {
		ItemConverter.apiRequest apiRequest = ItemConverter.apiRequest.valueOf(entity.toUpperCase());
		validateRequest(apiRequest, request);
		this.explicitValidation(apiRequest, request);
		validateContent(apiRequest, request);
		return getHandler().execute(point, request.getMonitor());
	}

	private void explicitValidation(final ItemConverter.apiRequest entity, final ApiRQ request) {
		if (entity == ItemConverter.apiRequest.SUPPLIERBYZONE && !StringUtils.hasText(request.getSupplierbyzone().getCodsis())) {
			throw new ValidateException(new Error("System code is mandatory", ErrorLayer.API_LAYER), request.getMonitor());
		}
	}
}
