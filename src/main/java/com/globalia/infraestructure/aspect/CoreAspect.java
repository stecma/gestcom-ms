package com.globalia.infraestructure.aspect;

import com.globalia.enumeration.StatType;
import com.globalia.monitoring.Monitor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CoreAspect {

	@Autowired
	private AspectHandler handler;

	@Around(value = "within(@org.springframework.stereotype.Repository *) && args(.., monitor)", argNames = "point,monitor")
	public Object loggerRedis(final ProceedingJoinPoint point, final Monitor monitor) throws Throwable {
		StatType stat = StatType.REDIS_GET;
		if (point.getSignature().getName().startsWith("add")) {
			stat = StatType.REDIS_ADD;
		}
		return this.handler.execute(point, monitor, stat, "Start Redis Access", "End Redis Access");
	}

	@Around(value = "within(@org.springframework.stereotype.Service *) && args(.., monitor)", argNames = "point,monitor")
	public Object loggerService(final ProceedingJoinPoint point, final Monitor monitor) throws Throwable {
		return this.handler.execute(point, monitor, null, "Start Service", "End Service");
	}
}