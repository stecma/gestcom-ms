package com.globalia.infraestructure.aspect;

import com.globalia.api.credential.ApiRQ;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class DeleteAspect extends AbstractAspect {

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.Delete.*(..)) && args(entity,request)", argNames = "point,entity,request")
	public Object deleteController(final ProceedingJoinPoint point, final String entity, final ApiRQ request) throws Throwable {
		ItemConverter.apiRequest apiRequest = ItemConverter.apiRequest.valueOf(entity.toUpperCase());
		validateRequest(apiRequest, request);
		if (isEmptyID(apiRequest, request)) {
			throw new ValidateException(new Error(AbstractAspect.ID_MANDATORY, ErrorLayer.API_LAYER), request.getMonitor());
		}
		validateContent(apiRequest, request, apiRequest == ItemConverter.apiRequest.SUPPLIERBYZONE);
		return getHandler().execute(point, request.getMonitor());
	}
}
