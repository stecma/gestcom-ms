package com.globalia.infraestructure.aspect;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.validator.master.MasterValidator;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ItemMasterAspect extends AbstractAspect {

	@Autowired
	private MasterValidator validator;

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && (execution(* com.globalia.infraestructure.rest.master.ItemsMaster.getAllItems*(..)) || execution(* com.globalia.infraestructure.rest.master.ItemMaster.types*(..))) && args(request)", argNames = "point,request")
	public Object getAllItems(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.MASTER, request);
		this.validator.validateEntity(request.getMaster(), request.getMonitor());
		return getHandler().execute(point, request.getMonitor());
	}

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.master.ItemMaster.get*(..)) && args(request)", argNames = "point,request")
	public Object getItem(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		validateRequest(ItemConverter.apiRequest.MASTER, request);
		this.validator.validateEntity(request.getMaster(), request.getMonitor());
		if (isEmptyID(ItemConverter.apiRequest.MASTER, request)) {
			throw new ValidateException(new Error(AbstractAspect.ID_MANDATORY, ErrorLayer.API_LAYER), request.getMonitor());
		}
		return getHandler().execute(point, request.getMonitor());
	}
}
