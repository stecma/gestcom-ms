package com.globalia.infraestructure.aspect;

import com.globalia.enumeration.LogType;
import com.globalia.enumeration.StatType;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.util.StringUtils;

@Component
@RefreshScope
public class AspectHandler {

	@Autowired
	private MonitorHandler handler;

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;
	@Value("${logLevel}")
	private LogType logLevel;

	public Object execute(final ProceedingJoinPoint point, final Monitor monitor) throws Throwable {
		return this.execute(point, monitor, null, "Start controller", "End controller");
	}

	Object execute(final ProceedingJoinPoint point, final Monitor monitor, final StatType statType, final String iniMsg, final String endMsg) throws Throwable {
		StopWatch watch = new StopWatch();
		this.addLog(monitor, point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), iniMsg);
		watch.start();
		Object result = point.proceed();
		watch.stop();
		this.addStat(monitor, statType, watch.getTotalTimeMillis());
		this.addLog(monitor, point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), endMsg);
		return result;
	}

	private void addLog(final Monitor monitor, final String className, final String methodName, final String message) {
		if (monitor != null && StringUtils.hasText(message)) {
			monitor.setService(this.appName);
			monitor.setPort(this.port);
			this.handler.addDebugLog(monitor, className, methodName, 0, message, this.logLevel);
		}
	}

	private void addStat(final Monitor monitor, final StatType statType, final long elaspsed) {
		if (statType != null) {
			this.handler.addStat(monitor, statType, elaspsed);
		}
	}
}