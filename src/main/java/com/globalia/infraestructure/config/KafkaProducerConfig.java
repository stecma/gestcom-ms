package com.globalia.infraestructure.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RefreshScope
public class KafkaProducerConfig {

	@Value("${kafka.topicname}")
	private String topicName;
	@Value("${spring.kafka.consumer.group-id}")
	private String groupId;
	@Value("${kafka.topicreplyname}")
	private String replyTopic;
	@Value("${spring.kafka.bootstrap-servers}")
	private String servers;

	@Bean
	public KafkaTemplate<String, String> kafkaTemplate() {
		return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(this.producerConfigs()));
	}

	@Bean
	public ReplyingKafkaTemplate<String, String, String> replyingTemplate(final ProducerFactory<String, String> pf, final ConcurrentMessageListenerContainer<String, String> repliesContainer) {
		return new ReplyingKafkaTemplate<>(pf, repliesContainer);
	}

	@Bean
	public ConcurrentMessageListenerContainer<String, String> repliesContainer(final ConcurrentKafkaListenerContainerFactory<String, String> containerFactory) {
		ConcurrentMessageListenerContainer<String, String> repliesContainer = containerFactory.createContainer(this.replyTopic);
		repliesContainer.getContainerProperties().setGroupId(this.groupId);
		repliesContainer.setAutoStartup(false);
		return repliesContainer;
	}

	@Bean
	public Map<String, Object> producerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, this.servers);
		return props;
	}

	@Bean
	public NewTopic topic() {
		return TopicBuilder.name(this.topicName).partitions(10).replicas(3).build();
	}

	@Bean
	public NewTopic replyTopic() {
		return TopicBuilder.name(this.replyTopic).partitions(10).replicas(3).build();
	}
}