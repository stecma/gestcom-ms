package com.globalia.infraestructure.config;

import com.globalia.enumeration.LogType;
import com.globalia.security.JwtTokenFilterConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;

import java.util.Collections;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtTokenFilterConfigurer tokenFilterConfigurer;

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;
	@Value("${ldap.uri}")
	private String ldapUrl;
	@Value("${ldap.domain}")
	private String ldapDomain;
	@Value("${logLevel}")
	private LogType logLevel;
	@Value("${jwt.secret}")
	private String jwtSecret;
	@Value("${jwt.encryptSecret}")
	private String encryptSecret;
	@Value("${jwt.encryptAlgorithm}")
	private String encryptAlgorithm;

	@Override
	@RefreshScope
	protected void configure(final HttpSecurity http) throws Exception {
		// Disable CSRF (cross site request forgery)
		http.cors().and().csrf().disable();

		// No session will be created or used by spring security
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		// Entry points
		http.authorizeRequests()
				.antMatchers("/**/health", "/**/reload").permitAll()
				// Disallow everything else..
				.anyRequest().authenticated();

		// Apply JWT
		this.tokenFilterConfigurer.init(this.logLevel, this.jwtSecret, this.encryptSecret, this.encryptAlgorithm, this.appName, this.port);
		http.apply(this.tokenFilterConfigurer);
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManager() {
		ActiveDirectoryLdapAuthenticationProvider provider = new ActiveDirectoryLdapAuthenticationProvider(this.ldapDomain, this.ldapUrl);
		provider.setConvertSubErrorCodesToExceptions(true);
		provider.setUseAuthenticationRequestCredentials(true);

		return new ProviderManager(Collections.singletonList(provider));
	}
}