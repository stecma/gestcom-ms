package com.globalia.application.service.group;

import com.globalia.application.repository.group.GetAssignGroup;
import com.globalia.application.repository.group.GetGroup;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class Group extends FindService<GroupCredentialItem> {

	@Autowired
	private GetGroup getGroup;
	@Autowired
	private GetAssignGroup assignGroup;

	public ItemResponse getItem(final GroupCredentialItem item, final Monitor monitor) {
		return this.getGroup.getItem(item, monitor);
	}

	public Set<GroupCredentialItem> getAssignItem(final String credential) {
		return this.assignGroup.getItem(credential);
	}
}