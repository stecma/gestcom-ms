package com.globalia.application.service.group;

import com.globalia.application.repository.group.SaveGroupRedis;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveGroup extends SaveService<GroupCredentialItem> {

	@Autowired
	private SaveGroupRedis save;

	public ItemResponse createItem(final GroupCredentialItem item, final Monitor monitor) {
		item.setId(null);
		return this.save.addItem(item, monitor);
	}

	public ItemResponse updateItem(final GroupCredentialItem item, final Monitor monitor) {
		return this.save.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final GroupCredentialItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.save.addItem(item, monitor);
	}

	public ItemResponse addAssignItem(final String credential, final GroupCredentialItem group, final Monitor monitor) {
		return this.save.addItemAssign(credential, group, monitor);
	}
}