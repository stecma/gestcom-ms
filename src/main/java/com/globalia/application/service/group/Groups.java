package com.globalia.application.service.group;

import com.globalia.application.repository.group.GetGroups;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Groups extends FindAllService<GroupCredentialItem> {

	@Autowired
	private GetGroups getGroups;

	@Override
	public AllItemsResponse getAllItems(final GroupCredentialItem item, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final GroupCredentialItem item, final boolean allRedis, final Monitor monitor) {
		return this.getGroups.getAllItems(item, allRedis, monitor);
	}
}