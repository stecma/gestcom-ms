package com.globalia.application.service.master;

import com.globalia.application.repository.master.SaveMasterRedis;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveMaster extends SaveService<MasterItem> {

	@Autowired
	private SaveMasterRedis save;

	public ItemResponse createItem(final MasterItem item, final Monitor monitor) {
		item.setCreated(true);
		return this.save.addItem(item, monitor);
	}

	public ItemResponse updateItem(final MasterItem item, final Monitor monitor) {
		return this.save.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final MasterItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.save.addItem(item, monitor);
	}
}