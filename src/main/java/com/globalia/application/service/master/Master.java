package com.globalia.application.service.master;

import com.globalia.Errors;
import com.globalia.application.repository.master.GetMaster;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class Master extends FindService<MasterItem> {

	@Autowired
	private GetMaster getMaster;
	@Autowired
	private ChildCodes childCodes;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final MasterItem item, final Monitor monitor) {
		ItemResponse master = this.getMaster.getItem(item, monitor);
		if (master.getError() == null) {
			this.completeMaster(master.getMaster(), monitor);
		}
		return master;
	}

	public void convertCodeToMaster(final Set<MasterItem> masters, final Set<PairValue> codes, final Monitor monitor) {
		if (codes != null) {
			for (PairValue code : codes) {
				MasterItem aux = this.getMaster(code.getKey(), codes, monitor);
				if (aux != null) {
					masters.add(aux);
				}
			}
		}
	}

	public void completeMaster(final MasterItem master, final Monitor monitor) {
		if (master.getCodes() != null) {
			this.addCodes(master, master.getCodes(), monitor);
			master.setCodes(null);
		}

		this.addCodes(master, this.childCodes.getChild(master.getEntity(), master.getId()), monitor);
	}

	public MasterItem getMaster(final String masterType, final Set<PairValue> codes, final Monitor monitor) {
		MasterItem master = null;
		if (codes != null && !codes.isEmpty()) {
			Set<MasterItem> masters = this.getFromRedis(masterType, codes, monitor);
			if (masters != null) {
				master = masters.iterator().next();
			}
		}
		return master;
	}

	public Error addMasterValues(final Class<?> clazz, final Object obj, final String attribute, final Set<PairValue> codes, final Monitor monitor) {
		Error err = null;
		try {
			String[] att = attribute.split(",");
			Field field = clazz.getDeclaredField(att[0]);
			field.setAccessible(true);
			if (Boolean.parseBoolean(att[2])) {
				field.set(obj, getMasters(att[1], codes, monitor));
			} else {
				field.set(obj, getMaster(att[1], codes, monitor));
			}
		} catch (NoSuchFieldException | IllegalAccessException e) {
			err = this.errors.addError(e.getLocalizedMessage(), ErrorLayer.SERVICE_LAYER, monitor);
		}
		return err;
	}

	public Set<MasterItem> getMasters(final String masterType, final Set<PairValue> codes, final Monitor monitor) {
		Set<MasterItem> masters = null;
		if (codes != null && !codes.isEmpty()) {
			masters = this.getFromRedis(masterType, codes, monitor);
		}
		return masters;
	}

	private void addCodes(final MasterItem master, final Set<PairValue> codes, final Monitor monitor) {
		MasterItem filter = new MasterItem();
		for (PairValue pair : codes) {
			for (String code : pair.getValue()) {
				if (master.getMasters() == null) {
					master.setMasters(new HashSet<>());
				}
				filter.setEntity(pair.getKey());
				filter.setId(code);
				master.getMasters().add(this.getMaster.getItem(filter, monitor).getMaster());
			}
		}
	}

	private Set<MasterItem> getFromRedis(final String masterType, final Set<PairValue> codes, final Monitor monitor) {
		Set<MasterItem> masters = null;
		Optional<PairValue> pair = codes.stream().filter(p -> p.getKey().equalsIgnoreCase(masterType)).findFirst();
		if (pair.isPresent()) {
			MasterItem filter = new MasterItem();
			for (String code : pair.get().getValue()) {
				filter.setEntity(pair.get().getKey().contains("¬") ? MasterType.COUNTRY.name() : pair.get().getKey());
				filter.setId(code);

				MasterItem aux = this.getMaster.getItem(filter, monitor).getMaster();
				if (aux != null) {
					if (masters == null) {
						masters = new HashSet<>();
					}
					masters.add(aux);
				}
			}
		}
		return masters;
	}
}