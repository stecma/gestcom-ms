package com.globalia.application.service.master;

import com.globalia.application.repository.master.GetTypes;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterTypes {

	@Autowired
	private GetTypes getTypes;

	/**
	 * Get permission by entity
	 *
	 * @param entity Entity.
	 * @return Permissions
	 */
	public String getType(final MasterType entity) {
		return this.getTypes.getType(entity);
	}
}