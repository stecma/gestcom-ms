package com.globalia.application.service.master;

import com.globalia.application.repository.master.GetMasters;
import com.globalia.dto.I18n;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class Masters extends FindAllService<MasterItem> {

	@Autowired
	private Master master;
	@Autowired
	private GetMasters getMasters;

	@Value("${entityScaned}")
	private String entityScaned;

	public AllItemsResponse getAllItems(final MasterItem item, final Monitor monitor) {
		AllItemsResponse items = this.getMasters.getAllItems(item, monitor);
		if (items.getError() == null) {
			for (MasterItem aux : items.getMasters()) {
				this.master.completeMaster(aux, monitor);
			}
		}
		return items;
	}

	public AllItemsResponse searchItems(final String entity, final String text, final String refser, final Monitor monitor) {
		AllItemsResponse items = null;
		if (StringUtils.hasText(entity) && StringUtils.hasText(text)) {
			if (this.entityScaned.contains(entity.toUpperCase())) {
				items = this.getMasters.getAllScan(entity.toUpperCase(), text, refser, monitor);
			} else {
				MasterItem filter = new MasterItem();
				filter.setEntity(entity);
				items = this.getMasters.getAllItems(filter, monitor);
				if (items.getError() == null) {
					items.setMasters(new LinkedHashSet<>(items.getMasters().stream().filter(m -> this.hasText(m.getNames(), text)).collect(Collectors.toSet())));
				}
			}

		}
		return items;
	}

	/**
	 * Validate master key.
	 *
	 * @param entity Entity.
	 * @param key    Master key.
	 * @return If the key is valid.
	 */
	public boolean validateKey(final String entity, final String key) {
		Set<String> codes = this.getMasters.findCodes(entity);
		return codes.contains(key);
	}

	private boolean hasText(final LinkedHashSet<I18n> names, final String text) {
		Optional<I18n> i18n = names.stream().filter(i -> i.getValue().toLowerCase().contains(text.toLowerCase())).findFirst();
		return i18n.isPresent();
	}
}