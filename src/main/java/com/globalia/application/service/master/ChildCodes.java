package com.globalia.application.service.master;

import com.globalia.application.repository.master.GetChildCodes;
import com.globalia.dto.PairValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ChildCodes {

	@Autowired
	private GetChildCodes getChild;

	public Set<PairValue> getChild(final String key, final String id) {
		return this.getChild.getChildCodes(key, id);
	}
}