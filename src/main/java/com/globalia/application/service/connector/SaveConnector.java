package com.globalia.application.service.connector;

import com.globalia.application.repository.connector.SaveConnectorRedis;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveConnector extends SaveService<ExternalCredentialItem> {

	@Autowired
	private SaveConnectorRedis saveConnectorRedis;

	@Override
	public ItemResponse createItem(final ExternalCredentialItem item, final Monitor monitor) {
		item.setId(null);
		return this.saveConnectorRedis.addItem(item, monitor);
	}

	@Override
	public ItemResponse updateItem(final ExternalCredentialItem item, final Monitor monitor) {
		return this.saveConnectorRedis.addItem(item, monitor);
	}

	@Override
	public ItemResponse deleteItem(final ExternalCredentialItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.saveConnectorRedis.addItem(item, monitor);
	}
}