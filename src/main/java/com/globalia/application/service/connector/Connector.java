package com.globalia.application.service.connector;

import com.globalia.application.repository.connector.GetConnector;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Connector extends FindService<ExternalCredentialItem> {

	@Autowired
	private GetConnector getConnector;

	public ItemResponse getItem(final ExternalCredentialItem item, final Monitor monitor) {
		return this.getConnector.getItem(item, monitor);
	}
}