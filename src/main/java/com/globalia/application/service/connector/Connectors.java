package com.globalia.application.service.connector;

import com.globalia.application.repository.connector.GetConnectors;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Connectors extends FindAllService<ExternalCredentialItem> {

	@Autowired
	private GetConnectors getConnectors;

	@Override
	public AllItemsResponse getAllItems(final ExternalCredentialItem externalCredentialItem, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final ExternalCredentialItem item, final boolean allRedis, final Monitor monitor) {
		return this.getConnectors.getAllItems(item, allRedis, monitor);
	}
}