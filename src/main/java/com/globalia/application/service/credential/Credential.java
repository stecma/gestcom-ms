package com.globalia.application.service.credential;

import com.globalia.Dates;
import com.globalia.application.repository.credential.GetCredential;
import com.globalia.application.service.master.ChildCodes;
import com.globalia.application.service.currency.Currencies;
import com.globalia.application.service.customer.Customers;
import com.globalia.application.service.group.Group;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.release.Releases;
import com.globalia.application.service.trade.Trade;
import com.globalia.application.service.cancelcost.CancelCost;
import com.globalia.dto.DateRange;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.ReleaseFilter;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RefreshScope
public class Credential extends FindService<CredentialItem> {

	@Value("${defaultDates.dateTo}")
	private String dateTo;
	@Value("${defaultDates.timeFrom}")
	private String timeFrom;
	@Value("${defaultDates.timeTo}")
	private String timeTo;
	@Value("${masterCredential}")
	private String masterCredential;
	@Value("${masterSalesChannel}")
	private String masterSalesChannel;
	@Value("${masterServiceType}")
	private String masterServiceType;
	@Value("${masterBrand}")
	private String masterBrand;

	@Autowired
	private Master master;
	@Autowired
	private GetCredential getCredential;
	@Autowired
	private ChildCodes getChild;
	@Autowired
	private Group group;
	@Autowired
	private Trade trade;
	@Autowired
	private Customers customers;
	@Autowired
	private Releases releases;
	@Autowired
	private CancelCost cancelCost;
	@Autowired
	private Currencies currencies;
	@Autowired
	private Dates dates;

	public ItemResponse getItem(final CredentialItem item, final Monitor monitor) {
		return this.getItem(item, true, monitor);
	}

	public ItemResponse getItem(final CredentialItem item, final boolean addMaster, final Monitor monitor) {
		ItemResponse response = this.getCredential.getItem(item, monitor);
		if (response.getError() == null) {
			Set<PairValue> codes = new HashSet<>(response.getCredential().getCodes());
			this.completeCredential(response.getCredential(), addMaster, monitor);
			if (addMaster) {
				this.completeBrand(response.getCredential().getBrand(), monitor);
				this.completeServiceType(response.getCredential().getServiceType(), monitor);
				this.completeSalesChannel(response.getCredential().getSaleChannel(), monitor);
			}
			this.completeCredentialGroup(response.getCredential());
			this.completeTrades(response.getCredential());
			this.completeClients(response.getCredential(), monitor);
			this.completeRelease(response.getCredential(), monitor);
			this.completeCancelCost(response.getCredential());
			this.completeCurrencies(response.getCredential(), monitor);
			response.getCredential().setStatus(this.getStatus(response.getCredential(), codes, monitor));
		}
		return response;
	}

	public void completeCredential(final CredentialItem credential, final boolean addMaster, final Monitor monitor) {
		if (StringUtils.hasText(this.masterCredential)) {
			for (String field : this.masterCredential.split("#")) {
				if (field.contains("AGENCY") || addMaster) {
					credential.setError(this.master.addMasterValues(CredentialItem.class, credential, field, credential.getCodes(), monitor));
				}
			}
			if (addMaster) {
				credential.setCodes(null);
			}
		}
	}

	public void completeCredentialGroup(final CredentialItem credential) {
		credential.setGroups(this.group.getAssignItem(credential.getId()));
	}

	public void completeTrades(final CredentialItem credential) {
		credential.setTrades(this.trade.getItem(credential.getId()));
	}

	public void completeCancelCost(final CredentialItem credential) {
		credential.setCancelCosts(this.cancelCost.getItem(credential.getId()));
	}

	private void completeCurrencies(final CredentialItem credential, final Monitor monitor) {
		CurrencyItem filter = new CurrencyItem();
		filter.setCredential(credential.getId());
		credential.setCurrencies(this.currencies.getAllItems(filter, monitor).getCurrencies());
	}

	public void completeClients(final CredentialItem credential, final Monitor monitor) {
		CustomerItem customer = new CustomerItem();
		customer.setCredential(credential.getId());
		credential.setClients(this.customers.getAllItems(customer, false, monitor).getCustomers());
	}

	public String getStatus(final CredentialItem credential, final Set<PairValue> codes, final Monitor monitor) {
		MatchLevel result = null;
		if (credential.getGroups() != null && codes.stream().anyMatch(c -> MasterType.PRODUCT_ORIGIN.name().equalsIgnoreCase(c.getKey()) && (c.getValue().contains("PPPULL") || c.getValue().contains("PULL3"))) && this.invalidsGroups(credential.getGroups(), monitor)) {
			result = MatchLevel.YELLOW;
		}
		if (result == null && credential.getTrades() != null && this.invalidsTrades(credential.getTrades(), monitor)) {
			result = MatchLevel.YELLOW;
		}
		if (result == null && credential.getClients() != null && this.invalidsClients(credential.getClients(), monitor)) {
			result = MatchLevel.YELLOW;
		}
		if (result == null && credential.getReleases() != null && this.invalidsReleases(credential.getReleases(), monitor)) {
			result = MatchLevel.YELLOW;
		}

		if (result == null) {
			result = MatchLevel.RED;
			if (credential.isActive()) {
				result = MatchLevel.GREEN;
			}
		}
		return result.name();
	}

	private void completeRelease(final CredentialItem credential, final Monitor monitor) {
		ReleaseItem release = new ReleaseItem();
		release.setFilter(new ReleaseFilter());
		release.getFilter().setCredential(credential.getId());
		credential.setReleases(this.releases.getAllItems(release, monitor).getReleases());
	}

	private void completeBrand(final MasterItem brand, final Monitor monitor) {
		if (StringUtils.hasText(this.masterBrand)) {
			brand.setMasters(new HashSet<>());
			Set<PairValue> childCodes = this.getChild.getChild(brand.getEntity(), brand.getId());
			for (String field : this.masterBrand.split("#")) {
				this.addMasters(brand.getMasters(), this.master.getMasters(field, childCodes, monitor));
			}
		}
	}

	private void completeServiceType(final MasterItem serviceType, final Monitor monitor) {
		if (StringUtils.hasText(this.masterServiceType)) {
			serviceType.setMasters(new HashSet<>());
			for (String field : this.masterServiceType.split("#")) {
				this.addMasters(serviceType.getMasters(), this.master.getMasters(field, serviceType.getCodes(), monitor));
			}
			serviceType.setCodes(null);
		}
	}

	private void completeSalesChannel(final MasterItem saleChannel, final Monitor monitor) {
		if (StringUtils.hasText(this.masterSalesChannel)) {
			saleChannel.setMasters(new HashSet<>());
			for (String field : this.masterSalesChannel.split("#")) {
				this.addMasters(saleChannel.getMasters(), this.master.getMasters(field, saleChannel.getCodes(), monitor));
			}
			saleChannel.setCodes(null);
		}
	}

	private boolean invalidsReleases(final Set<ReleaseItem> releases, final Monitor monitor) {
		return releases.stream().filter(c -> this.validDates(c.getAssignment().getBook(), monitor)).collect(Collectors.toSet()).isEmpty();
	}

	private boolean invalidsClients(final Set<CustomerItem> clients, final Monitor monitor) {
		return clients.stream().filter(c -> this.validDates(c.getAssignment().getBook(), monitor)).collect(Collectors.toSet()).isEmpty();
	}

	private boolean invalidsTrades(final Set<TradePolicyItem> trades, final Monitor monitor) {
		return trades.stream().filter(t -> this.validDates(t.getDateApl(), monitor)).collect(Collectors.toSet()).isEmpty();
	}

	private boolean invalidsGroups(final Set<GroupCredentialItem> groups, final Monitor monitor) {
		return groups.stream().filter(g -> g.isActive() && this.validDates(g.getAssignment().getBook(), monitor)).collect(Collectors.toSet()).isEmpty();
	}

	private boolean validDates(final DateRange range, final Monitor monitor) {
		if (range != null) {
			try {
				Date from = this.dates.getDateTime(range.getFrom(), StringUtils.hasText(range.getTimeFrom()) ? range.getTimeFrom() : this.timeFrom, monitor);
				Date to = this.dates.getDateTime(StringUtils.hasText(range.getTo()) ? range.getTo() : this.dateTo, StringUtils.hasText(range.getTimeTo()) ? range.getTimeTo() : this.timeTo, monitor);

				return this.dates.currentBetweenRange(from, to);
			} catch (ValidateException ignored) {
				//
			}
		}
		return false;
	}

	private void addMasters(final Set<MasterItem> target, final Set<MasterItem> source) {
		if (source != null) {
			target.addAll(source);
		}
	}
}
