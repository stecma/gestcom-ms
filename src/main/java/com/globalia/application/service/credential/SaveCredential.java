package com.globalia.application.service.credential;

import com.globalia.Errors;
import com.globalia.application.repository.credential.GetCredentialCount;
import com.globalia.application.repository.credential.SaveCredentialRedis;
import com.globalia.dto.credential.Assignment;
import com.globalia.dto.credential.CredentialCounterItem;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.Note;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.LinkedHashSet;

@Service
public class SaveCredential extends SaveService<CredentialItem> {

	@Autowired
	private SaveCredentialRedis save;

	@Autowired
	private GetCredentialCount getCredentialCount;

	@Autowired
	private Errors errors;

	public ItemResponse createItem(final CredentialItem item, final Monitor monitor) {
		return this.addRedis(item, monitor);
	}

	public ItemResponse updateItem(final CredentialItem item, final Monitor monitor) {
		return this.addRedis(item, monitor);
	}

	public ItemResponse deleteItem(final CredentialItem item, final Monitor monitor) {
		return this.addRedis(item, monitor);
	}

	private ItemResponse addRedis(final CredentialItem item, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		CredentialCounterItem indexCount = null;
		CredentialCounterItem aliasCount = null;
		try{
			item.setGroups(null);
			item.setTrades(null);
			item.setClients(null);
			item.setReleases(null);
			item.setCancelCosts(null);
			item.setCurrencies(null);
			item.setStatus(MatchLevel.RED.name());
			if (!item.isDeleted()) {
				indexCount = getCredentialCount.getCredentialIndexCount(item);
				aliasCount = getCredentialCount.getCredentialAliasCount(item);
			}
			response= this.save.addItem(item, monitor);
			if(null != indexCount){
				getCredentialCount.persistIndex(indexCount,response,monitor);
			}
			if(null != aliasCount){
				getCredentialCount.persistAlias(aliasCount,response,monitor);
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return response;
	}
}
