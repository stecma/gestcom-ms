package com.globalia.application.service.credential;

import com.globalia.Errors;
import com.globalia.application.repository.credential.GetCredentials;
import com.globalia.application.service.customer.Customers;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CredentialFilter;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.stream.Collectors;

@Service
public class Credentials extends FindAllService<CredentialItem> {

	@Autowired
	private GetCredentials getCredentials;
	@Autowired
	private Credential credential;
	@Autowired
	private Errors errors;
	@Autowired
	private Customers customers;
	@Autowired
	private CredentialsFilter credentialsFilter;

	public AllItemsResponse getAllItems(final CredentialItem item, final Monitor monitor) {
		AllItemsResponse response = this.getCredentials.getAllItems(monitor);
		if (response.getError() == null) {
			for (CredentialItem cred : response.getCredentials()) {
				cred.setAliasInDB(cred.getAlias());
				this.credential.completeCredentialGroup(cred);
				this.credential.completeTrades(cred);
				this.credential.completeClients(cred, monitor);
				cred.setStatus(this.credential.getStatus(cred, cred.getCodes(), monitor));
			}
			response.setCredentials(new LinkedHashSet<>(response.getCredentials().stream().filter(c -> this.completeFilter(item.getFilter(), c, monitor)).collect(Collectors.toSet())));
			for (CredentialItem cred : response.getCredentials()) {
				this.credential.completeCredential(cred, false, monitor);
			}

			if (response.getCredentials().isEmpty()) {
				response.setError(this.errors.addError("Cannot find credentials", ErrorLayer.SERVICE_LAYER, monitor));
			}
		}
		return response;
	}

	private boolean completeFilter(final CredentialFilter filter, final CredentialItem credential, final Monitor monitor) {
		CustomerItem customer = new CustomerItem();
		customer.setCredential(credential.getId());
		credential.setClients(this.customers.getAllItems(customer, true, monitor).getCustomers());

		return this.credentialsFilter.filter(filter, credential);
	}
}
