package com.globalia.application.service.credential;

import com.globalia.application.FilterUtils;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CredentialFilter;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class CredentialsFilter {

	public boolean filter(final CredentialFilter filter, final CredentialItem credential) {
		StringBuilder filterBuilder = new StringBuilder();
		StringBuilder credentialBuilder = new StringBuilder();
		Set<PairValue> codes = credential.getCodes();

		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getBrand(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.BRAND, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getServiceType(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.SERVICE_TYPE, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getDepth(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.DEPTH, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getSaleChannel(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.SALES_CHANNEL, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getDistributionModel(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.DISTRIBUTION_MODEL, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getSalesModel(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.SALES_MODEL, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getLanguage(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.LANGUAGES, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getEnvironment(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.ENVIRONMENTS, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getRequest(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.REQUEST, codes)));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getHotelX(), String.valueOf(credential.isHotelX()));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getAvailCost(), String.valueOf(credential.isAvailCost()));
		FilterUtils.getInstance().addFilterValues(filterBuilder, credentialBuilder, filter.getPriceChange(), String.valueOf(credential.isPriceChange()));

		boolean existAgencyGroup = FilterUtils.getInstance().exists(filter.getAgencyGroup(), this.getCodes(credential, MasterType.AGENCY_GROUP));
		boolean existClient = FilterUtils.getInstance().exists(filter.getCustomer(), this.getCodes(credential, MasterType.AGENCY));
		boolean existPlatform = FilterUtils.getInstance().exists(filter.getPlatform(), FilterUtils.getInstance().getCodes(MasterType.PLATFORM, codes));
		boolean existRate = FilterUtils.getInstance().exists(filter.getRate(), FilterUtils.getInstance().getCodes(MasterType.RATE_TYPE, codes));
		boolean existProduct = FilterUtils.getInstance().exists(filter.getProductOrigin(), FilterUtils.getInstance().getCodes(MasterType.PRODUCT_ORIGIN, codes));

		return filterBuilder.toString().equals(credentialBuilder.toString()) && existClient && existAgencyGroup && existPlatform && existRate && existProduct;
	}

	private Set<String> getCodes(final CredentialItem credential, final MasterType type) {
		// Recuperar los codigos de agencia de los clientes.
		Set<String> codes = new HashSet<>();
		if (credential.getClients() != null) {
			for (CustomerItem custom : credential.getClients()) {
				for (PairValue pair : custom.getCodes()) {
					if (pair.getKey().equals(type.name())) {
						codes.addAll(pair.getValue());
					}
				}
			}
		}
		return codes;
	}
}