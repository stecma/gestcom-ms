package com.globalia.application.service.release;

import com.globalia.application.Status;
import com.globalia.application.repository.release.GetReleases;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.stream.Collectors;

@Service
public class Releases extends FindAllService<ReleaseItem> {

	@Autowired
	private GetReleases getReleases;
	@Autowired
	private ReleaseHelperFilter releaseFilter;
	@Autowired
	private Release release;
	@Autowired
	private Status status;

	public AllItemsResponse getAllItems(final ReleaseItem item, final Monitor monitor) {
		AllItemsResponse response = this.getReleases.getAllItems(item, monitor);
		if (response.getError() == null) {
			// Filtrar sobre los proveedores.
			response.setReleases(response.getReleases().stream().filter(r -> this.releaseFilter.filter(item.getFilter(), r)).collect(Collectors.toSet()));

			Date current = new Date();
			for (ReleaseItem rel : response.getReleases()) {
				this.release.completeRelease(rel, monitor);
				rel.setStatus(this.status.getStatus(rel.getAssignment(), current, monitor));
			}
		}
		return response;
	}
}
