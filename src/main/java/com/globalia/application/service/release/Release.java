package com.globalia.application.service.release;

import com.globalia.application.Status;
import com.globalia.application.repository.release.GetRelease;
import com.globalia.application.service.master.Master;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;

@Service
public class Release extends FindService<ReleaseItem> {

	@Autowired
	private GetRelease getRelease;
	@Autowired
	private Master master;
	@Autowired
	private Status status;

	public ItemResponse getItem(final ReleaseItem item, final Monitor monitor) {
		ItemResponse release = this.getRelease.getItem(item, monitor);
		if (release.getError() == null) {
			this.completeRelease(release.getRelease(), monitor);
			release.getRelease().setStatus(this.status.getStatus(release.getRelease().getAssignment(), new Date(), monitor));
		}
		return release;
	}

	void completeRelease(final ReleaseItem release, final Monitor monitor) {
		if (release.getCodes() != null) {
			release.setMasters(new HashSet<>());
			this.master.convertCodeToMaster(release.getMasters(), release.getCodes(), monitor);
			release.setCodes(null);
		}
	}
}
