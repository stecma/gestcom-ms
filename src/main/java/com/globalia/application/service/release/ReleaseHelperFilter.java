package com.globalia.application.service.release;

import com.globalia.application.FilterUtils;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ReleaseFilter;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Set;

@Component
public class ReleaseHelperFilter {

	public boolean filter(final ReleaseFilter filter, final ReleaseItem release) {
		StringBuilder releaseValue = new StringBuilder();
		StringBuilder filterValue = new StringBuilder();
		Set<PairValue> codes = release.getCodes();

		FilterUtils.getInstance().addFilterValues(filterValue, releaseValue, filter.getBrand(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.BRAND, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, releaseValue, filter.getServiceType(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.SERVICE_TYPE, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, releaseValue, filter.getPayType(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.PAYMENT, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, releaseValue, filter.getCredential(), release.getCredential());
		if (StringUtils.hasText(filter.getOperationDays())) {
			FilterUtils.getInstance().addFilterValues(filterValue, releaseValue, filter.getOperationDays().toUpperCase(), release.getOperationDays().toUpperCase());
		}
		if (filter.getDays() > 0) {
			FilterUtils.getInstance().addFilterValues(filterValue, releaseValue, String.valueOf(filter.getDays()), String.valueOf(release.getDays()));
		}
		return filterValue.toString().equals(releaseValue.toString());
	}
}