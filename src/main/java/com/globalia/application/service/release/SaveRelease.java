package com.globalia.application.service.release;

import com.globalia.application.repository.release.SaveReleaseRedis;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveRelease extends SaveService<ReleaseItem> {

	@Autowired
	private SaveReleaseRedis save;
	@Autowired
	private Release release;

	public ItemResponse createItem(final ReleaseItem item, final Monitor monitor) {
		return this.addRedis(item, monitor);
	}

	public ItemResponse updateItem(final ReleaseItem item, final Monitor monitor) {
		return this.addRedis(item, monitor);
	}

	public ItemResponse deleteItem(final ReleaseItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.addRedis(item, monitor);
	}

	private ItemResponse addRedis(final ReleaseItem item, final Monitor monitor) {
		ItemResponse response = this.save.addItem(item, monitor);
		if (response.getError() == null) {
			response = this.release.getItem(item, monitor);
		}
		return response;
	}
}
