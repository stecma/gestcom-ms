package com.globalia.application.service.supplier;

import com.globalia.application.repository.supplier.SaveSupplierRedis;
import com.globalia.application.service.connector.SaveConnector;
import com.globalia.application.service.group.SaveGroup;
import com.globalia.application.service.master.Master;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;

@Service
@Slf4j
public class SaveSupplier extends SaveService<SupplierItem> {

	@Autowired
	private SaveSupplierRedis save;
	@Autowired
	private Supplier supplier;
	@Autowired
	private SaveGroup saveGroup;
	@Autowired
	private SaveConnector saveConnector;
	@Autowired
	private Master master;

	public ItemResponse createItem(final SupplierItem item, final Monitor monitor) {
		this.supplier.getStatus(item);
		return this.addRedis(item, monitor, true);
	}

	public ItemResponse updateItem(final SupplierItem item, final Monitor monitor) {
		this.supplier.getStatus(item);
		return this.addRedis(item, monitor, true);
	}

	public ItemResponse deleteItem(final SupplierItem item, final Monitor monitor) {
		this.supplier.getStatus(item);
		item.setDeleted(true);
		return this.addRedis(item, monitor, false);
	}

	public ItemResponse updateSap(final String system, final String sap, final boolean isNew, final Monitor monitor) {
		Item item = new Item();
		item.setSystem(system);
		item.setSap(sap);
		return this.updateSimpleValue(item, isNew, monitor);
	}

	public ItemResponse updateService(final String system, final String refser, final String tipser, final String srvOther, final String descSrv, final Monitor monitor) {
		Item item = new Item();
		item.setSystem(system);
		item.setRefser(refser);
		item.setTipser(tipser);
		item.setSrvOther(srvOther);
		item.setDescSrv(descSrv);
		return this.updateSimpleValue(item, false, monitor);
	}

	private ItemResponse updateSimpleValue(final Item item, final boolean isNew, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		response.setSupplier(new SupplierItem());

		MasterItem filter = new MasterItem();
		filter.setId(item.getSystem());
		filter.setEntity(MasterType.EXTERNAL_SYSTEM.name());

		ItemResponse aux = this.master.getItem(filter, monitor);
		if (aux.getError() == null) {
			if (StringUtils.hasText(item.getSap())) {
				this.save.addSapItem(item.getSystem(), item.getSap(), isNew, monitor);
				aux.getMaster().setSapCode(item.getSap());
			} else {
				this.save.addServiceItem(item.getSystem(), item.getRefser(), item.getTipser(), monitor);
				aux.getMaster().setServiceOther(item.getSrvOther());
				aux.getMaster().setDescSrvOther(item.getDescSrv());
			}
			response.getSupplier().setSystem(aux.getMaster());
		} else {
			response.setError(aux.getError());
		}
		return response;
	}

	private ItemResponse addRedis(final SupplierItem item, final Monitor monitor, final boolean forceGet) {
		Set<GroupCredentialItem> groups = this.getGroupCredential(item);
		ItemResponse response = this.save.addItem(item, monitor);
		if (response.getError() == null) {
			this.addGroupCredential(groups, item.getId(), monitor);
			if (forceGet) {
				response = this.supplier.getItem(item, monitor);
			}
		}
		return response;
	}

	private void addGroupCredential(final Set<GroupCredentialItem> groups, final String supplierId, final Monitor monitor) {
		if (groups != null) {
			for (GroupCredentialItem group : groups) {
				Set<ExternalCredentialItem> externals = this.getExternalCredentials(group);
				group.setSupplierId(supplierId);

				if (group.isDeleted()) {
					this.saveGroup.deleteItem(group, monitor);
				} else {
					if (group.isCreate()) {
						this.saveGroup.createItem(group, monitor);
					} else {
						this.saveGroup.updateItem(group, monitor);
					}
				}
				this.addExternalCredentials(externals, group.getId(), group.getSupplierId(), monitor);
			}
		}
	}

	private void addExternalCredentials(final Set<ExternalCredentialItem> externals, final String groupId, final String supplierId, final Monitor monitor) {
		if (externals != null) {
			for (ExternalCredentialItem external : externals) {
				external.setGroupId(groupId);
				external.setSupplierId(supplierId);

				if (external.isDeleted()) {
					this.saveConnector.deleteItem(external, monitor);
				} else {
					if (external.isCreate()) {
						this.saveConnector.createItem(external, monitor);
					} else {
						this.saveConnector.updateItem(external, monitor);
					}
				}
			}
		}
	}

	private Set<GroupCredentialItem> getGroupCredential(final SupplierItem supplier) {
		Set<GroupCredentialItem> groups = null;
		if (supplier.getExternalSys() != null && !supplier.getExternalSys().isEmpty()) {
			groups = new HashSet<>(supplier.getExternalSys());
			supplier.setExternalSys(null);
		}
		return groups;
	}

	private Set<ExternalCredentialItem> getExternalCredentials(final GroupCredentialItem group) {
		Set<ExternalCredentialItem> externals = null;
		if (group.getExternalConfig() != null && !group.getExternalConfig().isEmpty()) {
			externals = new HashSet<>(group.getExternalConfig());
			group.setExternalConfig(null);
		}
		return externals;
	}

	static class Item {
		@Getter
		@Setter
		private String system;
		@Getter
		@Setter
		private String sap;
		@Getter
		@Setter
		private String refser;
		@Getter
		@Setter
		private String tipser;
		@Getter
		@Setter
		private String srvOther;
		@Getter
		@Setter
		private String descSrv;
	}
}
