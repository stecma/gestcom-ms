package com.globalia.application.service.supplier;

import com.globalia.Errors;
import com.globalia.application.repository.supplier.GetSupplierByZones;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.stream.Collectors;

@Service
public class SupplierByZones extends FindAllService<SupplierByZoneItem> {

	@Autowired
	private GetSupplierByZones getSupplierByZones;
	@Autowired
	private Errors error;

	public AllItemsResponse getAllItems(final SupplierByZoneItem item, final Monitor monitor) {
		AllItemsResponse response = this.getSupplierByZones.getAllItems(monitor);
		if (response.getError() == null) {
			response.setSuppliersByZone(response.getSuppliersByZone().stream().filter(s -> this.filter(item, s)).collect(Collectors.toSet()));
			if (response.getSuppliersByZone().isEmpty()) {
				response.setError(this.error.addError("Cannot find suppliers by zone", ErrorLayer.SERVICE_LAYER, monitor));
			}
		}
		return response;
	}

	private boolean filter(final SupplierByZoneItem filter, final SupplierByZoneItem supplier) {
		boolean result = filter.getCodsis().equals(supplier.getCodsis());
		if (result && StringUtils.hasText(filter.getSubprv())) {
			result = supplier.getSubprv().equals(filter.getSubprv());
		}
		return result;
	}
}
