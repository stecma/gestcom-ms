package com.globalia.application.service.supplier;

import com.globalia.Dates;
import com.globalia.application.FilterUtils;
import com.globalia.dto.DateRange;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ExternalFilter;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.GroupFilter;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierFilter;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RefreshScope
public class SupplierCredentialFilter {

	@Value("${defaultDates.dateTo}")
	private String dateTo;
	@Value("${defaultDates.timeFrom}")
	private String timeFrom;
	@Value("${defaultDates.timeTo}")
	private String timeTo;

	@Autowired
	private Dates dates;

	public boolean filter(final SupplierFilter filter, final SupplierItem supplier, MasterItem item, final Monitor monitor) {
		StringBuilder filterValue = new StringBuilder();
		StringBuilder supplierValue = new StringBuilder();
		Set<PairValue> codes = supplier.getCodes();

		this.filterSystem(filter, supplier, filterValue, supplierValue, item);
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getBuyModel(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.BUY_MODEL, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getClientPref(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.CLIENT_PREF, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getServiceType(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.SERVICE_TYPE, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getUser(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.USERS, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getXsell(), String.valueOf(supplier.isXsell()));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getGiata(), supplier.getGiata());
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getMultiHotel(), String.valueOf(supplier.isMultiHotel()));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getTimeout(), String.valueOf(supplier.isTimeout()));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getGeneralRegimen(), String.valueOf(supplier.isGeneralRegimen()));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getAvailCost(), String.valueOf(supplier.isAvailCost()));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getSla(), String.valueOf(supplier.isHasSla()));
		boolean existClient = FilterUtils.getInstance().exists(filter.getClient(), FilterUtils.getInstance().getCodes(MasterType.AGENCY, codes));
		boolean existDestination = FilterUtils.getInstance().exists(filter.getTopDestination(), FilterUtils.getInstance().getCodes(MasterType.TOP_DESTINATION, codes));

		return this.getResult(filterValue, supplierValue, existClient, existDestination, supplier, filter, monitor);
	}

	private void filterSystem(final SupplierFilter filter, final SupplierItem supplier, final StringBuilder filterValue, final StringBuilder supplierValue, final MasterItem item) {
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getStatus(), supplier.getStatus().name());
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getOwnProduct(), item != null && item.getOwnProduct() != null ? String.valueOf(item.getOwnProduct()) : "");
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getOtherSrv(), item != null ? item.getServiceOther() : "");
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getSapCode(), item != null ? item.getSapCode() : "");
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getWbds(), item != null ? item.getWbdsCode() : "");
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getSupplier(), this.getCodeSystem(item));
		FilterUtils.getInstance().addFilterValues(filterValue, supplierValue, filter.getSubSupplier(), this.getCodeSubSystem(item));
	}

	private boolean getResult(final StringBuilder filterValue, final StringBuilder supplierValue, final boolean existClient, final boolean existDestination, final SupplierItem supplier, final SupplierFilter filter, final Monitor monitor) {
		boolean supplierResult = filterValue.toString().equals(supplierValue.toString()) && existClient && existDestination;
		if (supplierResult && (filter.getGroupFilter() != null || filter.getExternalFilter() != null)) {
			supplierResult = false;
			if (supplier.getExternalSys() != null) {
				Set<GroupCredentialItem> groups = supplier.getExternalSys().stream().filter(g -> this.filterGroups(filter.getGroupFilter(), filter.getExternalFilter(), g, monitor)).collect(Collectors.toSet());
				supplierResult = !groups.isEmpty();
			}
		}
		return supplierResult;
	}

	private String getCodeSubSystem(final MasterItem item) {
		String code = "";
		if (item != null && item.getId().contains("¬")) {
			code = item.getId().split("¬")[1];
		}
		return code;
	}

	private String getCodeSystem(final MasterItem item) {
		String code = "";
		if (item != null) {
			code = item.getId();
			if (item.getId().contains("¬")) {
				code = item.getId().split("¬")[0];
			}
		}
		return code;
	}

	private boolean filterGroups(final GroupFilter groupFilter, final ExternalFilter externalFilter, final GroupCredentialItem group, final Monitor monitor) {
		StringBuilder filterValue = new StringBuilder();
		StringBuilder groupValue = new StringBuilder();
		if (groupFilter != null) {
			this.existsGroup(groupFilter, group, filterValue, groupValue, group.getCodes());
		}

		boolean resultGroups = filterValue.toString().equals(groupValue.toString());
		if (resultGroups && externalFilter != null) {
			resultGroups = false;
			if (group.getExternalConfig() != null) {
				Set<ExternalCredentialItem> externals = group.getExternalConfig().stream().filter(e -> this.filterExternal(externalFilter, e, monitor)).collect(Collectors.toSet());
				resultGroups = !externals.isEmpty();
			}
		}

		return resultGroups;
	}

	private void existsGroup(final GroupFilter groupFilter, final GroupCredentialItem group, final StringBuilder filterValue, final StringBuilder groupValue, final Set<PairValue> codes) {
		if (StringUtils.hasText(groupFilter.getName()) && (!StringUtils.hasText(group.getSysName()) || !group.getSysName().contains(groupFilter.getName()))) {
			FilterUtils.getInstance().addFilterValues(filterValue, groupValue, "", "NO", true);
		}
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getBrand(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.BRAND, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getClientType(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.CLIENT_TYPE, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getActive(), String.valueOf(group.isActive()));
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getByDefault(), String.valueOf(group.isByDefault()));
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getTest(), String.valueOf(group.isTest()));
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getHotelx(), group.getHotelX().name());
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getTagdiv(), String.valueOf(group.isTagNationality()));
		FilterUtils.getInstance().addFilterValues(filterValue, groupValue, groupFilter.getTagnac(), String.valueOf(group.isTagCurrency()));
		if (!FilterUtils.getInstance().exists(groupFilter.getCurrency(), FilterUtils.getInstance().getCodes(MasterType.CURRENCY, codes))) {
			FilterUtils.getInstance().addFilterValues(filterValue, groupValue, "", "NO", true);
		}
		if (!FilterUtils.getInstance().exists(groupFilter.getCountry(), FilterUtils.getInstance().getCodes(MasterType.COUNTRY, codes))) {
			FilterUtils.getInstance().addFilterValues(filterValue, groupValue, "", "NO", true);
		}
		if (!FilterUtils.getInstance().exists(groupFilter.getDistributionModel(), FilterUtils.getInstance().getCodes(MasterType.DISTRIBUTION_MODEL, codes))) {
			FilterUtils.getInstance().addFilterValues(filterValue, groupValue, "", "NO", true);
		}
		if (!FilterUtils.getInstance().exists(groupFilter.getRates(), FilterUtils.getInstance().getCodes(MasterType.RATE_TYPE, codes))) {
			FilterUtils.getInstance().addFilterValues(filterValue, groupValue, "", "NO", true);
		}
	}

	private boolean filterExternal(final ExternalFilter externalFilter, final ExternalCredentialItem external, final Monitor monitor) {
		StringBuilder filterValue = new StringBuilder();
		StringBuilder externalValue = new StringBuilder();
		Set<PairValue> codes = external.getCodes();

		FilterUtils.getInstance().addFilterValues(filterValue, externalValue, externalFilter.getCorp(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.CORPORATION, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, externalValue, externalFilter.getFactType(), FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.BILLING_TYPE, codes)));
		FilterUtils.getInstance().addFilterValues(filterValue, externalValue, externalFilter.getUser(), external.getUser());
		if (StringUtils.hasText(externalFilter.getConnector()) && (!StringUtils.hasText(external.getConnector()) || !external.getConnector().contains(externalFilter.getConnector()))) {
			FilterUtils.getInstance().addFilterValues(filterValue, externalValue, "", "NO", true);
		}
		this.validationDates(externalFilter.getCheckIn(), external.getCheckIn(), filterValue, externalValue, monitor);
		this.validationDates(externalFilter.getBook(), external.getBook(), filterValue, externalValue, monitor);
		return filterValue.toString().equals(externalValue.toString());
	}

	private void validationDates(final DateRange filterDates, final DateRange dates, final StringBuilder filterValue, final StringBuilder externalValue, final Monitor monitor) {
		if (filterDates != null && (StringUtils.hasText(filterDates.getFrom()) || StringUtils.hasText(filterDates.getTo()))) {
			String from = null;
			String to = null;
			String timeFromObj = null;
			String timeToObj = null;
			if (dates != null) {
				from = dates.getFrom();
				to = dates.getTo();
				timeFromObj = dates.getTimeFrom();
				timeToObj = dates.getTimeTo();
			}
			DateControl dateControl = new DateControl(filterDates.getFrom(), filterDates.getTo(), from, timeFromObj, to, timeToObj);
			this.validateDates(dateControl, filterValue, externalValue, monitor);
		}
	}

	private void validateDates(final DateControl dateControl, final StringBuilder filterValue, final StringBuilder externalValue, final Monitor monitor) {
		try {
			Date filterFrom = StringUtils.hasText(dateControl.getFilterFrom()) ? this.dates.getDateTime(dateControl.getFilterFrom(), null, monitor) : new Date();
			Date filterTo = this.dates.getDateTime(StringUtils.hasText(dateControl.getFilterTo()) ? dateControl.getFilterTo() : this.dateTo, this.timeTo, monitor);
			Date from = this.dates.getDateTime(dateControl.getDateFrom(), StringUtils.hasText(dateControl.getTimeFrom()) ? dateControl.getTimeFrom() : this.timeFrom, monitor);
			Date to = this.dates.getDateTime(StringUtils.hasText(dateControl.getDateTo()) ? dateControl.getDateTo() : this.dateTo, StringUtils.hasText(dateControl.getTimeTo()) ? dateControl.getTimeTo() : this.timeTo, monitor);

			if (!this.dates.rangeOverlap(filterFrom, filterTo, from, to)) {
				filterValue.append('¬');
				externalValue.append("¬NO");
			}
		} catch (ValidateException ignored) {
			filterValue.append('¬');
			externalValue.append("¬NO");
		}
	}

	static class DateControl {

		@Getter
		private final String filterFrom;
		@Getter
		private final String filterTo;
		@Getter
		private final String dateFrom;
		@Getter
		private final String timeFrom;
		@Getter
		private final String dateTo;
		@Getter
		private final String timeTo;

		public DateControl(final String filterFrom, final String filterTo, final String dateFrom, final String timeFrom, final String dateTo, final String timeTo) {
			this.filterFrom = filterFrom;
			this.filterTo = filterTo;
			this.dateFrom = dateFrom;
			this.timeFrom = timeFrom;
			this.dateTo = dateTo;
			this.timeTo = timeTo;
		}
	}
}