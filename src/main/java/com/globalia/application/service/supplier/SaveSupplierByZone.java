package com.globalia.application.service.supplier;

import com.globalia.application.repository.supplier.SaveSupplierByZoneRedis;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveSupplierByZone extends SaveService<SupplierByZoneItem> {

	@Autowired
	private SaveSupplierByZoneRedis save;

	public ItemResponse createItem(final SupplierByZoneItem item, final Monitor monitor) {
		item.setCreated(true);
		return this.save.addItem(item, monitor);
	}

	public ItemResponse updateItem(final SupplierByZoneItem item, final Monitor monitor) {
		return this.save.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final SupplierByZoneItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.save.addItem(item, monitor);
	}
}
