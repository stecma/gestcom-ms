package com.globalia.application.service.supplier;

import com.globalia.application.repository.supplier.GetSupplierByZone;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplierByZone extends FindService<SupplierByZoneItem> {

	@Autowired
	private GetSupplierByZone getSupplierByZone;

	public ItemResponse getItem(final SupplierByZoneItem item, final Monitor monitor) {
		return this.getSupplierByZone.getItem(item, monitor);
	}
}
