package com.globalia.application.service.supplier;

import com.globalia.Errors;
import com.globalia.application.repository.supplier.GetSuppliers;
import com.globalia.application.service.master.Master;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class Suppliers extends FindAllService<SupplierItem> {

	@Autowired
	private GetSuppliers getSuppliers;
	@Autowired
	private Master master;
	@Autowired
	private Supplier supplier;
	@Autowired
	private SupplierCredentialFilter supplierHelper;
	@Autowired
	private Errors error;

	public AllItemsResponse getAllItems(final SupplierItem item, final Monitor monitor) {
		return this.getAllItems(item, false, monitor);
	}

	public AllItemsResponse getAllItems(final SupplierItem item, final boolean addMaster, final Monitor monitor) {
		AllItemsResponse response = this.getAllItems(monitor);
		if (response.getError() == null) {
			// Filtrar sobre los proveedores.
			response.setSuppliers(response.getSuppliers().stream().filter(s -> this.supplierHelper.filter(item.getFilter(), s, this.master.getMaster(MasterType.EXTERNAL_SYSTEM.name(), s.getCodes(), monitor), monitor)).collect(Collectors.toSet()));
			for (SupplierItem sup : response.getSuppliers()) {
				sup.setSystem(this.master.getMaster(MasterType.EXTERNAL_SYSTEM.name(), sup.getCodes(), monitor));
				if (addMaster) {
					this.supplier.completeSupplier(sup, monitor);
				}
				sup.setExternalSys(this.supplier.completeGroups(sup, addMaster, false, monitor));
				this.supplier.getStatus(sup);
			}

			// Filtrar sobre sus hijos.
			response.setSuppliers(response.getSuppliers().stream().filter(s -> this.supplierHelper.filter(item.getFilter(), s, this.master.getMaster(MasterType.EXTERNAL_SYSTEM.name(), s.getCodes(), monitor), monitor)).collect(Collectors.toSet()));
			if (response.getSuppliers().isEmpty()) {
				response.setError(this.error.addError("Cannot find suppliers", ErrorLayer.SERVICE_LAYER, monitor));
			}
		}
		return response;
	}

	public AllItemsResponse getAllItems(final Monitor monitor) {
		return this.getSuppliers.getAllItems(monitor);
	}
}
