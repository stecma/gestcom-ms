package com.globalia.application.service.supplier;

import com.globalia.application.repository.supplier.GetSupplier;
import com.globalia.application.service.connector.Connectors;
import com.globalia.application.service.group.Group;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.master.Master;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Set;

@Service
@RefreshScope
public class Supplier extends FindService<SupplierItem> {

	@Autowired
	private GetSupplier getSupplier;
	@Autowired
	private Master master;
	@Autowired
	private SupplierByZones supplierByZones;
	@Autowired
	private Groups groups;
	@Autowired
	private Connectors connectors;
	@Autowired
	private Group group;

	@Value("${masterConnector}")
	private String masterConnector;
	@Value("${masterGroup}")
	private String masterGroup;
	@Value("${masterSupplier}")
	private String masterSupplier;

	public ItemResponse getItem(final SupplierItem item, final Monitor monitor) {
		return this.getItem(item, true, monitor);
	}

	public ItemResponse getItem(final SupplierItem item, final boolean addMaster, final Monitor monitor) {
		return this.getItem(item, addMaster, false, monitor);
	}

	public ItemResponse getItem(final SupplierItem item, final boolean addMaster, final boolean allRedis, final Monitor monitor) {
		ItemResponse response = this.getSupplier.getItem(item, monitor);
		if (response.getError() == null) {
			response.getSupplier().setSystem(this.master.getMaster(MasterType.EXTERNAL_SYSTEM.name(), response.getSupplier().getCodes(), monitor));
			if (addMaster) {
				this.completeSupplier(response.getSupplier(), monitor);
				this.getSupByZone(response.getSupplier(), monitor);
			}
			response.getSupplier().setExternalSys(this.completeGroups(response.getSupplier(), addMaster, allRedis, monitor));
			this.getStatus(response.getSupplier());
		}
		return response;
	}

	public ItemResponse getMapKey(final SupplierItem item, final Monitor monitor) {
		return this.getSupplier.getMapKey(item.getId(), monitor);
	}

	public ItemResponse getGroup(final SupplierItem item, final Monitor monitor) {
		String[] keys = item.getId().split("¬");

		GroupCredentialItem filter = new GroupCredentialItem();
		filter.setId(keys[1]);
		filter.setSupplierId(keys[0]);

		ItemResponse response = this.group.getItem(filter, monitor);
		if (response.getError() == null) {
			response.getGroup().setExternalConfig(this.completeExternalCredentials(response.getGroup().getSupplierId(), response.getGroup().getId(), false, false, monitor));
		}
		return response;
	}

	public void completeSupplier(final SupplierItem supplier, final Monitor monitor) {
		if (StringUtils.hasText(this.masterSupplier)) {
			for (String field : this.masterSupplier.split("#")) {
				supplier.setError(this.master.addMasterValues(SupplierItem.class, supplier, field, supplier.getCodes(), monitor));
			}
		}
	}

	public Set<GroupCredentialItem> completeGroups(final SupplierItem supplier, final boolean addMaster, final boolean allRedis, final Monitor monitor) {
		Set<GroupCredentialItem> result = null;
		GroupCredentialItem filter = new GroupCredentialItem();
		filter.setSupplierId(supplier.getId());

		AllItemsResponse allGroups = this.groups.getAllItems(filter, allRedis, monitor);
		if (allGroups.getError() == null) {
			result = allGroups.getGroups();
			for (GroupCredentialItem grp : result) {
				if (addMaster) {
					this.completeExternalSys(grp, monitor);
				}
				grp.setExternalConfig(this.completeExternalCredentials(supplier.getId(), grp.getId(), addMaster, allRedis, monitor));
			}
		}
		return result;
	}

	public void getStatus(final SupplierItem supplier) {
		SupplierStatus status = SupplierStatus.DISABLED;
		if (supplier.getExternalSys() != null) {
			status = SupplierStatus.PARTIALLY;
			int total = 0;
			int enabled = 0;
			int disabled = 0;
			int test = 0;
			for (GroupCredentialItem grp : supplier.getExternalSys()) {
				if (grp.isActive()) {
					enabled++;
				} else {
					disabled++;
				}
				if (grp.isTest()) {
					test++;
				}
				total++;
			}

			if (total == enabled && test == 0) {
				status = SupplierStatus.ENABLED;
			} else if (total == disabled || test == total) {
				status = SupplierStatus.DISABLED;
			}
		}
		supplier.setStatus(status);
	}

	private Set<ExternalCredentialItem> completeExternalCredentials(final String supplierId, final String groupId, final boolean addMaster, final boolean allRedis, final Monitor monitor) {
		Set<ExternalCredentialItem> result = null;
		if (StringUtils.hasText(supplierId) && StringUtils.hasText(groupId)) {
			ExternalCredentialItem filter = new ExternalCredentialItem();
			filter.setSupplierId(supplierId);
			filter.setGroupId(groupId);

			AllItemsResponse externals = this.connectors.getAllItems(filter, allRedis, monitor);
			if (externals.getError() == null) {
				result = externals.getExternals();
				if (addMaster) {
					for (ExternalCredentialItem external : result) {
						this.completeExternalCredential(external, monitor);
					}
				}
			}
		}
		return result;
	}

	private void completeExternalCredential(final ExternalCredentialItem external, final Monitor monitor) {
		if (StringUtils.hasText(this.masterConnector)) {
			for (String field : this.masterConnector.split("#")) {
				external.setError(this.master.addMasterValues(ExternalCredentialItem.class, external, field, external.getCodes(), monitor));
			}
		}
	}

	private void completeExternalSys(final GroupCredentialItem group, final Monitor monitor) {
		if (StringUtils.hasText(this.masterGroup)) {
			for (String field : this.masterGroup.split("#")) {
				group.setError(this.master.addMasterValues(GroupCredentialItem.class, group, field, group.getCodes(), monitor));
			}
		}
	}

	private void getSupByZone(final SupplierItem supplier, final Monitor monitor) {
		String codsis = supplier.getSystem().getId();
		if (StringUtils.hasText(codsis)) {
			SupplierByZoneItem filter = new SupplierByZoneItem();
			if (codsis.contains("¬")) {
				String[] split = codsis.split("¬");
				filter.setCodsis(split[0]);
				filter.setSubprv(split[1]);
			} else {
				filter.setCodsis(codsis);
			}
			AllItemsResponse suppliers = this.supplierByZones.getAllItems(filter, monitor);
			supplier.setSupByZone(suppliers.getError() == null);
		}
	}
}
