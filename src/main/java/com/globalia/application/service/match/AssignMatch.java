package com.globalia.application.service.match;

import com.globalia.application.service.group.Group;
import com.globalia.application.service.group.SaveGroup;
import com.globalia.dto.DateRange;
import com.globalia.dto.Parameter;
import com.globalia.dto.credential.Assignment;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MatchItem;
import com.globalia.dto.credential.Note;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AssignMatch {

	@Autowired
	private Group group;
	@Autowired
	private SaveGroup saveGroup;

	public ItemResponse assign(final MatchItem match, final Monitor monitor) {
		if (!StringUtils.hasText(match.getCredentialId())) {
			return this.assignSupplier(match.getCodes(), match.getGroupId(), match.getAssignment(), monitor);
		}
		return this.assignCredential(match.getCodes(), match.getCredentialId(), match.getAssignment(), monitor);
	}

	public ItemResponse assignSupplier(final Set<Parameter> codes, final String groupId, final Assignment assignment, final Monitor monitor) {
		ItemResponse response = new ItemResponse();

		ItemResponse grp = this.getGroup(new GroupCredentialItem(), null, groupId, monitor);
		if (grp.getError() != null) {
			response.setError(grp.getError());
			return response;
		}

		this.addAssignment(grp.getGroup(), assignment);
		grp.getGroup().setExternalConfig(null);
		for (Parameter code : codes) {
			this.addNotes(grp.getGroup().getAssignment(), code.getValue());
			response = this.saveGroup.addAssignItem(code.getKey(), grp.getGroup(), monitor);
		}
		return response;
	}

	public ItemResponse assignCredential(final Set<Parameter> codes, final String credentialId, final Assignment assignment, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		GroupCredentialItem filter = new GroupCredentialItem();
		for (Parameter code : codes) {
			ItemResponse grp = this.getGroup(filter, credentialId, code.getKey(), monitor);
			if (grp.getError() != null) {
				response.setError(grp.getError());
				return response;
			}

			this.addAssignment(grp.getGroup(), assignment);
			this.addNotes(grp.getGroup().getAssignment(), code.getValue());
			grp.getGroup().setExternalConfig(null);
			response = this.saveGroup.addAssignItem(credentialId, grp.getGroup(), monitor);
			if (response.getError() != null) {
				return response;
			}
		}
		return response;
	}

	private void addAssignment(final GroupCredentialItem group, final Assignment assignment) {
		if (group.getAssignment() == null) {
			group.setAssignment(new Assignment());
		}
		if (assignment.getCheckIn() != null) {
			group.getAssignment().setCheckIn(new DateRange());
			group.getAssignment().getCheckIn().setFrom(assignment.getCheckIn().getFrom());
			group.getAssignment().getCheckIn().setTo(assignment.getCheckIn().getTo());
		}
		if (assignment.getBook() != null) {
			group.getAssignment().setBook(new DateRange());
			group.getAssignment().getBook().setFrom(assignment.getBook().getFrom());
			group.getAssignment().getBook().setTo(assignment.getBook().getTo());
			group.getAssignment().getBook().setTimeFrom(assignment.getBook().getTimeFrom());
			group.getAssignment().getBook().setTimeTo(assignment.getBook().getTimeTo());
		}
	}

	private void addNotes(final Assignment assignment, final String notes) {
		if (StringUtils.hasText(notes)) {
			String note = notes;
			String genNote = null;
			if (notes.contains("¬")) {
				String[] split = notes.split("¬");
				note = split[0];
				genNote = split[1];
			}

			if (StringUtils.hasText(note)) {
				if (assignment.getNotes() == null) {
					assignment.setNotes(new LinkedHashSet<>());
				}
				assignment.getNotes().add(this.addNote(note));
			}
			if (StringUtils.hasText(genNote)) {
				if (assignment.getGenNotes() == null) {
					assignment.setGenNotes(new LinkedHashSet<>());
				}
				assignment.getGenNotes().add(this.addNote(genNote));
			}
		}
	}

	private Note addNote(final String note) {
		String[] split = note.split("#");
		Note text = new Note();
		text.setUser(split[0]);
		text.setDate(split[1]);
		text.setData(split[2]);
		return text;
	}

	private ItemResponse getGroup(final GroupCredentialItem filter, final String credential, final String code, final Monitor monitor) {
		String[] keys = code.split("¬");
		if (StringUtils.hasText(credential)) {
			Set<GroupCredentialItem> groups = this.group.getAssignItem(credential);
			Optional<GroupCredentialItem> grp = groups.stream().filter(g -> keys[0].equals(g.getSupplierId()) && keys[1].equals(g.getId())).findFirst();
			if (grp.isPresent()) {
				ItemResponse item = new ItemResponse();
				item.setGroup(grp.get());
				return item;
			}

		}

		filter.setSupplierId(keys[0]);
		filter.setId(keys[1]);
		return this.group.getItem(filter, monitor);
	}
}
