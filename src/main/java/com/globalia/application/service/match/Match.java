package com.globalia.application.service.match;

import com.globalia.Dates;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.HotelX;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Component
@RefreshScope
public class Match {

	private static final long TEN_SECOND_FIXED = 10 * 1000L;
	private static final String BLOCK_FIELD = "blocks";

	@Value("${match.fields}")
	private String matchFields;
	@Value("${match.rates}")
	private String matchRates;
	@Value("${defaultDates.dateTo}")
	private String dateTo;
	@Value("${defaultDates.timeFrom}")
	private String timeFrom;

	@Autowired
	private Dates dates;

	private List<String> fieldList;
	private Set<String> rates;
	private Map<String, Boolean> mandatoryMap;
	private String fields;
	private String rate;

	@PostConstruct
	private void init() {
		this.reloadSentence();
	}

	public void execute(final Set<CredentialItem> credentials, final Map<String, String> resultMap, final SupplierItem supplier, final GroupCredentialItem group, final Monitor monitor) {
		Item itemSupplier = this.getItemSupplier(supplier, group);
		for (CredentialItem credential : credentials) {
			Item itemCredential = this.getItemCredential(credential);
			resultMap.computeIfPresent(credential.getId(), (k, v) -> v = this.getResult(itemSupplier, itemCredential, monitor));
		}
	}

	public void execute(final Set<SupplierItem> suppliers, final Map<String, String> resultMap, final CredentialItem credential, final Monitor monitor) {
		Item itemCredential = this.getItemCredential(credential);
		for (SupplierItem supplier : suppliers) {
			for (GroupCredentialItem group : supplier.getExternalSys()) {
				if (resultMap.containsKey(group.getId())) {
					Item itemSupplier = this.getItemSupplier(supplier, group);
					resultMap.computeIfPresent(credential.getId(), (k, v) -> v = this.getResult(itemSupplier, itemCredential, monitor));
				}
			}
		}
	}

	private String getResult(final Item itemSupplier, final Item itemCredential, final Monitor monitor) {
		MatchLevel level = MatchLevel.RED;

		Set<String> mandatoryFields = new HashSet<>();
		Set<String> recommendedFields = new HashSet<>();
		this.getFieldConfig(mandatoryFields, recommendedFields);
		if (!mandatoryFields.isEmpty()) {
			level = this.compareField(mandatoryFields, itemSupplier, itemCredential, true, monitor);
			if (level != MatchLevel.RED) {
				level = this.compareField(recommendedFields, itemSupplier, itemCredential, false, monitor);
			}
		}
		return level.name();
	}

	private MatchLevel compareField(final Set<String> fields, final Item itemSupplier, final Item itemCredential, final boolean isMandatory, final Monitor monitor) {
		int weight = fields.size();
		int count = 0;

		for (String fieldName : fields) {
			count += this.getEqualsValue(fieldName, itemSupplier, itemCredential);
			count += this.getStatus(fieldName, itemSupplier.getStatus());
			count += this.getOwnProduct(fieldName, itemSupplier.isOwnProduct(), itemCredential.getProducts());
			count += this.getMultiHotel(fieldName, itemSupplier.isMultiHotel(), itemCredential.getRequest());
			count += this.getValueBool(fieldName, Match.BLOCK_FIELD.equals(fieldName) ? itemSupplier.isBlocks() : itemSupplier.isAvailCost(), Match.BLOCK_FIELD.equals(fieldName) ? itemCredential.isBlocks() : itemCredential.isAvailCost());
			count += this.getHotelX(fieldName, itemSupplier.getHotelXEnum(), itemCredential.isHotelX());
			count += this.getMarket(fieldName, itemSupplier.isTagnac(), itemSupplier.getMarket(), itemCredential.isTagnac(), itemCredential.getMarket());
			count += this.getModelDistribution(fieldName, itemSupplier.getModelDistribution(), itemCredential.getModelDistribution(), itemSupplier.isAllModels(), itemCredential.isAllModels());
			count += this.getRates(fieldName, itemSupplier.getRates(), itemCredential.getRates(), itemSupplier.isAllRates(), itemCredential.isAllRates());
			count += this.getActive(fieldName, itemSupplier.isActive(), itemSupplier.getConnect(), monitor);
		}

		return this.getResult(isMandatory, weight, count);
	}

	private MatchLevel getResult(final boolean isMandatory, final int weight, final int count) {
		if (isMandatory) {
			return weight == count ? MatchLevel.GREEN : MatchLevel.RED;
		}
		return weight != count ? MatchLevel.YELLOW : MatchLevel.GREEN;
	}

	private int getActive(final String fieldName, final boolean active, final Set<ExternalCredentialItem> connectors, final Monitor monitor) {
		int result = 0;
		if ("active".equals(fieldName) && active && connectors != null) {
			// Si la fecha actual se encuentra entre las fechas de booking
			Optional<ExternalCredentialItem> connect = connectors.stream().filter(c -> this.validateDates(c.getBook().getFrom(), c.getBook().getTimeFrom(), c.getBook().getTo(), monitor)).findFirst();
			if (connect.isPresent()) {
				result = 1;
			}
		}
		return result;
	}

	private boolean validateDates(final String dateFrom, final String timeFrom, final String dateTo, final Monitor monitor) {
		boolean result = false;
		try {
			Date from = this.dates.getDateTime(dateFrom, StringUtils.hasText(timeFrom) ? timeFrom : this.timeFrom, monitor);
			Date to = this.dates.getDateTime(StringUtils.hasText(dateTo) ? dateTo : this.dateTo, null, monitor);

			result = this.dates.currentBetweenRange(from, to);
		} catch (ValidateException ignored) {
			//
		}
		return result;
	}

	private int getMarket(final String fieldName, final boolean allSupplier, final Set<String> supplier, final boolean allCredential, final Set<String> credential) {
		if ("market".equals(fieldName)) {
			if (allSupplier || allCredential) {
				return 1;
			}

			boolean found = this.containValue(supplier, credential);
			if (found) {
				return 1;
			}
		}
		return 0;
	}

	private int getHotelX(final String fieldName, final HotelX hotelXEnum, final boolean hotelX) {
		if ("hotelX".equals(fieldName) && ((hotelX && hotelXEnum != HotelX.NO) || (!hotelX && hotelXEnum != HotelX.EXCLUSIVO))) {
			return 1;
		}
		return 0;
	}

	private int getValueBool(final String fieldName, final boolean supplier, final boolean credential) {
		if (Match.BLOCK_FIELD.equals(fieldName) || "availCost".equals(fieldName)) {
			if (credential && !supplier) {
				return 0;
			}
			return 1;
		}
		return 0;
	}

	private int getMultiHotel(final String fieldName, final boolean multiHotel, final String request) {
		if ("multiHotel".equals(fieldName) && (multiHotel || "DEST".equals(request))) {
			return 1;
		}
		return 0;
	}

	private int getOwnProduct(final String fieldName, final boolean ownProduct, final Set<String> products) {
		if ("ownProduct".equals(fieldName) && ((ownProduct && products.contains("PPPULL")) || (!ownProduct && products.contains("PULL3")))) {
			return 1;
		}
		return 0;
	}

	private int getEqualsValue(final String fieldName, final Item itemSupplier, final Item itemCredential) {
		int result = 0;
		if ("brand".equals(fieldName) && itemSupplier.getBrand().equals(itemCredential.getBrand())) {
			result = 1;
		}
		if ("serviceType".equals(fieldName) && itemSupplier.getServiceType().equals(itemCredential.getServiceType())) {
			result = 1;
		}
		if ("test".equals(fieldName) && itemSupplier.isTest() == itemCredential.isTest()) {
			result = 1;
		}
		if ("timeOut".equals(fieldName) && itemSupplier.isTimeOut() == itemCredential.isTimeOut()) {
			result = 1;
		}
		return result;
	}

	private int getModelDistribution(final String fieldName, final Set<String> supplier, final Set<String> credential, final boolean allSupplier, final boolean allCredential) {
		if ("modelDistribution".equals(fieldName)) {
			if (allSupplier || allCredential) {
				return 1;
			}

			boolean found = this.containValue(supplier, credential);
			if (found) {
				return 1;
			}
		}
		return 0;
	}

	private int getRates(final String fieldName, final Set<String> supplier, final Set<String> credential, final boolean allSupplier, final boolean allCredential) {
		if ("rates".equals(fieldName)) {
			if (allSupplier || allCredential) {
				return 1;
			}

			boolean found = this.containValue(credential, this.rates);
			if (found) {
				return 1;
			}

			found = this.containValue(supplier, this.rates);
			if (found) {
				return 1;
			}
		}
		return 0;
	}

	private boolean containValue(final Set<String> values, final Set<String> source) {
		boolean found = false;
		Optional<String> exists = values.stream().filter(source::contains).findFirst();
		if (exists.isPresent()) {
			found = true;
		}
		return found;
	}

	private int getStatus(final String fieldName, final String status) {
		if ("status".equals(fieldName) && ("ENABLED".equals(status) || "PARTIALY".equals(status))) {
			return 1;
		}
		return 0;
	}

	private Item getItemSupplier(final SupplierItem supplier, final GroupCredentialItem group) {
		Item item = new Item();
		item.setBrand(this.getCode(this.getCodes(MasterType.BRAND, group.getCodes())));
		item.setServiceType(this.getCode(this.getCodes(MasterType.SERVICE_TYPE, supplier.getCodes())));
		item.setStatus(supplier.getStatus().name());
		item.setOwnProduct(supplier.getSystem().getOwnProduct() != null && supplier.getSystem().getOwnProduct());
		item.setMultiHotel(supplier.isMultiHotel());
		item.setTimeOut(supplier.isTimeout());
		item.setBlocks(supplier.isBlock());
		item.setAvailCost(supplier.isAvailCost());
		item.setActive(group.isActive());
		item.setConnect(group.getExternalConfig());
		item.setTest(group.isTest());
		item.setHotelXEnum(group.getHotelX());
		item.setTagnac(group.isTagNationality());
		Set<String> codes = this.getCodes(MasterType.COUNTRY, group.getCodes());
		if (!item.isTagnac() && codes.isEmpty()) {
			item.setTagnac(true);
		}
		item.setMarket(codes);
		item.setAllModels(group.getAllMasters() != null && group.getAllMasters().contains(MasterType.DISTRIBUTION_MODEL.name().toLowerCase()));
		codes = this.getCodes(MasterType.DISTRIBUTION_MODEL, group.getCodes());
		item.setModelDistribution(codes);
		item.setAllRates(group.getAllMasters() != null && group.getAllMasters().contains(MasterType.RATE_TYPE.name().toLowerCase()));
		codes = this.getCodes(MasterType.RATE_TYPE, group.getCodes());
		item.setRates(codes);
		return item;
	}

	private Item getItemCredential(final CredentialItem credential) {
		Item item = new Item();
		item.setBrand(this.getCode(this.getCodes(MasterType.BRAND, credential.getCodes())));
		item.setServiceType(this.getCode(this.getCodes(MasterType.SERVICE_TYPE, credential.getCodes())));
		item.setStatus(SupplierStatus.ENABLED.name());

		item.setProducts(this.getCodes(MasterType.PRODUCT_ORIGIN, credential.getCodes()));
		item.setRequest(this.getCode(this.getCodes(MasterType.REQUEST, credential.getCodes())));
		item.setTimeOut(true);
		item.setBlocks(credential.isPriceChange());
		item.setAvailCost(credential.isAvailCost());
		item.setActive(true);
		item.setTest(credential.isTest());
		item.setHotelX(credential.isHotelX());
		item.setTagnac(credential.isTagNationality());
		Set<String> codes = this.getCodes(MasterType.COUNTRY, credential.getCodes());
		if (!item.isTagnac() && codes.isEmpty()) {
			item.setTagnac(true);
		}
		item.setMarket(codes);
		codes = this.getCodes(MasterType.DISTRIBUTION_MODEL, credential.getCodes());
		item.setAllModels(codes.isEmpty());
		item.setModelDistribution(codes);
		codes = this.getCodes(MasterType.RATE_TYPE, credential.getCodes());
		item.setAllRates(codes.isEmpty());
		item.setRates(codes);
		return item;
	}

	private void getFieldConfig(final Set<String> mandatoryFields, final Set<String> recommendedFields) {
		for (String field : this.fieldList) {
			boolean mandatory = this.mandatoryMap.get(field);
			if (mandatory) {
				mandatoryFields.add(field);
			} else {
				recommendedFields.add(field);
			}
		}
	}

	private String getCode(final Set<String> codes) {
		String result = "";
		if (!codes.isEmpty()) {
			result = codes.iterator().next();
		}
		return result;
	}

	private Set<String> getCodes(final MasterType entity, final Set<PairValue> codes) {
		if (codes != null) {
			for (PairValue pair : codes) {
				if (pair.getKey().equals(entity.name())) {
					return pair.getValue();
				}
			}
		}
		return new HashSet<>();
	}

	@Scheduled(fixedRate = TEN_SECOND_FIXED)
	void reloadSentence() {
		if (!StringUtils.hasText(this.fields) || (StringUtils.hasText(this.matchFields) && !this.fields.equalsIgnoreCase(this.matchFields))) {
			this.getFields();
		}
		if (!StringUtils.hasText(this.rate) || (StringUtils.hasText(this.matchRates) && !this.rate.equalsIgnoreCase(this.matchRates))) {
			this.getRates();
		}
	}

	private void getRates() {
		JsonObject json = (JsonObject) JsonParser.parseString(this.matchRates);
		this.rate = this.matchRates;
		Set<String> aux = this.fromJsonRates(json.get("rates").getAsJsonArray());
		if (aux != null && !aux.isEmpty()) {
			this.rates = aux;
		}
	}

	private Set<String> fromJsonRates(final JsonArray rates) {
		Set<String> list = null;
		if (rates != null) {
			for (JsonElement rateElm : rates) {
				if (list == null) {
					list = new HashSet<>();
				}
				list.add(rateElm.getAsJsonObject().get("id").getAsString());
			}
		}
		return list;
	}

	private void getFields() {
		JsonObject json = (JsonObject) JsonParser.parseString(this.matchFields);
		this.fields = this.matchFields;
		Map<String, Boolean> oblMap = new HashMap<>();
		List<FieldOrder> list = this.fromJsonFields(json.get("fields").getAsJsonArray(), oblMap);
		if (!list.isEmpty()) {
			List<String> auxList = new ArrayList<>();
			for (FieldOrder param : list) {
				auxList.add(param.getFilter());
			}
			this.fieldList = auxList;
		}
		if (!oblMap.isEmpty()) {
			this.mandatoryMap = oblMap;
		}
	}

	private List<FieldOrder> fromJsonFields(final JsonArray content, final Map<String, Boolean> oblMap) {
		List<FieldOrder> list = new ArrayList<>();
		for (JsonElement query : content) {
			String key = query.getAsJsonObject().get("field").getAsString();
			if (!oblMap.containsKey(key)) {
				oblMap.put(key, query.getAsJsonObject().get("mandatory").getAsBoolean());
				if (query.getAsJsonObject().get("active").getAsBoolean()) {
					FieldOrder field = new FieldOrder();
					field.setFilter(key);
					field.setOrder(query.getAsJsonObject().get("order").getAsInt());
					list.add(field);
				}
			}
		}
		list.sort(Comparator.comparing(FieldOrder::getOrder));
		return list;
	}

	static class FieldOrder {

		@Getter
		@Setter
		private String filter;
		@Getter
		@Setter
		private int order;
	}

	static class Item {

		@Getter
		@Setter
		private String brand;
		@Getter
		@Setter
		private String serviceType;
		@Getter
		@Setter
		private String status;
		@Getter
		@Setter
		private boolean ownProduct;
		@Getter
		@Setter
		private Set<String> products;
		@Getter
		@Setter
		private boolean multiHotel;
		@Getter
		@Setter
		private String request;
		@Getter
		@Setter
		private boolean timeOut;
		@Getter
		@Setter
		private boolean blocks;
		@Getter
		@Setter
		private boolean availCost;
		@Getter
		@Setter
		private boolean active;
		@Getter
		@Setter
		private Set<ExternalCredentialItem> connect;
		@Getter
		@Setter
		private boolean test;
		@Getter
		@Setter
		private HotelX hotelXEnum;
		@Getter
		@Setter
		private boolean hotelX;
		@Getter
		@Setter
		private boolean tagnac;
		@Getter
		@Setter
		private Set<String> market;
		@Getter
		@Setter
		private boolean allModels;
		@Getter
		@Setter
		private Set<String> modelDistribution;
		@Getter
		@Setter
		private boolean allRates;
		@Getter
		@Setter
		private Set<String> rates;
	}
}