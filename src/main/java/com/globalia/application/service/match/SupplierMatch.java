package com.globalia.application.service.match;

import com.globalia.Errors;
import com.globalia.application.AbstractMatch;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.application.service.credential.Credentials;
import com.globalia.dto.Parameter;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MatchItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SupplierMatch extends AbstractMatch {

	@Autowired
	private Supplier supplier;
	@Autowired
	private Errors errors;
	@Autowired
	private Credentials credentials;

	public ItemResponse matchSupplier(final MatchItem match, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		response.setMatch(match);

		// Recuperar el proveedor pull
		ItemResponse supplierItem = this.getSupplier(match.getSupplierId(), monitor);
		if (supplierItem.getError() != null) {
			response.setError(supplierItem.getError());
			return response;
		}

		// Recuperar el grupo de credenciales.
		GroupCredentialItem group = this.getGroup(match.getGroupId(), supplierItem.getSupplier().getExternalSys());
		if (group == null) {
			response.setError(this.errors.addError(String.format("Credential group %s not exists in supplier %s", match.getGroupId(), match.getSupplierId()), ErrorLayer.SERVICE_LAYER, monitor));
			return response;
		}

		// Recuperar las credenciales de clientes
		AllItemsResponse allCredentials = this.credentials.getAllItems(match.getCustomer(), monitor);
		match.setCredentials(allCredentials.getCredentials());
		if (allCredentials.getError() != null) {
			response.setError(allCredentials.getError());
			return response;
		}

		// Averiguar si se trata de un grupo de credencial asignado o no.
		// Puede darse el caso de que la credencial tenga otra credencial del proveedor.
		Map<String, Map<String, String>> assignmentMap = this.assignmentSupplier(match, monitor);

		// Proceso de match.
		this.supplierMatch(ASSIGN_TYPE.UNASSIGN, match, assignmentMap, supplierItem.getSupplier(), group, monitor);
		this.supplierMatch(ASSIGN_TYPE.ASSIGN, match, assignmentMap, supplierItem.getSupplier(), group, monitor);
		match.setOtherGroupAssigned(convertMapToSet(assignmentMap.get(ASSIGN_TYPE.OTHERS.name())));
		return response;
	}

	private ItemResponse getSupplier(final String supplierId, final Monitor monitor) {
		SupplierItem filter = new SupplierItem();
		filter.setId(supplierId);

		ItemResponse item = this.supplier.getItem(filter, false, monitor);
		if (item.getError() == null && item.getSupplier().getStatus() == SupplierStatus.DISABLED) {
			item.setError(this.errors.addError(String.format("Supplier %s is disabled", supplierId), ErrorLayer.SERVICE_LAYER, monitor));
		}
		return item;
	}

	private GroupCredentialItem getGroup(final String groupId, final Set<GroupCredentialItem> groups) {
		if (groups != null) {
			Optional<GroupCredentialItem> group = groups.stream().filter(g -> groupId.equals(g.getId())).findFirst();
			if (group.isPresent()) {
				return group.get();
			}
		}
		return null;
	}

	private Map<String, Map<String, String>> assignmentSupplier(final MatchItem match, final Monitor monitor) {
		Map<String, Map<String, String>> map = new HashMap<>();
		map.put(ASSIGN_TYPE.ASSIGN.name(), new HashMap<>());
		map.put(ASSIGN_TYPE.UNASSIGN.name(), new HashMap<>());
		map.put(ASSIGN_TYPE.OTHERS.name(), new HashMap<>());

		Date currentDate = new Date();
		for (CredentialItem credential : match.getCredentials()) {
			// Si la credencial del cliente no tiene grupos. UNASSIGNED.
			if (credential.getGroups() == null) {
				this.allocateCredential(map, credential.getId(), false, false);
				continue;
			}

			boolean foundSupplier = false;
			boolean foundGroup = false;
			for (GroupCredentialItem group : credential.getGroups()) {
				if (!foundSupplier && group.getSupplierId().equals(match.getSupplierId()) && isValidDate(currentDate, group.getAssignment(), monitor)) {
					foundSupplier = true;
				}

				if (foundSupplier && group.getId().equals(match.getGroupId()) && isValidDate(currentDate, group.getAssignment(), monitor)) {
					foundGroup = true;
				}
			}
			this.allocateCredential(map, credential.getId(), foundSupplier, foundGroup);
		}
		return map;
	}

	private void allocateCredential(final Map<String, Map<String, String>> map, final String credential, final boolean foundSupplier, final boolean foundGroup) {
		if (foundGroup && foundSupplier) {
			map.get(ASSIGN_TYPE.ASSIGN.name()).put(credential, MatchLevel.RED.name());
		}

		if (!foundGroup && !foundSupplier) {
			map.get(ASSIGN_TYPE.UNASSIGN.name()).put(credential, MatchLevel.RED.name());
		}

		if (!foundGroup && foundSupplier) {
			map.get(ASSIGN_TYPE.OTHERS.name()).put(credential, MatchLevel.RED.name());
		}
	}

	private void supplierMatch(final ASSIGN_TYPE type, final MatchItem match, final Map<String, Map<String, String>> map, final SupplierItem supplier, final GroupCredentialItem group, final Monitor monitor) {
		if (!map.get(type.name()).isEmpty()) {
			Set<CredentialItem> allCredentials = match.getCredentials().stream().filter(c -> map.get(type.name()).containsKey(c.getId())).collect(Collectors.toSet());
			getMatchProcess().execute(allCredentials, map.get(type.name()), supplier, group, monitor);

			Set<Parameter> result = this.convertMapToSet(map.get(type.name()));
			if (type == ASSIGN_TYPE.UNASSIGN) {
				match.setUnAssigned(result);
			} else {
				match.setAssigned(result);
			}
		}
	}
}
