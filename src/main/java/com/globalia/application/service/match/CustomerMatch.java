package com.globalia.application.service.match;

import com.globalia.Errors;
import com.globalia.application.AbstractMatch;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.application.service.credential.Credential;
import com.globalia.dto.Parameter;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MatchItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.exception.Error;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CustomerMatch extends AbstractMatch {

	@Autowired
	private Credential credential;
	@Autowired
	private Suppliers suppliers;
	@Autowired
	private Errors errors;

	public ItemResponse matchCustomer(final MatchItem match, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		response.setMatch(match);

		// Recuperar la credencial de cliente
		ItemResponse credentialItem = this.getCredential(match.getCredentialId(), monitor);
		if (credentialItem.getError() != null) {
			response.setError(credentialItem.getError());
			return response;
		}

		// Recuperar las credenciales de los proveedores pull
		Set<GroupCredentialItem> groups = new HashSet<>();
		Error error = this.getSuppliers(match, groups, monitor);
		if (error != null) {
			response.setError(error);
			return response;
		}

		if (groups.isEmpty()) {
			response.setError(this.errors.addError("There aren't credential groups to do the match", ErrorLayer.SERVICE_LAYER, monitor));
			return response;
		}

		// Averiguar si se trata de un grupo de credencial asignado o no.
		Map<String, Map<String, String>> assignmentMap = this.assignmentCredential(credentialItem.getCredential(), groups, monitor);

		// Proceso de match.
		this.customerMatch(ASSIGN_TYPE.UNASSIGN, match, assignmentMap, credentialItem.getCredential(), monitor);
		this.customerMatch(ASSIGN_TYPE.ASSIGN, match, assignmentMap, credentialItem.getCredential(), monitor);
		return response;
	}

	private ItemResponse getCredential(final String credentialId, final Monitor monitor) {
		CredentialItem filter = new CredentialItem();
		filter.setId(credentialId);
		return this.credential.getItem(filter, false, monitor);
	}

	private Error getSuppliers(final MatchItem match, final Set<GroupCredentialItem> groups, final Monitor monitor) {
		Set<SupplierItem> allSuppliers = this.getAllSuppliers(match.getSupplier(), monitor);
		if (allSuppliers.isEmpty()) {
			return this.errors.addError("Cannot find suppliers", ErrorLayer.SERVICE_LAYER, monitor);
		}

		for (SupplierItem supplier : allSuppliers) {
			if (!this.addGroups(supplier.getStatus(), groups, supplier.getExternalSys())) {
				continue;
			}

			if (match.getSuppliers() == null) {
				match.setSuppliers(new HashSet<>());
			}
			match.getSuppliers().add(supplier);
		}
		return null;
	}

	private Set<SupplierItem> getAllSuppliers(final SupplierItem filter, final Monitor monitor) {
		if (filter.getFilter().getStatus() != null) {
			AllItemsResponse allItems = this.suppliers.getAllItems(filter, false, monitor);
			if (allItems.getError() == null) {
				return allItems.getSuppliers();
			}
		} else {
			Set<SupplierItem> allSuppliers = new HashSet<>();
			filter.getFilter().setStatus(SupplierStatus.ENABLED.name());
			AllItemsResponse allItems = this.suppliers.getAllItems(filter, false, monitor);
			if (allItems.getError() == null) {
				allSuppliers.addAll(allItems.getSuppliers());
			}

			filter.getFilter().setStatus(SupplierStatus.PARTIALLY.name());
			allItems = this.suppliers.getAllItems(filter, false, monitor);
			if (allItems.getError() == null) {
				allSuppliers.addAll(allItems.getSuppliers());
			}
			if (!allSuppliers.isEmpty()) {
				return allSuppliers;
			}
		}
		return new HashSet<>();
	}

	private boolean addGroups(final SupplierStatus status, final Set<GroupCredentialItem> groups, final Set<GroupCredentialItem> externalSys) {
		boolean addSupplier = false;
		if (status != SupplierStatus.DISABLED && externalSys != null) {
			for (GroupCredentialItem group : externalSys) {
				if (!group.isActive()) {
					continue;
				}
				groups.add(group);
				addSupplier = true;
			}
		}
		return addSupplier;
	}

	private Map<String, Map<String, String>> assignmentCredential(final CredentialItem credential, final Set<GroupCredentialItem> groups, final Monitor monitor) {
		Map<String, Map<String, String>> map = new HashMap<>();
		map.put(ASSIGN_TYPE.ASSIGN.name(), new HashMap<>());
		map.put(ASSIGN_TYPE.UNASSIGN.name(), new HashMap<>());

		Map<String, GroupCredentialItem> codeMap = null;
		if (credential.getGroups() != null && !credential.getGroups().isEmpty()) {
			for (GroupCredentialItem grp : credential.getGroups()) {
				if (codeMap == null) {
					codeMap = new HashMap<>();
				}
				if (!codeMap.containsKey(grp.getId())) {
					codeMap.put(grp.getId(), grp);
				}
			}
		}

		Date currentDate = new Date();
		for (GroupCredentialItem group : groups) {
			if (codeMap == null || !codeMap.containsKey(group.getId()) || !isValidDate(currentDate, codeMap.get(group.getId()).getAssignment(), monitor)) {
				// Sin Asignar
				map.get(ASSIGN_TYPE.UNASSIGN.name()).put(group.getId(), MatchLevel.RED.name());
			} else {
				map.get(ASSIGN_TYPE.ASSIGN.name()).put(group.getId(), MatchLevel.RED.name());
			}
		}
		return map;
	}

	private void customerMatch(final ASSIGN_TYPE type, final MatchItem match, final Map<String, Map<String, String>> map, final CredentialItem credential, final Monitor monitor) {
		if (!map.get(type.name()).isEmpty()) {
			Set<SupplierItem> allSuppliers = match.getSuppliers().stream().filter(s -> map.get(type.name()).containsKey(s.getId())).collect(Collectors.toSet());
			getMatchProcess().execute(allSuppliers, map.get(type.name()), credential, monitor);

			Set<Parameter> result = convertMapToSet(map.get(type.name()));
			if (type == ASSIGN_TYPE.UNASSIGN) {
				match.setUnAssigned(result);
			} else {
				match.setAssigned(result);
			}
		}
	}
}
