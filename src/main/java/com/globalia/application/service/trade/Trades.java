package com.globalia.application.service.trade;

import com.globalia.Errors;
import com.globalia.application.repository.trade.GetTrade;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class Trades extends FindAllService<TradePolicyItem> {

	@Autowired
	private GetTrade getTrade;
	@Autowired
	private Trade trade;
	@Autowired
	private Errors error;

	public AllItemsResponse getAllItems(final TradePolicyItem item, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();

		Set<TradePolicyItem> result = this.getTrade.getItem(item.getCredential());
		if (result == null || result.isEmpty()) {
			response.setError(this.error.addError("Cannot find trades policies", ErrorLayer.SERVICE_LAYER, monitor));
		} else {
			response.setTradePolicies(result);
			for (TradePolicyItem trd : response.getTradePolicies()) {
				this.trade.completeTrade(trd, monitor);
			}
		}
		return response;
	}
}
