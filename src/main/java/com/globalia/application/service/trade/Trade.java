package com.globalia.application.service.trade;

import com.globalia.Errors;
import com.globalia.application.repository.trade.GetTrade;
import com.globalia.application.service.master.Master;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class Trade extends FindService<TradePolicyItem> {

	@Autowired
	private GetTrade getTrade;
	@Autowired
	private Master master;
	@Autowired
	private Errors error;

	public ItemResponse getItem(final TradePolicyItem item, final Monitor monitor) {
		ItemResponse response = new ItemResponse();

		Set<TradePolicyItem> result = this.getItem(item.getCredential());
		if (result == null) {
			response.setError(this.error.addError("Cannot find trade policy", ErrorLayer.SERVICE_LAYER, monitor));
			return response;
		}
		result.stream().filter(t -> item.getId().equals(t.getId())).findFirst().ifPresent(response::setTradePolicy);
		if (response.getTradePolicy() == null) {
			response.setError(this.error.addError("Cannot find trade policy", ErrorLayer.SERVICE_LAYER, monitor));
		}
		this.completeTrade(response, monitor);
		return response;
	}

	public Set<TradePolicyItem> getItem(final String credential) {
		return this.getTrade.getItem(credential);
	}

	public void completeTrade(final TradePolicyItem trade, final Monitor monitor) {
		if (trade.getCodes() != null) {
			trade.setMasters(new HashSet<>());
			this.master.convertCodeToMaster(trade.getMasters(), trade.getCodes(), monitor);
			trade.setCodes(null);
		}
	}

	private void completeTrade(final ItemResponse trade, final Monitor monitor) {
		if (trade.getError() == null) {
			this.completeTrade(trade.getTradePolicy(), monitor);
		}
	}
}
