package com.globalia.application.service.trade;

import com.globalia.application.repository.trade.SaveTradeRedis;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MassiveItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class SaveTrade extends SaveService<TradePolicyItem> {

	@Autowired
	private Trade trade;
	@Autowired
	private SaveTradeRedis save;

	public ItemResponse createItem(final TradePolicyItem item, final Monitor monitor) {
		return this.execute(item, monitor);
	}

	public ItemResponse updateItem(final TradePolicyItem item, final Monitor monitor) {
		return this.execute(item, monitor);
	}

	public ItemResponse deleteItem(final TradePolicyItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.execute(item, monitor);
	}

	public AllItemsResponse massive(final MassiveItem massive, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();
		if (massive.getTrades() != null) {
			for (TradePolicyItem trd : massive.getTrades()) {
				if (!StringUtils.hasText(trd.getCredential())) {
					continue;
				}

				ItemResponse item = this.massiveAction(trd, monitor);
				if (item.getError() != null) {
					response.setError(item.getError());
					return response;
				}
			}
		}
		response.setTradePolicies(massive.getTrades());
		return response;
	}

	private ItemResponse massiveAction(final TradePolicyItem trade, final Monitor monitor) {
		if (!StringUtils.hasText(trade.getId())) {
			return this.createItem(trade, monitor);
		}
		return this.updateItem(trade, monitor);
	}

	private ItemResponse execute(final TradePolicyItem item, final Monitor monitor) {
		ItemResponse response = this.save.addItem(item, monitor);
		if (response.getError() == null) {
			response = this.trade.getItem(item, monitor);
		}
		return response;
	}
}
