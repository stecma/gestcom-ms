package com.globalia.application.service.cancelcost;

import com.globalia.Errors;
import com.globalia.application.Status;
import com.globalia.application.repository.cancelcost.GetCancelCost;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;

@Service
public class CancelCosts extends FindAllService<CancelCostItem> {

	@Autowired
	private GetCancelCost getCancelCost;
	@Autowired
	private Status status;
	@Autowired
	private Errors errors;

	public AllItemsResponse getAllItems(final CancelCostItem item, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();

		Set<CancelCostItem> result = this.getCancelCost.getItem(item.getCredential());
		if (result.isEmpty()) {
			response.setError(this.errors.addError("Cannot find cancellation policies", ErrorLayer.SERVICE_LAYER, monitor));
		} else {
			response.setCancelCosts(result);
			Date current = new Date();
			for (CancelCostItem cancel : response.getCancelCosts()) {
				cancel.setStatus(this.status.getStatus(cancel.getAssignment(), current, monitor));
			}
		}
		return response;
	}
}
