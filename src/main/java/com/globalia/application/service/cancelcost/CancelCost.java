package com.globalia.application.service.cancelcost;

import com.globalia.Errors;
import com.globalia.application.Status;
import com.globalia.application.repository.cancelcost.GetCancelCost;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;

@Service
public class CancelCost extends FindService<CancelCostItem> {

	@Autowired
	private GetCancelCost getCancelCost;
	@Autowired
	private Status status;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final CancelCostItem item, final Monitor monitor) {
		ItemResponse response = new ItemResponse();

		Set<CancelCostItem> result = this.getItem(item.getCredential());
		if (result == null) {
			response.setError(this.errors.addError("Cannot find cancel cost", ErrorLayer.SERVICE_LAYER, monitor));
			return response;
		}

		result.stream().filter(t -> item.getId().equals(t.getId())).findFirst().ifPresent(response::setCancelCost);
		if (response.getCancelCost() == null) {
			response.setError(this.errors.addError("Cannot find cancel cost", ErrorLayer.SERVICE_LAYER, monitor));
		}
		this.getStatus(response, monitor);
		return response;
	}

	public Set<CancelCostItem> getItem(final String credential) {
		return this.getCancelCost.getItem(credential);
	}

	private void getStatus(final ItemResponse item, final Monitor monitor) {
		if (item.getError() == null) {
			item.getCancelCost().setStatus(this.status.getStatus(item.getCancelCost().getAssignment(), new Date(), monitor));
		}
	}
}
