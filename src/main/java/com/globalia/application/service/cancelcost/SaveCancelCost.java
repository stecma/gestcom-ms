package com.globalia.application.service.cancelcost;

import com.globalia.application.Status;
import com.globalia.application.repository.cancelcost.SaveCancelCostRedis;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SaveCancelCost extends SaveService<CancelCostItem> {

	@Autowired
	private SaveCancelCostRedis save;

	@Autowired
	private Status status;

	public ItemResponse createItem(final CancelCostItem item, final Monitor monitor) {
		return fillStatus(this.save.addItem(item, monitor),monitor);
	}

	public ItemResponse updateItem(final CancelCostItem item, final Monitor monitor) {
		return fillStatus(this.save.addItem(item, monitor),monitor);
	}

	public ItemResponse deleteItem(final CancelCostItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.save.addItem(item, monitor);
	}

	private ItemResponse fillStatus(final ItemResponse item, final Monitor monitor) {
		if (item.getError() == null) {
			item.getCancelCost().setStatus(this.status.getStatus(item.getCancelCost().getAssignment(), new Date(), monitor));
		}
		return item;
	}
}
