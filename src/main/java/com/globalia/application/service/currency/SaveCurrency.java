package com.globalia.application.service.currency;

import com.globalia.application.repository.currency.SaveCurrencyRedis;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveCurrency extends SaveService<CurrencyItem> {

	@Autowired
	private SaveCurrencyRedis save;

	public ItemResponse createItem(final CurrencyItem item, final Monitor monitor) {
		return this.save.addItem(item, monitor);
	}

	public ItemResponse updateItem(final CurrencyItem item, final Monitor monitor) {
		return this.save.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final CurrencyItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.save.addItem(item, monitor);
	}
}
