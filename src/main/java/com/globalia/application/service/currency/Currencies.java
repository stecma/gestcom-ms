package com.globalia.application.service.currency;

import com.globalia.application.Status;
import com.globalia.application.repository.currency.GetCurrencies;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class Currencies extends FindAllService<CurrencyItem> {

	@Autowired
	private GetCurrencies getCurrencies;
	@Autowired
	private Status status;

	public AllItemsResponse getAllItemsByCountry(final Monitor monitor) {
		return this.getCurrencies.getAllItems(monitor);
	}

	@Override
	public AllItemsResponse getAllItems(final CurrencyItem item, final Monitor monitor) {
		AllItemsResponse items = this.getCurrencies.getAllItems(item.getCredential(), monitor);
		if (items.getError() == null) {
			Date currentDate = new Date();
			for (CurrencyItem currency : items.getCurrencies()) {
				currency.setStatus(this.status.getStatus(currency.getAssignment(), currentDate, monitor));
			}
		}
		return items;
	}
}
