package com.globalia.application.service.customer;

import com.globalia.Errors;
import com.globalia.application.Status;
import com.globalia.application.repository.customer.GetCustomer;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class Customer extends FindService<CustomerItem> {

	@Autowired
	private GetCustomer getCustomer;
	@Autowired
	private Master master;
	@Autowired
	private Status status;
	@Autowired
	private Suppliers suppliers;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final CustomerItem item, final Monitor monitor) {
		ItemResponse response = new ItemResponse();

		Set<CustomerItem> result = this.getCustomer.getItem(item.getCredential());
		if (result != null) {
			for (CustomerItem custom : result) {
				if (!custom.getId().equals(item.getId())) {
					continue;
				}
				response.setCustomer(custom);
			}
		}

		if (response.getCustomer() == null) {
			response.setError(this.errors.addError("Cannot find customer", ErrorLayer.SERVICE_LAYER, monitor));
		}
		this.completeXsell(response, this.suppliers.getAllItems(monitor), monitor);
		this.completeStatus(response, new Date(), monitor);
		this.completeCustomer(response, monitor);
		return response;
	}

	public void completeXsell(final CustomerItem customer, final AllItemsResponse suppliers, final Monitor monitor) {
		boolean result = false;
		if (suppliers.getError() == null) {
			MasterItem agency = this.master.getMaster(MasterType.AGENCY.name(), customer.getCodes(), monitor);
			if (agency != null && suppliers.getSuppliers() != null) {
				result = suppliers.getSuppliers().stream().anyMatch(s -> this.existAgency(MasterType.AGENCY.name(), agency.getId(), s.getCodes()));
			}
		}
		customer.setXsell(result);
	}

	public void completeCustomer(final CustomerItem customer, final Monitor monitor) {
		if (customer.getCodes() != null) {
			customer.setMasters(new HashSet<>());
			this.master.convertCodeToMaster(customer.getMasters(), customer.getCodes(), monitor);
			customer.setCodes(null);
		}
	}

	public void completeStatus(final CustomerItem customer, final Date current, final Monitor monitor) {
		customer.setStatus(this.status.getStatus(customer.getAssignment(), current, monitor));
	}

	private void completeCustomer(final ItemResponse customer, final Monitor monitor) {
		if (customer.getError() == null) {
			this.completeCustomer(customer.getCustomer(), monitor);
		}
	}

	private void completeStatus(final ItemResponse customer, final Date current, final Monitor monitor) {
		if (customer.getError() == null) {
			this.completeStatus(customer.getCustomer(), current, monitor);
		}
	}

	private void completeXsell(final ItemResponse customer, final AllItemsResponse suppliers, final Monitor monitor) {
		if (customer.getError() == null) {
			this.completeXsell(customer.getCustomer(), suppliers, monitor);
		}
	}

	private boolean existAgency(final String entity, final String agency, final Set<PairValue> codes) {
		if (codes != null) {
			return codes.stream().anyMatch(c -> entity.equalsIgnoreCase(c.getKey()) && c.getValue().contains(agency));
		}
		return false;
	}
}
