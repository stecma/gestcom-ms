package com.globalia.application.service.customer;

import com.globalia.application.FilterUtils;
import com.globalia.application.repository.customer.SaveCustomerRedis;
import com.globalia.application.service.credential.Credentials;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CredentialFilter;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MassiveItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SaveCustomer extends SaveService<CustomerItem> {

	@Autowired
	private SaveCustomerRedis save;
	@Autowired
	private Customer customer;
	@Autowired
	private Credentials credentials;

	public ItemResponse createItem(final CustomerItem item, final Monitor monitor) {
		return this.execute(item, false, monitor);
	}

	public ItemResponse updateItem(final CustomerItem item, final Monitor monitor) {
		return this.execute(item, false, monitor);
	}

	public ItemResponse deleteItem(final CustomerItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.execute(item, false, monitor);
	}

	public AllItemsResponse massive(final MassiveItem massive, final Monitor monitor) {
		AllItemsResponse customers = new AllItemsResponse();
		if (massive.getCustomers() != null) {
			customers.setCustomers(new HashSet<>());
			for (CustomerItem custom : massive.getCustomers()) {
				if (StringUtils.hasText(custom.getId())) {
					customers.getCustomers().add(this.execute(custom, false, monitor).getCustomer());
				} else {
					this.execute(custom, true, monitor);
				}
			}
		}
		return customers;
	}

	private ItemResponse execute(final CustomerItem item, final boolean onlyAdd, final Monitor monitor) {
		ItemResponse response = this.save.addItem(item, monitor);
		if (response.getError() == null) {
			if (!response.getCustomer().isDeleted() && response.getCustomer().isPriority()) {
				this.updatePriority(response.getCustomer(), monitor);
			}
			return onlyAdd ? response : this.customer.getItem(item, monitor);
		}
		return response;
	}

	private void updatePriority(final CustomerItem custom, final Monitor monitor) {
		CredentialItem filter = new CredentialItem();
		filter.setFilter(new CredentialFilter());
		AllItemsResponse allItems = this.credentials.getAllItems(filter, monitor);
		if (allItems.getCredentials() != null) {
			String comparator = this.getComparator(custom);
			Set<CredentialItem> credentialSet = allItems.getCredentials().stream().filter(c -> !c.getId().equals(custom.getCredential()) && c.getClients() != null).collect(Collectors.toSet());
			for (CredentialItem cred : credentialSet) {
				Optional<CustomerItem> item = cred.getClients().stream().filter(c -> comparator.equals(this.getComparator(c)) && c.isPriority()).findFirst();
				if (item.isPresent()) {
					item.get().setPriority(false);
					this.execute(item.get(), true, monitor);
				}
			}
		}
	}

	private String getComparator(final CustomerItem custom) {
		String grp = FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.AGENCY_GROUP, custom.getCodes()));
		String age = FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.AGENCY, custom.getCodes()));
		String branch = FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.BRANCH_OFFICE, custom.getCodes()));
		String agent = FilterUtils.getInstance().getCode(FilterUtils.getInstance().getCodes(MasterType.AGENT, custom.getCodes()));

		StringBuilder dummy = new StringBuilder();
		if (StringUtils.hasText(grp)) {
			dummy.append(grp);
		}
		if (StringUtils.hasText(age)) {
			dummy.append("¬").append(age);
		}
		if (StringUtils.hasText(branch)) {
			dummy.append("¬").append(branch);
		}
		if (StringUtils.hasText(agent)) {
			dummy.append("¬").append(agent);
		}
		return dummy.toString();
	}
}
