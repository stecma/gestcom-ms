package com.globalia.application.service.customer;

import com.globalia.Errors;
import com.globalia.application.repository.customer.GetCustomer;
import com.globalia.application.service.supplier.Suppliers;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class Customers extends FindAllService<CustomerItem> {

	@Autowired
	private GetCustomer getCustomer;
	@Autowired
	private Customer customer;
	@Autowired
	private Errors errors;
	@Autowired
	private Suppliers getSuppliers;

	public AllItemsResponse getAllItems(final CustomerItem item, final Monitor monitor) {
		return this.getAllItems(item, false, monitor);
	}

	public AllItemsResponse getAllItems(final CustomerItem item, final boolean onlyStatus, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();
		Set<CustomerItem> result = this.getCustomer.getItem(item.getCredential());
		if (result == null || result.isEmpty()) {
			response.setError(this.errors.addError("Cannot find customers", ErrorLayer.SERVICE_LAYER, monitor));
		} else {
			Date current = new Date();
			AllItemsResponse suppliers = null;
			for (CustomerItem custom : result) {
				if (response.getCustomers() == null) {
					response.setCustomers(new HashSet<>());
				}

				if (!onlyStatus) {
					if (suppliers == null) {
						suppliers = this.getSuppliers.getAllItems(monitor);
					}
					this.customer.completeXsell(custom, suppliers, monitor);
					this.customer.completeCustomer(custom, monitor);
				}
				this.customer.completeStatus(custom, current, monitor);
				response.getCustomers().add(custom);
			}
		}
		return response;
	}
}
