package com.globalia.application;

import com.globalia.dto.PairValue;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.util.StringUtils;

import java.util.Optional;
import java.util.Set;

public class FilterUtils {

	private static FilterUtils instance;

	private FilterUtils() {
	}

	public static FilterUtils getInstance() {
		if (FilterUtils.instance == null) {
			FilterUtils.instance = new FilterUtils();
		}
		return FilterUtils.instance;
	}

	public boolean exists(final String filter, final Set<String> codes) {
		boolean result = false;
		if (!StringUtils.hasText(filter)) {
			result = true;
		} else {
			if (StringUtils.hasText(this.getValue(filter, codes))) {
				result = true;
			}
		}
		return result;
	}

	public void addFilterValues(final StringBuilder filterBuilder, final StringBuilder valuesBuilder, final String filter, final String value) {
		this.addFilterValues(filterBuilder, valuesBuilder, filter, value, false);
	}

	public void addFilterValues(final StringBuilder filterBuilder, final StringBuilder valuesBuilder, final String filter, final String value, final boolean forced) {
		if (forced || StringUtils.hasText(filter)) {
			filterBuilder.append('¬').append(filter);
			valuesBuilder.append('¬').append(value);
		}
	}

	public String getCode(final Set<String> codes) {
		String result = "";
		if (codes != null) {
			result = codes.iterator().next();
		}
		return result;
	}

	public Set<String> getCodes(final MasterType entity, final Set<PairValue> codes) {
		Set<String> codeSet = null;
		if (codes != null) {
			Optional<PairValue> pair = codes.stream().filter(p -> p.getKey().equals(entity.name())).findFirst();
			if (pair.isPresent()) {
				codeSet = pair.get().getValue();
			}
		}
		return codeSet;
	}

	private String getValue(final String id, final Set<String> codes) {
		String result = null;
		if (codes != null) {
			Optional<String> code = codes.stream().filter(id::equals).findFirst();
			if (code.isPresent()) {
				result = code.get();
			}
		}
		return result;
	}
}