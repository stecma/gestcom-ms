package com.globalia.application;

import com.globalia.Dates;
import com.globalia.application.service.match.Match;
import com.globalia.dto.Parameter;
import com.globalia.dto.credential.Assignment;
import com.globalia.monitoring.Monitor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RefreshScope
public abstract class AbstractMatch {

	@Value("${defaultDates.dateTo}")
	private String dateTo;
	@Value("${defaultDates.timeTo}")
	private String timeTo;

	@Getter
	@Autowired
	private Match matchProcess;
	@Autowired
	private Dates dates;

	protected boolean isValidDate(final Date currentDate, final Assignment assignment, final Monitor monitor) {
		boolean result;
		if (assignment == null || assignment.getCheckIn() == null || assignment.getBook() == null || (!StringUtils.hasText(assignment.getCheckIn().getTo()) && !StringUtils.hasText(assignment.getBook().getTo()))) {
			result = true;
		} else {
			Date checkInTo = this.dates.getDateTime(StringUtils.hasText(assignment.getCheckIn().getTo()) ? assignment.getCheckIn().getTo() : this.dateTo, this.timeTo, monitor);
			Date bookTo = this.dates.getDateTime(StringUtils.hasText(assignment.getBook().getTo()) ? assignment.getBook().getTo() : this.dateTo, StringUtils.hasText(assignment.getBook().getTimeTo()) ? assignment.getBook().getTimeTo() : this.timeTo, monitor);
			result = checkInTo.after(currentDate) || bookTo.after(currentDate);
		}
		return result;
	}

	protected Set<Parameter> convertMapToSet(final Map<String, String> map) {
		Set<Parameter> params = null;
		if (!map.isEmpty()) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				if (params == null) {
					params = new HashSet<>();
				}
				params.add(this.addParameter(entry.getKey(), entry.getValue()));
			}
		}
		return params;
	}

	Parameter addParameter(final String credential, final String level) {
		Parameter param = new Parameter();
		param.setKey(credential);
		param.setValue(level);
		return param;
	}

	public enum ASSIGN_TYPE {
		ASSIGN,
		UNASSIGN,
		OTHERS
	}
}
