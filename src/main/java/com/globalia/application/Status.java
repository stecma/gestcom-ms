package com.globalia.application;

import com.globalia.Dates;
import com.globalia.dto.credential.Assignment;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;

@Component
@RefreshScope
public class Status {

	@Value("${defaultDates.dateTo}")
	private String dateTo;
	@Value("${defaultDates.timeFrom}")
	private String timeFrom;
	@Value("${defaultDates.timeTo}")
	private String timeTo;

	@Autowired
	private Dates dates;

	public MatchLevel getStatus(final Assignment assign, final Date current, final Monitor monitor) {
		MatchLevel result;
		try {
			result = MatchLevel.RED;
			if (assign != null && assign.getBook() != null) {
				String auxTimeFrom = StringUtils.hasText(assign.getBook().getTimeFrom()) ? assign.getBook().getTimeFrom() : this.timeFrom;
				String auxTimeTo = StringUtils.hasText(assign.getBook().getTimeTo()) ? assign.getBook().getTimeTo() : this.timeTo;
				String auxDateTo = StringUtils.hasText(assign.getBook().getTo()) ? assign.getBook().getTo() : this.dateTo;

				result = MatchLevel.GREEN;
				Date startDate = this.dates.getDateTime(assign.getBook().getFrom(), auxTimeFrom, monitor);
				Date endDate = this.dates.getDateTime(auxDateTo, auxTimeTo, monitor);
				if (current.before(startDate)) {
					result = MatchLevel.YELLOW;
				}
				if (current.after(endDate)) {
					result = MatchLevel.RED;
				}
			}
		} catch (ValidateException p) {
			result = MatchLevel.RED;
		}
		return result;
	}
}
