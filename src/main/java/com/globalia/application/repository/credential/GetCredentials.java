package com.globalia.application.repository.credential;

import com.globalia.Errors;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GetCredentials extends FindAllRedis<CredentialItem> {

	@Value("${redis.credentialsKey}")
	private String credentialsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	@Override
	public AllItemsResponse getAllItems(final CredentialItem item, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final Monitor monitor) {
		return this.getAllCredentials(this.redisAccess.getRediskey(this.credentialsKey), monitor);
	}

	private AllItemsResponse getAllCredentials(final String redisKey, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addCredential((String) entry.getValue(), response);
			}

			if (response.getCredentials() == null) {
				response.setError(this.errors.addError("Cannot find suppliers", ErrorLayer.SERVICE_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return response;
	}

	private void addCredential(final String json, final AllItemsResponse credentials) {
		try {
			CredentialItem credential = (CredentialItem) getJsonHandler().fromJson(json, CredentialItem.class);
			if (!credential.isDeleted()) {
				if (credentials.getCredentials() == null) {
					credentials.setCredentials(new LinkedHashSet<>());
				}
				credentials.getCredentials().add(credential);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}