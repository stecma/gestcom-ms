package com.globalia.application.repository.credential;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.credential.CredentialCounterItem;
import com.globalia.dto.credential.ItemResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

@RefreshScope
@Repository
public class SaveCredentialIndexCountRedis extends AbstractSaveRedis<CredentialCounterItem> {

    @Value("${redis.credentialIndexCounterKey}")
    private String credentialIndexCounterKey;

    @Override
    protected String getItemAction(final CredentialCounterItem item) {
        String action = getAction(item.getCount().toString().equalsIgnoreCase("1") ? null : item.getCount().toString(), false);
        return action;
    }

    @Override
    protected void getResponse(CredentialCounterItem item, ItemResponse response, String json) {
        getClient().add(getRedisAccess().getRediskey(this.credentialIndexCounterKey), item.getName(), json);
        response.setCredentialIndexCounter(item);

    }

    @Override
    protected String getEntity(final CredentialCounterItem item) {
        return "credentialIndex";
    }

}