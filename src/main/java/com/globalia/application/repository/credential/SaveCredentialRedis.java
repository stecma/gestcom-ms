package com.globalia.application.repository.credential;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveCredentialRedis extends AbstractSaveRedis<CredentialItem> {

	@Value("${redis.credentialsKey}")
	private String credentialsKey;

	@Override
	protected String getItemAction(final CredentialItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(CredentialItem item, ItemResponse response, String json) {
		getClient().add(getRedisAccess().getRediskey(this.credentialsKey), item.getId(), json);
		response.setCredential(item);

	}

	@Override
	protected String getEntity(final CredentialItem item) {
		return "credential";
	}

	private void addInternalCodes(final CredentialItem item) {
		Set<PairValue> values = new HashSet<>();
		if (item.getBrand() != null) {
			values.add(addPairValue(MasterType.BRAND, new LinkedHashSet<>(Set.of(item.getBrand().getId())), false));
			item.setBrand(null);
		}
		if (item.getServiceType() != null) {
			values.add(addPairValue(MasterType.SERVICE_TYPE, new LinkedHashSet<>(Set.of(item.getServiceType().getId())), false));
			item.setServiceType(null);
		}
		if (item.getSaleChannel() != null) {
			values.add(addPairValue(MasterType.SALES_CHANNEL, new LinkedHashSet<>(Set.of(item.getSaleChannel().getId())), false));
			item.setSaleChannel(null);
		}
		if (item.getDistributionModel() != null) {
			values.add(addPairValue(MasterType.DISTRIBUTION_MODEL, new LinkedHashSet<>(Set.of(item.getDistributionModel().getId())), false));
			item.setDistributionModel(null);
		}
		if (item.getMarket() != null) {
			values.add(addPairValue(MasterType.COUNTRY, new LinkedHashSet<>(Set.of(item.getMarket().getId())), false));
			item.setMarket(null);
		}
		if (item.getDepth() != null) {
			values.add(addPairValue(MasterType.DEPTH, new LinkedHashSet<>(Set.of(item.getDepth().getId())), false));
			item.setDepth(null);
		}
		if (item.getSalesModel() != null) {
			values.add(addPairValue(MasterType.SALES_MODEL, new LinkedHashSet<>(Set.of(item.getSalesModel().getId())), false));
			item.setSalesModel(null);
		}
		if (item.getLanguage() != null) {
			values.add(addPairValue(MasterType.LANGUAGES, new LinkedHashSet<>(Set.of(item.getLanguage().getId())), false));
			item.setLanguage(null);
		}
		if (item.getEnvironment() != null) {
			values.add(addPairValue(MasterType.ENVIRONMENTS, new LinkedHashSet<>(Set.of(item.getEnvironment().getId())), false));
			item.setEnvironment(null);
		}
		if (item.getRequest() != null) {
			values.add(addPairValue(MasterType.REQUEST, new LinkedHashSet<>(Set.of(item.getRequest().getId())), false));
			item.setRequest(null);
		}
		if (item.getUser() != null) {
			values.add(addPairValue(MasterType.USERS, new LinkedHashSet<>(Set.of(item.getUser().getId())), false));
			item.setUser(null);
		}
		if (item.getCurrency() != null) {
			values.add(addPairValue(MasterType.CURRENCY, new LinkedHashSet<>(Set.of(item.getCurrency().getId())), false));
			item.setCurrency(null);
		}

		this.addInternalCodesList(values, MasterType.PLATFORM, item.getPlatforms(), item);
		this.addInternalCodesList(values, MasterType.RATE_TYPE, item.getRates(), item);
		this.addInternalCodesList(values, MasterType.PRODUCT_ORIGIN, item.getProducts(), item);
		item.setCodes(values);
	}

	private void addInternalCodesList(final Set<PairValue> values, final MasterType masterType, final Set<MasterItem> masters, final CredentialItem item) {
		if (masters != null) {
			LinkedHashSet<String> codes = new LinkedHashSet<>();
			for (MasterItem master : masters) {
				codes.add(master.getId());
			}
			values.add(addPairValue(masterType, codes, false));
			if (masterType == MasterType.PLATFORM) {
				item.setPlatforms(null);
			} else if (masterType == MasterType.RATE_TYPE) {
				item.setRates(null);
			} else {
				item.setProducts(null);
			}
		}
	}
}