package com.globalia.application.repository.credential;

import com.globalia.dto.credential.CredentialCounterItem;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MatchLevel;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Repository
@RefreshScope
public class GetCredentialCount extends FindRedis<CredentialCounterItem> {

    @Value("${redis.credentialAliasCounterKey}")
    private String credentialAliasCounterKey;

    @Value("${redis.credentialIndexCounterKey}")
    private String credentialIndexCounterKey;

    @Autowired
    private RedisAccess redisAccess;

    @Autowired
    private SaveCredentialIndexCountRedis saveIndex;

    @Autowired
    private SaveCredentialAliasCountRedis saveAlias;


    @Override
    public ItemResponse getItem(CredentialCounterItem item, Monitor monitor) {
        return null;
    }

    public CredentialCounterItem getCredentialIndexCount(CredentialItem credentialItem) throws IOException {
        CredentialCounterItem item = null;
        if (!StringUtils.hasText(credentialItem.getSystemIdInDB()) && credentialItem.getStatus().equalsIgnoreCase(MatchLevel.GREEN.name())) {
            item = getItemCounter(this.redisAccess.getRediskey(this.credentialIndexCounterKey), credentialItem.getSystemId());
            credentialItem.setSystemId(item.getName().concat(indexToStrFormat(item.getCount())));
        }else if(!StringUtils.hasText(credentialItem.getSystemIdInDB())){
            credentialItem.setSystemId(null);
        }
        return item;
    }

    public CredentialCounterItem getCredentialAliasCount(CredentialItem credentialItem) throws IOException {

        CredentialCounterItem item = null;
        if (!StringUtils.hasText(credentialItem.getAliasInDB()) ||
                !credentialItem.getAliasInDB().equalsIgnoreCase(credentialItem.getAlias())) {
            item = getItemCounter(this.redisAccess.getRediskey(this.credentialAliasCounterKey), credentialItem.getAlias());
            credentialItem.setAlias(item.getName().concat(indexToStrFormat(item.getCount())));
        }
        return item;
    }

    public void persistIndex(CredentialCounterItem item, ItemResponse response, Monitor monitor) {
        this.saveIndex.getResponse(item, response, item.getCount().toString());
        this.saveIndex.addItem(item, monitor);
    }

    public void persistAlias(CredentialCounterItem item, ItemResponse response, Monitor monitor) {
        this.saveAlias.getResponse(item, response, item.getCount().toString());
        this.saveAlias.addItem(item, monitor);
    }

    private CredentialCounterItem getItemCounter(String redisKey, String key) throws IOException {
        CredentialCounterItem item = (CredentialCounterItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), CredentialCounterItem.class);
        if (item == null) {
            item = new CredentialCounterItem();
            item.setCount(0);
            item.setName(key);
        }
        item.setCount(item.getCount() + 1);
        return item;
    }

    private String indexToStrFormat(Integer count) {
        return count > 9 ? "_".concat(count.toString()) : "0".concat(count.toString());
    }
}
