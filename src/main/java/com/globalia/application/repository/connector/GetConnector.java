package com.globalia.application.repository.connector;

import com.globalia.Errors;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class GetConnector extends FindRedis<ExternalCredentialItem> {

	@Value("${redis.connectorsKey}")
	private String connectorsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final ExternalCredentialItem item, final Monitor monitor) {
		return this.getExternal(this.redisAccess.getRediskey(this.connectorsKey), String.format("%s¬%s¬%s", item.getId(), item.getSupplierId(), item.getGroupId()), monitor);
	}

	private ItemResponse getExternal(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		try {
			response.setExternal((ExternalCredentialItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), ExternalCredentialItem.class));
			if (response.getExternal() == null || response.getExternal().isDeleted()) {
				response.setError(this.errors.addError(String.format("Cannot find connector with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return response;
	}
}