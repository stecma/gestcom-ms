package com.globalia.application.repository.connector;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveConnectorRedis extends AbstractSaveRedis<ExternalCredentialItem> {

	private static final String KEY_FORMAT = "%s¬%s¬%s";

	@Value("${redis.connectorsKey}")
	private String connectorsKey;

	@Override
	protected String getItemAction(final ExternalCredentialItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final ExternalCredentialItem item, final ItemResponse response, final String json) {
		getClient().add(getRedisAccess().getRediskey(this.connectorsKey), String.format(SaveConnectorRedis.KEY_FORMAT, item.getId(), item.getSupplierId(), item.getGroupId()), json);
		response.setExternal(item);
	}

	@Override
	protected String getEntity(final ExternalCredentialItem item) {
		return "connector";
	}

	private void addInternalCodes(final ExternalCredentialItem item) {
		Set<PairValue> values = new HashSet<>();
		if (item.getCorporation() != null) {
			values.add(addPairValue(MasterType.CORPORATION, new LinkedHashSet<>(Set.of(item.getCorporation().getId())), false));
			item.setCorporation(null);
		}
		if (item.getInvoicingType() != null) {
			values.add(addPairValue(MasterType.BILLING_TYPE, new LinkedHashSet<>(Set.of(item.getInvoicingType().getId())), false));
			item.setInvoicingType(null);
		}
		item.setCodes(values);
	}
}