package com.globalia.application.repository.connector;

import com.globalia.Errors;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GetConnectors extends FindAllRedis<ExternalCredentialItem> {

	@Value("${redis.connectorsKey}")
	private String connectorsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	@Override
	public AllItemsResponse getAllItems(final ExternalCredentialItem item, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final ExternalCredentialItem item, final boolean allRedis, final Monitor monitor) {
		return this.getAllExternals(this.redisAccess.getRediskey(this.connectorsKey), item, allRedis, monitor);
	}

	private AllItemsResponse getAllExternals(final String redisKey, final ExternalCredentialItem item, final boolean allRedis, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			String key = item.getSupplierId() + "¬" + item.getGroupId();
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (key.length() == 1 || entry.getKey().contains(key)) {
					this.addExternal((String) entry.getValue(), response, allRedis);
				}
			}

			if (response.getExternals() == null) {
				response.setError(this.errors.addError(String.format("Cannot find connectors for %s", item.getSupplierId() + "¬" + item.getGroupId()), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return response;
	}

	private void addExternal(final String json, final AllItemsResponse externals, final boolean allRedis) {
		try {
			ExternalCredentialItem external = (ExternalCredentialItem) getJsonHandler().fromJson(json, ExternalCredentialItem.class);
			if (allRedis || !external.isDeleted()) {
				if (externals.getExternals() == null) {
					externals.setExternals(new LinkedHashSet<>());
				}
				externals.getExternals().add(external);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}