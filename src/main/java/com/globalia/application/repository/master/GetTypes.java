package com.globalia.application.repository.master;

import com.globalia.enumeration.credential.MasterType;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

@Repository
@RefreshScope
public class GetTypes {

	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private RedisClient client;

	/**
	 * Get permission by entity
	 *
	 * @param entity Entity.
	 * @return Permissions
	 */
	public String getType(final MasterType entity) {
		return (String) this.client.get(this.redisAccess.getRediskey(String.format("%s:permission", this.mastersKey)), entity.name());
	}
}