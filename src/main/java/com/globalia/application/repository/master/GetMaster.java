package com.globalia.application.repository.master;

import com.globalia.Errors;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class GetMaster extends FindRedis<MasterItem> {

	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final MasterItem item, final Monitor monitor) {
		return this.getMaster(String.format(RedisAccess.REDIS_KEY_FORMAT, this.redisAccess.getRediskey(this.mastersKey), item.getEntity().toUpperCase()), item.getId(), monitor);
	}

	public ItemResponse getMaster(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse master = new ItemResponse();
		try {
			master.setMaster((MasterItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), MasterItem.class));
			if (master.getMaster() == null || master.getMaster().isDeleted()) {
				master.setError(this.errors.addError(String.format("Cannot find master with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			master.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			master.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return master;
	}
}