package com.globalia.application.repository.master;

import com.globalia.dto.PairValue;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@Repository
@RefreshScope
public class GetChildCodes {

	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private RedisClient client;

	public Set<PairValue> getChildCodes(final String key, final String masterId) {
		Set<PairValue> childCodes = new HashSet<>();
		String redisKey = String.format("%s:%s:*", this.redisAccess.getRediskey(this.mastersKey), key);
		Set<String> codes = this.client.patternKeys(redisKey);
		if (!codes.isEmpty()) {
			for (String code : codes) {
				if (!code.contains(masterId)) {
					continue;
				}

				Set<String> values = this.client.members(code);
				if (!values.isEmpty()) {
					String subKey = code.replace(String.format(":%s", masterId), "");
					PairValue pair = new PairValue();
					pair.setKey(subKey.substring(subKey.lastIndexOf(':') + 1));
					pair.setValue(new LinkedHashSet<>(values));
					childCodes.add(pair);
				}
			}
		}
		return childCodes;
	}
}