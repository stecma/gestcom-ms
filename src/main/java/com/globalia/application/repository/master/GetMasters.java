package com.globalia.application.repository.master;

import com.globalia.Errors;
import com.globalia.dto.Item;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
@RefreshScope
public class GetMasters extends FindAllRedis<MasterItem> {

	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Autowired
	private GetMaster master;
	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public AllItemsResponse getAllItems(final MasterItem item, final Monitor monitor) {
		return this.getAllMasters(String.format(RedisAccess.REDIS_KEY_FORMAT, this.redisAccess.getRediskey(this.mastersKey), item.getEntity().toUpperCase()), MasterType.valueOf(item.getEntity().toUpperCase()) == MasterType.EXTERNAL_SYSTEM, monitor);
	}

	public AllItemsResponse getAllScan(final String entity, final String text, final String refser, final Monitor monitor) {
		AllItemsResponse masters = new AllItemsResponse();
		try {
			Set<String> codes = getClient().scanKeys(String.format(RedisAccess.REDIS_KEY_FORMAT, this.redisAccess.getRediskey(String.format("%s:scan", this.mastersKey)), entity), text.toUpperCase());
			if (codes != null) {
				for (String code : codes) {
					this.addScanMaster(entity, masters, refser, code, monitor);
				}
			}

			if (masters.getMasters() == null) {
				masters.setError(this.errors.addError("Cannot find masters", ErrorLayer.REPOSITORY_LAYER, monitor));
			} else {
				masters.setMasters(new LinkedHashSet<>(this.resultSorted(false, masters.getMasters())));
			}
		} catch (IOException r) {
			masters.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return masters;
	}

	public Set<String> findCodes(final String entity) {
		Set<String> codes = null;
		try {
			codes = getClient().keys(String.format(RedisAccess.REDIS_KEY_FORMAT, this.redisAccess.getRediskey(this.mastersKey), entity.toUpperCase()));
		} catch (RuntimeException ignored) {
			//
		}
		return codes;
	}

	private AllItemsResponse getAllMasters(final String redisKey, final boolean byId, final Monitor monitor) {
		AllItemsResponse masters = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addMaster((String) entry.getValue(), masters);
			}


			if (masters.getMasters() == null) {
				masters.setError(this.errors.addError("Cannot find masters", ErrorLayer.REPOSITORY_LAYER, monitor));
			} else {
				masters.setMasters(new LinkedHashSet<>(this.resultSorted(byId, masters.getMasters())));
			}
		} catch (RuntimeException r) {
			masters.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return masters;
	}

	private void addMaster(final String json, final AllItemsResponse masters) {
		try {
			MasterItem item = (MasterItem) getJsonHandler().fromJson(json, MasterItem.class);
			if (!item.isDeleted()) {
				if (masters.getMasters() == null) {
					masters.setMasters(new LinkedHashSet<>());
				}
				masters.getMasters().add(item);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}

	private List<MasterItem> resultSorted(final boolean byId, final Set<MasterItem> masters) {
		List<MasterItem> list = new ArrayList<>(masters);
		Comparator<MasterItem> comparator;
		if (byId) {
			comparator = Comparator.comparing(Item::getId);
		} else {
			comparator = Comparator.comparing((MasterItem m) -> m.getNames().iterator().next().getValue().substring(m.getNames().iterator().next().getValue().lastIndexOf(']') + 1));
		}
		list.sort(comparator);
		return list;
	}

	private void addScanMaster(final String entity, final AllItemsResponse masters, final String refser, final String code, final Monitor monitor) {
		if (!StringUtils.hasText(refser) || code.contains(refser)) {
			ItemResponse item = this.master.getMaster(String.format(RedisAccess.REDIS_KEY_FORMAT, this.redisAccess.getRediskey(this.mastersKey), entity), code, monitor);
			if (item.getError() == null) {
				if (masters.getMasters() == null) {
					masters.setMasters(new LinkedHashSet<>());
				}
				masters.getMasters().add(item.getMaster());
			}
		}
	}
}