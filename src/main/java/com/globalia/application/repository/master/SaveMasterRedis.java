package com.globalia.application.repository.master;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;

@RefreshScope
@Repository
public class SaveMasterRedis extends AbstractSaveRedis<MasterItem> {

	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Override
	protected String getItemAction(final MasterItem item) {
		if (MasterType.valueOf(item.getEntity().toUpperCase()) != MasterType.SERVICE_DISTRIBUTION) {
			this.addInternalCodes(item);
		}
		if (item.isCreated()) {
			return AbstractSaveRedis.CRE_ACTION;
		}
		if (item.isDeleted()) {
			return AbstractSaveRedis.DEL_ACTION;
		}
		return AbstractSaveRedis.UPD_ACTION;
	}

	@Override
	protected void getResponse(final MasterItem item, final ItemResponse response, final String json) {
		getClient().add(String.format(RedisAccess.REDIS_KEY_FORMAT, getRedisAccess().getRediskey(this.mastersKey), item.getEntity().toUpperCase()), item.getId(), json);
		response.setMaster(item);
	}

	@Override
	protected String getEntity(final MasterItem item) {
		return "master";
	}

	private void addInternalCodes(final MasterItem item) {
		if (item.getMasters() != null) {
			Map<String, PairValue> map = new HashMap<>();
			for (MasterItem master : item.getMasters()) {
				map.computeIfAbsent(master.getEntity(), k -> addPairValue(MasterType.valueOf(master.getEntity().toUpperCase()), new LinkedHashSet<>(), false));
				map.get(master.getEntity()).getValue().add(master.getId());
			}

			if (!map.isEmpty()) {
				item.setCodes(new HashSet<>(map.values()));
				item.setMasters(null);
			}
		}
	}
}