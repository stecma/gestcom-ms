package com.globalia.application.repository.supplier;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@RefreshScope
@Repository
public class SaveSupplierByZoneRedis extends AbstractSaveRedis<SupplierByZoneItem> {

	@Value("${redis.byzonesKey}")
	private String byzonesKey;

	@Override
	protected String getItemAction(final SupplierByZoneItem item) {
		if (!StringUtils.hasText(item.getId())) {
			item.setId(String.format("%s¬%s¬%s¬%s¬%s¬%s¬%s¬%s¬ID", item.getCodsis(), item.getCodpro(), (item.getRefzge() == null) ? "" : item.getRefzge(), (item.getCodpai() == null) ? "" : item.getCodpai(), (item.getCodepr() == null) ? "" : item.getCodepr(), (item.getCodare() == null) ? "" : item.getCodare(), (item.getSubprv() == null) ? "" : item.getSubprv(), (item.getCodprvext() == null) ? "" : item.getCodprvext()));
		}

		if (item.isCreated()) {
			return AbstractSaveRedis.CRE_ACTION;
		}
		if (item.isDeleted()) {
			return AbstractSaveRedis.DEL_ACTION;
		}
		return AbstractSaveRedis.UPD_ACTION;
	}

	@Override
	protected void getResponse(final SupplierByZoneItem item, final ItemResponse response, final String json) {
		if (item.isDeleted()) {
			getClient().delete(getRedisAccess().getRediskey(this.byzonesKey), item.getId());
		} else {
			getClient().add(getRedisAccess().getRediskey(this.byzonesKey), item.getId(), json);
		}
		response.setSupplierByZone(item);
	}

	@Override
	protected String getEntity(SupplierByZoneItem item) {
		return "supplierbyzone";
	}
}