package com.globalia.application.repository.supplier;

import com.globalia.Errors;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.KeyMap;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

@Repository
@RefreshScope
public class GetSupplier extends FindRedis<SupplierItem> {

	@Value("${redis.suppliersKey}")
	private String suppliersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors error;

	public ItemResponse getItem(final SupplierItem item, final Monitor monitor) {
		return this.getSupplier(this.redisAccess.getRediskey(this.suppliersKey), item.getId(), monitor);
	}

	public ItemResponse getMapKey(final String entity, final Monitor monitor) {
		return this.getAllMapKeys(this.redisAccess.getRediskey(String.format("%s:mapKey", this.suppliersKey)), entity, monitor);
	}

	private ItemResponse getSupplier(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse supplier = new ItemResponse();
		try {
			supplier.setSupplier((SupplierItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), SupplierItem.class));
			if (supplier.getSupplier() == null || supplier.getSupplier().isDeleted()) {
				supplier.setError(this.error.addError(HttpStatus.NO_CONTENT, String.format("Cannot find supplier with id: %s", key), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
			}
		} catch (RuntimeException r) {
			supplier.setError(this.error.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			supplier.setError(this.error.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return supplier;
	}

	private ItemResponse getAllMapKeys(final String redisKey, final String entity, final Monitor monitor) {
		ItemResponse supplier = new ItemResponse();
		try {
			List<KeyMap> list = new ArrayList<>();
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().startsWith(entity)) {
					this.addKeyMap((String) entry.getValue(), list);
				}
			}

			if (!list.isEmpty()) {
				list.sort(Comparator.comparing(KeyMap::getOrder));
				supplier.setSupplier(new SupplierItem());
				supplier.getSupplier().setKeyMap(new LinkedHashSet<>(list));
			} else {
				supplier.setError(this.error.addError(HttpStatus.NO_CONTENT, "Cannot find keyMap", monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
			}
		} catch (RuntimeException r) {
			supplier.setError(this.error.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return supplier;
	}

	private void addKeyMap(final String json, final List<KeyMap> list) {
		try {
			list.add((KeyMap) getJsonHandler().fromJson(json, KeyMap.class));
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}