package com.globalia.application.repository.supplier;

import com.globalia.Errors;
import com.globalia.application.service.supplier.Supplier;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.SupplierStatus;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GetSuppliers extends FindAllRedis<SupplierItem> {

	@Value("${redis.suppliersKey}")
	private String suppliersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors error;
	@Autowired
	private Supplier supplier;

	@Override
	public AllItemsResponse getAllItems(final SupplierItem item, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final Monitor monitor) {
		return this.getAllSuppliers(this.redisAccess.getRediskey(this.suppliersKey), monitor);
	}

	private AllItemsResponse getAllSuppliers(final String redisKey, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addSupplier((String) entry.getValue(), response,monitor);
			}

			if (response.getSuppliers() == null) {
				response.setError(this.error.addError(HttpStatus.NO_CONTENT, "Cannot find suppliers", monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
			}
		} catch (RuntimeException r) {
			response.setError(this.error.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return response;
	}

	private void addSupplier(final String json, final AllItemsResponse suppliers,final Monitor monitor) {
		try {
			SupplierItem supplierItem = (SupplierItem) getJsonHandler().fromJson(json, SupplierItem.class);
			if (!supplierItem.isDeleted()) {
				if (suppliers.getSuppliers() == null) {
					suppliers.setSuppliers(new LinkedHashSet<>());
				}
				if (supplierItem.getStatus() == null) {
					supplierItem.setStatus(SupplierStatus.DISABLED);
				}
				supplierItem.setExternalSys(supplier.completeGroups(supplierItem, true, true, monitor));
				suppliers.getSuppliers().add(supplierItem);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}

}