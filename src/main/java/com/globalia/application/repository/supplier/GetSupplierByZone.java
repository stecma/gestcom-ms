package com.globalia.application.repository.supplier;

import com.globalia.Errors;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class GetSupplierByZone extends FindRedis<SupplierByZoneItem> {

	@Value("${redis.byzonesKey}")
	private String byzonesKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors error;

	public ItemResponse getItem(final SupplierByZoneItem item, final Monitor monitor) {
		return this.getSupplier(this.redisAccess.getRediskey(this.byzonesKey), item.getId(), monitor);
	}

	private ItemResponse getSupplier(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		try {
			response.setSupplierByZone((SupplierByZoneItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), SupplierByZoneItem.class));
			if (response.getSupplierByZone() == null) {
				response.setError(this.error.addError(String.format("Cannot find supplier by zone with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.error.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			response.setError(this.error.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return response;
	}
}