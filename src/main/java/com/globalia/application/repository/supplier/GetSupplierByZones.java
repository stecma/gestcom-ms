package com.globalia.application.repository.supplier;

import com.globalia.Errors;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GetSupplierByZones extends FindAllRedis<SupplierByZoneItem> {

	@Value("${redis.byzonesKey}")
	private String byzonesKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors error;

	@Override
	public AllItemsResponse getAllItems(final SupplierByZoneItem supplierByZoneItem, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final Monitor monitor) {
		return this.getAllExternals(this.redisAccess.getRediskey(this.byzonesKey), monitor);
	}

	private AllItemsResponse getAllExternals(final String redisKey, final Monitor monitor) {
		AllItemsResponse suppliersByZone = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addSupplierByZone((String) entry.getValue(), suppliersByZone);
			}

			if (suppliersByZone.getSuppliersByZone() == null) {
				suppliersByZone.setError(this.error.addError("Cannot find suppliers by zone", ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			suppliersByZone.setError(this.error.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return suppliersByZone;
	}

	private void addSupplierByZone(final String json, final AllItemsResponse suppliers) {
		try {
			SupplierByZoneItem supplier = (SupplierByZoneItem) getJsonHandler().fromJson(json, SupplierByZoneItem.class);
			if (suppliers.getSuppliersByZone() == null) {
				suppliers.setSuppliersByZone(new HashSet<>());
			}
			suppliers.getSuppliersByZone().add(supplier);
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}