package com.globalia.application.repository.supplier;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveSupplierRedis extends AbstractSaveRedis<SupplierItem> {

	@Value("${redis.suppliersKey}")
	private String suppliersKey;

	@Override
	protected String getItemAction(final SupplierItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final SupplierItem item, final ItemResponse response, final String json) {
		getClient().add(getRedisAccess().getRediskey(this.suppliersKey), item.getId(), json);
		response.setSupplier(item);

	}

	@Override
	protected String getEntity(final SupplierItem item) {
		return "supplier";
	}

	public ItemResponse addSapItem(final String system, final String sap, final boolean isNew, final Monitor monitor) {
		return this.update("addsapitem", String.format("%s#%s#%s", system, sap, isNew), monitor);
	}

	public ItemResponse addServiceItem(final String system, final String refser, final String tipser, final Monitor monitor) {
		return this.update("addserviceitem", String.format("%s#%s#%s", system, refser, tipser), monitor);
	}

	private ItemResponse update(final String entity, final String value, final Monitor monitor) {
		sendMessage(entity, UPD_ACTION, value, monitor);
		return new ItemResponse();
	}

	private void addInternalCodes(final SupplierItem item) {
		Set<PairValue> values = new HashSet<>();
		if (item.getServiceType() != null) {
			values.add(this.addPairValue(MasterType.SERVICE_TYPE, new LinkedHashSet<>(Set.of(item.getServiceType().getId())), false));
			item.setServiceType(null);
		}
		if (item.getSystem() != null) {
			values.add(this.addPairValue(MasterType.EXTERNAL_SYSTEM, new LinkedHashSet<>(Set.of(item.getSystem().getId())), false));
			item.setSystem(null);
		}
		if (item.getBuyModel() != null) {
			values.add(this.addPairValue(MasterType.BUY_MODEL, new LinkedHashSet<>(Set.of(item.getBuyModel().getId())), false));
			item.setBuyModel(null);
		}
		if (item.getUser() != null) {
			values.add(this.addPairValue(MasterType.USERS, new LinkedHashSet<>(Set.of(item.getUser().getId())), false));
			item.setUser(null);
		}
		if (item.getClientPref() != null) {
			values.add(this.addPairValue(MasterType.CLIENT_PREF, new LinkedHashSet<>(Set.of(item.getClientPref().getId())), false));
			item.setClientPref(null);
		}
		this.addInternalCodesList(values, MasterType.AGENCY, item.getClients(), item);
		this.addInternalCodesList(values, MasterType.TOP_DESTINATION, item.getTopDestinations(), item);
		item.setCodes(values);
	}

	private void addInternalCodesList(final Set<PairValue> values, final MasterType masterType, final Set<MasterItem> masters, final SupplierItem item) {
		if (masters != null && !masters.isEmpty()) {
			LinkedHashSet<String> codes = new LinkedHashSet<>();
			for (MasterItem master : masters) {
				codes.add(master.getId());
			}
			values.add(addPairValue(masterType, codes, false));
			if (masterType == MasterType.AGENCY) {
				item.setClients(null);
			} else {
				item.setTopDestinations(null);
			}
		}
	}
}