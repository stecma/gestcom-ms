package com.globalia.application.repository.currency;

import com.globalia.Errors;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GetCurrencies extends FindAllRedis<CurrencyItem> {

	@Value("${redis.currencyByCountryKey}")
	private String currencyByCountryKey;
	@Value("${redis.currencyKey}")
	private String currencyKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	@Override
	public AllItemsResponse getAllItems(final CurrencyItem item, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final Monitor monitor) {
		return this.getAllCurrencies(this.redisAccess.getRediskey(this.currencyByCountryKey), monitor);
	}

	public AllItemsResponse getAllItems(final String credential, final Monitor monitor) {
		return this.getAllCurrencies(this.redisAccess.getRediskey(String.format(RedisAccess.REDIS_KEY_FORMAT, this.currencyKey, credential)), monitor);
	}

	private AllItemsResponse getAllCurrencies(final String redisKey, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addCurrency((String) entry.getValue(), response);
			}

			if (response.getCurrencies() == null) {
				response.setError(this.errors.addError("Cannot find currencies", ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return response;
	}

	private void addCurrency(final String json, final AllItemsResponse currencies) {
		try {
			CurrencyItem currency = (CurrencyItem) getJsonHandler().fromJson(json, CurrencyItem.class);
			if (!currency.isDeleted()) {
				if (currencies.getCurrencies() == null) {
					currencies.setCurrencies(new HashSet<>());
				}
				currencies.getCurrencies().add(currency);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}