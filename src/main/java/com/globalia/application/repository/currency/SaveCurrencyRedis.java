package com.globalia.application.repository.currency;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveCurrencyRedis extends AbstractSaveRedis<CurrencyItem> {

	@Value("${redis.currencyByCountryKey}")
	private String currencyByCountryKey;
	@Value("${redis.currencyKey}")
	private String currencyKey;

	@Override
	protected String getItemAction(final CurrencyItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final CurrencyItem item, final ItemResponse response, final String json) {
		String redisKey;
		if (StringUtils.hasText(item.getCredential())) {
			redisKey = String.format(RedisAccess.REDIS_KEY_FORMAT, getRedisAccess().getRediskey(this.currencyKey), item.getCredential());
		} else {
			redisKey = getRedisAccess().getRediskey(this.currencyByCountryKey);
		}
		getClient().add(redisKey, item.getId(), json);
		response.setCurrency(item);
	}

	@Override
	protected String getEntity(final CurrencyItem item) {
		if (StringUtils.hasText(item.getCredential())) {
			return "currency";
		}
		return "currencymaster";
	}

	private void addInternalCodes(final CurrencyItem item) {
		if (item.getMasters() != null) {
			for (MasterItem master : item.getMasters()) {
				if (item.getCodes() == null) {
					item.setCodes(new HashSet<>());
				}
				item.getCodes().add(addPairValue(MasterType.valueOf(master.getEntity().toUpperCase()), new LinkedHashSet<>(Set.of(master.getId())), false));
			}
		}
		item.setMasters(null);
	}
}