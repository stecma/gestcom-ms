package com.globalia.application.repository.group;

import com.globalia.Errors;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GetGroups extends FindAllRedis<GroupCredentialItem> {

	@Value("${redis.groupsKey}")
	private String groupsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	@Override
	public AllItemsResponse getAllItems(final GroupCredentialItem groupCredentialItem, final Monitor monitor) {
		return null;
	}

	public AllItemsResponse getAllItems(final GroupCredentialItem item, final boolean allRedis, final Monitor monitor) {
		return this.getAllGroups(this.redisAccess.getRediskey(this.groupsKey), item, allRedis, monitor);
	}

	private AllItemsResponse getAllGroups(final String redisKey, final GroupCredentialItem item, final boolean allRedis, final Monitor monitor) {
		AllItemsResponse response = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				if (!StringUtils.hasText(item.getSupplierId()) || entry.getKey().contains(item.getSupplierId())) {
					this.addGroup((String) entry.getValue(), response, allRedis);
				}
			}

			if (response.getGroups() == null) {
				response.setError(this.errors.addError(String.format("Cannot find groups for supplier %s", item.getSupplierId()), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return response;
	}

	private void addGroup(final String json, final AllItemsResponse groups, final boolean allRedis) {
		try {
			GroupCredentialItem group = (GroupCredentialItem) getJsonHandler().fromJson(json, GroupCredentialItem.class);
			if (allRedis || !group.isDeleted()) {
				if (groups.getGroups() == null) {
					groups.setGroups(new LinkedHashSet<>());
				}
				groups.getGroups().add(group);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}