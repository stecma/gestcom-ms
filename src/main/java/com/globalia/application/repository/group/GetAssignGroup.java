package com.globalia.application.repository.group;

import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Repository
@RefreshScope
public class GetAssignGroup extends FindRedis<GroupCredentialItem> {

	@Value("${redis.credGroupsKey}")
	private String credGroupsKey;

	@Autowired
	private RedisAccess redisAccess;

	@Override
	public ItemResponse getItem(final GroupCredentialItem item, final Monitor monitor) {
		return null;
	}

	public Set<GroupCredentialItem> getItem(final String credential) {
		return this.getGroup(this.redisAccess.getRediskey(String.format(RedisAccess.REDIS_KEY_FORMAT, this.credGroupsKey, credential)));
	}

	public Set<GroupCredentialItem> getGroup(final String redisKey) {
		Set<GroupCredentialItem> values = new HashSet<>();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addGroup((String) entry.getValue(), values);
			}
		} catch (RuntimeException r) {
			//
		}
		return values;
	}

	private void addGroup(final String json, final Set<GroupCredentialItem> values) {
		try {
			GroupCredentialItem group = (GroupCredentialItem) getJsonHandler().fromJson(json, GroupCredentialItem.class);
			if (group != null) {
				values.add(group);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}