package com.globalia.application.repository.group;

import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Repository
@RefreshScope
public class GetAssignGroups extends FindAllRedis<GroupCredentialItem> {

	@Value("${redis.credGroupsKey}")
	private String credGroupsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private GetAssignGroup group;

	@Override
	public AllItemsResponse getAllItems(final GroupCredentialItem item, final Monitor monitor) {
		return null;
	}

	public Map<String, Set<GroupCredentialItem>> getAllItems() {
		return this.getAllGroups();
	}

	private Map<String, Set<GroupCredentialItem>> getAllGroups() {
		Map<String, Set<GroupCredentialItem>> groups = new HashMap<>();
		try {
			Set<String> codes = getClient().patternKeys(this.redisAccess.getRediskey(this.credGroupsKey));
			if (!codes.isEmpty()) {
				for (String code : codes) {
					String key = code.substring(code.lastIndexOf(':') + 1);
					groups.put(key, this.group.getGroup(code));
				}
			}
		} catch (RuntimeException r) {
			//
		}
		return groups;
	}
}