package com.globalia.application.repository.group;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.Errors;
import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveGroupRedis extends AbstractSaveRedis<GroupCredentialItem> {

	@Value("${redis.groupsKey}")
	private String groupsKey;
	@Value("${redis.credGroupsKey}")
	private String credGroupsKey;

	@Override
	protected String getItemAction(final GroupCredentialItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final GroupCredentialItem item, final ItemResponse response, final String json) {
		getClient().add(getRedisAccess().getRediskey(this.groupsKey), String.format(RedisAccess.REDIS_KEYHASH_FORMAT, item.getId(), item.getSupplierId()), json);
		response.setGroup(item);
	}

	@Override
	protected String getEntity(final GroupCredentialItem item) {
		return "group";
	}

	public ItemResponse addItemAssign(final String credential, final GroupCredentialItem item, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		try {
			String json = getJsonHandler().toJson(item);
			getClient().add(getRedisAccess().getRediskey(String.format(RedisAccess.REDIS_KEY_FORMAT, this.credGroupsKey, credential)), String.format(RedisAccess.REDIS_KEYHASH_FORMAT, item.getId(), item.getSupplierId()), json);
			response.setGroup(item);
			sendMessage("assign", credential, json, monitor);
		} catch (RuntimeException r) {
			response.setError(getErrors().addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (JsonProcessingException e) {
			response.setError(getErrors().addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return response;
	}

	private void addInternalCodes(final GroupCredentialItem item) {
		Set<PairValue> values = new HashSet<>();
		this.addInternalCodesList(values, MasterType.BRAND, Set.of(item.getBrand()), item, false);
		this.addInternalCodesList(values, MasterType.CLIENT_TYPE, Set.of(item.getClientType()), item, false);
		this.addInternalCodesList(values, MasterType.COUNTRY, item.getCountries(), item, false);
		this.addInternalCodesList(values, MasterType.COUNTRY, item.getExcludedCountries(), item, true);
		this.addInternalCodesList(values, MasterType.CURRENCY, item.getCurrencies(), item, false);
		this.addInternalCodesList(values, MasterType.DISTRIBUTION_MODEL, item.getDistriModels(), item, false);
		this.addInternalCodesList(values, MasterType.RATE_TYPE, item.getRates(), item, false);
		item.setCodes(values);
	}

	private void addInternalCodesList(final Set<PairValue> values, final MasterType masterType, final Set<MasterItem> masters, final GroupCredentialItem item, final boolean excluded) {
		if (masters != null && !masters.isEmpty()) {
			LinkedHashSet<String> codes = new LinkedHashSet<>();
			for (MasterItem master : masters) {
				codes.add(master.getId());
			}
			values.add(addPairValue(masterType, codes, excluded));
			if (masterType == MasterType.COUNTRY) {
				if (excluded) {
					item.setExcludedCountries(null);
				} else {
					item.setCountries(null);
				}
			} else if (masterType == MasterType.CURRENCY) {
				item.setCurrencies(null);
			} else if (masterType == MasterType.DISTRIBUTION_MODEL) {
				item.setDistriModels(null);
			} else if (masterType == MasterType.RATE_TYPE) {
				item.setRates(null);
			} else if (masterType == MasterType.BRAND) {
				item.setBrand(null);
			} else if (masterType == MasterType.CLIENT_TYPE) {
				item.setClientType(null);
			}
		}
	}
}