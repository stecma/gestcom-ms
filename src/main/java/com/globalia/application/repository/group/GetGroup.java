package com.globalia.application.repository.group;

import com.globalia.Errors;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class GetGroup extends FindRedis<GroupCredentialItem> {

	@Value("${redis.groupsKey}")
	private String groupsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final GroupCredentialItem item, final Monitor monitor) {
		return this.getGroup(this.redisAccess.getRediskey(this.groupsKey), String.format(RedisAccess.REDIS_KEYHASH_FORMAT, item.getId(), item.getSupplierId()), monitor);
	}

	private ItemResponse getGroup(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		try {
			response.setGroup((GroupCredentialItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), GroupCredentialItem.class));
			if (response.getGroup() == null || response.getGroup().isDeleted()) {
				response.setError(this.errors.addError(String.format("Cannot find credential group with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return response;
	}
}