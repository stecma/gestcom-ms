package com.globalia.application.repository.customer;

import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Repository
@RefreshScope
public class GetCustomer extends FindRedis<CustomerItem> {

	@Value("${redis.customersKey}")
	private String customersKey;

	@Autowired
	private RedisAccess redisAccess;

	@Override
	public ItemResponse getItem(final CustomerItem customerItem, final Monitor monitor) {
		return null;
	}

	public Set<CustomerItem> getItem(final String credential) {
		return this.getCustomer(this.redisAccess.getRediskey(String.format(RedisAccess.REDIS_KEY_FORMAT, this.customersKey, credential)));
	}

	private Set<CustomerItem> getCustomer(final String redisKey) {
		Set<CustomerItem> values = null;
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				CustomerItem custom = this.addCustomer((String) entry.getValue());
				if (custom != null) {
					if (values == null) {
						values = new HashSet<>();
					}
					values.add(custom);
				}
			}
		} catch (RuntimeException r) {
			//
		}
		return values;
	}

	private CustomerItem addCustomer(final String json) {
		try {
			return (CustomerItem) getJsonHandler().fromJson(json, CustomerItem.class);
		} catch (IOException ignored) {
			// Ignored exception
		}
		return null;
	}
}