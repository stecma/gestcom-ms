package com.globalia.application.repository.customer;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveCustomerRedis extends AbstractSaveRedis<CustomerItem> {

	@Value("${redis.customersKey}")
	private String customersKey;

	@Override
	protected String getItemAction(final CustomerItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final CustomerItem item, final ItemResponse response, final String json) {
		getClient().add(String.format(RedisAccess.REDIS_KEY_FORMAT, getRedisAccess().getRediskey(this.customersKey), item.getCredential()), item.getId(), json);
		response.setCustomer(item);
	}

	@Override
	protected String getEntity(final CustomerItem item) {
		return "customer";
	}

	private void addInternalCodes(final CustomerItem customer) {
		if (customer.getMasters() != null) {
			for (MasterItem master : customer.getMasters()) {
				if (customer.getCodes() == null) {
					customer.setCodes(new HashSet<>());
				}
				customer.getCodes().add(addPairValue(MasterType.valueOf(master.getEntity().toUpperCase()), new LinkedHashSet<>(Set.of(master.getId())), false));
			}
		}
		customer.setMasters(null);
	}
}