package com.globalia.application.repository.release;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.enumeration.credential.MasterType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveReleaseRedis extends AbstractSaveRedis<ReleaseItem> {

	@Value("${redis.releasesKey}")
	private String releasesKey;


	@Override
	protected String getItemAction(final ReleaseItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final ReleaseItem item, final ItemResponse response, final String json) {
		getClient().add(getRedisAccess().getRediskey(this.releasesKey), item.getId(), json);
		response.setRelease(item);
	}

	@Override
	protected String getEntity(final ReleaseItem item) {
		return "release";
	}

	private void addInternalCodes(final ReleaseItem release) {
		if (release.getMasters() != null) {
			for (MasterItem master : release.getMasters()) {
				if (release.getCodes() == null) {
					release.setCodes(new HashSet<>());
				}
				release.getCodes().add(addPairValue(MasterType.valueOf(master.getEntity().toUpperCase()), new LinkedHashSet<>(Set.of(master.getId())), false));
			}
		}
		release.setMasters(null);
	}
}