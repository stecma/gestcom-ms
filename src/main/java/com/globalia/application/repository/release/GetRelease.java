package com.globalia.application.repository.release;

import com.globalia.Errors;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class GetRelease extends FindRedis<ReleaseItem> {

	@Value("${redis.releasesKey}")
	private String releasesKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final ReleaseItem item, final Monitor monitor) {
		return this.getRelease(this.redisAccess.getRediskey(this.releasesKey), item.getId(), monitor);
	}

	private ItemResponse getRelease(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse release = new ItemResponse();
		try {
			release.setRelease((ReleaseItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), ReleaseItem.class));
			if (release.getRelease() == null || release.getRelease().isDeleted()) {
				release.setError(this.errors.addError(String.format("Cannot find release with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			release.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			release.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return release;
	}
}