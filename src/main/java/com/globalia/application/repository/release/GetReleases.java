package com.globalia.application.repository.release;

import com.globalia.Errors;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GetReleases extends FindAllRedis<ReleaseItem> {

	@Value("${redis.releasesKey}")
	private String releasesKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	@Override
	public AllItemsResponse getAllItems(final ReleaseItem item, final Monitor monitor) {
		return this.getAllReleases(monitor);
	}

	private AllItemsResponse getAllReleases(final Monitor monitor) {
		AllItemsResponse releases = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(this.redisAccess.getRediskey(this.releasesKey));
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addRelease((String) entry.getValue(), releases);
			}

			if (releases.getReleases() == null) {
				releases.setError(this.errors.addError("Cannot find releases", ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			releases.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return releases;
	}

	private void addRelease(final String json, final AllItemsResponse releases) {
		try {
			ReleaseItem release = (ReleaseItem) getJsonHandler().fromJson(json, ReleaseItem.class);
			if (!release.isDeleted()) {
				if (releases.getReleases() == null) {
					releases.setReleases(new LinkedHashSet<>());
				}
				releases.getReleases().add(release);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}