package com.globalia.application.repository.trade;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RefreshScope
@Repository
public class SaveTradeRedis extends AbstractSaveRedis<TradePolicyItem> {

	@Value("${redis.tradesKey}")
	private String tradesKey;

	@Override
	protected String getItemAction(final TradePolicyItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final TradePolicyItem item, final ItemResponse response, final String json) {
		getClient().add(String.format(RedisAccess.REDIS_KEY_FORMAT, getRedisAccess().getRediskey(this.tradesKey), item.getCredential()), item.getId(), json);
		response.setTradePolicy(item);
	}

	@Override
	protected String getEntity(final TradePolicyItem item) {
		return "trade";
	}

	private void addInternalCodes(final TradePolicyItem trade) {
		if (trade.getMasters() != null) {
			for (MasterItem master : trade.getMasters()) {
				if (trade.getCodes() == null) {
					trade.setCodes(new HashSet<>());
				}
				trade.getCodes().add(addPairValue(MasterType.valueOf(master.getEntity().toUpperCase()), new LinkedHashSet<>(Set.of(master.getId())), false));
			}
		}
		trade.setMasters(null);
	}
}