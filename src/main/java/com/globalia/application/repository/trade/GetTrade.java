package com.globalia.application.repository.trade;

import com.globalia.dto.credential.ItemResponse;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Repository
@RefreshScope
public class GetTrade extends FindRedis<TradePolicyItem> {

	@Value("${redis.tradesKey}")
	private String tradesKey;

	@Autowired
	private RedisAccess redisAccess;

	@Override
	public ItemResponse getItem(final TradePolicyItem item, final Monitor monitor) {
		return null;
	}

	public Set<TradePolicyItem> getItem(final String credential) {
		return this.getTrade(this.redisAccess.getRediskey(String.format(RedisAccess.REDIS_KEY_FORMAT, this.tradesKey, credential)));
	}

	private Set<TradePolicyItem> getTrade(final String redisKey) {
		Set<TradePolicyItem> values = null;
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				TradePolicyItem trd = this.addTrade((String) entry.getValue());
				if (trd != null) {
					if (values == null) {
						values = new HashSet<>();
					}
					values.add(trd);
				}
			}
		} catch (RuntimeException r) {
			//
		}
		return values;
	}

	private TradePolicyItem addTrade(final String json) {
		TradePolicyItem trade = null;
		try {
			trade = (TradePolicyItem) getJsonHandler().fromJson(json, TradePolicyItem.class);
		} catch (IOException ignored) {
			// Ignored exception
		}
		return trade;
	}
}