package com.globalia.application.repository.cancelcost;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.UUID;

@RefreshScope
@Repository
public class SaveCancelCostRedis extends AbstractSaveRedis<CancelCostItem> {

	@Value("${redis.cancelcostKey}")
	private String cancelcostKey;

	@Override
	protected String getItemAction(final CancelCostItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		return action;
	}

	@Override
	protected void getResponse(final CancelCostItem item, final ItemResponse response, final String json) {
		getClient().add(String.format(RedisAccess.REDIS_KEY_FORMAT, getRedisAccess().getRediskey(this.cancelcostKey), item.getCredential()), item.getId(), json);
		response.setCancelCost(item);
	}

	@Override
	protected String getEntity(final CancelCostItem item) {
		return "cancelcost";
	}
}