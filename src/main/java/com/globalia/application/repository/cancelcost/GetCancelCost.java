package com.globalia.application.repository.cancelcost;

import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Repository
@RefreshScope
public class GetCancelCost extends FindRedis<CancelCostItem> {

	@Value("${redis.cancelcostKey}")
	private String cancelcostKey;

	@Autowired
	private RedisAccess redisAccess;

	@Override
	public ItemResponse getItem(final CancelCostItem item, final Monitor monitor) {
		return null;
	}

	public Set<CancelCostItem> getItem(final String credential) {
		return this.getCancelCost(this.redisAccess.getRediskey(String.format(RedisAccess.REDIS_KEY_FORMAT, this.cancelcostKey, credential)));
	}

	private Set<CancelCostItem> getCancelCost(final String redisKey) {
		Set<CancelCostItem> values = new HashSet<>();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				CancelCostItem trd = this.addCancelCost((String) entry.getValue());
				if (trd != null) {
					values.add(trd);
				}
			}
		} catch (RuntimeException r) {
			//
		}
		return values;
	}

	private CancelCostItem addCancelCost(final String json) {
		try {
			return (CancelCostItem) getJsonHandler().fromJson(json, CancelCostItem.class);
		} catch (IOException ignored) {
			// Ignored exception
		}
		return null;
	}
}