package com.globalia.application.validator.currency;

import com.globalia.dto.credential.CurrencyItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;

@Component
public class CurrencyByCountryValidator {

	public void currencyValidation(final CurrencyItem request, final Monitor monitor) {
		if (request.getParams() == null || request.getParams().isEmpty()) {
			throw new ValidateException(new Error("Must indicate a currency", ErrorLayer.API_LAYER), monitor);
		}
	}
}
