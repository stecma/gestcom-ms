package com.globalia.application.validator.currency;

import com.globalia.dto.credential.CurrencyItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CurrencyValidator {

	public void currencyValidation(final CurrencyItem request, final boolean all, final Monitor monitor) {
		if (!StringUtils.hasText(request.getCredential())) {
			throw new ValidateException(new Error("Must indicate a credential", ErrorLayer.API_LAYER), monitor);
		}

		if (!all && (request.getParams() == null || request.getParams().isEmpty())) {
			throw new ValidateException(new Error("Must indicate a currency", ErrorLayer.API_LAYER), monitor);
		}
	}
}
