package com.globalia.application.validator.customer;

import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CustomerValidator {

	public void customerValidation(final CustomerItem request, final boolean all, final Monitor monitor) {
		if (!StringUtils.hasText(request.getCredential())) {
			throw new ValidateException(new Error("Credential Id is mandatory", ErrorLayer.API_LAYER), monitor);
		}

		if (!all) {
			int count = 0;
			if (request.getMasters() != null) {
				for (MasterItem master : request.getMasters()) {
					if ("agency_group".equalsIgnoreCase(master.getEntity()) || "agency".equalsIgnoreCase(master.getEntity())) {
						count++;
					}
				}
			}

			if (count != 1) {
				throw new ValidateException(new Error("Must indicate an agency or a group agency", ErrorLayer.API_LAYER), monitor);
			}
		}
	}
}
