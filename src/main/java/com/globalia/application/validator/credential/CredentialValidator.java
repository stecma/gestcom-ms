package com.globalia.application.validator.credential;

import com.globalia.application.validator.master.MasterValidator;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CredentialValidator {

	@Autowired
	private MasterValidator validator;

	public void credentialValidation(final CredentialItem request, final Monitor monitor) {
		if (!StringUtils.hasText(request.getAlias())) {
			throw new ValidateException(new Error("Alias is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (!StringUtils.hasText(request.getDescription())) {
			throw new ValidateException(new Error("Description is mandatory", ErrorLayer.API_LAYER), monitor);
		}

		this.validateMaster(request.getBrand(), "Brand is mandatory", monitor);
		this.validateMaster(request.getServiceType(), "Service type is mandatory", monitor);
		this.validateMaster(request.getSaleChannel(), "Sales channel is mandatory", monitor);
		if("HTL".equalsIgnoreCase(request.getServiceType().getId()) && !"NWW".equalsIgnoreCase(request.getSaleChannel().getId())){
			this.validateMaster(request.getDistributionModel(), "Distribution model is mandatory", monitor);
		}
		if (!request.isTagNationality() && "HTL".equalsIgnoreCase(request.getServiceType().getId()) && "API".equalsIgnoreCase(request.getSaleChannel().getId())) {
			this.validateMaster(request.getMarket(), "Market is mandatory", monitor);
		}
		this.validateMaster(request.getDepth(), "Depth is mandatory", monitor);
		this.validateMaster(request.getSalesModel(), "Sales model is mandatory", monitor);
		if(!request.isTest()){
			this.validateMaster(request.getUser(), "KAM is mandatory", monitor);
		}
	}

	private void validateMaster(final MasterItem master, final String message, final Monitor monitor) {
		if (master == null) {
			throw new ValidateException(new Error(message, ErrorLayer.API_LAYER), monitor);
		}
		this.validator.masterValidation(master.getEntity(), master.getId(), monitor);
	}
}
