package com.globalia.application.validator.master;

import com.globalia.application.service.master.Masters;
import com.globalia.dto.I18n;
import com.globalia.dto.credential.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.LinkedHashSet;
import java.util.Set;

@Component
public class MasterValidator {

	@Autowired
	private Masters masters;

	public void masterValidation(final String masterType, final String masterKey, final Monitor monitor) {
		if (!this.masters.validateKey(masterType, masterKey)) {
			throw new ValidateException(new Error(String.format("Cannot find master with id: %s", masterKey), ErrorLayer.API_LAYER), monitor);
		}
	}

	public void validateEntity(final MasterItem request, final Monitor monitor) {
		if (!StringUtils.hasText(request.getEntity())) {
			throw new ValidateException(new Error("Entity is mandatory", ErrorLayer.API_LAYER), monitor);
		}
	}

	public void validateMaster(final MasterItem request, final Monitor monitor) {
		MasterType entity = MasterType.valueOf(request.getEntity().toUpperCase());
		if (entity == MasterType.BRANCH_OFFICE && !StringUtils.hasText(request.getFilter())) {
			throw new ValidateException(new Error("Agency Id is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (entity == MasterType.DISTRIBUTION_MODEL && !StringUtils.hasText(request.getAlias())) {
			throw new ValidateException(new Error("Alias is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (entity == MasterType.RATE_TYPE && request.getRateCategory() == null) {
			throw new ValidateException(new Error("Must indicate a rate category", ErrorLayer.API_LAYER), monitor);
		}

		this.i18nValidation(request.getNames(), monitor);
		this.childValidation(request.getMasters(), monitor);
	}

	private void i18nValidation(final LinkedHashSet<I18n> names, final Monitor monitor) {
		if (names == null || names.isEmpty()) {
			throw new ValidateException(new Error("Name is mandatory", ErrorLayer.API_LAYER), monitor);
		}

		for (I18n name : names) {
			this.masterValidation(MasterType.LANGUAGES.name(), name.getKey(), monitor);
		}
	}

	private void childValidation(final Set<MasterItem> children, final Monitor monitor) {
		if (children != null && !children.isEmpty()) {
			for (MasterItem item : children) {
				this.masterValidation(item.getEntity(), item.getId(), monitor);
			}
		}
	}
}
