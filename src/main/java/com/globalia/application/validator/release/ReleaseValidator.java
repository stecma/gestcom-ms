package com.globalia.application.validator.release;

import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ReleaseValidator {

	public void releaseValidation(final ReleaseItem request, final Monitor monitor) {
		int count = 0;
		if (request.getMasters() != null) {
			for (MasterItem master : request.getMasters()) {
				if ("brand".equalsIgnoreCase(master.getEntity())) {
					count++;
				}
			}
		}

		if (count == 0 && !StringUtils.hasText(request.getCredential())) {
			throw new ValidateException(new Error("Must indicate a brand or a credential", ErrorLayer.API_LAYER), monitor);
		}
	}
}
