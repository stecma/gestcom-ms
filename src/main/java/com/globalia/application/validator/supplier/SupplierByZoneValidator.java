package com.globalia.application.validator.supplier;

import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class SupplierByZoneValidator {

	public void supplierByZoneValidation(final SupplierByZoneItem request, final Monitor monitor) {
		if (!StringUtils.hasText(request.getCodpro())) {
			throw new ValidateException(new Error("SAP code is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (!StringUtils.hasText(request.getCoduser())) {
			throw new ValidateException(new Error("User code is mandatory", ErrorLayer.API_LAYER), monitor);
		}
	}
}
