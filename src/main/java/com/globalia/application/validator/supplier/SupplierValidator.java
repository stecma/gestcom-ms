package com.globalia.application.validator.supplier;

import com.globalia.Dates;
import com.globalia.application.validator.master.MasterValidator;
import com.globalia.dto.credential.ExternalCredentialItem;
import com.globalia.dto.credential.GroupCredentialItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class SupplierValidator {

	@Autowired
	private Dates dates;
	@Autowired
	private MasterValidator validator;

	public void supplierValidation(final SupplierItem request, final Monitor monitor) {
		if (request.getClientPref() == null) {
			throw new ValidateException(new Error("Client preference is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (request.getRoomMax() == 0) {
			throw new ValidateException(new Error("Maximum room number is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (request.getServiceType() == null) {
			throw new ValidateException(new Error("Service type is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (request.getBuyModel() == null) {
			throw new ValidateException(new Error("Buy model code is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (request.getUser() == null) {
			throw new ValidateException(new Error("KAM is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		if (request.isXsell() && (request.getClients() == null || request.getClients().isEmpty())) {
			throw new ValidateException(new Error("Must indicate at least a customer", ErrorLayer.API_LAYER), monitor);
		}
		if (request.getSystem() == null) {
			throw new ValidateException(new Error("System is mandatory", ErrorLayer.API_LAYER), monitor);
		}
		this.validateMaster(request, monitor);
	}

	public void supplierGroups(final SupplierItem request, final Monitor monitor) {
		if (request.getExternalSys() != null) {
			for (GroupCredentialItem group : request.getExternalSys()) {
				this.validateGroup(group, monitor);
				this.validateGroupMaster(group.getCountries(), monitor);
				this.validateGroupMaster(group.getExcludedCountries(), monitor);
				this.validateGroupMaster(group.getCurrencies(), monitor);
				this.validateGroupMaster(group.getRates(), monitor);
				this.validateGroupMaster(group.getDistriModels(), monitor);
				this.validateGroupMaster(Set.of(group.getBrand()), monitor);
				this.validateGroupMaster(Set.of(group.getClientType()), monitor);
				this.validateExternals(group.getExternalConfig(), monitor);
			}
		}
	}

	private void validateGroup(final GroupCredentialItem group, final Monitor monitor) {
		if (group.getDistriModels() == null && group.getAllMasters() != null && !group.getAllMasters().contains(MasterType.DISTRIBUTION_MODEL.name().toLowerCase())) {
			throw new ValidateException(new Error("Must indicate at least a distribution model", ErrorLayer.API_LAYER), monitor);
		}
		if (group.getRates() == null && group.getAllMasters() != null && !group.getAllMasters().contains(MasterType.RATE_TYPE.name().toLowerCase())) {
			throw new ValidateException(new Error("Must indicate at least a rate", ErrorLayer.API_LAYER), monitor);
		}
		if (group.getBrand() == null) {
			throw new ValidateException(new Error("Must indicate at least a trademark", ErrorLayer.API_LAYER), monitor);
		}
		if (group.getClientType() == null) {
			throw new ValidateException(new Error("Must indicate at least a client type", ErrorLayer.API_LAYER), monitor);
		}
	}

	private void validateExternals(final Set<ExternalCredentialItem> externals, final Monitor monitor) {
		if (externals != null) {
			for (ExternalCredentialItem external : externals) {
				if (external.getCheckIn() != null) {
					this.dates.validateDateTimeRange(external.getCheckIn().getFrom(), external.getCheckIn().getTo(), null, null, monitor);
				}
				if (external.getBook() != null) {
					this.dates.validateDateTimeRange(external.getBook().getFrom(), external.getBook().getTo(), external.getBook().getTimeFrom(), external.getBook().getTimeTo(), monitor);
				}
				if (external.getInvoicingType() != null) {
					this.validator.masterValidation(external.getInvoicingType().getEntity(), external.getInvoicingType().getId(), monitor);
				}
				if (external.getCorporation() != null) {
					this.validator.masterValidation(external.getCorporation().getEntity(), external.getCorporation().getId(), monitor);
				}
			}
		}
	}

	private void validateGroupMaster(final Set<MasterItem> masters, final Monitor monitor) {
		if (masters != null) {
			for (MasterItem master : masters) {
				this.validator.masterValidation(master.getEntity(), master.getId(), monitor);
			}
		}
	}

	private void validateMaster(final SupplierItem supplier, final Monitor monitor) {
		this.validator.masterValidation(supplier.getClientPref().getEntity(), supplier.getClientPref().getId(), monitor);
		this.validator.masterValidation(supplier.getServiceType().getEntity(), supplier.getServiceType().getId(), monitor);
		this.validator.masterValidation(supplier.getBuyModel().getEntity(), supplier.getBuyModel().getId(), monitor);
		this.validator.masterValidation(supplier.getUser().getEntity(), supplier.getUser().getId(), monitor);
		this.validator.masterValidation(supplier.getSystem().getEntity(), supplier.getSystem().getId(), monitor);
		if (supplier.getClients() != null) {
			for (MasterItem client : supplier.getClients()) {
				this.validator.masterValidation(client.getEntity(), client.getId(), monitor);
			}
		}
		if (supplier.getTopDestinations() != null) {
			for (MasterItem destination : supplier.getTopDestinations()) {
				this.validator.masterValidation(destination.getEntity(), destination.getId(), monitor);
			}
		}
	}
}
