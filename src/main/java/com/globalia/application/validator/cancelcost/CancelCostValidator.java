package com.globalia.application.validator.cancelcost;

import com.globalia.dto.credential.CancelCostItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CancelCostValidator {

	public void cancelCostValidation(final CancelCostItem request, final Monitor monitor) {
		if (!StringUtils.hasText(request.getCredential())) {
			throw new ValidateException(new Error("Must indicate a credential", ErrorLayer.API_LAYER), monitor);
		}
	}
}
