package com.globalia.application.validator.trade;

import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class TradeValidator {

	public void tradeValidation(final TradePolicyItem request, final boolean all, final Monitor monitor) {
		if (!StringUtils.hasText(request.getCredential())) {
			throw new ValidateException(new Error("Credential Id is mandatory", ErrorLayer.API_LAYER), monitor);
		}

		if (!all) {
			if (request.getPercentage() == null) {
				throw new ValidateException(new Error("Percentage is mandatory", ErrorLayer.API_LAYER), monitor);
			}
			if (request.getPercentageType() == null) {
				throw new ValidateException(new Error("Percentage type is mandatory", ErrorLayer.API_LAYER), monitor);
			}
		}
	}
}
