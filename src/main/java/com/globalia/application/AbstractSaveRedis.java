package com.globalia.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.Errors;
import com.globalia.application.producer.MessageProducer;
import com.globalia.dto.KafkaItem;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.SaveRedis;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import java.util.LinkedHashSet;

public abstract class AbstractSaveRedis<T> extends SaveRedis<T> {

	public static final String CRE_ACTION = "create";
	public static final String UPD_ACTION = "update";
	public static final String DEL_ACTION = "delete";

	@Getter
	@Autowired
	private RedisAccess redisAccess;
	@Getter
	@Autowired
	private Errors errors;
	@Autowired
	private MessageProducer producer;

	protected abstract String getItemAction(final T item);

	protected abstract void getResponse(final T item, final ItemResponse response, final String json);

	protected abstract String getEntity(final T item);

	public ItemResponse addItem(final T item, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		try {
			String action = this.getItemAction(item);

			String json = getJsonHandler().toJson(item);
			this.getResponse(item, response, json);
			this.sendMessage(this.getEntity(item), action, json, monitor);
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (JsonProcessingException e) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return response;
	}

	protected String getAction(final String id, final boolean isDeleted) {
		if (!StringUtils.hasText(id)) {
			return AbstractSaveRedis.CRE_ACTION;
		}
		if (isDeleted) {
			return AbstractSaveRedis.DEL_ACTION;
		}
		return AbstractSaveRedis.UPD_ACTION;
	}

	protected PairValue addPairValue(final MasterType masterType, final LinkedHashSet<String> codes, final boolean excluded) {
		PairValue pairValue = new PairValue();
		String key = masterType.name();
		if (excluded) {
			key = String.format("%s¬exc", masterType.name());
		}
		pairValue.setKey(key);
		pairValue.setValue(codes);
		return pairValue;
	}

	protected void sendMessage(final String entity, final String action, final String json, final Monitor monitor) {
		KafkaItem message = new KafkaItem();
		message.setKey(monitor.getCorrelationID());
		message.setEntity(entity);
		message.setAction(action);
		message.setJson(json);
		this.producer.reply(message);
	}
}