package com.globalia;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.Arrays;

@SpringBootApplication
@EnableConfigurationProperties
@EnableAutoConfiguration
@Slf4j
public class Application implements ApplicationRunner {

	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}

	public void run(final ApplicationArguments args) {
		Application.log.info("Application started with command-line arguments: {}", Arrays.toString(args.getSourceArgs()));
	}
}